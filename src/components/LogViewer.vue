<!--
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<template>
  <div class="log">
    <div class="flex-row">
      <p class="command" v-if="command">
        <span class="prompt">
          {{ command.workingDirectory || '.' }} $
        </span>
        <span class="command-word" v-for="word in command.argumentsList" :key="word">
          {{ word }}{{' '}}
        </span>
      </p>
      <div class="control-group live" v-if="live">
        <span class="dot"></span>
        Live!
      </div>
    </div>
    <div class="control-group" v-if="resolvedResult && !live">
      <div v-if="downloadableStdout || downloadableStderr">
        Download:
        <a class="btn" :href="stdoutUrl" v-if="result.stdoutDigest">stdout</a>
        <a class="btn" :href="stderrUrl" v-if="result.stderrDigest">stderr</a>
      </div>
      <div v-else>
        Show:
        <a class="btn" :class="{'active': showStdout}"
            @click="showStdout = !showStdout">stdout</a>
        <a class="btn" :class="{'active': showStderr}"
            @click="showStderr = !showStderr">stderr</a>
      </div>
    </div>
    <div v-if="!live">
      <div class="log-content" v-if="showStdout">
        <h2>Standard Output</h2>
        <p class="stdout-line"
          v-for="line in stdoutLines" :key="line">
          {{ line }}
        </p>
      </div>
      <div class="log-content" v-if="showStderr">
        <h2>Standard Error</h2>
        <p class="stdout-line"
          v-for="line in stderrLines" :key="line">
          {{ line }}
        </p>
      </div>
    </div>
    <div v-if="live">
      <div class="log-content">
        <h2>Live Output</h2>
        <p class="stdout-line"
          v-for="(line, index) in liveLogs" :key="index">
          {{ line }}
        </p>
      </div>
    </div>
  </div>
</template>

<script>
import {
  ReadRequest,
  ReadResponse
} from '@/protos/google/bytestream/bytestream_pb.js'

import base from '@/apis/base.js'
import bytestream from '@/apis/bytestream.js'

const config = base.getConfig()

// Upper limit on the size of the content that can be rendered.
// Logs larger than this size will be linked to a downloadable URL.
const MAX_LOG_SIZE_BYTES = 1024 * 1024

export default {
  name: 'LogViewer',
  data () {
    return {
      handledResult: false,
      live: false,
      liveLogs: [],
      showStderr: false,
      showStdout: false,
      stderr: '',
      stdout: '',
      ws: null
    }
  },
  props: {
    command: {
      type: Object,
      default: () => null
    },
    operation: {
      type: Object,
      default: () => null
    },
    result: {
      type: Object,
      default: () => null
    }
  },
  computed: {
    resolvedResult () {
      if (this.result) {
        return this.result
      } else if (this.operation?.response?.result) {
        return this.operation?.response?.result
      }
      return null
    },
    stderrLines () {
      return this.stderr.split('\n')
    },
    stdoutLines () {
      return this.stdout.split('\n')
    },
    downloadableStdout () {
      return this.resolvedResult?.stdoutDigest && this.resolvedResult?.stdoutDigest.sizeBytes > MAX_LOG_SIZE_BYTES
    },
    downloadableStderr () {
      return this.resolvedResult?.stderrDigest && this.resolvedResult?.stderrDigest.sizeBytes > MAX_LOG_SIZE_BYTES
    },
    stdoutUrl () {
      return bytestream.blobUrl(this.resolvedResult?.stdoutDigest)
    },
    stderrUrl () {
      return bytestream.blobUrl(this.resolvedResult?.stderrDigest)
    }
  },
  created () {
    this.getStdoutStderr()
    this.getLogStream()
  },
  methods: {
    _receiveLogStream (event) {
      let message = {}
      try {
        message = JSON.parse(event.data)
      } catch (e) {
        console.warn(`Unexpected error receiving LogStream message: ${e}`)
        return
      }
      if (message.complete) {
        this._finaliseLogStream()
      } else if (message.data) {
        const response = ReadResponse.deserializeBinary(message.data).toObject()
        if (response.data) {
          const data = atob(response.data)
          const lines = data.split(/\r\n|\r|\n|/)
          this.liveLogs = this.liveLogs.concat(lines)
        }
      }
    },
    _finaliseLogStream () {
      this.live = false
    },
    async _getLogsFromResult (result) {
      this.stderr = atob(result.stderrRaw)
      this.stdout = atob(result.stdoutRaw)
      if (result.stderrDigest && !result.stderrRaw) {
        const digest = result.stderrDigest
        this.stderr = await bytestream.readBlob(digest.hash, digest.sizeBytes)
      }
      if (result.stdoutDigest && !result.stdoutRaw) {
        const digest = result.stdoutDigest
        this.stdout = await bytestream.readBlob(digest.hash, digest.sizeBytes)
      }
      this.handledResult = true
    },
    async getStdoutStderr () {
      if (this.result && !this.handledResult) {
        await this._getLogsFromResult(this.result)
      } else if (this.operation?.response?.result) {
        await this._getLogsFromResult(this.operation.response.result)
      }
    },
    getLogStream () {
      if (this.operation) {
        if (!this.operation.done) {
          this.live = true
          const resourceName = this.operation.metadata.stdoutStreamName
          const request = new ReadRequest()
          request.setResourceName(resourceName)

          if (this.ws === null) {
            this.ws = new WebSocket(config.logStreamUrl)
            this.ws.onmessage = this._receiveLogStream
          }

          const req = request.serializeBinary()
          this.ws.onopen = (event) => {
            this.ws.send(req)
          }
        }
      }
    }
  }
}
</script>

<style lang="scss" scoped>
.log {
  font-family: 'Roboto Mono';
  background-color: #333;
  padding: 30px;
  border-radius: 10px;
  color: #fff;
  font-size: 1.35rem;
  box-shadow: 0 -0.2rem .4rem rgba(0,0,0,.1) inset;

  .flex-row {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 15px;
  }

  .control-group {
    font-size: 1rem;

    a {
      cursor: pointer;
      transition: 150ms ease-in-out all;

      &.btn {
        padding: 10px;
        color: #7BB2D9;

        &.active {
          color: #B9CDDA;
          box-shadow: 0 -4px 0 #B9CDDA inset;

          &:hover {
            box-shadow: 0 -2px 0 #B9CDDA inset;
          }
        }

        &:hover {
          color: #B9CDDA;
        }
      }
    }

    &.live {
      color: #B9CDDA;
      display: flex;
      align-items: center;

      .dot {
        height: 10px;
        width: 10px;
        margin-right: 15px;
        background-color: #f24949;
        border-radius: 50%;
        display: inline-block;
        box-shadow: 0 0 5px #f24949;
      }
    }
  }

  .command {
    margin: 0;
    flex: 1 0 0;
    word-break: break-word;

    .prompt {
      color: #aaa;
      margin-right: 15px;
    }

    .command-word:first-of-type {
      font-weight: 500;
    }
  }

  .log-content {
    margin-top: 2rem;
    word-break: break-word;

    h2 {
      font-size: 0.9rem;
      color: #aaa;
      margin: 0;
    }

    p {
      font-size: 1.1rem;
      margin: 0;
    }
  }
}
</style>
