<!--
  Copyright 2021 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<template>
  <div class="operation" v-bind:class="{ 'expanded-operation': expandedRowIsVisible }">
    <div class="operations-summary">
      <component class="stage-icon" :style="{ color: stageColor }" :is="stageIcon"></component>

      <div class="operation-summary-info" v-on:click="toggleExpandedRow()">
        <div class="operation-info-row">
          <div class="command-listing" v-if="operation.command">
            <span class="command-argument" v-for="argument in operation.command.argumentsList" :key="argument">
              {{ argument }}
            </span>
          </div>
          <div v-else class="unknown-command-message">
            Unknown command
          </div>
          <div class="operation-link">
            <router-link :to="actionRoute" @click="storeOperation(operation)" v-if="!actionResultIsMissing" class="operation-name">{{ operation.name }}</router-link>
            <span v-if="actionResultIsMissing" class="operation-name">{{ operation.name }}</span>

            <a class="toggle-expand-row-button" v-if="!actionResultIsMissing">
              <ChevronDownIcon v-if="!expandedRowIsVisible" />
              <ChevronUpIcon v-if="expandedRowIsVisible" />
            </a>
          </div>
        </div>

        <div class="operation-info-row">
          <div>
            <span class="operation-stage" v-bind:style="{ color: stageColor }">
              <span class="operation-stage-name">{{ stageName }}</span>
              <span v-if="commandFailed" class="exit-code">
                (exit code {{ exitCode }})
              </span>
            </span>
            <span v-if="workerCompletedTimestamp">
              {{ formatTimestamp(workerCompletedTimestamp)  }}
              <span v-if="workerName">
                on
                <router-link class="worker-name" :to="workerRoute" @click="storeOperation(operation)">{{ workerName }}</router-link>
              </span>
              <span class="execution-time"> (took {{ formattedTotalExecutionTime(queuedToCompletedTimeDelta) }})</span>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div v-if="expandedRowIsVisible" class="expanded-operation-info">
      <LogViewer :command="operation.command"
                 :result="operation.response ? operation.response.result : undefined" />
      <CommandMetadata v-if="operation.command" :command="operation.command" />

      <div v-if="operation.response">
        <h2>Timeline</h2>
        <ResultTimeline :result="operation.response.result" />
      </div>

      <div class="collapse-expanded-row-bottom-button">
        <a class="toggle-expand-row-button" v-on:click="toggleExpandedRow(operation)">
          <ChevronUpIcon />
        </a>
      </div>
    </div>
  </div>
</template>

<script>
import { ChevronDownIcon, ChevronUpIcon } from '@heroicons/vue/outline'
import { mapActions } from 'pinia'

import CommandMetadata from '@/components/CommandMetadata.vue'
import LogViewer from '@/components/LogViewer.vue'
import ResultTimeline from '@/components/ResultTimeline.vue'

import operations from '@/apis/operations.js'
import { useOperationsCache } from '@/stores/operations.js'

export default {
  name: 'CorrelatedInvocationsDetails',
  components: {
    ChevronDownIcon,
    ChevronUpIcon,
    CommandMetadata,
    LogViewer,
    ResultTimeline
  },
  data () {
    return {
      expandedRowIsVisible: false
    }
  },
  props: {
    operation: {
      type: Object,
      default: () => null
    }
  },
  computed: {
    stageName () {
      return operations.stageName(this.operation)
    },
    workerName () {
      return this.operation.response?.result?.executionMetadata?.worker
    },
    workerCompletedTimestamp () {
      const ts = this.operation.response?.result?.executionMetadata?.workerCompletedTimestamp
      return ts ? this.dateFromTimestamp(ts) : undefined
    },
    queuedToCompletedTimeDelta () {
      const executionMetadata = this.operation.response?.result?.executionMetadata
      if (executionMetadata) {
        const start = this.dateFromTimestamp(executionMetadata.queuedTimestamp)
        const end = this.dateFromTimestamp(executionMetadata.workerCompletedTimestamp)
        const deltaSecs = (end.getTime() - start.getTime()) / 1000
        return deltaSecs
      }
      return undefined
    },
    operationWasCancelled () {
      return operations.operationWasCancelled(this.operation)
    },
    exitCode () {
      return operations.exitCode(this.operation)
    },
    commandFailed () {
      return operations.commandFailed(this.operation)
    },
    actionResultIsMissing () {
      return operations.operationResultIsMissing(this.operation)
    },
    actionRoute () {
      return {
        name: 'Action',
        params: {
          hash: this.operation.metadata.actionDigest.hash,
          sizeBytes: this.operation.metadata.actionDigest.sizeBytes
        },
        query: {
          operation: this.operation.name
        }
      }
    },
    workerRoute () {
      return {
        name: 'Worker Invocations',
        params: {
          workerName: this.workerName
        }
      }
    },
    stageColor () {
      return operations.stageColor(this.operation)
    },
    stageIcon () {
      return operations.stageIcon(this.operation)
    }
  },
  methods: {
    ...mapActions(useOperationsCache, ['storeOperation']),
    toggleExpandedRow () {
      if (!this.actionResultIsMissing) {
        this.expandedRowIsVisible = !this.expandedRowIsVisible
      }
    },
    dateFromTimestamp (timestamp) {
      return new Date((timestamp.seconds * 1000) + (timestamp.nanos * 0.000001))
    },
    formatTimestamp (datetime) {
      return datetime.toLocaleString()
    },
    formattedTotalExecutionTime (numSecs) {
      if (numSecs) {
        return numSecs.toFixed(1) + ' s'
      }
      return ''
    }
  }
}
</script>

<style lang="scss" scoped>
.expanded-operation {
  background-color: #e5e4e7;
}

.operation {
  border-collapse: collapse;
  width: 100vw;
  position: relative;
  left: 50%;
  right: 50%;
  margin-left: -50vw;
  margin-right: -50vw;

  .operations-summary {
    display: flex;
    align-items: center;
    margin-top: 2.0em;
    margin-bottom: 2.0em;
    padding-bottom: 0.5em;

    margin-left: 20%;
    margin-right: 20%;
  }

  .operation-summary-info {
    width: 100%;
    padding-top: 0.5em;
  }

  .operations-summary .stage-icon {
    flex: 0 0 30px;
    padding-right: 1.5em;
  }

  .operation-info-row {
    display: flex;
    justify-content: space-between;

    .command-listing {
      flex: 1 0 auto;
    }

    .operation-link {
      flex: 0 0 auto;
    }
  }

  .operation-stage {
    margin-right: 0.5em;
  }

  .operation-stage-name {
    font-weight: 900;
  }

  .command-listing {
    font-family: 'Roboto Mono';
    margin-bottom: 0.5em;
    max-width: 45rem;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .unknown-command-message {
    color: grey;
    margin-bottom: 0.5em;
  }

  .command-listing > .command-argument:first-child {
    font-weight: 900;
  }

  .expanded-operation-info {
    padding-top: 0.5em;
    margin-bottom: 2em;
    margin-left: 20%;
    margin-right: 20%;
  }

  .operation-name {
    font-family: 'Roboto Mono';
    font-size: 0.75rem;
    color: var(--color-link);
    text-decoration: none;

    &:hover {
      box-shadow: 0 -2px 0 var(--color-link) inset;
    }
  }

  .worker-name {
    font-family: 'Roboto Mono';
    font-size: 80%;
    color: var(--color-link);
    text-decoration: none;

    &:hover {
      box-shadow: 0 -2px 0 var(--color-link) inset;
    }
  }

  .exit-code {
    font-size: 0.9rem;
    color: var(--color-red);
  }

  .execution-time {
    font-size: 0.8rem;
  }

  .toggle-expand-row-button {
    color: var(--color-title);
    margin-left: 1em;

    cursor: pointer;

    svg {
      width: 1em;
      height: 1em;
    }
  }

  .collapse-expanded-row-bottom-button {
    text-align: center;

    cursor: pointer;

    svg {
      width: 1em;
      height: 1em;
    }
  }
}
</style>
