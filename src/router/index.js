/**
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */
import { createRouter, createWebHistory } from 'vue-router'

import indexdb from '@/apis/indexdb.js'

const routes = [

  {
    path: '/home',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/browse',
    name: 'Browse',
    component: () => import(/* webpackChunkName: "browse" */ '../views/Browse.vue')
  },
  {
    path: '/action/:hash/:sizeBytes',
    name: 'Action',
    component: () => import(/* webpackChunkName: "action" */ '../views/Action.vue')
  },
  {
    path: '/directory/:hash/:sizeBytes',
    name: 'Directory',
    component: () => import(/* webpackChunkName: "directory" */ '../views/Directory.vue')
  },
  {
    path: '/tree/:hash/:sizeBytes',
    name: 'Tree',
    component: () => import(/* webpackChunkName: "tree" */ '../views/Tree.vue')
  },
  {
    path: '/file/:hash/:sizeBytes',
    name: 'File',
    component: () => import(/* webpackChunkName: "file" */ '../views/File.vue')
  },
  {
    path: '/correlated-invocations/:correlationId',
    name: 'Correlated Invocations',
    component: () => import(/* webpackChunkName: "correlated-invocations" */ '../views/CorrelatedInvocations.vue')
  },
  {
    path: '/tool-invocations/:toolInvocationId',
    name: 'Tool Invocation',
    component: () => import(/* webpackChunkName: "tool-invocation" */ '../views/ToolInvocations.vue')
  },
  {
    path: '/worker/:workerName',
    name: 'Worker Invocations',
    component: () => import(/* webpackChunkName: "worker-invocations" */ '../views/WorkerInvocations.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.afterEach(async (to, from) => {
  const fullUrl = window.location.href
  const routeInfo = {
    name: to.name,
    hash: to.hash,
    params: to.params,
    query: to.query,
    fullPath: to.fullPath
  }

  const page = { timestamp: Date.now(), url: fullUrl, route: routeInfo }
  await indexdb.savePage(page)
})

export default router
