// source: google/devtools/build/v1/publish_build_event.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_api_annotations_pb = require('../../../../google/api/annotations_pb.js');
goog.object.extend(proto, google_api_annotations_pb);
var google_api_client_pb = require('../../../../google/api/client_pb.js');
goog.object.extend(proto, google_api_client_pb);
var google_api_field_behavior_pb = require('../../../../google/api/field_behavior_pb.js');
goog.object.extend(proto, google_api_field_behavior_pb);
var google_devtools_build_v1_build_events_pb = require('../../../../google/devtools/build/v1/build_events_pb.js');
goog.object.extend(proto, google_devtools_build_v1_build_events_pb);
var google_protobuf_duration_pb = require('google-protobuf/google/protobuf/duration_pb.js');
goog.object.extend(proto, google_protobuf_duration_pb);
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');
goog.object.extend(proto, google_protobuf_empty_pb);
goog.exportSymbol('proto.google.devtools.build.v1.OrderedBuildEvent', null, global);
goog.exportSymbol('proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest', null, global);
goog.exportSymbol('proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse', null, global);
goog.exportSymbol('proto.google.devtools.build.v1.PublishLifecycleEventRequest', null, global);
goog.exportSymbol('proto.google.devtools.build.v1.PublishLifecycleEventRequest.ServiceLevel', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.google.devtools.build.v1.PublishLifecycleEventRequest.repeatedFields_, null);
};
goog.inherits(proto.google.devtools.build.v1.PublishLifecycleEventRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.google.devtools.build.v1.PublishLifecycleEventRequest.displayName = 'proto.google.devtools.build.v1.PublishLifecycleEventRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.displayName = 'proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.google.devtools.build.v1.OrderedBuildEvent = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.google.devtools.build.v1.OrderedBuildEvent, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.google.devtools.build.v1.OrderedBuildEvent.displayName = 'proto.google.devtools.build.v1.OrderedBuildEvent';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.repeatedFields_, null);
};
goog.inherits(proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.displayName = 'proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.repeatedFields_ = [4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.google.devtools.build.v1.PublishLifecycleEventRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    serviceLevel: jspb.Message.getFieldWithDefault(msg, 1, 0),
    buildEvent: (f = msg.getBuildEvent()) && proto.google.devtools.build.v1.OrderedBuildEvent.toObject(includeInstance, f),
    streamTimeout: (f = msg.getStreamTimeout()) && google_protobuf_duration_pb.Duration.toObject(includeInstance, f),
    notificationKeywordsList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f,
    projectId: jspb.Message.getFieldWithDefault(msg, 6, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.google.devtools.build.v1.PublishLifecycleEventRequest;
  return proto.google.devtools.build.v1.PublishLifecycleEventRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.google.devtools.build.v1.PublishLifecycleEventRequest.ServiceLevel} */ (reader.readEnum());
      msg.setServiceLevel(value);
      break;
    case 2:
      var value = new proto.google.devtools.build.v1.OrderedBuildEvent;
      reader.readMessage(value,proto.google.devtools.build.v1.OrderedBuildEvent.deserializeBinaryFromReader);
      msg.setBuildEvent(value);
      break;
    case 3:
      var value = new google_protobuf_duration_pb.Duration;
      reader.readMessage(value,google_protobuf_duration_pb.Duration.deserializeBinaryFromReader);
      msg.setStreamTimeout(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.addNotificationKeywords(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setProjectId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.google.devtools.build.v1.PublishLifecycleEventRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getServiceLevel();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getBuildEvent();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.google.devtools.build.v1.OrderedBuildEvent.serializeBinaryToWriter
    );
  }
  f = message.getStreamTimeout();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_protobuf_duration_pb.Duration.serializeBinaryToWriter
    );
  }
  f = message.getNotificationKeywordsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      4,
      f
    );
  }
  f = message.getProjectId();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.ServiceLevel = {
  NONINTERACTIVE: 0,
  INTERACTIVE: 1
};

/**
 * optional ServiceLevel service_level = 1;
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest.ServiceLevel}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.getServiceLevel = function() {
  return /** @type {!proto.google.devtools.build.v1.PublishLifecycleEventRequest.ServiceLevel} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.google.devtools.build.v1.PublishLifecycleEventRequest.ServiceLevel} value
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.setServiceLevel = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional OrderedBuildEvent build_event = 2;
 * @return {?proto.google.devtools.build.v1.OrderedBuildEvent}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.getBuildEvent = function() {
  return /** @type{?proto.google.devtools.build.v1.OrderedBuildEvent} */ (
    jspb.Message.getWrapperField(this, proto.google.devtools.build.v1.OrderedBuildEvent, 2));
};


/**
 * @param {?proto.google.devtools.build.v1.OrderedBuildEvent|undefined} value
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
*/
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.setBuildEvent = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.clearBuildEvent = function() {
  return this.setBuildEvent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.hasBuildEvent = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional google.protobuf.Duration stream_timeout = 3;
 * @return {?proto.google.protobuf.Duration}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.getStreamTimeout = function() {
  return /** @type{?proto.google.protobuf.Duration} */ (
    jspb.Message.getWrapperField(this, google_protobuf_duration_pb.Duration, 3));
};


/**
 * @param {?proto.google.protobuf.Duration|undefined} value
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
*/
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.setStreamTimeout = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.clearStreamTimeout = function() {
  return this.setStreamTimeout(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.hasStreamTimeout = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * repeated string notification_keywords = 4;
 * @return {!Array<string>}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.getNotificationKeywordsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.setNotificationKeywordsList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.addNotificationKeywords = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.clearNotificationKeywordsList = function() {
  return this.setNotificationKeywordsList([]);
};


/**
 * optional string project_id = 6;
 * @return {string}
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.getProjectId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.google.devtools.build.v1.PublishLifecycleEventRequest} returns this
 */
proto.google.devtools.build.v1.PublishLifecycleEventRequest.prototype.setProjectId = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    streamId: (f = msg.getStreamId()) && google_devtools_build_v1_build_events_pb.StreamId.toObject(includeInstance, f),
    sequenceNumber: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse;
  return proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_devtools_build_v1_build_events_pb.StreamId;
      reader.readMessage(value,google_devtools_build_v1_build_events_pb.StreamId.deserializeBinaryFromReader);
      msg.setStreamId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSequenceNumber(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStreamId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_devtools_build_v1_build_events_pb.StreamId.serializeBinaryToWriter
    );
  }
  f = message.getSequenceNumber();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
};


/**
 * optional StreamId stream_id = 1;
 * @return {?proto.google.devtools.build.v1.StreamId}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.prototype.getStreamId = function() {
  return /** @type{?proto.google.devtools.build.v1.StreamId} */ (
    jspb.Message.getWrapperField(this, google_devtools_build_v1_build_events_pb.StreamId, 1));
};


/**
 * @param {?proto.google.devtools.build.v1.StreamId|undefined} value
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse} returns this
*/
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.prototype.setStreamId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse} returns this
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.prototype.clearStreamId = function() {
  return this.setStreamId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.prototype.hasStreamId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int64 sequence_number = 2;
 * @return {number}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.prototype.getSequenceNumber = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse} returns this
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamResponse.prototype.setSequenceNumber = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.toObject = function(opt_includeInstance) {
  return proto.google.devtools.build.v1.OrderedBuildEvent.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.google.devtools.build.v1.OrderedBuildEvent} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.devtools.build.v1.OrderedBuildEvent.toObject = function(includeInstance, msg) {
  var f, obj = {
    streamId: (f = msg.getStreamId()) && google_devtools_build_v1_build_events_pb.StreamId.toObject(includeInstance, f),
    sequenceNumber: jspb.Message.getFieldWithDefault(msg, 2, 0),
    event: (f = msg.getEvent()) && google_devtools_build_v1_build_events_pb.BuildEvent.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.google.devtools.build.v1.OrderedBuildEvent}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.google.devtools.build.v1.OrderedBuildEvent;
  return proto.google.devtools.build.v1.OrderedBuildEvent.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.google.devtools.build.v1.OrderedBuildEvent} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.google.devtools.build.v1.OrderedBuildEvent}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_devtools_build_v1_build_events_pb.StreamId;
      reader.readMessage(value,google_devtools_build_v1_build_events_pb.StreamId.deserializeBinaryFromReader);
      msg.setStreamId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSequenceNumber(value);
      break;
    case 3:
      var value = new google_devtools_build_v1_build_events_pb.BuildEvent;
      reader.readMessage(value,google_devtools_build_v1_build_events_pb.BuildEvent.deserializeBinaryFromReader);
      msg.setEvent(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.google.devtools.build.v1.OrderedBuildEvent.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.google.devtools.build.v1.OrderedBuildEvent} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.devtools.build.v1.OrderedBuildEvent.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStreamId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_devtools_build_v1_build_events_pb.StreamId.serializeBinaryToWriter
    );
  }
  f = message.getSequenceNumber();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getEvent();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_devtools_build_v1_build_events_pb.BuildEvent.serializeBinaryToWriter
    );
  }
};


/**
 * optional StreamId stream_id = 1;
 * @return {?proto.google.devtools.build.v1.StreamId}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.getStreamId = function() {
  return /** @type{?proto.google.devtools.build.v1.StreamId} */ (
    jspb.Message.getWrapperField(this, google_devtools_build_v1_build_events_pb.StreamId, 1));
};


/**
 * @param {?proto.google.devtools.build.v1.StreamId|undefined} value
 * @return {!proto.google.devtools.build.v1.OrderedBuildEvent} returns this
*/
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.setStreamId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.google.devtools.build.v1.OrderedBuildEvent} returns this
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.clearStreamId = function() {
  return this.setStreamId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.hasStreamId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int64 sequence_number = 2;
 * @return {number}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.getSequenceNumber = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.google.devtools.build.v1.OrderedBuildEvent} returns this
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.setSequenceNumber = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional BuildEvent event = 3;
 * @return {?proto.google.devtools.build.v1.BuildEvent}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.getEvent = function() {
  return /** @type{?proto.google.devtools.build.v1.BuildEvent} */ (
    jspb.Message.getWrapperField(this, google_devtools_build_v1_build_events_pb.BuildEvent, 3));
};


/**
 * @param {?proto.google.devtools.build.v1.BuildEvent|undefined} value
 * @return {!proto.google.devtools.build.v1.OrderedBuildEvent} returns this
*/
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.setEvent = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.google.devtools.build.v1.OrderedBuildEvent} returns this
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.clearEvent = function() {
  return this.setEvent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.google.devtools.build.v1.OrderedBuildEvent.prototype.hasEvent = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.repeatedFields_ = [5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    orderedBuildEvent: (f = msg.getOrderedBuildEvent()) && proto.google.devtools.build.v1.OrderedBuildEvent.toObject(includeInstance, f),
    notificationKeywordsList: (f = jspb.Message.getRepeatedField(msg, 5)) == null ? undefined : f,
    projectId: jspb.Message.getFieldWithDefault(msg, 6, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest;
  return proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 4:
      var value = new proto.google.devtools.build.v1.OrderedBuildEvent;
      reader.readMessage(value,proto.google.devtools.build.v1.OrderedBuildEvent.deserializeBinaryFromReader);
      msg.setOrderedBuildEvent(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.addNotificationKeywords(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setProjectId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOrderedBuildEvent();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.google.devtools.build.v1.OrderedBuildEvent.serializeBinaryToWriter
    );
  }
  f = message.getNotificationKeywordsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      5,
      f
    );
  }
  f = message.getProjectId();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
};


/**
 * optional OrderedBuildEvent ordered_build_event = 4;
 * @return {?proto.google.devtools.build.v1.OrderedBuildEvent}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.getOrderedBuildEvent = function() {
  return /** @type{?proto.google.devtools.build.v1.OrderedBuildEvent} */ (
    jspb.Message.getWrapperField(this, proto.google.devtools.build.v1.OrderedBuildEvent, 4));
};


/**
 * @param {?proto.google.devtools.build.v1.OrderedBuildEvent|undefined} value
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} returns this
*/
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.setOrderedBuildEvent = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} returns this
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.clearOrderedBuildEvent = function() {
  return this.setOrderedBuildEvent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.hasOrderedBuildEvent = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * repeated string notification_keywords = 5;
 * @return {!Array<string>}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.getNotificationKeywordsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 5));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} returns this
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.setNotificationKeywordsList = function(value) {
  return jspb.Message.setField(this, 5, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} returns this
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.addNotificationKeywords = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 5, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} returns this
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.clearNotificationKeywordsList = function() {
  return this.setNotificationKeywordsList([]);
};


/**
 * optional string project_id = 6;
 * @return {string}
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.getProjectId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest} returns this
 */
proto.google.devtools.build.v1.PublishBuildToolEventStreamRequest.prototype.setProjectId = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


goog.object.extend(exports, proto.google.devtools.build.v1);
