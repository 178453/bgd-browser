/**
 * @fileoverview gRPC-Web generated client stub for build.bazel.remote.logstream.v1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.build = {};
proto.build.bazel = {};
proto.build.bazel.remote = {};
proto.build.bazel.remote.logstream = {};
proto.build.bazel.remote.logstream.v1 = require('./remote_logstream_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.logstream.v1.LogStreamServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.logstream.v1.LogStreamServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.logstream.v1.CreateLogStreamRequest,
 *   !proto.build.bazel.remote.logstream.v1.LogStream>}
 */
const methodDescriptor_LogStreamService_CreateLogStream = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.logstream.v1.LogStreamService/CreateLogStream',
  grpc.web.MethodType.UNARY,
  proto.build.bazel.remote.logstream.v1.CreateLogStreamRequest,
  proto.build.bazel.remote.logstream.v1.LogStream,
  /**
   * @param {!proto.build.bazel.remote.logstream.v1.CreateLogStreamRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.logstream.v1.LogStream.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.logstream.v1.CreateLogStreamRequest,
 *   !proto.build.bazel.remote.logstream.v1.LogStream>}
 */
const methodInfo_LogStreamService_CreateLogStream = new grpc.web.AbstractClientBase.MethodInfo(
  proto.build.bazel.remote.logstream.v1.LogStream,
  /**
   * @param {!proto.build.bazel.remote.logstream.v1.CreateLogStreamRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.logstream.v1.LogStream.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.logstream.v1.CreateLogStreamRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.build.bazel.remote.logstream.v1.LogStream)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.logstream.v1.LogStream>|undefined}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.logstream.v1.LogStreamServiceClient.prototype.createLogStream =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/build.bazel.remote.logstream.v1.LogStreamService/CreateLogStream',
      request,
      metadata || {},
      methodDescriptor_LogStreamService_CreateLogStream,
      callback);
};


/**
 * @param {!proto.build.bazel.remote.logstream.v1.CreateLogStreamRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.build.bazel.remote.logstream.v1.LogStream>}
 *     A native promise that resolves to the response
 */
proto.build.bazel.remote.logstream.v1.LogStreamServicePromiseClient.prototype.createLogStream =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/build.bazel.remote.logstream.v1.LogStreamService/CreateLogStream',
      request,
      metadata || {},
      methodDescriptor_LogStreamService_CreateLogStream);
};


module.exports = proto.build.bazel.remote.logstream.v1;

