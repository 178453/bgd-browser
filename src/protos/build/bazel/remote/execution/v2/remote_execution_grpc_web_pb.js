/**
 * @fileoverview gRPC-Web generated client stub for build.bazel.remote.execution.v2
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var build_bazel_semver_semver_pb = require('../../../../../build/bazel/semver/semver_pb.js')

var google_api_annotations_pb = require('../../../../../google/api/annotations_pb.js')

var google_longrunning_operations_pb = require('../../../../../google/longrunning/operations_pb.js')

var google_protobuf_duration_pb = require('google-protobuf/google/protobuf/duration_pb.js')

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js')

var google_rpc_status_pb = require('../../../../../google/rpc/status_pb.js')
const proto = {};
proto.build = {};
proto.build.bazel = {};
proto.build.bazel.remote = {};
proto.build.bazel.remote.execution = {};
proto.build.bazel.remote.execution.v2 = require('./remote_execution_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.execution.v2.ExecutionClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.execution.v2.ExecutionPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.ExecuteRequest,
 *   !proto.google.longrunning.Operation>}
 */
const methodDescriptor_Execution_Execute = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.Execution/Execute',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.build.bazel.remote.execution.v2.ExecuteRequest,
  google_longrunning_operations_pb.Operation,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.ExecuteRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_longrunning_operations_pb.Operation.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.ExecuteRequest,
 *   !proto.google.longrunning.Operation>}
 */
const methodInfo_Execution_Execute = new grpc.web.AbstractClientBase.MethodInfo(
  google_longrunning_operations_pb.Operation,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.ExecuteRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_longrunning_operations_pb.Operation.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.ExecuteRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.google.longrunning.Operation>}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ExecutionClient.prototype.execute =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/build.bazel.remote.execution.v2.Execution/Execute',
      request,
      metadata || {},
      methodDescriptor_Execution_Execute);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.ExecuteRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.google.longrunning.Operation>}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ExecutionPromiseClient.prototype.execute =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/build.bazel.remote.execution.v2.Execution/Execute',
      request,
      metadata || {},
      methodDescriptor_Execution_Execute);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.WaitExecutionRequest,
 *   !proto.google.longrunning.Operation>}
 */
const methodDescriptor_Execution_WaitExecution = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.Execution/WaitExecution',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.build.bazel.remote.execution.v2.WaitExecutionRequest,
  google_longrunning_operations_pb.Operation,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.WaitExecutionRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_longrunning_operations_pb.Operation.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.WaitExecutionRequest,
 *   !proto.google.longrunning.Operation>}
 */
const methodInfo_Execution_WaitExecution = new grpc.web.AbstractClientBase.MethodInfo(
  google_longrunning_operations_pb.Operation,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.WaitExecutionRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_longrunning_operations_pb.Operation.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.WaitExecutionRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.google.longrunning.Operation>}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ExecutionClient.prototype.waitExecution =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/build.bazel.remote.execution.v2.Execution/WaitExecution',
      request,
      metadata || {},
      methodDescriptor_Execution_WaitExecution);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.WaitExecutionRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.google.longrunning.Operation>}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ExecutionPromiseClient.prototype.waitExecution =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/build.bazel.remote.execution.v2.Execution/WaitExecution',
      request,
      metadata || {},
      methodDescriptor_Execution_WaitExecution);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.execution.v2.ActionCacheClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.execution.v2.ActionCachePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.GetActionResultRequest,
 *   !proto.build.bazel.remote.execution.v2.ActionResult>}
 */
const methodDescriptor_ActionCache_GetActionResult = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.ActionCache/GetActionResult',
  grpc.web.MethodType.UNARY,
  proto.build.bazel.remote.execution.v2.GetActionResultRequest,
  proto.build.bazel.remote.execution.v2.ActionResult,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.GetActionResultRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.ActionResult.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.GetActionResultRequest,
 *   !proto.build.bazel.remote.execution.v2.ActionResult>}
 */
const methodInfo_ActionCache_GetActionResult = new grpc.web.AbstractClientBase.MethodInfo(
  proto.build.bazel.remote.execution.v2.ActionResult,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.GetActionResultRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.ActionResult.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.GetActionResultRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.build.bazel.remote.execution.v2.ActionResult)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.execution.v2.ActionResult>|undefined}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ActionCacheClient.prototype.getActionResult =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ActionCache/GetActionResult',
      request,
      metadata || {},
      methodDescriptor_ActionCache_GetActionResult,
      callback);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.GetActionResultRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.build.bazel.remote.execution.v2.ActionResult>}
 *     A native promise that resolves to the response
 */
proto.build.bazel.remote.execution.v2.ActionCachePromiseClient.prototype.getActionResult =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ActionCache/GetActionResult',
      request,
      metadata || {},
      methodDescriptor_ActionCache_GetActionResult);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.UpdateActionResultRequest,
 *   !proto.build.bazel.remote.execution.v2.ActionResult>}
 */
const methodDescriptor_ActionCache_UpdateActionResult = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.ActionCache/UpdateActionResult',
  grpc.web.MethodType.UNARY,
  proto.build.bazel.remote.execution.v2.UpdateActionResultRequest,
  proto.build.bazel.remote.execution.v2.ActionResult,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.UpdateActionResultRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.ActionResult.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.UpdateActionResultRequest,
 *   !proto.build.bazel.remote.execution.v2.ActionResult>}
 */
const methodInfo_ActionCache_UpdateActionResult = new grpc.web.AbstractClientBase.MethodInfo(
  proto.build.bazel.remote.execution.v2.ActionResult,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.UpdateActionResultRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.ActionResult.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.UpdateActionResultRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.build.bazel.remote.execution.v2.ActionResult)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.execution.v2.ActionResult>|undefined}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ActionCacheClient.prototype.updateActionResult =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ActionCache/UpdateActionResult',
      request,
      metadata || {},
      methodDescriptor_ActionCache_UpdateActionResult,
      callback);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.UpdateActionResultRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.build.bazel.remote.execution.v2.ActionResult>}
 *     A native promise that resolves to the response
 */
proto.build.bazel.remote.execution.v2.ActionCachePromiseClient.prototype.updateActionResult =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ActionCache/UpdateActionResult',
      request,
      metadata || {},
      methodDescriptor_ActionCache_UpdateActionResult);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStorageClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStoragePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.FindMissingBlobsRequest,
 *   !proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse>}
 */
const methodDescriptor_ContentAddressableStorage_FindMissingBlobs = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.ContentAddressableStorage/FindMissingBlobs',
  grpc.web.MethodType.UNARY,
  proto.build.bazel.remote.execution.v2.FindMissingBlobsRequest,
  proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.FindMissingBlobsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.FindMissingBlobsRequest,
 *   !proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse>}
 */
const methodInfo_ContentAddressableStorage_FindMissingBlobs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.FindMissingBlobsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.FindMissingBlobsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStorageClient.prototype.findMissingBlobs =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ContentAddressableStorage/FindMissingBlobs',
      request,
      metadata || {},
      methodDescriptor_ContentAddressableStorage_FindMissingBlobs,
      callback);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.FindMissingBlobsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.build.bazel.remote.execution.v2.FindMissingBlobsResponse>}
 *     A native promise that resolves to the response
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStoragePromiseClient.prototype.findMissingBlobs =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ContentAddressableStorage/FindMissingBlobs',
      request,
      metadata || {},
      methodDescriptor_ContentAddressableStorage_FindMissingBlobs);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest,
 *   !proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse>}
 */
const methodDescriptor_ContentAddressableStorage_BatchUpdateBlobs = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.ContentAddressableStorage/BatchUpdateBlobs',
  grpc.web.MethodType.UNARY,
  proto.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest,
  proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest,
 *   !proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse>}
 */
const methodInfo_ContentAddressableStorage_BatchUpdateBlobs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStorageClient.prototype.batchUpdateBlobs =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ContentAddressableStorage/BatchUpdateBlobs',
      request,
      metadata || {},
      methodDescriptor_ContentAddressableStorage_BatchUpdateBlobs,
      callback);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse>}
 *     A native promise that resolves to the response
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStoragePromiseClient.prototype.batchUpdateBlobs =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ContentAddressableStorage/BatchUpdateBlobs',
      request,
      metadata || {},
      methodDescriptor_ContentAddressableStorage_BatchUpdateBlobs);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.BatchReadBlobsRequest,
 *   !proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse>}
 */
const methodDescriptor_ContentAddressableStorage_BatchReadBlobs = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.ContentAddressableStorage/BatchReadBlobs',
  grpc.web.MethodType.UNARY,
  proto.build.bazel.remote.execution.v2.BatchReadBlobsRequest,
  proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.BatchReadBlobsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.BatchReadBlobsRequest,
 *   !proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse>}
 */
const methodInfo_ContentAddressableStorage_BatchReadBlobs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.BatchReadBlobsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.BatchReadBlobsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStorageClient.prototype.batchReadBlobs =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ContentAddressableStorage/BatchReadBlobs',
      request,
      metadata || {},
      methodDescriptor_ContentAddressableStorage_BatchReadBlobs,
      callback);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.BatchReadBlobsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.build.bazel.remote.execution.v2.BatchReadBlobsResponse>}
 *     A native promise that resolves to the response
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStoragePromiseClient.prototype.batchReadBlobs =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.ContentAddressableStorage/BatchReadBlobs',
      request,
      metadata || {},
      methodDescriptor_ContentAddressableStorage_BatchReadBlobs);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.GetTreeRequest,
 *   !proto.build.bazel.remote.execution.v2.GetTreeResponse>}
 */
const methodDescriptor_ContentAddressableStorage_GetTree = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.ContentAddressableStorage/GetTree',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.build.bazel.remote.execution.v2.GetTreeRequest,
  proto.build.bazel.remote.execution.v2.GetTreeResponse,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.GetTreeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.GetTreeResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.GetTreeRequest,
 *   !proto.build.bazel.remote.execution.v2.GetTreeResponse>}
 */
const methodInfo_ContentAddressableStorage_GetTree = new grpc.web.AbstractClientBase.MethodInfo(
  proto.build.bazel.remote.execution.v2.GetTreeResponse,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.GetTreeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.GetTreeResponse.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.GetTreeRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.execution.v2.GetTreeResponse>}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStorageClient.prototype.getTree =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/build.bazel.remote.execution.v2.ContentAddressableStorage/GetTree',
      request,
      metadata || {},
      methodDescriptor_ContentAddressableStorage_GetTree);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.GetTreeRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.execution.v2.GetTreeResponse>}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.ContentAddressableStoragePromiseClient.prototype.getTree =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/build.bazel.remote.execution.v2.ContentAddressableStorage/GetTree',
      request,
      metadata || {},
      methodDescriptor_ContentAddressableStorage_GetTree);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.execution.v2.CapabilitiesClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.build.bazel.remote.execution.v2.CapabilitiesPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.build.bazel.remote.execution.v2.GetCapabilitiesRequest,
 *   !proto.build.bazel.remote.execution.v2.ServerCapabilities>}
 */
const methodDescriptor_Capabilities_GetCapabilities = new grpc.web.MethodDescriptor(
  '/build.bazel.remote.execution.v2.Capabilities/GetCapabilities',
  grpc.web.MethodType.UNARY,
  proto.build.bazel.remote.execution.v2.GetCapabilitiesRequest,
  proto.build.bazel.remote.execution.v2.ServerCapabilities,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.GetCapabilitiesRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.ServerCapabilities.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.build.bazel.remote.execution.v2.GetCapabilitiesRequest,
 *   !proto.build.bazel.remote.execution.v2.ServerCapabilities>}
 */
const methodInfo_Capabilities_GetCapabilities = new grpc.web.AbstractClientBase.MethodInfo(
  proto.build.bazel.remote.execution.v2.ServerCapabilities,
  /**
   * @param {!proto.build.bazel.remote.execution.v2.GetCapabilitiesRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.build.bazel.remote.execution.v2.ServerCapabilities.deserializeBinary
);


/**
 * @param {!proto.build.bazel.remote.execution.v2.GetCapabilitiesRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.build.bazel.remote.execution.v2.ServerCapabilities)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.build.bazel.remote.execution.v2.ServerCapabilities>|undefined}
 *     The XHR Node Readable Stream
 */
proto.build.bazel.remote.execution.v2.CapabilitiesClient.prototype.getCapabilities =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.Capabilities/GetCapabilities',
      request,
      metadata || {},
      methodDescriptor_Capabilities_GetCapabilities,
      callback);
};


/**
 * @param {!proto.build.bazel.remote.execution.v2.GetCapabilitiesRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.build.bazel.remote.execution.v2.ServerCapabilities>}
 *     A native promise that resolves to the response
 */
proto.build.bazel.remote.execution.v2.CapabilitiesPromiseClient.prototype.getCapabilities =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/build.bazel.remote.execution.v2.Capabilities/GetCapabilities',
      request,
      metadata || {},
      methodDescriptor_Capabilities_GetCapabilities);
};


module.exports = proto.build.bazel.remote.execution.v2;

