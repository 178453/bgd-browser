// source: build_event_stream/failure_details.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_descriptor_pb = require('google-protobuf/google/protobuf/descriptor_pb.js');
goog.object.extend(proto, google_protobuf_descriptor_pb);
goog.exportSymbol('proto.failure_details.ActionCache', null, global);
goog.exportSymbol('proto.failure_details.ActionCache.Code', null, global);
goog.exportSymbol('proto.failure_details.ActionQuery', null, global);
goog.exportSymbol('proto.failure_details.ActionQuery.Code', null, global);
goog.exportSymbol('proto.failure_details.ActionRewinding', null, global);
goog.exportSymbol('proto.failure_details.ActionRewinding.Code', null, global);
goog.exportSymbol('proto.failure_details.Analysis', null, global);
goog.exportSymbol('proto.failure_details.Analysis.Code', null, global);
goog.exportSymbol('proto.failure_details.BuildConfiguration', null, global);
goog.exportSymbol('proto.failure_details.BuildConfiguration.Code', null, global);
goog.exportSymbol('proto.failure_details.BuildProgress', null, global);
goog.exportSymbol('proto.failure_details.BuildProgress.Code', null, global);
goog.exportSymbol('proto.failure_details.CanonicalizeFlags', null, global);
goog.exportSymbol('proto.failure_details.CanonicalizeFlags.Code', null, global);
goog.exportSymbol('proto.failure_details.CleanCommand', null, global);
goog.exportSymbol('proto.failure_details.CleanCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.ClientEnvironment', null, global);
goog.exportSymbol('proto.failure_details.ClientEnvironment.Code', null, global);
goog.exportSymbol('proto.failure_details.Command', null, global);
goog.exportSymbol('proto.failure_details.Command.Code', null, global);
goog.exportSymbol('proto.failure_details.ConfigCommand', null, global);
goog.exportSymbol('proto.failure_details.ConfigCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.ConfigurableQuery', null, global);
goog.exportSymbol('proto.failure_details.ConfigurableQuery.Code', null, global);
goog.exportSymbol('proto.failure_details.CppCompile', null, global);
goog.exportSymbol('proto.failure_details.CppCompile.Code', null, global);
goog.exportSymbol('proto.failure_details.CppLink', null, global);
goog.exportSymbol('proto.failure_details.CppLink.Code', null, global);
goog.exportSymbol('proto.failure_details.Crash', null, global);
goog.exportSymbol('proto.failure_details.Crash.Code', null, global);
goog.exportSymbol('proto.failure_details.CrashOptions', null, global);
goog.exportSymbol('proto.failure_details.CrashOptions.Code', null, global);
goog.exportSymbol('proto.failure_details.DiffAwareness', null, global);
goog.exportSymbol('proto.failure_details.DiffAwareness.Code', null, global);
goog.exportSymbol('proto.failure_details.DumpCommand', null, global);
goog.exportSymbol('proto.failure_details.DumpCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.DynamicExecution', null, global);
goog.exportSymbol('proto.failure_details.DynamicExecution.Code', null, global);
goog.exportSymbol('proto.failure_details.Execution', null, global);
goog.exportSymbol('proto.failure_details.Execution.Code', null, global);
goog.exportSymbol('proto.failure_details.ExecutionOptions', null, global);
goog.exportSymbol('proto.failure_details.ExecutionOptions.Code', null, global);
goog.exportSymbol('proto.failure_details.ExternalDeps', null, global);
goog.exportSymbol('proto.failure_details.ExternalDeps.Code', null, global);
goog.exportSymbol('proto.failure_details.ExternalRepository', null, global);
goog.exportSymbol('proto.failure_details.ExternalRepository.Code', null, global);
goog.exportSymbol('proto.failure_details.FailAction', null, global);
goog.exportSymbol('proto.failure_details.FailAction.Code', null, global);
goog.exportSymbol('proto.failure_details.FailureDetail', null, global);
goog.exportSymbol('proto.failure_details.FailureDetail.CategoryCase', null, global);
goog.exportSymbol('proto.failure_details.FailureDetailMetadata', null, global);
goog.exportSymbol('proto.failure_details.FetchCommand', null, global);
goog.exportSymbol('proto.failure_details.FetchCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.Filesystem', null, global);
goog.exportSymbol('proto.failure_details.Filesystem.Code', null, global);
goog.exportSymbol('proto.failure_details.GrpcServer', null, global);
goog.exportSymbol('proto.failure_details.GrpcServer.Code', null, global);
goog.exportSymbol('proto.failure_details.HelpCommand', null, global);
goog.exportSymbol('proto.failure_details.HelpCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.IncludeScanning', null, global);
goog.exportSymbol('proto.failure_details.IncludeScanning.Code', null, global);
goog.exportSymbol('proto.failure_details.InfoCommand', null, global);
goog.exportSymbol('proto.failure_details.InfoCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.Interrupted', null, global);
goog.exportSymbol('proto.failure_details.Interrupted.Code', null, global);
goog.exportSymbol('proto.failure_details.JavaCompile', null, global);
goog.exportSymbol('proto.failure_details.JavaCompile.Code', null, global);
goog.exportSymbol('proto.failure_details.LocalExecution', null, global);
goog.exportSymbol('proto.failure_details.LocalExecution.Code', null, global);
goog.exportSymbol('proto.failure_details.LtoAction', null, global);
goog.exportSymbol('proto.failure_details.LtoAction.Code', null, global);
goog.exportSymbol('proto.failure_details.MemoryOptions', null, global);
goog.exportSymbol('proto.failure_details.MemoryOptions.Code', null, global);
goog.exportSymbol('proto.failure_details.MobileInstall', null, global);
goog.exportSymbol('proto.failure_details.MobileInstall.Code', null, global);
goog.exportSymbol('proto.failure_details.NinjaAction', null, global);
goog.exportSymbol('proto.failure_details.NinjaAction.Code', null, global);
goog.exportSymbol('proto.failure_details.PackageLoading', null, global);
goog.exportSymbol('proto.failure_details.PackageLoading.Code', null, global);
goog.exportSymbol('proto.failure_details.PackageOptions', null, global);
goog.exportSymbol('proto.failure_details.PackageOptions.Code', null, global);
goog.exportSymbol('proto.failure_details.PrintActionCommand', null, global);
goog.exportSymbol('proto.failure_details.PrintActionCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.ProfileCommand', null, global);
goog.exportSymbol('proto.failure_details.ProfileCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.Query', null, global);
goog.exportSymbol('proto.failure_details.Query.Code', null, global);
goog.exportSymbol('proto.failure_details.RemoteExecution', null, global);
goog.exportSymbol('proto.failure_details.RemoteExecution.Code', null, global);
goog.exportSymbol('proto.failure_details.RemoteOptions', null, global);
goog.exportSymbol('proto.failure_details.RemoteOptions.Code', null, global);
goog.exportSymbol('proto.failure_details.RunCommand', null, global);
goog.exportSymbol('proto.failure_details.RunCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.Sandbox', null, global);
goog.exportSymbol('proto.failure_details.Sandbox.Code', null, global);
goog.exportSymbol('proto.failure_details.Spawn', null, global);
goog.exportSymbol('proto.failure_details.Spawn.Code', null, global);
goog.exportSymbol('proto.failure_details.StarlarkAction', null, global);
goog.exportSymbol('proto.failure_details.StarlarkAction.Code', null, global);
goog.exportSymbol('proto.failure_details.StarlarkLoading', null, global);
goog.exportSymbol('proto.failure_details.StarlarkLoading.Code', null, global);
goog.exportSymbol('proto.failure_details.SymlinkAction', null, global);
goog.exportSymbol('proto.failure_details.SymlinkAction.Code', null, global);
goog.exportSymbol('proto.failure_details.SymlinkForest', null, global);
goog.exportSymbol('proto.failure_details.SymlinkForest.Code', null, global);
goog.exportSymbol('proto.failure_details.SyncCommand', null, global);
goog.exportSymbol('proto.failure_details.SyncCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.TargetPatterns', null, global);
goog.exportSymbol('proto.failure_details.TargetPatterns.Code', null, global);
goog.exportSymbol('proto.failure_details.TestAction', null, global);
goog.exportSymbol('proto.failure_details.TestAction.Code', null, global);
goog.exportSymbol('proto.failure_details.TestCommand', null, global);
goog.exportSymbol('proto.failure_details.TestCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.Throwable', null, global);
goog.exportSymbol('proto.failure_details.Toolchain', null, global);
goog.exportSymbol('proto.failure_details.Toolchain.Code', null, global);
goog.exportSymbol('proto.failure_details.VersionCommand', null, global);
goog.exportSymbol('proto.failure_details.VersionCommand.Code', null, global);
goog.exportSymbol('proto.failure_details.Worker', null, global);
goog.exportSymbol('proto.failure_details.Worker.Code', null, global);
goog.exportSymbol('proto.failure_details.WorkspaceStatus', null, global);
goog.exportSymbol('proto.failure_details.WorkspaceStatus.Code', null, global);
goog.exportSymbol('proto.failure_details.Workspaces', null, global);
goog.exportSymbol('proto.failure_details.Workspaces.Code', null, global);
goog.exportSymbol('proto.failure_details.metadata', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.FailureDetailMetadata = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.FailureDetailMetadata, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.FailureDetailMetadata.displayName = 'proto.failure_details.FailureDetailMetadata';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.FailureDetail = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.failure_details.FailureDetail.oneofGroups_);
};
goog.inherits(proto.failure_details.FailureDetail, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.FailureDetail.displayName = 'proto.failure_details.FailureDetail';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Interrupted = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Interrupted, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Interrupted.displayName = 'proto.failure_details.Interrupted';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Spawn = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Spawn, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Spawn.displayName = 'proto.failure_details.Spawn';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ExternalRepository = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ExternalRepository, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ExternalRepository.displayName = 'proto.failure_details.ExternalRepository';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.BuildProgress = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.BuildProgress, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.BuildProgress.displayName = 'proto.failure_details.BuildProgress';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.RemoteOptions = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.RemoteOptions, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.RemoteOptions.displayName = 'proto.failure_details.RemoteOptions';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ClientEnvironment = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ClientEnvironment, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ClientEnvironment.displayName = 'proto.failure_details.ClientEnvironment';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Crash = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.failure_details.Crash.repeatedFields_, null);
};
goog.inherits(proto.failure_details.Crash, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Crash.displayName = 'proto.failure_details.Crash';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Throwable = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.failure_details.Throwable.repeatedFields_, null);
};
goog.inherits(proto.failure_details.Throwable, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Throwable.displayName = 'proto.failure_details.Throwable';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.SymlinkForest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.SymlinkForest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.SymlinkForest.displayName = 'proto.failure_details.SymlinkForest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.PackageOptions = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.PackageOptions, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.PackageOptions.displayName = 'proto.failure_details.PackageOptions';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.RemoteExecution = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.RemoteExecution, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.RemoteExecution.displayName = 'proto.failure_details.RemoteExecution';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Execution = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Execution, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Execution.displayName = 'proto.failure_details.Execution';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Workspaces = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Workspaces, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Workspaces.displayName = 'proto.failure_details.Workspaces';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.CrashOptions = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.CrashOptions, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.CrashOptions.displayName = 'proto.failure_details.CrashOptions';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Filesystem = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Filesystem, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Filesystem.displayName = 'proto.failure_details.Filesystem';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ExecutionOptions = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ExecutionOptions, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ExecutionOptions.displayName = 'proto.failure_details.ExecutionOptions';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Command = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Command, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Command.displayName = 'proto.failure_details.Command';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.GrpcServer = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.GrpcServer, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.GrpcServer.displayName = 'proto.failure_details.GrpcServer';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.CanonicalizeFlags = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.CanonicalizeFlags, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.CanonicalizeFlags.displayName = 'proto.failure_details.CanonicalizeFlags';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.BuildConfiguration = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.BuildConfiguration, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.BuildConfiguration.displayName = 'proto.failure_details.BuildConfiguration';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.InfoCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.InfoCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.InfoCommand.displayName = 'proto.failure_details.InfoCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.MemoryOptions = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.MemoryOptions, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.MemoryOptions.displayName = 'proto.failure_details.MemoryOptions';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Query = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Query, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Query.displayName = 'proto.failure_details.Query';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.LocalExecution = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.LocalExecution, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.LocalExecution.displayName = 'proto.failure_details.LocalExecution';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ActionCache = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ActionCache, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ActionCache.displayName = 'proto.failure_details.ActionCache';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.FetchCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.FetchCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.FetchCommand.displayName = 'proto.failure_details.FetchCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.SyncCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.SyncCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.SyncCommand.displayName = 'proto.failure_details.SyncCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Sandbox = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Sandbox, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Sandbox.displayName = 'proto.failure_details.Sandbox';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.IncludeScanning = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.IncludeScanning, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.IncludeScanning.displayName = 'proto.failure_details.IncludeScanning';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.TestCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.TestCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.TestCommand.displayName = 'proto.failure_details.TestCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ActionQuery = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ActionQuery, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ActionQuery.displayName = 'proto.failure_details.ActionQuery';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.TargetPatterns = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.TargetPatterns, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.TargetPatterns.displayName = 'proto.failure_details.TargetPatterns';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.CleanCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.CleanCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.CleanCommand.displayName = 'proto.failure_details.CleanCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ConfigCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ConfigCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ConfigCommand.displayName = 'proto.failure_details.ConfigCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ConfigurableQuery = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ConfigurableQuery, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ConfigurableQuery.displayName = 'proto.failure_details.ConfigurableQuery';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.DumpCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.DumpCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.DumpCommand.displayName = 'proto.failure_details.DumpCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.HelpCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.HelpCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.HelpCommand.displayName = 'proto.failure_details.HelpCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.MobileInstall = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.MobileInstall, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.MobileInstall.displayName = 'proto.failure_details.MobileInstall';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ProfileCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ProfileCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ProfileCommand.displayName = 'proto.failure_details.ProfileCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.RunCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.RunCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.RunCommand.displayName = 'proto.failure_details.RunCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.VersionCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.VersionCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.VersionCommand.displayName = 'proto.failure_details.VersionCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.PrintActionCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.PrintActionCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.PrintActionCommand.displayName = 'proto.failure_details.PrintActionCommand';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.WorkspaceStatus = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.WorkspaceStatus, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.WorkspaceStatus.displayName = 'proto.failure_details.WorkspaceStatus';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.JavaCompile = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.JavaCompile, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.JavaCompile.displayName = 'proto.failure_details.JavaCompile';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ActionRewinding = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ActionRewinding, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ActionRewinding.displayName = 'proto.failure_details.ActionRewinding';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.CppCompile = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.CppCompile, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.CppCompile.displayName = 'proto.failure_details.CppCompile';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.StarlarkAction = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.StarlarkAction, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.StarlarkAction.displayName = 'proto.failure_details.StarlarkAction';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.NinjaAction = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.NinjaAction, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.NinjaAction.displayName = 'proto.failure_details.NinjaAction';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.DynamicExecution = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.DynamicExecution, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.DynamicExecution.displayName = 'proto.failure_details.DynamicExecution';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.FailAction = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.FailAction, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.FailAction.displayName = 'proto.failure_details.FailAction';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.SymlinkAction = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.SymlinkAction, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.SymlinkAction.displayName = 'proto.failure_details.SymlinkAction';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.CppLink = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.CppLink, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.CppLink.displayName = 'proto.failure_details.CppLink';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.LtoAction = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.LtoAction, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.LtoAction.displayName = 'proto.failure_details.LtoAction';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.TestAction = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.TestAction, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.TestAction.displayName = 'proto.failure_details.TestAction';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Worker = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Worker, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Worker.displayName = 'proto.failure_details.Worker';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Analysis = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Analysis, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Analysis.displayName = 'proto.failure_details.Analysis';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.PackageLoading = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.PackageLoading, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.PackageLoading.displayName = 'proto.failure_details.PackageLoading';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.Toolchain = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.Toolchain, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.Toolchain.displayName = 'proto.failure_details.Toolchain';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.StarlarkLoading = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.StarlarkLoading, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.StarlarkLoading.displayName = 'proto.failure_details.StarlarkLoading';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.ExternalDeps = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.ExternalDeps, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.ExternalDeps.displayName = 'proto.failure_details.ExternalDeps';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.failure_details.DiffAwareness = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.failure_details.DiffAwareness, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.failure_details.DiffAwareness.displayName = 'proto.failure_details.DiffAwareness';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.FailureDetailMetadata.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.FailureDetailMetadata.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.FailureDetailMetadata} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.FailureDetailMetadata.toObject = function(includeInstance, msg) {
  var f, obj = {
    exitCode: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.FailureDetailMetadata}
 */
proto.failure_details.FailureDetailMetadata.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.FailureDetailMetadata;
  return proto.failure_details.FailureDetailMetadata.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.FailureDetailMetadata} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.FailureDetailMetadata}
 */
proto.failure_details.FailureDetailMetadata.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setExitCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.FailureDetailMetadata.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.FailureDetailMetadata.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.FailureDetailMetadata} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.FailureDetailMetadata.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getExitCode();
  if (f !== 0) {
    writer.writeUint32(
      1,
      f
    );
  }
};


/**
 * optional uint32 exit_code = 1;
 * @return {number}
 */
proto.failure_details.FailureDetailMetadata.prototype.getExitCode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.failure_details.FailureDetailMetadata} returns this
 */
proto.failure_details.FailureDetailMetadata.prototype.setExitCode = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.failure_details.FailureDetail.oneofGroups_ = [[101,103,104,106,107,108,110,114,115,116,117,118,119,121,122,123,124,125,126,127,129,130,132,134,135,136,137,139,140,141,142,144,145,146,147,148,150,151,152,153,154,158,159,160,161,162,163,164,166,167,168,169,172,173,174,175,177,179,181,182]];

/**
 * @enum {number}
 */
proto.failure_details.FailureDetail.CategoryCase = {
  CATEGORY_NOT_SET: 0,
  INTERRUPTED: 101,
  EXTERNAL_REPOSITORY: 103,
  BUILD_PROGRESS: 104,
  REMOTE_OPTIONS: 106,
  CLIENT_ENVIRONMENT: 107,
  CRASH: 108,
  SYMLINK_FOREST: 110,
  PACKAGE_OPTIONS: 114,
  REMOTE_EXECUTION: 115,
  EXECUTION: 116,
  WORKSPACES: 117,
  CRASH_OPTIONS: 118,
  FILESYSTEM: 119,
  EXECUTION_OPTIONS: 121,
  COMMAND: 122,
  SPAWN: 123,
  GRPC_SERVER: 124,
  CANONICALIZE_FLAGS: 125,
  BUILD_CONFIGURATION: 126,
  INFO_COMMAND: 127,
  MEMORY_OPTIONS: 129,
  QUERY: 130,
  LOCAL_EXECUTION: 132,
  ACTION_CACHE: 134,
  FETCH_COMMAND: 135,
  SYNC_COMMAND: 136,
  SANDBOX: 137,
  INCLUDE_SCANNING: 139,
  TEST_COMMAND: 140,
  ACTION_QUERY: 141,
  TARGET_PATTERNS: 142,
  CLEAN_COMMAND: 144,
  CONFIG_COMMAND: 145,
  CONFIGURABLE_QUERY: 146,
  DUMP_COMMAND: 147,
  HELP_COMMAND: 148,
  MOBILE_INSTALL: 150,
  PROFILE_COMMAND: 151,
  RUN_COMMAND: 152,
  VERSION_COMMAND: 153,
  PRINT_ACTION_COMMAND: 154,
  WORKSPACE_STATUS: 158,
  JAVA_COMPILE: 159,
  ACTION_REWINDING: 160,
  CPP_COMPILE: 161,
  STARLARK_ACTION: 162,
  NINJA_ACTION: 163,
  DYNAMIC_EXECUTION: 164,
  FAIL_ACTION: 166,
  SYMLINK_ACTION: 167,
  CPP_LINK: 168,
  LTO_ACTION: 169,
  TEST_ACTION: 172,
  WORKER: 173,
  ANALYSIS: 174,
  PACKAGE_LOADING: 175,
  TOOLCHAIN: 177,
  STARLARK_LOADING: 179,
  EXTERNAL_DEPS: 181,
  DIFF_AWARENESS: 182
};

/**
 * @return {proto.failure_details.FailureDetail.CategoryCase}
 */
proto.failure_details.FailureDetail.prototype.getCategoryCase = function() {
  return /** @type {proto.failure_details.FailureDetail.CategoryCase} */(jspb.Message.computeOneofCase(this, proto.failure_details.FailureDetail.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.FailureDetail.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.FailureDetail.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.FailureDetail} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.FailureDetail.toObject = function(includeInstance, msg) {
  var f, obj = {
    message: jspb.Message.getFieldWithDefault(msg, 1, ""),
    interrupted: (f = msg.getInterrupted()) && proto.failure_details.Interrupted.toObject(includeInstance, f),
    externalRepository: (f = msg.getExternalRepository()) && proto.failure_details.ExternalRepository.toObject(includeInstance, f),
    buildProgress: (f = msg.getBuildProgress()) && proto.failure_details.BuildProgress.toObject(includeInstance, f),
    remoteOptions: (f = msg.getRemoteOptions()) && proto.failure_details.RemoteOptions.toObject(includeInstance, f),
    clientEnvironment: (f = msg.getClientEnvironment()) && proto.failure_details.ClientEnvironment.toObject(includeInstance, f),
    crash: (f = msg.getCrash()) && proto.failure_details.Crash.toObject(includeInstance, f),
    symlinkForest: (f = msg.getSymlinkForest()) && proto.failure_details.SymlinkForest.toObject(includeInstance, f),
    packageOptions: (f = msg.getPackageOptions()) && proto.failure_details.PackageOptions.toObject(includeInstance, f),
    remoteExecution: (f = msg.getRemoteExecution()) && proto.failure_details.RemoteExecution.toObject(includeInstance, f),
    execution: (f = msg.getExecution()) && proto.failure_details.Execution.toObject(includeInstance, f),
    workspaces: (f = msg.getWorkspaces()) && proto.failure_details.Workspaces.toObject(includeInstance, f),
    crashOptions: (f = msg.getCrashOptions()) && proto.failure_details.CrashOptions.toObject(includeInstance, f),
    filesystem: (f = msg.getFilesystem()) && proto.failure_details.Filesystem.toObject(includeInstance, f),
    executionOptions: (f = msg.getExecutionOptions()) && proto.failure_details.ExecutionOptions.toObject(includeInstance, f),
    command: (f = msg.getCommand()) && proto.failure_details.Command.toObject(includeInstance, f),
    spawn: (f = msg.getSpawn()) && proto.failure_details.Spawn.toObject(includeInstance, f),
    grpcServer: (f = msg.getGrpcServer()) && proto.failure_details.GrpcServer.toObject(includeInstance, f),
    canonicalizeFlags: (f = msg.getCanonicalizeFlags()) && proto.failure_details.CanonicalizeFlags.toObject(includeInstance, f),
    buildConfiguration: (f = msg.getBuildConfiguration()) && proto.failure_details.BuildConfiguration.toObject(includeInstance, f),
    infoCommand: (f = msg.getInfoCommand()) && proto.failure_details.InfoCommand.toObject(includeInstance, f),
    memoryOptions: (f = msg.getMemoryOptions()) && proto.failure_details.MemoryOptions.toObject(includeInstance, f),
    query: (f = msg.getQuery()) && proto.failure_details.Query.toObject(includeInstance, f),
    localExecution: (f = msg.getLocalExecution()) && proto.failure_details.LocalExecution.toObject(includeInstance, f),
    actionCache: (f = msg.getActionCache()) && proto.failure_details.ActionCache.toObject(includeInstance, f),
    fetchCommand: (f = msg.getFetchCommand()) && proto.failure_details.FetchCommand.toObject(includeInstance, f),
    syncCommand: (f = msg.getSyncCommand()) && proto.failure_details.SyncCommand.toObject(includeInstance, f),
    sandbox: (f = msg.getSandbox()) && proto.failure_details.Sandbox.toObject(includeInstance, f),
    includeScanning: (f = msg.getIncludeScanning()) && proto.failure_details.IncludeScanning.toObject(includeInstance, f),
    testCommand: (f = msg.getTestCommand()) && proto.failure_details.TestCommand.toObject(includeInstance, f),
    actionQuery: (f = msg.getActionQuery()) && proto.failure_details.ActionQuery.toObject(includeInstance, f),
    targetPatterns: (f = msg.getTargetPatterns()) && proto.failure_details.TargetPatterns.toObject(includeInstance, f),
    cleanCommand: (f = msg.getCleanCommand()) && proto.failure_details.CleanCommand.toObject(includeInstance, f),
    configCommand: (f = msg.getConfigCommand()) && proto.failure_details.ConfigCommand.toObject(includeInstance, f),
    configurableQuery: (f = msg.getConfigurableQuery()) && proto.failure_details.ConfigurableQuery.toObject(includeInstance, f),
    dumpCommand: (f = msg.getDumpCommand()) && proto.failure_details.DumpCommand.toObject(includeInstance, f),
    helpCommand: (f = msg.getHelpCommand()) && proto.failure_details.HelpCommand.toObject(includeInstance, f),
    mobileInstall: (f = msg.getMobileInstall()) && proto.failure_details.MobileInstall.toObject(includeInstance, f),
    profileCommand: (f = msg.getProfileCommand()) && proto.failure_details.ProfileCommand.toObject(includeInstance, f),
    runCommand: (f = msg.getRunCommand()) && proto.failure_details.RunCommand.toObject(includeInstance, f),
    versionCommand: (f = msg.getVersionCommand()) && proto.failure_details.VersionCommand.toObject(includeInstance, f),
    printActionCommand: (f = msg.getPrintActionCommand()) && proto.failure_details.PrintActionCommand.toObject(includeInstance, f),
    workspaceStatus: (f = msg.getWorkspaceStatus()) && proto.failure_details.WorkspaceStatus.toObject(includeInstance, f),
    javaCompile: (f = msg.getJavaCompile()) && proto.failure_details.JavaCompile.toObject(includeInstance, f),
    actionRewinding: (f = msg.getActionRewinding()) && proto.failure_details.ActionRewinding.toObject(includeInstance, f),
    cppCompile: (f = msg.getCppCompile()) && proto.failure_details.CppCompile.toObject(includeInstance, f),
    starlarkAction: (f = msg.getStarlarkAction()) && proto.failure_details.StarlarkAction.toObject(includeInstance, f),
    ninjaAction: (f = msg.getNinjaAction()) && proto.failure_details.NinjaAction.toObject(includeInstance, f),
    dynamicExecution: (f = msg.getDynamicExecution()) && proto.failure_details.DynamicExecution.toObject(includeInstance, f),
    failAction: (f = msg.getFailAction()) && proto.failure_details.FailAction.toObject(includeInstance, f),
    symlinkAction: (f = msg.getSymlinkAction()) && proto.failure_details.SymlinkAction.toObject(includeInstance, f),
    cppLink: (f = msg.getCppLink()) && proto.failure_details.CppLink.toObject(includeInstance, f),
    ltoAction: (f = msg.getLtoAction()) && proto.failure_details.LtoAction.toObject(includeInstance, f),
    testAction: (f = msg.getTestAction()) && proto.failure_details.TestAction.toObject(includeInstance, f),
    worker: (f = msg.getWorker()) && proto.failure_details.Worker.toObject(includeInstance, f),
    analysis: (f = msg.getAnalysis()) && proto.failure_details.Analysis.toObject(includeInstance, f),
    packageLoading: (f = msg.getPackageLoading()) && proto.failure_details.PackageLoading.toObject(includeInstance, f),
    toolchain: (f = msg.getToolchain()) && proto.failure_details.Toolchain.toObject(includeInstance, f),
    starlarkLoading: (f = msg.getStarlarkLoading()) && proto.failure_details.StarlarkLoading.toObject(includeInstance, f),
    externalDeps: (f = msg.getExternalDeps()) && proto.failure_details.ExternalDeps.toObject(includeInstance, f),
    diffAwareness: (f = msg.getDiffAwareness()) && proto.failure_details.DiffAwareness.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.FailureDetail}
 */
proto.failure_details.FailureDetail.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.FailureDetail;
  return proto.failure_details.FailureDetail.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.FailureDetail} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.FailureDetail}
 */
proto.failure_details.FailureDetail.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    case 101:
      var value = new proto.failure_details.Interrupted;
      reader.readMessage(value,proto.failure_details.Interrupted.deserializeBinaryFromReader);
      msg.setInterrupted(value);
      break;
    case 103:
      var value = new proto.failure_details.ExternalRepository;
      reader.readMessage(value,proto.failure_details.ExternalRepository.deserializeBinaryFromReader);
      msg.setExternalRepository(value);
      break;
    case 104:
      var value = new proto.failure_details.BuildProgress;
      reader.readMessage(value,proto.failure_details.BuildProgress.deserializeBinaryFromReader);
      msg.setBuildProgress(value);
      break;
    case 106:
      var value = new proto.failure_details.RemoteOptions;
      reader.readMessage(value,proto.failure_details.RemoteOptions.deserializeBinaryFromReader);
      msg.setRemoteOptions(value);
      break;
    case 107:
      var value = new proto.failure_details.ClientEnvironment;
      reader.readMessage(value,proto.failure_details.ClientEnvironment.deserializeBinaryFromReader);
      msg.setClientEnvironment(value);
      break;
    case 108:
      var value = new proto.failure_details.Crash;
      reader.readMessage(value,proto.failure_details.Crash.deserializeBinaryFromReader);
      msg.setCrash(value);
      break;
    case 110:
      var value = new proto.failure_details.SymlinkForest;
      reader.readMessage(value,proto.failure_details.SymlinkForest.deserializeBinaryFromReader);
      msg.setSymlinkForest(value);
      break;
    case 114:
      var value = new proto.failure_details.PackageOptions;
      reader.readMessage(value,proto.failure_details.PackageOptions.deserializeBinaryFromReader);
      msg.setPackageOptions(value);
      break;
    case 115:
      var value = new proto.failure_details.RemoteExecution;
      reader.readMessage(value,proto.failure_details.RemoteExecution.deserializeBinaryFromReader);
      msg.setRemoteExecution(value);
      break;
    case 116:
      var value = new proto.failure_details.Execution;
      reader.readMessage(value,proto.failure_details.Execution.deserializeBinaryFromReader);
      msg.setExecution(value);
      break;
    case 117:
      var value = new proto.failure_details.Workspaces;
      reader.readMessage(value,proto.failure_details.Workspaces.deserializeBinaryFromReader);
      msg.setWorkspaces(value);
      break;
    case 118:
      var value = new proto.failure_details.CrashOptions;
      reader.readMessage(value,proto.failure_details.CrashOptions.deserializeBinaryFromReader);
      msg.setCrashOptions(value);
      break;
    case 119:
      var value = new proto.failure_details.Filesystem;
      reader.readMessage(value,proto.failure_details.Filesystem.deserializeBinaryFromReader);
      msg.setFilesystem(value);
      break;
    case 121:
      var value = new proto.failure_details.ExecutionOptions;
      reader.readMessage(value,proto.failure_details.ExecutionOptions.deserializeBinaryFromReader);
      msg.setExecutionOptions(value);
      break;
    case 122:
      var value = new proto.failure_details.Command;
      reader.readMessage(value,proto.failure_details.Command.deserializeBinaryFromReader);
      msg.setCommand(value);
      break;
    case 123:
      var value = new proto.failure_details.Spawn;
      reader.readMessage(value,proto.failure_details.Spawn.deserializeBinaryFromReader);
      msg.setSpawn(value);
      break;
    case 124:
      var value = new proto.failure_details.GrpcServer;
      reader.readMessage(value,proto.failure_details.GrpcServer.deserializeBinaryFromReader);
      msg.setGrpcServer(value);
      break;
    case 125:
      var value = new proto.failure_details.CanonicalizeFlags;
      reader.readMessage(value,proto.failure_details.CanonicalizeFlags.deserializeBinaryFromReader);
      msg.setCanonicalizeFlags(value);
      break;
    case 126:
      var value = new proto.failure_details.BuildConfiguration;
      reader.readMessage(value,proto.failure_details.BuildConfiguration.deserializeBinaryFromReader);
      msg.setBuildConfiguration(value);
      break;
    case 127:
      var value = new proto.failure_details.InfoCommand;
      reader.readMessage(value,proto.failure_details.InfoCommand.deserializeBinaryFromReader);
      msg.setInfoCommand(value);
      break;
    case 129:
      var value = new proto.failure_details.MemoryOptions;
      reader.readMessage(value,proto.failure_details.MemoryOptions.deserializeBinaryFromReader);
      msg.setMemoryOptions(value);
      break;
    case 130:
      var value = new proto.failure_details.Query;
      reader.readMessage(value,proto.failure_details.Query.deserializeBinaryFromReader);
      msg.setQuery(value);
      break;
    case 132:
      var value = new proto.failure_details.LocalExecution;
      reader.readMessage(value,proto.failure_details.LocalExecution.deserializeBinaryFromReader);
      msg.setLocalExecution(value);
      break;
    case 134:
      var value = new proto.failure_details.ActionCache;
      reader.readMessage(value,proto.failure_details.ActionCache.deserializeBinaryFromReader);
      msg.setActionCache(value);
      break;
    case 135:
      var value = new proto.failure_details.FetchCommand;
      reader.readMessage(value,proto.failure_details.FetchCommand.deserializeBinaryFromReader);
      msg.setFetchCommand(value);
      break;
    case 136:
      var value = new proto.failure_details.SyncCommand;
      reader.readMessage(value,proto.failure_details.SyncCommand.deserializeBinaryFromReader);
      msg.setSyncCommand(value);
      break;
    case 137:
      var value = new proto.failure_details.Sandbox;
      reader.readMessage(value,proto.failure_details.Sandbox.deserializeBinaryFromReader);
      msg.setSandbox(value);
      break;
    case 139:
      var value = new proto.failure_details.IncludeScanning;
      reader.readMessage(value,proto.failure_details.IncludeScanning.deserializeBinaryFromReader);
      msg.setIncludeScanning(value);
      break;
    case 140:
      var value = new proto.failure_details.TestCommand;
      reader.readMessage(value,proto.failure_details.TestCommand.deserializeBinaryFromReader);
      msg.setTestCommand(value);
      break;
    case 141:
      var value = new proto.failure_details.ActionQuery;
      reader.readMessage(value,proto.failure_details.ActionQuery.deserializeBinaryFromReader);
      msg.setActionQuery(value);
      break;
    case 142:
      var value = new proto.failure_details.TargetPatterns;
      reader.readMessage(value,proto.failure_details.TargetPatterns.deserializeBinaryFromReader);
      msg.setTargetPatterns(value);
      break;
    case 144:
      var value = new proto.failure_details.CleanCommand;
      reader.readMessage(value,proto.failure_details.CleanCommand.deserializeBinaryFromReader);
      msg.setCleanCommand(value);
      break;
    case 145:
      var value = new proto.failure_details.ConfigCommand;
      reader.readMessage(value,proto.failure_details.ConfigCommand.deserializeBinaryFromReader);
      msg.setConfigCommand(value);
      break;
    case 146:
      var value = new proto.failure_details.ConfigurableQuery;
      reader.readMessage(value,proto.failure_details.ConfigurableQuery.deserializeBinaryFromReader);
      msg.setConfigurableQuery(value);
      break;
    case 147:
      var value = new proto.failure_details.DumpCommand;
      reader.readMessage(value,proto.failure_details.DumpCommand.deserializeBinaryFromReader);
      msg.setDumpCommand(value);
      break;
    case 148:
      var value = new proto.failure_details.HelpCommand;
      reader.readMessage(value,proto.failure_details.HelpCommand.deserializeBinaryFromReader);
      msg.setHelpCommand(value);
      break;
    case 150:
      var value = new proto.failure_details.MobileInstall;
      reader.readMessage(value,proto.failure_details.MobileInstall.deserializeBinaryFromReader);
      msg.setMobileInstall(value);
      break;
    case 151:
      var value = new proto.failure_details.ProfileCommand;
      reader.readMessage(value,proto.failure_details.ProfileCommand.deserializeBinaryFromReader);
      msg.setProfileCommand(value);
      break;
    case 152:
      var value = new proto.failure_details.RunCommand;
      reader.readMessage(value,proto.failure_details.RunCommand.deserializeBinaryFromReader);
      msg.setRunCommand(value);
      break;
    case 153:
      var value = new proto.failure_details.VersionCommand;
      reader.readMessage(value,proto.failure_details.VersionCommand.deserializeBinaryFromReader);
      msg.setVersionCommand(value);
      break;
    case 154:
      var value = new proto.failure_details.PrintActionCommand;
      reader.readMessage(value,proto.failure_details.PrintActionCommand.deserializeBinaryFromReader);
      msg.setPrintActionCommand(value);
      break;
    case 158:
      var value = new proto.failure_details.WorkspaceStatus;
      reader.readMessage(value,proto.failure_details.WorkspaceStatus.deserializeBinaryFromReader);
      msg.setWorkspaceStatus(value);
      break;
    case 159:
      var value = new proto.failure_details.JavaCompile;
      reader.readMessage(value,proto.failure_details.JavaCompile.deserializeBinaryFromReader);
      msg.setJavaCompile(value);
      break;
    case 160:
      var value = new proto.failure_details.ActionRewinding;
      reader.readMessage(value,proto.failure_details.ActionRewinding.deserializeBinaryFromReader);
      msg.setActionRewinding(value);
      break;
    case 161:
      var value = new proto.failure_details.CppCompile;
      reader.readMessage(value,proto.failure_details.CppCompile.deserializeBinaryFromReader);
      msg.setCppCompile(value);
      break;
    case 162:
      var value = new proto.failure_details.StarlarkAction;
      reader.readMessage(value,proto.failure_details.StarlarkAction.deserializeBinaryFromReader);
      msg.setStarlarkAction(value);
      break;
    case 163:
      var value = new proto.failure_details.NinjaAction;
      reader.readMessage(value,proto.failure_details.NinjaAction.deserializeBinaryFromReader);
      msg.setNinjaAction(value);
      break;
    case 164:
      var value = new proto.failure_details.DynamicExecution;
      reader.readMessage(value,proto.failure_details.DynamicExecution.deserializeBinaryFromReader);
      msg.setDynamicExecution(value);
      break;
    case 166:
      var value = new proto.failure_details.FailAction;
      reader.readMessage(value,proto.failure_details.FailAction.deserializeBinaryFromReader);
      msg.setFailAction(value);
      break;
    case 167:
      var value = new proto.failure_details.SymlinkAction;
      reader.readMessage(value,proto.failure_details.SymlinkAction.deserializeBinaryFromReader);
      msg.setSymlinkAction(value);
      break;
    case 168:
      var value = new proto.failure_details.CppLink;
      reader.readMessage(value,proto.failure_details.CppLink.deserializeBinaryFromReader);
      msg.setCppLink(value);
      break;
    case 169:
      var value = new proto.failure_details.LtoAction;
      reader.readMessage(value,proto.failure_details.LtoAction.deserializeBinaryFromReader);
      msg.setLtoAction(value);
      break;
    case 172:
      var value = new proto.failure_details.TestAction;
      reader.readMessage(value,proto.failure_details.TestAction.deserializeBinaryFromReader);
      msg.setTestAction(value);
      break;
    case 173:
      var value = new proto.failure_details.Worker;
      reader.readMessage(value,proto.failure_details.Worker.deserializeBinaryFromReader);
      msg.setWorker(value);
      break;
    case 174:
      var value = new proto.failure_details.Analysis;
      reader.readMessage(value,proto.failure_details.Analysis.deserializeBinaryFromReader);
      msg.setAnalysis(value);
      break;
    case 175:
      var value = new proto.failure_details.PackageLoading;
      reader.readMessage(value,proto.failure_details.PackageLoading.deserializeBinaryFromReader);
      msg.setPackageLoading(value);
      break;
    case 177:
      var value = new proto.failure_details.Toolchain;
      reader.readMessage(value,proto.failure_details.Toolchain.deserializeBinaryFromReader);
      msg.setToolchain(value);
      break;
    case 179:
      var value = new proto.failure_details.StarlarkLoading;
      reader.readMessage(value,proto.failure_details.StarlarkLoading.deserializeBinaryFromReader);
      msg.setStarlarkLoading(value);
      break;
    case 181:
      var value = new proto.failure_details.ExternalDeps;
      reader.readMessage(value,proto.failure_details.ExternalDeps.deserializeBinaryFromReader);
      msg.setExternalDeps(value);
      break;
    case 182:
      var value = new proto.failure_details.DiffAwareness;
      reader.readMessage(value,proto.failure_details.DiffAwareness.deserializeBinaryFromReader);
      msg.setDiffAwareness(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.FailureDetail.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.FailureDetail.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.FailureDetail} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.FailureDetail.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getInterrupted();
  if (f != null) {
    writer.writeMessage(
      101,
      f,
      proto.failure_details.Interrupted.serializeBinaryToWriter
    );
  }
  f = message.getExternalRepository();
  if (f != null) {
    writer.writeMessage(
      103,
      f,
      proto.failure_details.ExternalRepository.serializeBinaryToWriter
    );
  }
  f = message.getBuildProgress();
  if (f != null) {
    writer.writeMessage(
      104,
      f,
      proto.failure_details.BuildProgress.serializeBinaryToWriter
    );
  }
  f = message.getRemoteOptions();
  if (f != null) {
    writer.writeMessage(
      106,
      f,
      proto.failure_details.RemoteOptions.serializeBinaryToWriter
    );
  }
  f = message.getClientEnvironment();
  if (f != null) {
    writer.writeMessage(
      107,
      f,
      proto.failure_details.ClientEnvironment.serializeBinaryToWriter
    );
  }
  f = message.getCrash();
  if (f != null) {
    writer.writeMessage(
      108,
      f,
      proto.failure_details.Crash.serializeBinaryToWriter
    );
  }
  f = message.getSymlinkForest();
  if (f != null) {
    writer.writeMessage(
      110,
      f,
      proto.failure_details.SymlinkForest.serializeBinaryToWriter
    );
  }
  f = message.getPackageOptions();
  if (f != null) {
    writer.writeMessage(
      114,
      f,
      proto.failure_details.PackageOptions.serializeBinaryToWriter
    );
  }
  f = message.getRemoteExecution();
  if (f != null) {
    writer.writeMessage(
      115,
      f,
      proto.failure_details.RemoteExecution.serializeBinaryToWriter
    );
  }
  f = message.getExecution();
  if (f != null) {
    writer.writeMessage(
      116,
      f,
      proto.failure_details.Execution.serializeBinaryToWriter
    );
  }
  f = message.getWorkspaces();
  if (f != null) {
    writer.writeMessage(
      117,
      f,
      proto.failure_details.Workspaces.serializeBinaryToWriter
    );
  }
  f = message.getCrashOptions();
  if (f != null) {
    writer.writeMessage(
      118,
      f,
      proto.failure_details.CrashOptions.serializeBinaryToWriter
    );
  }
  f = message.getFilesystem();
  if (f != null) {
    writer.writeMessage(
      119,
      f,
      proto.failure_details.Filesystem.serializeBinaryToWriter
    );
  }
  f = message.getExecutionOptions();
  if (f != null) {
    writer.writeMessage(
      121,
      f,
      proto.failure_details.ExecutionOptions.serializeBinaryToWriter
    );
  }
  f = message.getCommand();
  if (f != null) {
    writer.writeMessage(
      122,
      f,
      proto.failure_details.Command.serializeBinaryToWriter
    );
  }
  f = message.getSpawn();
  if (f != null) {
    writer.writeMessage(
      123,
      f,
      proto.failure_details.Spawn.serializeBinaryToWriter
    );
  }
  f = message.getGrpcServer();
  if (f != null) {
    writer.writeMessage(
      124,
      f,
      proto.failure_details.GrpcServer.serializeBinaryToWriter
    );
  }
  f = message.getCanonicalizeFlags();
  if (f != null) {
    writer.writeMessage(
      125,
      f,
      proto.failure_details.CanonicalizeFlags.serializeBinaryToWriter
    );
  }
  f = message.getBuildConfiguration();
  if (f != null) {
    writer.writeMessage(
      126,
      f,
      proto.failure_details.BuildConfiguration.serializeBinaryToWriter
    );
  }
  f = message.getInfoCommand();
  if (f != null) {
    writer.writeMessage(
      127,
      f,
      proto.failure_details.InfoCommand.serializeBinaryToWriter
    );
  }
  f = message.getMemoryOptions();
  if (f != null) {
    writer.writeMessage(
      129,
      f,
      proto.failure_details.MemoryOptions.serializeBinaryToWriter
    );
  }
  f = message.getQuery();
  if (f != null) {
    writer.writeMessage(
      130,
      f,
      proto.failure_details.Query.serializeBinaryToWriter
    );
  }
  f = message.getLocalExecution();
  if (f != null) {
    writer.writeMessage(
      132,
      f,
      proto.failure_details.LocalExecution.serializeBinaryToWriter
    );
  }
  f = message.getActionCache();
  if (f != null) {
    writer.writeMessage(
      134,
      f,
      proto.failure_details.ActionCache.serializeBinaryToWriter
    );
  }
  f = message.getFetchCommand();
  if (f != null) {
    writer.writeMessage(
      135,
      f,
      proto.failure_details.FetchCommand.serializeBinaryToWriter
    );
  }
  f = message.getSyncCommand();
  if (f != null) {
    writer.writeMessage(
      136,
      f,
      proto.failure_details.SyncCommand.serializeBinaryToWriter
    );
  }
  f = message.getSandbox();
  if (f != null) {
    writer.writeMessage(
      137,
      f,
      proto.failure_details.Sandbox.serializeBinaryToWriter
    );
  }
  f = message.getIncludeScanning();
  if (f != null) {
    writer.writeMessage(
      139,
      f,
      proto.failure_details.IncludeScanning.serializeBinaryToWriter
    );
  }
  f = message.getTestCommand();
  if (f != null) {
    writer.writeMessage(
      140,
      f,
      proto.failure_details.TestCommand.serializeBinaryToWriter
    );
  }
  f = message.getActionQuery();
  if (f != null) {
    writer.writeMessage(
      141,
      f,
      proto.failure_details.ActionQuery.serializeBinaryToWriter
    );
  }
  f = message.getTargetPatterns();
  if (f != null) {
    writer.writeMessage(
      142,
      f,
      proto.failure_details.TargetPatterns.serializeBinaryToWriter
    );
  }
  f = message.getCleanCommand();
  if (f != null) {
    writer.writeMessage(
      144,
      f,
      proto.failure_details.CleanCommand.serializeBinaryToWriter
    );
  }
  f = message.getConfigCommand();
  if (f != null) {
    writer.writeMessage(
      145,
      f,
      proto.failure_details.ConfigCommand.serializeBinaryToWriter
    );
  }
  f = message.getConfigurableQuery();
  if (f != null) {
    writer.writeMessage(
      146,
      f,
      proto.failure_details.ConfigurableQuery.serializeBinaryToWriter
    );
  }
  f = message.getDumpCommand();
  if (f != null) {
    writer.writeMessage(
      147,
      f,
      proto.failure_details.DumpCommand.serializeBinaryToWriter
    );
  }
  f = message.getHelpCommand();
  if (f != null) {
    writer.writeMessage(
      148,
      f,
      proto.failure_details.HelpCommand.serializeBinaryToWriter
    );
  }
  f = message.getMobileInstall();
  if (f != null) {
    writer.writeMessage(
      150,
      f,
      proto.failure_details.MobileInstall.serializeBinaryToWriter
    );
  }
  f = message.getProfileCommand();
  if (f != null) {
    writer.writeMessage(
      151,
      f,
      proto.failure_details.ProfileCommand.serializeBinaryToWriter
    );
  }
  f = message.getRunCommand();
  if (f != null) {
    writer.writeMessage(
      152,
      f,
      proto.failure_details.RunCommand.serializeBinaryToWriter
    );
  }
  f = message.getVersionCommand();
  if (f != null) {
    writer.writeMessage(
      153,
      f,
      proto.failure_details.VersionCommand.serializeBinaryToWriter
    );
  }
  f = message.getPrintActionCommand();
  if (f != null) {
    writer.writeMessage(
      154,
      f,
      proto.failure_details.PrintActionCommand.serializeBinaryToWriter
    );
  }
  f = message.getWorkspaceStatus();
  if (f != null) {
    writer.writeMessage(
      158,
      f,
      proto.failure_details.WorkspaceStatus.serializeBinaryToWriter
    );
  }
  f = message.getJavaCompile();
  if (f != null) {
    writer.writeMessage(
      159,
      f,
      proto.failure_details.JavaCompile.serializeBinaryToWriter
    );
  }
  f = message.getActionRewinding();
  if (f != null) {
    writer.writeMessage(
      160,
      f,
      proto.failure_details.ActionRewinding.serializeBinaryToWriter
    );
  }
  f = message.getCppCompile();
  if (f != null) {
    writer.writeMessage(
      161,
      f,
      proto.failure_details.CppCompile.serializeBinaryToWriter
    );
  }
  f = message.getStarlarkAction();
  if (f != null) {
    writer.writeMessage(
      162,
      f,
      proto.failure_details.StarlarkAction.serializeBinaryToWriter
    );
  }
  f = message.getNinjaAction();
  if (f != null) {
    writer.writeMessage(
      163,
      f,
      proto.failure_details.NinjaAction.serializeBinaryToWriter
    );
  }
  f = message.getDynamicExecution();
  if (f != null) {
    writer.writeMessage(
      164,
      f,
      proto.failure_details.DynamicExecution.serializeBinaryToWriter
    );
  }
  f = message.getFailAction();
  if (f != null) {
    writer.writeMessage(
      166,
      f,
      proto.failure_details.FailAction.serializeBinaryToWriter
    );
  }
  f = message.getSymlinkAction();
  if (f != null) {
    writer.writeMessage(
      167,
      f,
      proto.failure_details.SymlinkAction.serializeBinaryToWriter
    );
  }
  f = message.getCppLink();
  if (f != null) {
    writer.writeMessage(
      168,
      f,
      proto.failure_details.CppLink.serializeBinaryToWriter
    );
  }
  f = message.getLtoAction();
  if (f != null) {
    writer.writeMessage(
      169,
      f,
      proto.failure_details.LtoAction.serializeBinaryToWriter
    );
  }
  f = message.getTestAction();
  if (f != null) {
    writer.writeMessage(
      172,
      f,
      proto.failure_details.TestAction.serializeBinaryToWriter
    );
  }
  f = message.getWorker();
  if (f != null) {
    writer.writeMessage(
      173,
      f,
      proto.failure_details.Worker.serializeBinaryToWriter
    );
  }
  f = message.getAnalysis();
  if (f != null) {
    writer.writeMessage(
      174,
      f,
      proto.failure_details.Analysis.serializeBinaryToWriter
    );
  }
  f = message.getPackageLoading();
  if (f != null) {
    writer.writeMessage(
      175,
      f,
      proto.failure_details.PackageLoading.serializeBinaryToWriter
    );
  }
  f = message.getToolchain();
  if (f != null) {
    writer.writeMessage(
      177,
      f,
      proto.failure_details.Toolchain.serializeBinaryToWriter
    );
  }
  f = message.getStarlarkLoading();
  if (f != null) {
    writer.writeMessage(
      179,
      f,
      proto.failure_details.StarlarkLoading.serializeBinaryToWriter
    );
  }
  f = message.getExternalDeps();
  if (f != null) {
    writer.writeMessage(
      181,
      f,
      proto.failure_details.ExternalDeps.serializeBinaryToWriter
    );
  }
  f = message.getDiffAwareness();
  if (f != null) {
    writer.writeMessage(
      182,
      f,
      proto.failure_details.DiffAwareness.serializeBinaryToWriter
    );
  }
};


/**
 * optional string message = 1;
 * @return {string}
 */
proto.failure_details.FailureDetail.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional Interrupted interrupted = 101;
 * @return {?proto.failure_details.Interrupted}
 */
proto.failure_details.FailureDetail.prototype.getInterrupted = function() {
  return /** @type{?proto.failure_details.Interrupted} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Interrupted, 101));
};


/**
 * @param {?proto.failure_details.Interrupted|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setInterrupted = function(value) {
  return jspb.Message.setOneofWrapperField(this, 101, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearInterrupted = function() {
  return this.setInterrupted(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasInterrupted = function() {
  return jspb.Message.getField(this, 101) != null;
};


/**
 * optional ExternalRepository external_repository = 103;
 * @return {?proto.failure_details.ExternalRepository}
 */
proto.failure_details.FailureDetail.prototype.getExternalRepository = function() {
  return /** @type{?proto.failure_details.ExternalRepository} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ExternalRepository, 103));
};


/**
 * @param {?proto.failure_details.ExternalRepository|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setExternalRepository = function(value) {
  return jspb.Message.setOneofWrapperField(this, 103, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearExternalRepository = function() {
  return this.setExternalRepository(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasExternalRepository = function() {
  return jspb.Message.getField(this, 103) != null;
};


/**
 * optional BuildProgress build_progress = 104;
 * @return {?proto.failure_details.BuildProgress}
 */
proto.failure_details.FailureDetail.prototype.getBuildProgress = function() {
  return /** @type{?proto.failure_details.BuildProgress} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.BuildProgress, 104));
};


/**
 * @param {?proto.failure_details.BuildProgress|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setBuildProgress = function(value) {
  return jspb.Message.setOneofWrapperField(this, 104, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearBuildProgress = function() {
  return this.setBuildProgress(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasBuildProgress = function() {
  return jspb.Message.getField(this, 104) != null;
};


/**
 * optional RemoteOptions remote_options = 106;
 * @return {?proto.failure_details.RemoteOptions}
 */
proto.failure_details.FailureDetail.prototype.getRemoteOptions = function() {
  return /** @type{?proto.failure_details.RemoteOptions} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.RemoteOptions, 106));
};


/**
 * @param {?proto.failure_details.RemoteOptions|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setRemoteOptions = function(value) {
  return jspb.Message.setOneofWrapperField(this, 106, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearRemoteOptions = function() {
  return this.setRemoteOptions(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasRemoteOptions = function() {
  return jspb.Message.getField(this, 106) != null;
};


/**
 * optional ClientEnvironment client_environment = 107;
 * @return {?proto.failure_details.ClientEnvironment}
 */
proto.failure_details.FailureDetail.prototype.getClientEnvironment = function() {
  return /** @type{?proto.failure_details.ClientEnvironment} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ClientEnvironment, 107));
};


/**
 * @param {?proto.failure_details.ClientEnvironment|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setClientEnvironment = function(value) {
  return jspb.Message.setOneofWrapperField(this, 107, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearClientEnvironment = function() {
  return this.setClientEnvironment(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasClientEnvironment = function() {
  return jspb.Message.getField(this, 107) != null;
};


/**
 * optional Crash crash = 108;
 * @return {?proto.failure_details.Crash}
 */
proto.failure_details.FailureDetail.prototype.getCrash = function() {
  return /** @type{?proto.failure_details.Crash} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Crash, 108));
};


/**
 * @param {?proto.failure_details.Crash|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setCrash = function(value) {
  return jspb.Message.setOneofWrapperField(this, 108, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearCrash = function() {
  return this.setCrash(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasCrash = function() {
  return jspb.Message.getField(this, 108) != null;
};


/**
 * optional SymlinkForest symlink_forest = 110;
 * @return {?proto.failure_details.SymlinkForest}
 */
proto.failure_details.FailureDetail.prototype.getSymlinkForest = function() {
  return /** @type{?proto.failure_details.SymlinkForest} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.SymlinkForest, 110));
};


/**
 * @param {?proto.failure_details.SymlinkForest|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setSymlinkForest = function(value) {
  return jspb.Message.setOneofWrapperField(this, 110, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearSymlinkForest = function() {
  return this.setSymlinkForest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasSymlinkForest = function() {
  return jspb.Message.getField(this, 110) != null;
};


/**
 * optional PackageOptions package_options = 114;
 * @return {?proto.failure_details.PackageOptions}
 */
proto.failure_details.FailureDetail.prototype.getPackageOptions = function() {
  return /** @type{?proto.failure_details.PackageOptions} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.PackageOptions, 114));
};


/**
 * @param {?proto.failure_details.PackageOptions|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setPackageOptions = function(value) {
  return jspb.Message.setOneofWrapperField(this, 114, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearPackageOptions = function() {
  return this.setPackageOptions(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasPackageOptions = function() {
  return jspb.Message.getField(this, 114) != null;
};


/**
 * optional RemoteExecution remote_execution = 115;
 * @return {?proto.failure_details.RemoteExecution}
 */
proto.failure_details.FailureDetail.prototype.getRemoteExecution = function() {
  return /** @type{?proto.failure_details.RemoteExecution} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.RemoteExecution, 115));
};


/**
 * @param {?proto.failure_details.RemoteExecution|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setRemoteExecution = function(value) {
  return jspb.Message.setOneofWrapperField(this, 115, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearRemoteExecution = function() {
  return this.setRemoteExecution(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasRemoteExecution = function() {
  return jspb.Message.getField(this, 115) != null;
};


/**
 * optional Execution execution = 116;
 * @return {?proto.failure_details.Execution}
 */
proto.failure_details.FailureDetail.prototype.getExecution = function() {
  return /** @type{?proto.failure_details.Execution} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Execution, 116));
};


/**
 * @param {?proto.failure_details.Execution|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setExecution = function(value) {
  return jspb.Message.setOneofWrapperField(this, 116, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearExecution = function() {
  return this.setExecution(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasExecution = function() {
  return jspb.Message.getField(this, 116) != null;
};


/**
 * optional Workspaces workspaces = 117;
 * @return {?proto.failure_details.Workspaces}
 */
proto.failure_details.FailureDetail.prototype.getWorkspaces = function() {
  return /** @type{?proto.failure_details.Workspaces} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Workspaces, 117));
};


/**
 * @param {?proto.failure_details.Workspaces|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setWorkspaces = function(value) {
  return jspb.Message.setOneofWrapperField(this, 117, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearWorkspaces = function() {
  return this.setWorkspaces(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasWorkspaces = function() {
  return jspb.Message.getField(this, 117) != null;
};


/**
 * optional CrashOptions crash_options = 118;
 * @return {?proto.failure_details.CrashOptions}
 */
proto.failure_details.FailureDetail.prototype.getCrashOptions = function() {
  return /** @type{?proto.failure_details.CrashOptions} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.CrashOptions, 118));
};


/**
 * @param {?proto.failure_details.CrashOptions|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setCrashOptions = function(value) {
  return jspb.Message.setOneofWrapperField(this, 118, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearCrashOptions = function() {
  return this.setCrashOptions(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasCrashOptions = function() {
  return jspb.Message.getField(this, 118) != null;
};


/**
 * optional Filesystem filesystem = 119;
 * @return {?proto.failure_details.Filesystem}
 */
proto.failure_details.FailureDetail.prototype.getFilesystem = function() {
  return /** @type{?proto.failure_details.Filesystem} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Filesystem, 119));
};


/**
 * @param {?proto.failure_details.Filesystem|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setFilesystem = function(value) {
  return jspb.Message.setOneofWrapperField(this, 119, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearFilesystem = function() {
  return this.setFilesystem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasFilesystem = function() {
  return jspb.Message.getField(this, 119) != null;
};


/**
 * optional ExecutionOptions execution_options = 121;
 * @return {?proto.failure_details.ExecutionOptions}
 */
proto.failure_details.FailureDetail.prototype.getExecutionOptions = function() {
  return /** @type{?proto.failure_details.ExecutionOptions} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ExecutionOptions, 121));
};


/**
 * @param {?proto.failure_details.ExecutionOptions|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setExecutionOptions = function(value) {
  return jspb.Message.setOneofWrapperField(this, 121, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearExecutionOptions = function() {
  return this.setExecutionOptions(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasExecutionOptions = function() {
  return jspb.Message.getField(this, 121) != null;
};


/**
 * optional Command command = 122;
 * @return {?proto.failure_details.Command}
 */
proto.failure_details.FailureDetail.prototype.getCommand = function() {
  return /** @type{?proto.failure_details.Command} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Command, 122));
};


/**
 * @param {?proto.failure_details.Command|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 122, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearCommand = function() {
  return this.setCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasCommand = function() {
  return jspb.Message.getField(this, 122) != null;
};


/**
 * optional Spawn spawn = 123;
 * @return {?proto.failure_details.Spawn}
 */
proto.failure_details.FailureDetail.prototype.getSpawn = function() {
  return /** @type{?proto.failure_details.Spawn} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Spawn, 123));
};


/**
 * @param {?proto.failure_details.Spawn|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setSpawn = function(value) {
  return jspb.Message.setOneofWrapperField(this, 123, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearSpawn = function() {
  return this.setSpawn(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasSpawn = function() {
  return jspb.Message.getField(this, 123) != null;
};


/**
 * optional GrpcServer grpc_server = 124;
 * @return {?proto.failure_details.GrpcServer}
 */
proto.failure_details.FailureDetail.prototype.getGrpcServer = function() {
  return /** @type{?proto.failure_details.GrpcServer} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.GrpcServer, 124));
};


/**
 * @param {?proto.failure_details.GrpcServer|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setGrpcServer = function(value) {
  return jspb.Message.setOneofWrapperField(this, 124, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearGrpcServer = function() {
  return this.setGrpcServer(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasGrpcServer = function() {
  return jspb.Message.getField(this, 124) != null;
};


/**
 * optional CanonicalizeFlags canonicalize_flags = 125;
 * @return {?proto.failure_details.CanonicalizeFlags}
 */
proto.failure_details.FailureDetail.prototype.getCanonicalizeFlags = function() {
  return /** @type{?proto.failure_details.CanonicalizeFlags} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.CanonicalizeFlags, 125));
};


/**
 * @param {?proto.failure_details.CanonicalizeFlags|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setCanonicalizeFlags = function(value) {
  return jspb.Message.setOneofWrapperField(this, 125, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearCanonicalizeFlags = function() {
  return this.setCanonicalizeFlags(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasCanonicalizeFlags = function() {
  return jspb.Message.getField(this, 125) != null;
};


/**
 * optional BuildConfiguration build_configuration = 126;
 * @return {?proto.failure_details.BuildConfiguration}
 */
proto.failure_details.FailureDetail.prototype.getBuildConfiguration = function() {
  return /** @type{?proto.failure_details.BuildConfiguration} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.BuildConfiguration, 126));
};


/**
 * @param {?proto.failure_details.BuildConfiguration|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setBuildConfiguration = function(value) {
  return jspb.Message.setOneofWrapperField(this, 126, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearBuildConfiguration = function() {
  return this.setBuildConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasBuildConfiguration = function() {
  return jspb.Message.getField(this, 126) != null;
};


/**
 * optional InfoCommand info_command = 127;
 * @return {?proto.failure_details.InfoCommand}
 */
proto.failure_details.FailureDetail.prototype.getInfoCommand = function() {
  return /** @type{?proto.failure_details.InfoCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.InfoCommand, 127));
};


/**
 * @param {?proto.failure_details.InfoCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setInfoCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 127, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearInfoCommand = function() {
  return this.setInfoCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasInfoCommand = function() {
  return jspb.Message.getField(this, 127) != null;
};


/**
 * optional MemoryOptions memory_options = 129;
 * @return {?proto.failure_details.MemoryOptions}
 */
proto.failure_details.FailureDetail.prototype.getMemoryOptions = function() {
  return /** @type{?proto.failure_details.MemoryOptions} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.MemoryOptions, 129));
};


/**
 * @param {?proto.failure_details.MemoryOptions|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setMemoryOptions = function(value) {
  return jspb.Message.setOneofWrapperField(this, 129, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearMemoryOptions = function() {
  return this.setMemoryOptions(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasMemoryOptions = function() {
  return jspb.Message.getField(this, 129) != null;
};


/**
 * optional Query query = 130;
 * @return {?proto.failure_details.Query}
 */
proto.failure_details.FailureDetail.prototype.getQuery = function() {
  return /** @type{?proto.failure_details.Query} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Query, 130));
};


/**
 * @param {?proto.failure_details.Query|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setQuery = function(value) {
  return jspb.Message.setOneofWrapperField(this, 130, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearQuery = function() {
  return this.setQuery(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasQuery = function() {
  return jspb.Message.getField(this, 130) != null;
};


/**
 * optional LocalExecution local_execution = 132;
 * @return {?proto.failure_details.LocalExecution}
 */
proto.failure_details.FailureDetail.prototype.getLocalExecution = function() {
  return /** @type{?proto.failure_details.LocalExecution} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.LocalExecution, 132));
};


/**
 * @param {?proto.failure_details.LocalExecution|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setLocalExecution = function(value) {
  return jspb.Message.setOneofWrapperField(this, 132, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearLocalExecution = function() {
  return this.setLocalExecution(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasLocalExecution = function() {
  return jspb.Message.getField(this, 132) != null;
};


/**
 * optional ActionCache action_cache = 134;
 * @return {?proto.failure_details.ActionCache}
 */
proto.failure_details.FailureDetail.prototype.getActionCache = function() {
  return /** @type{?proto.failure_details.ActionCache} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ActionCache, 134));
};


/**
 * @param {?proto.failure_details.ActionCache|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setActionCache = function(value) {
  return jspb.Message.setOneofWrapperField(this, 134, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearActionCache = function() {
  return this.setActionCache(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasActionCache = function() {
  return jspb.Message.getField(this, 134) != null;
};


/**
 * optional FetchCommand fetch_command = 135;
 * @return {?proto.failure_details.FetchCommand}
 */
proto.failure_details.FailureDetail.prototype.getFetchCommand = function() {
  return /** @type{?proto.failure_details.FetchCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.FetchCommand, 135));
};


/**
 * @param {?proto.failure_details.FetchCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setFetchCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 135, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearFetchCommand = function() {
  return this.setFetchCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasFetchCommand = function() {
  return jspb.Message.getField(this, 135) != null;
};


/**
 * optional SyncCommand sync_command = 136;
 * @return {?proto.failure_details.SyncCommand}
 */
proto.failure_details.FailureDetail.prototype.getSyncCommand = function() {
  return /** @type{?proto.failure_details.SyncCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.SyncCommand, 136));
};


/**
 * @param {?proto.failure_details.SyncCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setSyncCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 136, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearSyncCommand = function() {
  return this.setSyncCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasSyncCommand = function() {
  return jspb.Message.getField(this, 136) != null;
};


/**
 * optional Sandbox sandbox = 137;
 * @return {?proto.failure_details.Sandbox}
 */
proto.failure_details.FailureDetail.prototype.getSandbox = function() {
  return /** @type{?proto.failure_details.Sandbox} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Sandbox, 137));
};


/**
 * @param {?proto.failure_details.Sandbox|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setSandbox = function(value) {
  return jspb.Message.setOneofWrapperField(this, 137, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearSandbox = function() {
  return this.setSandbox(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasSandbox = function() {
  return jspb.Message.getField(this, 137) != null;
};


/**
 * optional IncludeScanning include_scanning = 139;
 * @return {?proto.failure_details.IncludeScanning}
 */
proto.failure_details.FailureDetail.prototype.getIncludeScanning = function() {
  return /** @type{?proto.failure_details.IncludeScanning} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.IncludeScanning, 139));
};


/**
 * @param {?proto.failure_details.IncludeScanning|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setIncludeScanning = function(value) {
  return jspb.Message.setOneofWrapperField(this, 139, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearIncludeScanning = function() {
  return this.setIncludeScanning(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasIncludeScanning = function() {
  return jspb.Message.getField(this, 139) != null;
};


/**
 * optional TestCommand test_command = 140;
 * @return {?proto.failure_details.TestCommand}
 */
proto.failure_details.FailureDetail.prototype.getTestCommand = function() {
  return /** @type{?proto.failure_details.TestCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.TestCommand, 140));
};


/**
 * @param {?proto.failure_details.TestCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setTestCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 140, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearTestCommand = function() {
  return this.setTestCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasTestCommand = function() {
  return jspb.Message.getField(this, 140) != null;
};


/**
 * optional ActionQuery action_query = 141;
 * @return {?proto.failure_details.ActionQuery}
 */
proto.failure_details.FailureDetail.prototype.getActionQuery = function() {
  return /** @type{?proto.failure_details.ActionQuery} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ActionQuery, 141));
};


/**
 * @param {?proto.failure_details.ActionQuery|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setActionQuery = function(value) {
  return jspb.Message.setOneofWrapperField(this, 141, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearActionQuery = function() {
  return this.setActionQuery(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasActionQuery = function() {
  return jspb.Message.getField(this, 141) != null;
};


/**
 * optional TargetPatterns target_patterns = 142;
 * @return {?proto.failure_details.TargetPatterns}
 */
proto.failure_details.FailureDetail.prototype.getTargetPatterns = function() {
  return /** @type{?proto.failure_details.TargetPatterns} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.TargetPatterns, 142));
};


/**
 * @param {?proto.failure_details.TargetPatterns|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setTargetPatterns = function(value) {
  return jspb.Message.setOneofWrapperField(this, 142, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearTargetPatterns = function() {
  return this.setTargetPatterns(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasTargetPatterns = function() {
  return jspb.Message.getField(this, 142) != null;
};


/**
 * optional CleanCommand clean_command = 144;
 * @return {?proto.failure_details.CleanCommand}
 */
proto.failure_details.FailureDetail.prototype.getCleanCommand = function() {
  return /** @type{?proto.failure_details.CleanCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.CleanCommand, 144));
};


/**
 * @param {?proto.failure_details.CleanCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setCleanCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 144, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearCleanCommand = function() {
  return this.setCleanCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasCleanCommand = function() {
  return jspb.Message.getField(this, 144) != null;
};


/**
 * optional ConfigCommand config_command = 145;
 * @return {?proto.failure_details.ConfigCommand}
 */
proto.failure_details.FailureDetail.prototype.getConfigCommand = function() {
  return /** @type{?proto.failure_details.ConfigCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ConfigCommand, 145));
};


/**
 * @param {?proto.failure_details.ConfigCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setConfigCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 145, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearConfigCommand = function() {
  return this.setConfigCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasConfigCommand = function() {
  return jspb.Message.getField(this, 145) != null;
};


/**
 * optional ConfigurableQuery configurable_query = 146;
 * @return {?proto.failure_details.ConfigurableQuery}
 */
proto.failure_details.FailureDetail.prototype.getConfigurableQuery = function() {
  return /** @type{?proto.failure_details.ConfigurableQuery} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ConfigurableQuery, 146));
};


/**
 * @param {?proto.failure_details.ConfigurableQuery|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setConfigurableQuery = function(value) {
  return jspb.Message.setOneofWrapperField(this, 146, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearConfigurableQuery = function() {
  return this.setConfigurableQuery(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasConfigurableQuery = function() {
  return jspb.Message.getField(this, 146) != null;
};


/**
 * optional DumpCommand dump_command = 147;
 * @return {?proto.failure_details.DumpCommand}
 */
proto.failure_details.FailureDetail.prototype.getDumpCommand = function() {
  return /** @type{?proto.failure_details.DumpCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.DumpCommand, 147));
};


/**
 * @param {?proto.failure_details.DumpCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setDumpCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 147, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearDumpCommand = function() {
  return this.setDumpCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasDumpCommand = function() {
  return jspb.Message.getField(this, 147) != null;
};


/**
 * optional HelpCommand help_command = 148;
 * @return {?proto.failure_details.HelpCommand}
 */
proto.failure_details.FailureDetail.prototype.getHelpCommand = function() {
  return /** @type{?proto.failure_details.HelpCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.HelpCommand, 148));
};


/**
 * @param {?proto.failure_details.HelpCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setHelpCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 148, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearHelpCommand = function() {
  return this.setHelpCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasHelpCommand = function() {
  return jspb.Message.getField(this, 148) != null;
};


/**
 * optional MobileInstall mobile_install = 150;
 * @return {?proto.failure_details.MobileInstall}
 */
proto.failure_details.FailureDetail.prototype.getMobileInstall = function() {
  return /** @type{?proto.failure_details.MobileInstall} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.MobileInstall, 150));
};


/**
 * @param {?proto.failure_details.MobileInstall|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setMobileInstall = function(value) {
  return jspb.Message.setOneofWrapperField(this, 150, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearMobileInstall = function() {
  return this.setMobileInstall(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasMobileInstall = function() {
  return jspb.Message.getField(this, 150) != null;
};


/**
 * optional ProfileCommand profile_command = 151;
 * @return {?proto.failure_details.ProfileCommand}
 */
proto.failure_details.FailureDetail.prototype.getProfileCommand = function() {
  return /** @type{?proto.failure_details.ProfileCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ProfileCommand, 151));
};


/**
 * @param {?proto.failure_details.ProfileCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setProfileCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 151, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearProfileCommand = function() {
  return this.setProfileCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasProfileCommand = function() {
  return jspb.Message.getField(this, 151) != null;
};


/**
 * optional RunCommand run_command = 152;
 * @return {?proto.failure_details.RunCommand}
 */
proto.failure_details.FailureDetail.prototype.getRunCommand = function() {
  return /** @type{?proto.failure_details.RunCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.RunCommand, 152));
};


/**
 * @param {?proto.failure_details.RunCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setRunCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 152, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearRunCommand = function() {
  return this.setRunCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasRunCommand = function() {
  return jspb.Message.getField(this, 152) != null;
};


/**
 * optional VersionCommand version_command = 153;
 * @return {?proto.failure_details.VersionCommand}
 */
proto.failure_details.FailureDetail.prototype.getVersionCommand = function() {
  return /** @type{?proto.failure_details.VersionCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.VersionCommand, 153));
};


/**
 * @param {?proto.failure_details.VersionCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setVersionCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 153, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearVersionCommand = function() {
  return this.setVersionCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasVersionCommand = function() {
  return jspb.Message.getField(this, 153) != null;
};


/**
 * optional PrintActionCommand print_action_command = 154;
 * @return {?proto.failure_details.PrintActionCommand}
 */
proto.failure_details.FailureDetail.prototype.getPrintActionCommand = function() {
  return /** @type{?proto.failure_details.PrintActionCommand} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.PrintActionCommand, 154));
};


/**
 * @param {?proto.failure_details.PrintActionCommand|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setPrintActionCommand = function(value) {
  return jspb.Message.setOneofWrapperField(this, 154, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearPrintActionCommand = function() {
  return this.setPrintActionCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasPrintActionCommand = function() {
  return jspb.Message.getField(this, 154) != null;
};


/**
 * optional WorkspaceStatus workspace_status = 158;
 * @return {?proto.failure_details.WorkspaceStatus}
 */
proto.failure_details.FailureDetail.prototype.getWorkspaceStatus = function() {
  return /** @type{?proto.failure_details.WorkspaceStatus} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.WorkspaceStatus, 158));
};


/**
 * @param {?proto.failure_details.WorkspaceStatus|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setWorkspaceStatus = function(value) {
  return jspb.Message.setOneofWrapperField(this, 158, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearWorkspaceStatus = function() {
  return this.setWorkspaceStatus(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasWorkspaceStatus = function() {
  return jspb.Message.getField(this, 158) != null;
};


/**
 * optional JavaCompile java_compile = 159;
 * @return {?proto.failure_details.JavaCompile}
 */
proto.failure_details.FailureDetail.prototype.getJavaCompile = function() {
  return /** @type{?proto.failure_details.JavaCompile} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.JavaCompile, 159));
};


/**
 * @param {?proto.failure_details.JavaCompile|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setJavaCompile = function(value) {
  return jspb.Message.setOneofWrapperField(this, 159, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearJavaCompile = function() {
  return this.setJavaCompile(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasJavaCompile = function() {
  return jspb.Message.getField(this, 159) != null;
};


/**
 * optional ActionRewinding action_rewinding = 160;
 * @return {?proto.failure_details.ActionRewinding}
 */
proto.failure_details.FailureDetail.prototype.getActionRewinding = function() {
  return /** @type{?proto.failure_details.ActionRewinding} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ActionRewinding, 160));
};


/**
 * @param {?proto.failure_details.ActionRewinding|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setActionRewinding = function(value) {
  return jspb.Message.setOneofWrapperField(this, 160, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearActionRewinding = function() {
  return this.setActionRewinding(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasActionRewinding = function() {
  return jspb.Message.getField(this, 160) != null;
};


/**
 * optional CppCompile cpp_compile = 161;
 * @return {?proto.failure_details.CppCompile}
 */
proto.failure_details.FailureDetail.prototype.getCppCompile = function() {
  return /** @type{?proto.failure_details.CppCompile} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.CppCompile, 161));
};


/**
 * @param {?proto.failure_details.CppCompile|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setCppCompile = function(value) {
  return jspb.Message.setOneofWrapperField(this, 161, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearCppCompile = function() {
  return this.setCppCompile(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasCppCompile = function() {
  return jspb.Message.getField(this, 161) != null;
};


/**
 * optional StarlarkAction starlark_action = 162;
 * @return {?proto.failure_details.StarlarkAction}
 */
proto.failure_details.FailureDetail.prototype.getStarlarkAction = function() {
  return /** @type{?proto.failure_details.StarlarkAction} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.StarlarkAction, 162));
};


/**
 * @param {?proto.failure_details.StarlarkAction|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setStarlarkAction = function(value) {
  return jspb.Message.setOneofWrapperField(this, 162, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearStarlarkAction = function() {
  return this.setStarlarkAction(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasStarlarkAction = function() {
  return jspb.Message.getField(this, 162) != null;
};


/**
 * optional NinjaAction ninja_action = 163;
 * @return {?proto.failure_details.NinjaAction}
 */
proto.failure_details.FailureDetail.prototype.getNinjaAction = function() {
  return /** @type{?proto.failure_details.NinjaAction} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.NinjaAction, 163));
};


/**
 * @param {?proto.failure_details.NinjaAction|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setNinjaAction = function(value) {
  return jspb.Message.setOneofWrapperField(this, 163, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearNinjaAction = function() {
  return this.setNinjaAction(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasNinjaAction = function() {
  return jspb.Message.getField(this, 163) != null;
};


/**
 * optional DynamicExecution dynamic_execution = 164;
 * @return {?proto.failure_details.DynamicExecution}
 */
proto.failure_details.FailureDetail.prototype.getDynamicExecution = function() {
  return /** @type{?proto.failure_details.DynamicExecution} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.DynamicExecution, 164));
};


/**
 * @param {?proto.failure_details.DynamicExecution|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setDynamicExecution = function(value) {
  return jspb.Message.setOneofWrapperField(this, 164, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearDynamicExecution = function() {
  return this.setDynamicExecution(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasDynamicExecution = function() {
  return jspb.Message.getField(this, 164) != null;
};


/**
 * optional FailAction fail_action = 166;
 * @return {?proto.failure_details.FailAction}
 */
proto.failure_details.FailureDetail.prototype.getFailAction = function() {
  return /** @type{?proto.failure_details.FailAction} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.FailAction, 166));
};


/**
 * @param {?proto.failure_details.FailAction|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setFailAction = function(value) {
  return jspb.Message.setOneofWrapperField(this, 166, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearFailAction = function() {
  return this.setFailAction(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasFailAction = function() {
  return jspb.Message.getField(this, 166) != null;
};


/**
 * optional SymlinkAction symlink_action = 167;
 * @return {?proto.failure_details.SymlinkAction}
 */
proto.failure_details.FailureDetail.prototype.getSymlinkAction = function() {
  return /** @type{?proto.failure_details.SymlinkAction} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.SymlinkAction, 167));
};


/**
 * @param {?proto.failure_details.SymlinkAction|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setSymlinkAction = function(value) {
  return jspb.Message.setOneofWrapperField(this, 167, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearSymlinkAction = function() {
  return this.setSymlinkAction(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasSymlinkAction = function() {
  return jspb.Message.getField(this, 167) != null;
};


/**
 * optional CppLink cpp_link = 168;
 * @return {?proto.failure_details.CppLink}
 */
proto.failure_details.FailureDetail.prototype.getCppLink = function() {
  return /** @type{?proto.failure_details.CppLink} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.CppLink, 168));
};


/**
 * @param {?proto.failure_details.CppLink|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setCppLink = function(value) {
  return jspb.Message.setOneofWrapperField(this, 168, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearCppLink = function() {
  return this.setCppLink(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasCppLink = function() {
  return jspb.Message.getField(this, 168) != null;
};


/**
 * optional LtoAction lto_action = 169;
 * @return {?proto.failure_details.LtoAction}
 */
proto.failure_details.FailureDetail.prototype.getLtoAction = function() {
  return /** @type{?proto.failure_details.LtoAction} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.LtoAction, 169));
};


/**
 * @param {?proto.failure_details.LtoAction|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setLtoAction = function(value) {
  return jspb.Message.setOneofWrapperField(this, 169, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearLtoAction = function() {
  return this.setLtoAction(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasLtoAction = function() {
  return jspb.Message.getField(this, 169) != null;
};


/**
 * optional TestAction test_action = 172;
 * @return {?proto.failure_details.TestAction}
 */
proto.failure_details.FailureDetail.prototype.getTestAction = function() {
  return /** @type{?proto.failure_details.TestAction} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.TestAction, 172));
};


/**
 * @param {?proto.failure_details.TestAction|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setTestAction = function(value) {
  return jspb.Message.setOneofWrapperField(this, 172, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearTestAction = function() {
  return this.setTestAction(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasTestAction = function() {
  return jspb.Message.getField(this, 172) != null;
};


/**
 * optional Worker worker = 173;
 * @return {?proto.failure_details.Worker}
 */
proto.failure_details.FailureDetail.prototype.getWorker = function() {
  return /** @type{?proto.failure_details.Worker} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Worker, 173));
};


/**
 * @param {?proto.failure_details.Worker|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setWorker = function(value) {
  return jspb.Message.setOneofWrapperField(this, 173, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearWorker = function() {
  return this.setWorker(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasWorker = function() {
  return jspb.Message.getField(this, 173) != null;
};


/**
 * optional Analysis analysis = 174;
 * @return {?proto.failure_details.Analysis}
 */
proto.failure_details.FailureDetail.prototype.getAnalysis = function() {
  return /** @type{?proto.failure_details.Analysis} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Analysis, 174));
};


/**
 * @param {?proto.failure_details.Analysis|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setAnalysis = function(value) {
  return jspb.Message.setOneofWrapperField(this, 174, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearAnalysis = function() {
  return this.setAnalysis(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasAnalysis = function() {
  return jspb.Message.getField(this, 174) != null;
};


/**
 * optional PackageLoading package_loading = 175;
 * @return {?proto.failure_details.PackageLoading}
 */
proto.failure_details.FailureDetail.prototype.getPackageLoading = function() {
  return /** @type{?proto.failure_details.PackageLoading} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.PackageLoading, 175));
};


/**
 * @param {?proto.failure_details.PackageLoading|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setPackageLoading = function(value) {
  return jspb.Message.setOneofWrapperField(this, 175, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearPackageLoading = function() {
  return this.setPackageLoading(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasPackageLoading = function() {
  return jspb.Message.getField(this, 175) != null;
};


/**
 * optional Toolchain toolchain = 177;
 * @return {?proto.failure_details.Toolchain}
 */
proto.failure_details.FailureDetail.prototype.getToolchain = function() {
  return /** @type{?proto.failure_details.Toolchain} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.Toolchain, 177));
};


/**
 * @param {?proto.failure_details.Toolchain|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setToolchain = function(value) {
  return jspb.Message.setOneofWrapperField(this, 177, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearToolchain = function() {
  return this.setToolchain(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasToolchain = function() {
  return jspb.Message.getField(this, 177) != null;
};


/**
 * optional StarlarkLoading starlark_loading = 179;
 * @return {?proto.failure_details.StarlarkLoading}
 */
proto.failure_details.FailureDetail.prototype.getStarlarkLoading = function() {
  return /** @type{?proto.failure_details.StarlarkLoading} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.StarlarkLoading, 179));
};


/**
 * @param {?proto.failure_details.StarlarkLoading|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setStarlarkLoading = function(value) {
  return jspb.Message.setOneofWrapperField(this, 179, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearStarlarkLoading = function() {
  return this.setStarlarkLoading(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasStarlarkLoading = function() {
  return jspb.Message.getField(this, 179) != null;
};


/**
 * optional ExternalDeps external_deps = 181;
 * @return {?proto.failure_details.ExternalDeps}
 */
proto.failure_details.FailureDetail.prototype.getExternalDeps = function() {
  return /** @type{?proto.failure_details.ExternalDeps} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.ExternalDeps, 181));
};


/**
 * @param {?proto.failure_details.ExternalDeps|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setExternalDeps = function(value) {
  return jspb.Message.setOneofWrapperField(this, 181, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearExternalDeps = function() {
  return this.setExternalDeps(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasExternalDeps = function() {
  return jspb.Message.getField(this, 181) != null;
};


/**
 * optional DiffAwareness diff_awareness = 182;
 * @return {?proto.failure_details.DiffAwareness}
 */
proto.failure_details.FailureDetail.prototype.getDiffAwareness = function() {
  return /** @type{?proto.failure_details.DiffAwareness} */ (
    jspb.Message.getWrapperField(this, proto.failure_details.DiffAwareness, 182));
};


/**
 * @param {?proto.failure_details.DiffAwareness|undefined} value
 * @return {!proto.failure_details.FailureDetail} returns this
*/
proto.failure_details.FailureDetail.prototype.setDiffAwareness = function(value) {
  return jspb.Message.setOneofWrapperField(this, 182, proto.failure_details.FailureDetail.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.failure_details.FailureDetail} returns this
 */
proto.failure_details.FailureDetail.prototype.clearDiffAwareness = function() {
  return this.setDiffAwareness(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.failure_details.FailureDetail.prototype.hasDiffAwareness = function() {
  return jspb.Message.getField(this, 182) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Interrupted.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Interrupted.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Interrupted} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Interrupted.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Interrupted}
 */
proto.failure_details.Interrupted.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Interrupted;
  return proto.failure_details.Interrupted.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Interrupted} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Interrupted}
 */
proto.failure_details.Interrupted.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Interrupted.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Interrupted.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Interrupted.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Interrupted} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Interrupted.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Interrupted.Code = {
  INTERRUPTED_UNKNOWN: 0,
  INTERRUPTED: 28,
  DEPRECATED_BUILD: 4,
  DEPRECATED_BUILD_COMPLETION: 5,
  DEPRECATED_PACKAGE_LOADING_SYNC: 6,
  DEPRECATED_EXECUTOR_COMPLETION: 7,
  DEPRECATED_COMMAND_DISPATCH: 8,
  DEPRECATED_INFO_ITEM: 9,
  DEPRECATED_AFTER_QUERY: 10,
  DEPRECATED_FETCH_COMMAND: 17,
  DEPRECATED_SYNC_COMMAND: 18,
  DEPRECATED_CLEAN_COMMAND: 20,
  DEPRECATED_MOBILE_INSTALL_COMMAND: 21,
  DEPRECATED_QUERY: 22,
  DEPRECATED_RUN_COMMAND: 23,
  DEPRECATED_OPTIONS_PARSING: 27
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Interrupted.Code}
 */
proto.failure_details.Interrupted.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Interrupted.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Interrupted.Code} value
 * @return {!proto.failure_details.Interrupted} returns this
 */
proto.failure_details.Interrupted.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Spawn.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Spawn.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Spawn} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Spawn.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0),
    catastrophic: jspb.Message.getBooleanFieldWithDefault(msg, 2, false),
    spawnExitCode: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Spawn}
 */
proto.failure_details.Spawn.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Spawn;
  return proto.failure_details.Spawn.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Spawn} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Spawn}
 */
proto.failure_details.Spawn.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Spawn.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCatastrophic(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSpawnExitCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Spawn.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Spawn.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Spawn} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Spawn.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getCatastrophic();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
  f = message.getSpawnExitCode();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Spawn.Code = {
  SPAWN_UNKNOWN: 0,
  NON_ZERO_EXIT: 1,
  TIMEOUT: 2,
  OUT_OF_MEMORY: 3,
  EXECUTION_FAILED: 4,
  EXECUTION_DENIED: 5,
  REMOTE_CACHE_FAILED: 6,
  COMMAND_LINE_EXPANSION_FAILURE: 7,
  EXEC_IO_EXCEPTION: 8,
  INVALID_TIMEOUT: 9,
  INVALID_REMOTE_EXECUTION_PROPERTIES: 10,
  NO_USABLE_STRATEGY_FOUND: 11,
  UNSPECIFIED_EXECUTION_FAILURE: 12,
  FORBIDDEN_INPUT: 13
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Spawn.Code}
 */
proto.failure_details.Spawn.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Spawn.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Spawn.Code} value
 * @return {!proto.failure_details.Spawn} returns this
 */
proto.failure_details.Spawn.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional bool catastrophic = 2;
 * @return {boolean}
 */
proto.failure_details.Spawn.prototype.getCatastrophic = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.failure_details.Spawn} returns this
 */
proto.failure_details.Spawn.prototype.setCatastrophic = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};


/**
 * optional int32 spawn_exit_code = 3;
 * @return {number}
 */
proto.failure_details.Spawn.prototype.getSpawnExitCode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.failure_details.Spawn} returns this
 */
proto.failure_details.Spawn.prototype.setSpawnExitCode = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ExternalRepository.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ExternalRepository.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ExternalRepository} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ExternalRepository.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ExternalRepository}
 */
proto.failure_details.ExternalRepository.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ExternalRepository;
  return proto.failure_details.ExternalRepository.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ExternalRepository} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ExternalRepository}
 */
proto.failure_details.ExternalRepository.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ExternalRepository.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ExternalRepository.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ExternalRepository.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ExternalRepository} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ExternalRepository.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ExternalRepository.Code = {
  EXTERNAL_REPOSITORY_UNKNOWN: 0,
  OVERRIDE_DISALLOWED_MANAGED_DIRECTORIES: 1,
  BAD_DOWNLOADER_CONFIG: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ExternalRepository.Code}
 */
proto.failure_details.ExternalRepository.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ExternalRepository.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ExternalRepository.Code} value
 * @return {!proto.failure_details.ExternalRepository} returns this
 */
proto.failure_details.ExternalRepository.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.BuildProgress.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.BuildProgress.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.BuildProgress} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.BuildProgress.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.BuildProgress}
 */
proto.failure_details.BuildProgress.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.BuildProgress;
  return proto.failure_details.BuildProgress.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.BuildProgress} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.BuildProgress}
 */
proto.failure_details.BuildProgress.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.BuildProgress.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.BuildProgress.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.BuildProgress.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.BuildProgress} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.BuildProgress.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.BuildProgress.Code = {
  BUILD_PROGRESS_UNKNOWN: 0,
  OUTPUT_INITIALIZATION: 3,
  BES_RUNS_PER_TEST_LIMIT_UNSUPPORTED: 4,
  BES_LOCAL_WRITE_ERROR: 5,
  BES_INITIALIZATION_ERROR: 6,
  BES_UPLOAD_TIMEOUT_ERROR: 7,
  BES_FILE_WRITE_TIMEOUT: 8,
  BES_FILE_WRITE_IO_ERROR: 9,
  BES_FILE_WRITE_INTERRUPTED: 10,
  BES_FILE_WRITE_CANCELED: 11,
  BES_FILE_WRITE_UNKNOWN_ERROR: 12,
  BES_UPLOAD_LOCAL_FILE_ERROR: 13,
  BES_STREAM_NOT_RETRYING_FAILURE: 14,
  BES_STREAM_COMPLETED_WITH_UNACK_EVENTS_ERROR: 15,
  BES_STREAM_COMPLETED_WITH_UNSENT_EVENTS_ERROR: 16,
  BES_STREAM_COMPLETED_WITH_REMOTE_ERROR: 19,
  BES_UPLOAD_RETRY_LIMIT_EXCEEDED_FAILURE: 17
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.BuildProgress.Code}
 */
proto.failure_details.BuildProgress.prototype.getCode = function() {
  return /** @type {!proto.failure_details.BuildProgress.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.BuildProgress.Code} value
 * @return {!proto.failure_details.BuildProgress} returns this
 */
proto.failure_details.BuildProgress.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.RemoteOptions.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.RemoteOptions.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.RemoteOptions} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.RemoteOptions.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.RemoteOptions}
 */
proto.failure_details.RemoteOptions.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.RemoteOptions;
  return proto.failure_details.RemoteOptions.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.RemoteOptions} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.RemoteOptions}
 */
proto.failure_details.RemoteOptions.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.RemoteOptions.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.RemoteOptions.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.RemoteOptions.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.RemoteOptions} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.RemoteOptions.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.RemoteOptions.Code = {
  REMOTE_OPTIONS_UNKNOWN: 0,
  REMOTE_DEFAULT_EXEC_PROPERTIES_LOGIC_ERROR: 1,
  CREDENTIALS_READ_FAILURE: 2,
  CREDENTIALS_WRITE_FAILURE: 3,
  DOWNLOADER_WITHOUT_GRPC_CACHE: 4,
  EXECUTION_WITH_INVALID_CACHE: 5
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.RemoteOptions.Code}
 */
proto.failure_details.RemoteOptions.prototype.getCode = function() {
  return /** @type {!proto.failure_details.RemoteOptions.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.RemoteOptions.Code} value
 * @return {!proto.failure_details.RemoteOptions} returns this
 */
proto.failure_details.RemoteOptions.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ClientEnvironment.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ClientEnvironment.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ClientEnvironment} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ClientEnvironment.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ClientEnvironment}
 */
proto.failure_details.ClientEnvironment.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ClientEnvironment;
  return proto.failure_details.ClientEnvironment.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ClientEnvironment} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ClientEnvironment}
 */
proto.failure_details.ClientEnvironment.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ClientEnvironment.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ClientEnvironment.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ClientEnvironment.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ClientEnvironment} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ClientEnvironment.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ClientEnvironment.Code = {
  CLIENT_ENVIRONMENT_UNKNOWN: 0,
  CLIENT_CWD_MALFORMED: 1
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ClientEnvironment.Code}
 */
proto.failure_details.ClientEnvironment.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ClientEnvironment.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ClientEnvironment.Code} value
 * @return {!proto.failure_details.ClientEnvironment} returns this
 */
proto.failure_details.ClientEnvironment.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.failure_details.Crash.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Crash.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Crash.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Crash} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Crash.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0),
    causesList: jspb.Message.toObjectList(msg.getCausesList(),
    proto.failure_details.Throwable.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Crash}
 */
proto.failure_details.Crash.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Crash;
  return proto.failure_details.Crash.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Crash} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Crash}
 */
proto.failure_details.Crash.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Crash.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    case 2:
      var value = new proto.failure_details.Throwable;
      reader.readMessage(value,proto.failure_details.Throwable.deserializeBinaryFromReader);
      msg.addCauses(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Crash.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Crash.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Crash} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Crash.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getCausesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.failure_details.Throwable.serializeBinaryToWriter
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Crash.Code = {
  CRASH_UNKNOWN: 0,
  CRASH_OOM: 1
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Crash.Code}
 */
proto.failure_details.Crash.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Crash.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Crash.Code} value
 * @return {!proto.failure_details.Crash} returns this
 */
proto.failure_details.Crash.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * repeated Throwable causes = 2;
 * @return {!Array<!proto.failure_details.Throwable>}
 */
proto.failure_details.Crash.prototype.getCausesList = function() {
  return /** @type{!Array<!proto.failure_details.Throwable>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.failure_details.Throwable, 2));
};


/**
 * @param {!Array<!proto.failure_details.Throwable>} value
 * @return {!proto.failure_details.Crash} returns this
*/
proto.failure_details.Crash.prototype.setCausesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.failure_details.Throwable=} opt_value
 * @param {number=} opt_index
 * @return {!proto.failure_details.Throwable}
 */
proto.failure_details.Crash.prototype.addCauses = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.failure_details.Throwable, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.failure_details.Crash} returns this
 */
proto.failure_details.Crash.prototype.clearCausesList = function() {
  return this.setCausesList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.failure_details.Throwable.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Throwable.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Throwable.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Throwable} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Throwable.toObject = function(includeInstance, msg) {
  var f, obj = {
    throwableClass: jspb.Message.getFieldWithDefault(msg, 1, ""),
    message: jspb.Message.getFieldWithDefault(msg, 2, ""),
    stackTraceList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Throwable}
 */
proto.failure_details.Throwable.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Throwable;
  return proto.failure_details.Throwable.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Throwable} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Throwable}
 */
proto.failure_details.Throwable.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setThrowableClass(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addStackTrace(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Throwable.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Throwable.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Throwable} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Throwable.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getThrowableClass();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getStackTraceList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
};


/**
 * optional string throwable_class = 1;
 * @return {string}
 */
proto.failure_details.Throwable.prototype.getThrowableClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.failure_details.Throwable} returns this
 */
proto.failure_details.Throwable.prototype.setThrowableClass = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string message = 2;
 * @return {string}
 */
proto.failure_details.Throwable.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.failure_details.Throwable} returns this
 */
proto.failure_details.Throwable.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * repeated string stack_trace = 3;
 * @return {!Array<string>}
 */
proto.failure_details.Throwable.prototype.getStackTraceList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.failure_details.Throwable} returns this
 */
proto.failure_details.Throwable.prototype.setStackTraceList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.failure_details.Throwable} returns this
 */
proto.failure_details.Throwable.prototype.addStackTrace = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.failure_details.Throwable} returns this
 */
proto.failure_details.Throwable.prototype.clearStackTraceList = function() {
  return this.setStackTraceList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.SymlinkForest.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.SymlinkForest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.SymlinkForest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.SymlinkForest.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.SymlinkForest}
 */
proto.failure_details.SymlinkForest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.SymlinkForest;
  return proto.failure_details.SymlinkForest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.SymlinkForest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.SymlinkForest}
 */
proto.failure_details.SymlinkForest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.SymlinkForest.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.SymlinkForest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.SymlinkForest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.SymlinkForest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.SymlinkForest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.SymlinkForest.Code = {
  SYMLINK_FOREST_UNKNOWN: 0,
  TOPLEVEL_OUTDIR_PACKAGE_PATH_CONFLICT: 1,
  TOPLEVEL_OUTDIR_USED_AS_SOURCE: 2,
  CREATION_FAILED: 3
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.SymlinkForest.Code}
 */
proto.failure_details.SymlinkForest.prototype.getCode = function() {
  return /** @type {!proto.failure_details.SymlinkForest.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.SymlinkForest.Code} value
 * @return {!proto.failure_details.SymlinkForest} returns this
 */
proto.failure_details.SymlinkForest.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.PackageOptions.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.PackageOptions.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.PackageOptions} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.PackageOptions.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.PackageOptions}
 */
proto.failure_details.PackageOptions.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.PackageOptions;
  return proto.failure_details.PackageOptions.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.PackageOptions} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.PackageOptions}
 */
proto.failure_details.PackageOptions.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.PackageOptions.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.PackageOptions.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.PackageOptions.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.PackageOptions} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.PackageOptions.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.PackageOptions.Code = {
  PACKAGE_OPTIONS_UNKNOWN: 0,
  PACKAGE_PATH_INVALID: 1
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.PackageOptions.Code}
 */
proto.failure_details.PackageOptions.prototype.getCode = function() {
  return /** @type {!proto.failure_details.PackageOptions.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.PackageOptions.Code} value
 * @return {!proto.failure_details.PackageOptions} returns this
 */
proto.failure_details.PackageOptions.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.RemoteExecution.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.RemoteExecution.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.RemoteExecution} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.RemoteExecution.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.RemoteExecution}
 */
proto.failure_details.RemoteExecution.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.RemoteExecution;
  return proto.failure_details.RemoteExecution.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.RemoteExecution} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.RemoteExecution}
 */
proto.failure_details.RemoteExecution.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.RemoteExecution.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.RemoteExecution.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.RemoteExecution.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.RemoteExecution} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.RemoteExecution.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.RemoteExecution.Code = {
  REMOTE_EXECUTION_UNKNOWN: 0,
  CAPABILITIES_QUERY_FAILURE: 1,
  CREDENTIALS_INIT_FAILURE: 2,
  CACHE_INIT_FAILURE: 3,
  RPC_LOG_FAILURE: 4,
  EXEC_CHANNEL_INIT_FAILURE: 5,
  CACHE_CHANNEL_INIT_FAILURE: 6,
  DOWNLOADER_CHANNEL_INIT_FAILURE: 7,
  LOG_DIR_CLEANUP_FAILURE: 8,
  CLIENT_SERVER_INCOMPATIBLE: 9,
  DOWNLOADED_INPUTS_DELETION_FAILURE: 10,
  REMOTE_DOWNLOAD_OUTPUTS_MINIMAL_WITHOUT_INMEMORY_DOTD: 11,
  REMOTE_DOWNLOAD_OUTPUTS_MINIMAL_WITHOUT_INMEMORY_JDEPS: 12,
  INCOMPLETE_OUTPUT_DOWNLOAD_CLEANUP_FAILURE: 13,
  REMOTE_DEFAULT_PLATFORM_PROPERTIES_PARSE_FAILURE: 14,
  ILLEGAL_OUTPUT: 15,
  INVALID_EXEC_AND_PLATFORM_PROPERTIES: 16
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.RemoteExecution.Code}
 */
proto.failure_details.RemoteExecution.prototype.getCode = function() {
  return /** @type {!proto.failure_details.RemoteExecution.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.RemoteExecution.Code} value
 * @return {!proto.failure_details.RemoteExecution} returns this
 */
proto.failure_details.RemoteExecution.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Execution.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Execution.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Execution} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Execution.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Execution}
 */
proto.failure_details.Execution.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Execution;
  return proto.failure_details.Execution.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Execution} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Execution}
 */
proto.failure_details.Execution.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Execution.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Execution.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Execution.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Execution} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Execution.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Execution.Code = {
  EXECUTION_UNKNOWN: 0,
  EXECUTION_LOG_INITIALIZATION_FAILURE: 1,
  EXECUTION_LOG_WRITE_FAILURE: 2,
  EXECROOT_CREATION_FAILURE: 3,
  TEMP_ACTION_OUTPUT_DIRECTORY_DELETION_FAILURE: 4,
  TEMP_ACTION_OUTPUT_DIRECTORY_CREATION_FAILURE: 5,
  PERSISTENT_ACTION_OUTPUT_DIRECTORY_CREATION_FAILURE: 6,
  LOCAL_OUTPUT_DIRECTORY_SYMLINK_FAILURE: 7,
  LOCAL_TEMPLATE_EXPANSION_FAILURE: 9,
  INPUT_DIRECTORY_CHECK_IO_EXCEPTION: 10,
  EXTRA_ACTION_OUTPUT_CREATION_FAILURE: 11,
  TEST_RUNNER_IO_EXCEPTION: 12,
  FILE_WRITE_IO_EXCEPTION: 13,
  TEST_OUT_ERR_IO_EXCEPTION: 14,
  SYMLINK_TREE_MANIFEST_COPY_IO_EXCEPTION: 15,
  SYMLINK_TREE_MANIFEST_LINK_IO_EXCEPTION: 16,
  SYMLINK_TREE_CREATION_IO_EXCEPTION: 17,
  SYMLINK_TREE_CREATION_COMMAND_EXCEPTION: 18,
  ACTION_INPUT_READ_IO_EXCEPTION: 19,
  ACTION_NOT_UP_TO_DATE: 20,
  PSEUDO_ACTION_EXECUTION_PROHIBITED: 21,
  DISCOVERED_INPUT_DOES_NOT_EXIST: 22,
  ACTION_OUTPUTS_DELETION_FAILURE: 23,
  ACTION_OUTPUTS_NOT_CREATED: 24,
  ACTION_FINALIZATION_FAILURE: 25,
  ACTION_INPUT_LOST: 26,
  FILESYSTEM_CONTEXT_UPDATE_FAILURE: 27,
  ACTION_OUTPUT_CLOSE_FAILURE: 28,
  INPUT_DISCOVERY_IO_EXCEPTION: 29,
  TREE_ARTIFACT_DIRECTORY_CREATION_FAILURE: 30,
  ACTION_OUTPUT_DIRECTORY_CREATION_FAILURE: 31,
  ACTION_FS_OUTPUT_DIRECTORY_CREATION_FAILURE: 32,
  ACTION_FS_OUT_ERR_DIRECTORY_CREATION_FAILURE: 33,
  NON_ACTION_EXECUTION_FAILURE: 34,
  CYCLE: 35,
  SOURCE_INPUT_MISSING: 36,
  UNEXPECTED_EXCEPTION: 37,
  SOURCE_INPUT_IO_EXCEPTION: 39
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Execution.Code}
 */
proto.failure_details.Execution.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Execution.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Execution.Code} value
 * @return {!proto.failure_details.Execution} returns this
 */
proto.failure_details.Execution.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Workspaces.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Workspaces.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Workspaces} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Workspaces.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Workspaces}
 */
proto.failure_details.Workspaces.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Workspaces;
  return proto.failure_details.Workspaces.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Workspaces} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Workspaces}
 */
proto.failure_details.Workspaces.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Workspaces.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Workspaces.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Workspaces.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Workspaces} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Workspaces.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Workspaces.Code = {
  WORKSPACES_UNKNOWN: 0,
  WORKSPACES_LOG_INITIALIZATION_FAILURE: 1,
  WORKSPACES_LOG_WRITE_FAILURE: 2,
  ILLEGAL_WORKSPACE_FILE_SYMLINK_WITH_MANAGED_DIRECTORIES: 3,
  WORKSPACE_FILE_READ_FAILURE_WITH_MANAGED_DIRECTORIES: 4
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Workspaces.Code}
 */
proto.failure_details.Workspaces.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Workspaces.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Workspaces.Code} value
 * @return {!proto.failure_details.Workspaces} returns this
 */
proto.failure_details.Workspaces.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.CrashOptions.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.CrashOptions.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.CrashOptions} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CrashOptions.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.CrashOptions}
 */
proto.failure_details.CrashOptions.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.CrashOptions;
  return proto.failure_details.CrashOptions.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.CrashOptions} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.CrashOptions}
 */
proto.failure_details.CrashOptions.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.CrashOptions.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.CrashOptions.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.CrashOptions.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.CrashOptions} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CrashOptions.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.CrashOptions.Code = {
  CRASH_OPTIONS_UNKNOWN: 0
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.CrashOptions.Code}
 */
proto.failure_details.CrashOptions.prototype.getCode = function() {
  return /** @type {!proto.failure_details.CrashOptions.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.CrashOptions.Code} value
 * @return {!proto.failure_details.CrashOptions} returns this
 */
proto.failure_details.CrashOptions.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Filesystem.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Filesystem.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Filesystem} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Filesystem.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Filesystem}
 */
proto.failure_details.Filesystem.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Filesystem;
  return proto.failure_details.Filesystem.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Filesystem} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Filesystem}
 */
proto.failure_details.Filesystem.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Filesystem.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Filesystem.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Filesystem.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Filesystem} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Filesystem.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Filesystem.Code = {
  FILESYSTEM_UNKNOWN: 0,
  EMBEDDED_BINARIES_ENUMERATION_FAILURE: 3,
  SERVER_PID_TXT_FILE_READ_FAILURE: 4,
  SERVER_FILE_WRITE_FAILURE: 5,
  DEFAULT_DIGEST_HASH_FUNCTION_INVALID_VALUE: 6
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Filesystem.Code}
 */
proto.failure_details.Filesystem.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Filesystem.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Filesystem.Code} value
 * @return {!proto.failure_details.Filesystem} returns this
 */
proto.failure_details.Filesystem.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ExecutionOptions.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ExecutionOptions.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ExecutionOptions} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ExecutionOptions.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ExecutionOptions}
 */
proto.failure_details.ExecutionOptions.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ExecutionOptions;
  return proto.failure_details.ExecutionOptions.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ExecutionOptions} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ExecutionOptions}
 */
proto.failure_details.ExecutionOptions.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ExecutionOptions.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ExecutionOptions.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ExecutionOptions.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ExecutionOptions} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ExecutionOptions.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ExecutionOptions.Code = {
  EXECUTION_OPTIONS_UNKNOWN: 0,
  INVALID_STRATEGY: 3,
  REQUESTED_STRATEGY_INCOMPATIBLE_WITH_SANDBOXING: 4,
  DEPRECATED_LOCAL_RESOURCES_USED: 5,
  INVALID_CYCLIC_DYNAMIC_STRATEGY: 6,
  RESTRICTION_UNMATCHED_TO_ACTION_CONTEXT: 7,
  REMOTE_FALLBACK_STRATEGY_NOT_ABSTRACT_SPAWN: 8,
  STRATEGY_NOT_FOUND: 9,
  DYNAMIC_STRATEGY_NOT_SANDBOXED: 10
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ExecutionOptions.Code}
 */
proto.failure_details.ExecutionOptions.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ExecutionOptions.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ExecutionOptions.Code} value
 * @return {!proto.failure_details.ExecutionOptions} returns this
 */
proto.failure_details.ExecutionOptions.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Command.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Command.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Command} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Command.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Command}
 */
proto.failure_details.Command.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Command;
  return proto.failure_details.Command.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Command} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Command}
 */
proto.failure_details.Command.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Command.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Command.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Command.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Command} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Command.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Command.Code = {
  COMMAND_FAILURE_UNKNOWN: 0,
  COMMAND_NOT_FOUND: 1,
  ANOTHER_COMMAND_RUNNING: 2,
  PREVIOUSLY_SHUTDOWN: 3,
  STARLARK_CPU_PROFILE_FILE_INITIALIZATION_FAILURE: 4,
  STARLARK_CPU_PROFILING_INITIALIZATION_FAILURE: 5,
  STARLARK_CPU_PROFILE_FILE_WRITE_FAILURE: 6,
  INVOCATION_POLICY_PARSE_FAILURE: 7,
  INVOCATION_POLICY_INVALID: 8,
  OPTIONS_PARSE_FAILURE: 9,
  STARLARK_OPTIONS_PARSE_FAILURE: 10,
  ARGUMENTS_NOT_RECOGNIZED: 11,
  NOT_IN_WORKSPACE: 12,
  SPACES_IN_WORKSPACE_PATH: 13,
  IN_OUTPUT_DIRECTORY: 14
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Command.Code}
 */
proto.failure_details.Command.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Command.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Command.Code} value
 * @return {!proto.failure_details.Command} returns this
 */
proto.failure_details.Command.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.GrpcServer.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.GrpcServer.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.GrpcServer} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.GrpcServer.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.GrpcServer}
 */
proto.failure_details.GrpcServer.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.GrpcServer;
  return proto.failure_details.GrpcServer.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.GrpcServer} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.GrpcServer}
 */
proto.failure_details.GrpcServer.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.GrpcServer.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.GrpcServer.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.GrpcServer.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.GrpcServer} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.GrpcServer.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.GrpcServer.Code = {
  GRPC_SERVER_UNKNOWN: 0,
  GRPC_SERVER_NOT_COMPILED_IN: 1,
  SERVER_BIND_FAILURE: 2,
  BAD_COOKIE: 3,
  NO_CLIENT_DESCRIPTION: 4
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.GrpcServer.Code}
 */
proto.failure_details.GrpcServer.prototype.getCode = function() {
  return /** @type {!proto.failure_details.GrpcServer.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.GrpcServer.Code} value
 * @return {!proto.failure_details.GrpcServer} returns this
 */
proto.failure_details.GrpcServer.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.CanonicalizeFlags.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.CanonicalizeFlags.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.CanonicalizeFlags} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CanonicalizeFlags.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.CanonicalizeFlags}
 */
proto.failure_details.CanonicalizeFlags.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.CanonicalizeFlags;
  return proto.failure_details.CanonicalizeFlags.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.CanonicalizeFlags} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.CanonicalizeFlags}
 */
proto.failure_details.CanonicalizeFlags.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.CanonicalizeFlags.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.CanonicalizeFlags.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.CanonicalizeFlags.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.CanonicalizeFlags} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CanonicalizeFlags.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.CanonicalizeFlags.Code = {
  CANONICALIZE_FLAGS_UNKNOWN: 0,
  FOR_COMMAND_INVALID: 1
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.CanonicalizeFlags.Code}
 */
proto.failure_details.CanonicalizeFlags.prototype.getCode = function() {
  return /** @type {!proto.failure_details.CanonicalizeFlags.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.CanonicalizeFlags.Code} value
 * @return {!proto.failure_details.CanonicalizeFlags} returns this
 */
proto.failure_details.CanonicalizeFlags.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.BuildConfiguration.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.BuildConfiguration.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.BuildConfiguration} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.BuildConfiguration.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.BuildConfiguration}
 */
proto.failure_details.BuildConfiguration.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.BuildConfiguration;
  return proto.failure_details.BuildConfiguration.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.BuildConfiguration} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.BuildConfiguration}
 */
proto.failure_details.BuildConfiguration.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.BuildConfiguration.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.BuildConfiguration.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.BuildConfiguration.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.BuildConfiguration} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.BuildConfiguration.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.BuildConfiguration.Code = {
  BUILD_CONFIGURATION_UNKNOWN: 0,
  PLATFORM_MAPPING_EVALUATION_FAILURE: 1,
  PLATFORM_MAPPINGS_FILE_IS_DIRECTORY: 2,
  PLATFORM_MAPPINGS_FILE_NOT_FOUND: 3,
  TOP_LEVEL_CONFIGURATION_CREATION_FAILURE: 4,
  INVALID_CONFIGURATION: 5,
  INVALID_BUILD_OPTIONS: 6,
  MULTI_CPU_PREREQ_UNMET: 7,
  HEURISTIC_INSTRUMENTATION_FILTER_INVALID: 8,
  CYCLE: 9,
  CONFLICTING_CONFIGURATIONS: 10,
  INVALID_OUTPUT_DIRECTORY_MNEMONIC: 11
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.BuildConfiguration.Code}
 */
proto.failure_details.BuildConfiguration.prototype.getCode = function() {
  return /** @type {!proto.failure_details.BuildConfiguration.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.BuildConfiguration.Code} value
 * @return {!proto.failure_details.BuildConfiguration} returns this
 */
proto.failure_details.BuildConfiguration.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.InfoCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.InfoCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.InfoCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.InfoCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.InfoCommand}
 */
proto.failure_details.InfoCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.InfoCommand;
  return proto.failure_details.InfoCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.InfoCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.InfoCommand}
 */
proto.failure_details.InfoCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.InfoCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.InfoCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.InfoCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.InfoCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.InfoCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.InfoCommand.Code = {
  INFO_COMMAND_UNKNOWN: 0,
  TOO_MANY_KEYS: 1,
  KEY_NOT_RECOGNIZED: 2,
  INFO_BLOCK_WRITE_FAILURE: 3,
  ALL_INFO_WRITE_FAILURE: 4
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.InfoCommand.Code}
 */
proto.failure_details.InfoCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.InfoCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.InfoCommand.Code} value
 * @return {!proto.failure_details.InfoCommand} returns this
 */
proto.failure_details.InfoCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.MemoryOptions.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.MemoryOptions.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.MemoryOptions} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.MemoryOptions.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.MemoryOptions}
 */
proto.failure_details.MemoryOptions.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.MemoryOptions;
  return proto.failure_details.MemoryOptions.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.MemoryOptions} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.MemoryOptions}
 */
proto.failure_details.MemoryOptions.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.MemoryOptions.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.MemoryOptions.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.MemoryOptions.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.MemoryOptions} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.MemoryOptions.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.MemoryOptions.Code = {
  MEMORY_OPTIONS_UNKNOWN: 0,
  EXPERIMENTAL_OOM_MORE_EAGERLY_THRESHOLD_INVALID_VALUE: 1,
  EXPERIMENTAL_OOM_MORE_EAGERLY_NO_TENURED_COLLECTORS_FOUND: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.MemoryOptions.Code}
 */
proto.failure_details.MemoryOptions.prototype.getCode = function() {
  return /** @type {!proto.failure_details.MemoryOptions.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.MemoryOptions.Code} value
 * @return {!proto.failure_details.MemoryOptions} returns this
 */
proto.failure_details.MemoryOptions.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Query.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Query.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Query} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Query.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Query}
 */
proto.failure_details.Query.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Query;
  return proto.failure_details.Query.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Query} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Query}
 */
proto.failure_details.Query.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Query.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Query.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Query.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Query} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Query.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Query.Code = {
  QUERY_UNKNOWN: 0,
  QUERY_FILE_WITH_COMMAND_LINE_EXPRESSION: 1,
  QUERY_FILE_READ_FAILURE: 2,
  COMMAND_LINE_EXPRESSION_MISSING: 3,
  OUTPUT_FORMAT_INVALID: 4,
  GRAPHLESS_PREREQ_UNMET: 5,
  QUERY_OUTPUT_WRITE_FAILURE: 6,
  QUERY_STDOUT_FLUSH_FAILURE: 13,
  ANALYSIS_QUERY_PREREQ_UNMET: 14,
  QUERY_RESULTS_FLUSH_FAILURE: 15,
  DEPRECATED_UNCLOSED_QUOTATION_EXPRESSION_ERROR: 16,
  VARIABLE_NAME_INVALID: 17,
  VARIABLE_UNDEFINED: 18,
  BUILDFILES_AND_LOADFILES_CANNOT_USE_OUTPUT_LOCATION_ERROR: 19,
  BUILD_FILE_ERROR: 20,
  CYCLE: 21,
  UNIQUE_SKYKEY_THRESHOLD_EXCEEDED: 22,
  TARGET_NOT_IN_UNIVERSE_SCOPE: 23,
  INVALID_FULL_UNIVERSE_EXPRESSION: 24,
  UNIVERSE_SCOPE_LIMIT_EXCEEDED: 25,
  INVALIDATION_LIMIT_EXCEEDED: 26,
  OUTPUT_FORMAT_PREREQ_UNMET: 27,
  ARGUMENTS_MISSING: 28,
  RBUILDFILES_FUNCTION_REQUIRES_SKYQUERY: 29,
  FULL_TARGETS_NOT_SUPPORTED: 30,
  DEPRECATED_UNEXPECTED_TOKEN_ERROR: 31,
  DEPRECATED_INTEGER_LITERAL_MISSING: 32,
  DEPRECATED_INVALID_STARTING_CHARACTER_ERROR: 33,
  DEPRECATED_PREMATURE_END_OF_INPUT_ERROR: 34,
  SYNTAX_ERROR: 35,
  OUTPUT_FORMATTER_IO_EXCEPTION: 36,
  SKYQUERY_TRANSITIVE_TARGET_ERROR: 37,
  SKYQUERY_TARGET_EXCEPTION: 38,
  INVALID_LABEL_IN_TEST_SUITE: 39,
  ILLEGAL_FLAG_COMBINATION: 40
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Query.Code}
 */
proto.failure_details.Query.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Query.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Query.Code} value
 * @return {!proto.failure_details.Query} returns this
 */
proto.failure_details.Query.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.LocalExecution.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.LocalExecution.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.LocalExecution} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.LocalExecution.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.LocalExecution}
 */
proto.failure_details.LocalExecution.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.LocalExecution;
  return proto.failure_details.LocalExecution.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.LocalExecution} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.LocalExecution}
 */
proto.failure_details.LocalExecution.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.LocalExecution.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.LocalExecution.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.LocalExecution.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.LocalExecution} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.LocalExecution.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.LocalExecution.Code = {
  LOCAL_EXECUTION_UNKNOWN: 0,
  LOCKFREE_OUTPUT_PREREQ_UNMET: 1
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.LocalExecution.Code}
 */
proto.failure_details.LocalExecution.prototype.getCode = function() {
  return /** @type {!proto.failure_details.LocalExecution.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.LocalExecution.Code} value
 * @return {!proto.failure_details.LocalExecution} returns this
 */
proto.failure_details.LocalExecution.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ActionCache.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ActionCache.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ActionCache} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ActionCache.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ActionCache}
 */
proto.failure_details.ActionCache.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ActionCache;
  return proto.failure_details.ActionCache.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ActionCache} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ActionCache}
 */
proto.failure_details.ActionCache.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ActionCache.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ActionCache.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ActionCache.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ActionCache} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ActionCache.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ActionCache.Code = {
  ACTION_CACHE_UNKNOWN: 0,
  INITIALIZATION_FAILURE: 1
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ActionCache.Code}
 */
proto.failure_details.ActionCache.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ActionCache.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ActionCache.Code} value
 * @return {!proto.failure_details.ActionCache} returns this
 */
proto.failure_details.ActionCache.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.FetchCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.FetchCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.FetchCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.FetchCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.FetchCommand}
 */
proto.failure_details.FetchCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.FetchCommand;
  return proto.failure_details.FetchCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.FetchCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.FetchCommand}
 */
proto.failure_details.FetchCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.FetchCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.FetchCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.FetchCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.FetchCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.FetchCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.FetchCommand.Code = {
  FETCH_COMMAND_UNKNOWN: 0,
  EXPRESSION_MISSING: 1,
  OPTIONS_INVALID: 2,
  QUERY_PARSE_ERROR: 3,
  QUERY_EVALUATION_ERROR: 4
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.FetchCommand.Code}
 */
proto.failure_details.FetchCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.FetchCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.FetchCommand.Code} value
 * @return {!proto.failure_details.FetchCommand} returns this
 */
proto.failure_details.FetchCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.SyncCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.SyncCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.SyncCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.SyncCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.SyncCommand}
 */
proto.failure_details.SyncCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.SyncCommand;
  return proto.failure_details.SyncCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.SyncCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.SyncCommand}
 */
proto.failure_details.SyncCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.SyncCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.SyncCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.SyncCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.SyncCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.SyncCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.SyncCommand.Code = {
  SYNC_COMMAND_UNKNOWN: 0,
  PACKAGE_LOOKUP_ERROR: 1,
  WORKSPACE_EVALUATION_ERROR: 2,
  REPOSITORY_FETCH_ERRORS: 3,
  REPOSITORY_NAME_INVALID: 4
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.SyncCommand.Code}
 */
proto.failure_details.SyncCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.SyncCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.SyncCommand.Code} value
 * @return {!proto.failure_details.SyncCommand} returns this
 */
proto.failure_details.SyncCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Sandbox.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Sandbox.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Sandbox} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Sandbox.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Sandbox}
 */
proto.failure_details.Sandbox.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Sandbox;
  return proto.failure_details.Sandbox.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Sandbox} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Sandbox}
 */
proto.failure_details.Sandbox.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Sandbox.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Sandbox.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Sandbox.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Sandbox} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Sandbox.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Sandbox.Code = {
  SANDBOX_FAILURE_UNKNOWN: 0,
  INITIALIZATION_FAILURE: 1,
  EXECUTION_IO_EXCEPTION: 2,
  DOCKER_COMMAND_FAILURE: 3,
  NO_DOCKER_IMAGE: 4,
  DOCKER_IMAGE_PREPARATION_FAILURE: 5,
  BIND_MOUNT_ANALYSIS_FAILURE: 6,
  MOUNT_SOURCE_DOES_NOT_EXIST: 7,
  MOUNT_SOURCE_TARGET_TYPE_MISMATCH: 8,
  MOUNT_TARGET_DOES_NOT_EXIST: 9,
  SUBPROCESS_START_FAILED: 10,
  FORBIDDEN_INPUT: 11
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Sandbox.Code}
 */
proto.failure_details.Sandbox.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Sandbox.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Sandbox.Code} value
 * @return {!proto.failure_details.Sandbox} returns this
 */
proto.failure_details.Sandbox.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.IncludeScanning.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.IncludeScanning.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.IncludeScanning} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.IncludeScanning.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0),
    packageLoadingCode: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.IncludeScanning}
 */
proto.failure_details.IncludeScanning.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.IncludeScanning;
  return proto.failure_details.IncludeScanning.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.IncludeScanning} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.IncludeScanning}
 */
proto.failure_details.IncludeScanning.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.IncludeScanning.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    case 2:
      var value = /** @type {!proto.failure_details.PackageLoading.Code} */ (reader.readEnum());
      msg.setPackageLoadingCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.IncludeScanning.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.IncludeScanning.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.IncludeScanning} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.IncludeScanning.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getPackageLoadingCode();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.IncludeScanning.Code = {
  INCLUDE_SCANNING_UNKNOWN: 0,
  INITIALIZE_INCLUDE_HINTS_ERROR: 1,
  SCANNING_IO_EXCEPTION: 2,
  INCLUDE_HINTS_FILE_NOT_IN_PACKAGE: 3,
  INCLUDE_HINTS_READ_FAILURE: 4,
  ILLEGAL_ABSOLUTE_PATH: 5,
  PACKAGE_LOAD_FAILURE: 6,
  USER_PACKAGE_LOAD_FAILURE: 7,
  SYSTEM_PACKAGE_LOAD_FAILURE: 8,
  UNDIFFERENTIATED_PACKAGE_LOAD_FAILURE: 9
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.IncludeScanning.Code}
 */
proto.failure_details.IncludeScanning.prototype.getCode = function() {
  return /** @type {!proto.failure_details.IncludeScanning.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.IncludeScanning.Code} value
 * @return {!proto.failure_details.IncludeScanning} returns this
 */
proto.failure_details.IncludeScanning.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional PackageLoading.Code package_loading_code = 2;
 * @return {!proto.failure_details.PackageLoading.Code}
 */
proto.failure_details.IncludeScanning.prototype.getPackageLoadingCode = function() {
  return /** @type {!proto.failure_details.PackageLoading.Code} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.failure_details.PackageLoading.Code} value
 * @return {!proto.failure_details.IncludeScanning} returns this
 */
proto.failure_details.IncludeScanning.prototype.setPackageLoadingCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.TestCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.TestCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.TestCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.TestCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.TestCommand}
 */
proto.failure_details.TestCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.TestCommand;
  return proto.failure_details.TestCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.TestCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.TestCommand}
 */
proto.failure_details.TestCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.TestCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.TestCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.TestCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.TestCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.TestCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.TestCommand.Code = {
  TEST_COMMAND_UNKNOWN: 0,
  NO_TEST_TARGETS: 1,
  TEST_WITH_NOANALYZE: 2,
  TESTS_FAILED: 3
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.TestCommand.Code}
 */
proto.failure_details.TestCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.TestCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.TestCommand.Code} value
 * @return {!proto.failure_details.TestCommand} returns this
 */
proto.failure_details.TestCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ActionQuery.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ActionQuery.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ActionQuery} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ActionQuery.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ActionQuery}
 */
proto.failure_details.ActionQuery.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ActionQuery;
  return proto.failure_details.ActionQuery.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ActionQuery} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ActionQuery}
 */
proto.failure_details.ActionQuery.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ActionQuery.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ActionQuery.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ActionQuery.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ActionQuery} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ActionQuery.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ActionQuery.Code = {
  ACTION_QUERY_UNKNOWN: 0,
  COMMAND_LINE_EXPANSION_FAILURE: 1,
  OUTPUT_FAILURE: 2,
  COMMAND_LINE_EXPRESSION_MISSING: 3,
  EXPRESSION_PARSE_FAILURE: 4,
  SKYFRAME_STATE_WITH_COMMAND_LINE_EXPRESSION: 5,
  INVALID_AQUERY_EXPRESSION: 6,
  SKYFRAME_STATE_PREREQ_UNMET: 7,
  AQUERY_OUTPUT_TOO_BIG: 8,
  ILLEGAL_PATTERN_SYNTAX: 9,
  INCORRECT_ARGUMENTS: 10,
  TOP_LEVEL_TARGETS_WITH_SKYFRAME_STATE_NOT_SUPPORTED: 11,
  SKYFRAME_STATE_AFTER_EXECUTION: 12,
  LABELS_FUNCTION_NOT_SUPPORTED: 13
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ActionQuery.Code}
 */
proto.failure_details.ActionQuery.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ActionQuery.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ActionQuery.Code} value
 * @return {!proto.failure_details.ActionQuery} returns this
 */
proto.failure_details.ActionQuery.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.TargetPatterns.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.TargetPatterns.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.TargetPatterns} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.TargetPatterns.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.TargetPatterns}
 */
proto.failure_details.TargetPatterns.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.TargetPatterns;
  return proto.failure_details.TargetPatterns.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.TargetPatterns} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.TargetPatterns}
 */
proto.failure_details.TargetPatterns.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.TargetPatterns.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.TargetPatterns.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.TargetPatterns.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.TargetPatterns} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.TargetPatterns.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.TargetPatterns.Code = {
  TARGET_PATTERNS_UNKNOWN: 0,
  TARGET_PATTERN_FILE_WITH_COMMAND_LINE_PATTERN: 1,
  TARGET_PATTERN_FILE_READ_FAILURE: 2,
  TARGET_PATTERN_PARSE_FAILURE: 3,
  PACKAGE_NOT_FOUND: 4,
  TARGET_FORMAT_INVALID: 5,
  ABSOLUTE_TARGET_PATTERN_INVALID: 6,
  CANNOT_DETERMINE_TARGET_FROM_FILENAME: 7,
  LABEL_SYNTAX_ERROR: 8,
  TARGET_CANNOT_BE_EMPTY_STRING: 9,
  PACKAGE_PART_CANNOT_END_IN_SLASH: 10,
  CYCLE: 11,
  CANNOT_PRELOAD_TARGET: 12,
  TARGETS_MISSING: 13,
  RECURSIVE_TARGET_PATTERNS_NOT_ALLOWED: 14,
  UP_LEVEL_REFERENCES_NOT_ALLOWED: 15,
  NEGATIVE_TARGET_PATTERN_NOT_ALLOWED: 16,
  TARGET_MUST_BE_A_FILE: 17,
  DEPENDENCY_NOT_FOUND: 18,
  PACKAGE_NAME_INVALID: 19
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.TargetPatterns.Code}
 */
proto.failure_details.TargetPatterns.prototype.getCode = function() {
  return /** @type {!proto.failure_details.TargetPatterns.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.TargetPatterns.Code} value
 * @return {!proto.failure_details.TargetPatterns} returns this
 */
proto.failure_details.TargetPatterns.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.CleanCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.CleanCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.CleanCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CleanCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.CleanCommand}
 */
proto.failure_details.CleanCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.CleanCommand;
  return proto.failure_details.CleanCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.CleanCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.CleanCommand}
 */
proto.failure_details.CleanCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.CleanCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.CleanCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.CleanCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.CleanCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CleanCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.CleanCommand.Code = {
  CLEAN_COMMAND_UNKNOWN: 0,
  OUTPUT_SERVICE_CLEAN_FAILURE: 1,
  ACTION_CACHE_CLEAN_FAILURE: 2,
  OUT_ERR_CLOSE_FAILURE: 3,
  OUTPUT_BASE_DELETE_FAILURE: 4,
  OUTPUT_BASE_TEMP_MOVE_FAILURE: 5,
  ASYNC_OUTPUT_BASE_DELETE_FAILURE: 6,
  EXECROOT_DELETE_FAILURE: 7,
  EXECROOT_TEMP_MOVE_FAILURE: 8,
  ASYNC_EXECROOT_DELETE_FAILURE: 9,
  ARGUMENTS_NOT_RECOGNIZED: 10
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.CleanCommand.Code}
 */
proto.failure_details.CleanCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.CleanCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.CleanCommand.Code} value
 * @return {!proto.failure_details.CleanCommand} returns this
 */
proto.failure_details.CleanCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ConfigCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ConfigCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ConfigCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ConfigCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ConfigCommand}
 */
proto.failure_details.ConfigCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ConfigCommand;
  return proto.failure_details.ConfigCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ConfigCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ConfigCommand}
 */
proto.failure_details.ConfigCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ConfigCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ConfigCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ConfigCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ConfigCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ConfigCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ConfigCommand.Code = {
  CONFIG_COMMAND_UNKNOWN: 0,
  TOO_MANY_CONFIG_IDS: 1,
  CONFIGURATION_NOT_FOUND: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ConfigCommand.Code}
 */
proto.failure_details.ConfigCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ConfigCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ConfigCommand.Code} value
 * @return {!proto.failure_details.ConfigCommand} returns this
 */
proto.failure_details.ConfigCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ConfigurableQuery.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ConfigurableQuery.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ConfigurableQuery} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ConfigurableQuery.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ConfigurableQuery}
 */
proto.failure_details.ConfigurableQuery.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ConfigurableQuery;
  return proto.failure_details.ConfigurableQuery.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ConfigurableQuery} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ConfigurableQuery}
 */
proto.failure_details.ConfigurableQuery.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ConfigurableQuery.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ConfigurableQuery.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ConfigurableQuery.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ConfigurableQuery} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ConfigurableQuery.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ConfigurableQuery.Code = {
  CONFIGURABLE_QUERY_UNKNOWN: 0,
  COMMAND_LINE_EXPRESSION_MISSING: 1,
  EXPRESSION_PARSE_FAILURE: 2,
  FILTERS_NOT_SUPPORTED: 3,
  BUILDFILES_FUNCTION_NOT_SUPPORTED: 4,
  SIBLINGS_FUNCTION_NOT_SUPPORTED: 5,
  VISIBLE_FUNCTION_NOT_SUPPORTED: 6,
  ATTRIBUTE_MISSING: 7,
  INCORRECT_CONFIG_ARGUMENT_ERROR: 8,
  TARGET_MISSING: 9,
  STARLARK_SYNTAX_ERROR: 10,
  STARLARK_EVAL_ERROR: 11,
  FORMAT_FUNCTION_ERROR: 12
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ConfigurableQuery.Code}
 */
proto.failure_details.ConfigurableQuery.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ConfigurableQuery.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ConfigurableQuery.Code} value
 * @return {!proto.failure_details.ConfigurableQuery} returns this
 */
proto.failure_details.ConfigurableQuery.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.DumpCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.DumpCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.DumpCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.DumpCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.DumpCommand}
 */
proto.failure_details.DumpCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.DumpCommand;
  return proto.failure_details.DumpCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.DumpCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.DumpCommand}
 */
proto.failure_details.DumpCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.DumpCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.DumpCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.DumpCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.DumpCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.DumpCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.DumpCommand.Code = {
  DUMP_COMMAND_UNKNOWN: 0,
  NO_OUTPUT_SPECIFIED: 1,
  ACTION_CACHE_DUMP_FAILED: 2,
  COMMAND_LINE_EXPANSION_FAILURE: 3,
  ACTION_GRAPH_DUMP_FAILED: 4,
  STARLARK_HEAP_DUMP_FAILED: 5
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.DumpCommand.Code}
 */
proto.failure_details.DumpCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.DumpCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.DumpCommand.Code} value
 * @return {!proto.failure_details.DumpCommand} returns this
 */
proto.failure_details.DumpCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.HelpCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.HelpCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.HelpCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.HelpCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.HelpCommand}
 */
proto.failure_details.HelpCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.HelpCommand;
  return proto.failure_details.HelpCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.HelpCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.HelpCommand}
 */
proto.failure_details.HelpCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.HelpCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.HelpCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.HelpCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.HelpCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.HelpCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.HelpCommand.Code = {
  HELP_COMMAND_UNKNOWN: 0,
  MISSING_ARGUMENT: 1,
  COMMAND_NOT_FOUND: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.HelpCommand.Code}
 */
proto.failure_details.HelpCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.HelpCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.HelpCommand.Code} value
 * @return {!proto.failure_details.HelpCommand} returns this
 */
proto.failure_details.HelpCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.MobileInstall.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.MobileInstall.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.MobileInstall} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.MobileInstall.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.MobileInstall}
 */
proto.failure_details.MobileInstall.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.MobileInstall;
  return proto.failure_details.MobileInstall.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.MobileInstall} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.MobileInstall}
 */
proto.failure_details.MobileInstall.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.MobileInstall.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.MobileInstall.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.MobileInstall.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.MobileInstall} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.MobileInstall.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.MobileInstall.Code = {
  MOBILE_INSTALL_UNKNOWN: 0,
  CLASSIC_UNSUPPORTED: 1,
  NO_TARGET_SPECIFIED: 2,
  MULTIPLE_TARGETS_SPECIFIED: 3,
  TARGET_TYPE_INVALID: 4,
  NON_ZERO_EXIT: 5,
  ERROR_RUNNING_PROGRAM: 6
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.MobileInstall.Code}
 */
proto.failure_details.MobileInstall.prototype.getCode = function() {
  return /** @type {!proto.failure_details.MobileInstall.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.MobileInstall.Code} value
 * @return {!proto.failure_details.MobileInstall} returns this
 */
proto.failure_details.MobileInstall.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ProfileCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ProfileCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ProfileCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ProfileCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ProfileCommand}
 */
proto.failure_details.ProfileCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ProfileCommand;
  return proto.failure_details.ProfileCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ProfileCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ProfileCommand}
 */
proto.failure_details.ProfileCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ProfileCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ProfileCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ProfileCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ProfileCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ProfileCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ProfileCommand.Code = {
  PROFILE_COMMAND_UNKNOWN: 0,
  OLD_BINARY_FORMAT_UNSUPPORTED: 1,
  FILE_READ_FAILURE: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ProfileCommand.Code}
 */
proto.failure_details.ProfileCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ProfileCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ProfileCommand.Code} value
 * @return {!proto.failure_details.ProfileCommand} returns this
 */
proto.failure_details.ProfileCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.RunCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.RunCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.RunCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.RunCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.RunCommand}
 */
proto.failure_details.RunCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.RunCommand;
  return proto.failure_details.RunCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.RunCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.RunCommand}
 */
proto.failure_details.RunCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.RunCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.RunCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.RunCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.RunCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.RunCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.RunCommand.Code = {
  RUN_COMMAND_UNKNOWN: 0,
  NO_TARGET_SPECIFIED: 1,
  TOO_MANY_TARGETS_SPECIFIED: 2,
  TARGET_NOT_EXECUTABLE: 3,
  TARGET_BUILT_BUT_PATH_NOT_EXECUTABLE: 4,
  TARGET_BUILT_BUT_PATH_VALIDATION_FAILED: 5,
  RUN_UNDER_TARGET_NOT_BUILT: 6,
  RUN_PREREQ_UNMET: 7,
  TOO_MANY_TEST_SHARDS_OR_RUNS: 8,
  TEST_ENVIRONMENT_SETUP_FAILURE: 9,
  COMMAND_LINE_EXPANSION_FAILURE: 10,
  NO_SHELL_SPECIFIED: 11,
  SCRIPT_WRITE_FAILURE: 12,
  RUNFILES_DIRECTORIES_CREATION_FAILURE: 13,
  RUNFILES_SYMLINKS_CREATION_FAILURE: 14,
  TEST_ENVIRONMENT_SETUP_INTERRUPTED: 15
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.RunCommand.Code}
 */
proto.failure_details.RunCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.RunCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.RunCommand.Code} value
 * @return {!proto.failure_details.RunCommand} returns this
 */
proto.failure_details.RunCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.VersionCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.VersionCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.VersionCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.VersionCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.VersionCommand}
 */
proto.failure_details.VersionCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.VersionCommand;
  return proto.failure_details.VersionCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.VersionCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.VersionCommand}
 */
proto.failure_details.VersionCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.VersionCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.VersionCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.VersionCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.VersionCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.VersionCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.VersionCommand.Code = {
  VERSION_COMMAND_UNKNOWN: 0,
  NOT_AVAILABLE: 1
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.VersionCommand.Code}
 */
proto.failure_details.VersionCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.VersionCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.VersionCommand.Code} value
 * @return {!proto.failure_details.VersionCommand} returns this
 */
proto.failure_details.VersionCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.PrintActionCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.PrintActionCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.PrintActionCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.PrintActionCommand.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.PrintActionCommand}
 */
proto.failure_details.PrintActionCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.PrintActionCommand;
  return proto.failure_details.PrintActionCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.PrintActionCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.PrintActionCommand}
 */
proto.failure_details.PrintActionCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.PrintActionCommand.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.PrintActionCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.PrintActionCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.PrintActionCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.PrintActionCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.PrintActionCommand.Code = {
  PRINT_ACTION_COMMAND_UNKNOWN: 0,
  TARGET_NOT_FOUND: 1,
  COMMAND_LINE_EXPANSION_FAILURE: 2,
  TARGET_KIND_UNSUPPORTED: 3,
  ACTIONS_NOT_FOUND: 4
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.PrintActionCommand.Code}
 */
proto.failure_details.PrintActionCommand.prototype.getCode = function() {
  return /** @type {!proto.failure_details.PrintActionCommand.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.PrintActionCommand.Code} value
 * @return {!proto.failure_details.PrintActionCommand} returns this
 */
proto.failure_details.PrintActionCommand.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.WorkspaceStatus.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.WorkspaceStatus.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.WorkspaceStatus} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.WorkspaceStatus.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.WorkspaceStatus}
 */
proto.failure_details.WorkspaceStatus.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.WorkspaceStatus;
  return proto.failure_details.WorkspaceStatus.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.WorkspaceStatus} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.WorkspaceStatus}
 */
proto.failure_details.WorkspaceStatus.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.WorkspaceStatus.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.WorkspaceStatus.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.WorkspaceStatus.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.WorkspaceStatus} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.WorkspaceStatus.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.WorkspaceStatus.Code = {
  WORKSPACE_STATUS_UNKNOWN: 0,
  NON_ZERO_EXIT: 1,
  ABNORMAL_TERMINATION: 2,
  EXEC_FAILED: 3,
  PARSE_FAILURE: 4,
  VALIDATION_FAILURE: 5,
  CONTENT_UPDATE_IO_EXCEPTION: 6,
  STDERR_IO_EXCEPTION: 7
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.WorkspaceStatus.Code}
 */
proto.failure_details.WorkspaceStatus.prototype.getCode = function() {
  return /** @type {!proto.failure_details.WorkspaceStatus.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.WorkspaceStatus.Code} value
 * @return {!proto.failure_details.WorkspaceStatus} returns this
 */
proto.failure_details.WorkspaceStatus.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.JavaCompile.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.JavaCompile.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.JavaCompile} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.JavaCompile.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.JavaCompile}
 */
proto.failure_details.JavaCompile.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.JavaCompile;
  return proto.failure_details.JavaCompile.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.JavaCompile} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.JavaCompile}
 */
proto.failure_details.JavaCompile.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.JavaCompile.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.JavaCompile.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.JavaCompile.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.JavaCompile} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.JavaCompile.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.JavaCompile.Code = {
  JAVA_COMPILE_UNKNOWN: 0,
  REDUCED_CLASSPATH_FAILURE: 1,
  COMMAND_LINE_EXPANSION_FAILURE: 2,
  JDEPS_READ_IO_EXCEPTION: 3,
  REDUCED_CLASSPATH_FALLBACK_CLEANUP_FAILURE: 4
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.JavaCompile.Code}
 */
proto.failure_details.JavaCompile.prototype.getCode = function() {
  return /** @type {!proto.failure_details.JavaCompile.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.JavaCompile.Code} value
 * @return {!proto.failure_details.JavaCompile} returns this
 */
proto.failure_details.JavaCompile.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ActionRewinding.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ActionRewinding.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ActionRewinding} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ActionRewinding.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ActionRewinding}
 */
proto.failure_details.ActionRewinding.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ActionRewinding;
  return proto.failure_details.ActionRewinding.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ActionRewinding} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ActionRewinding}
 */
proto.failure_details.ActionRewinding.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ActionRewinding.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ActionRewinding.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ActionRewinding.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ActionRewinding} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ActionRewinding.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ActionRewinding.Code = {
  ACTION_REWINDING_UNKNOWN: 0,
  LOST_INPUT_TOO_MANY_TIMES: 1,
  LOST_INPUT_IS_SOURCE: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ActionRewinding.Code}
 */
proto.failure_details.ActionRewinding.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ActionRewinding.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ActionRewinding.Code} value
 * @return {!proto.failure_details.ActionRewinding} returns this
 */
proto.failure_details.ActionRewinding.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.CppCompile.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.CppCompile.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.CppCompile} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CppCompile.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.CppCompile}
 */
proto.failure_details.CppCompile.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.CppCompile;
  return proto.failure_details.CppCompile.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.CppCompile} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.CppCompile}
 */
proto.failure_details.CppCompile.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.CppCompile.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.CppCompile.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.CppCompile.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.CppCompile} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CppCompile.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.CppCompile.Code = {
  CPP_COMPILE_UNKNOWN: 0,
  FIND_USED_HEADERS_IO_EXCEPTION: 1,
  COPY_OUT_ERR_FAILURE: 2,
  D_FILE_READ_FAILURE: 3,
  COMMAND_GENERATION_FAILURE: 4,
  MODULE_EXPANSION_TIMEOUT: 5,
  INCLUDE_PATH_OUTSIDE_EXEC_ROOT: 6,
  FAKE_COMMAND_GENERATION_FAILURE: 7,
  UNDECLARED_INCLUSIONS: 8,
  D_FILE_PARSE_FAILURE: 9,
  COVERAGE_NOTES_CREATION_FAILURE: 10,
  MODULE_EXPANSION_MISSING_DATA: 11
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.CppCompile.Code}
 */
proto.failure_details.CppCompile.prototype.getCode = function() {
  return /** @type {!proto.failure_details.CppCompile.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.CppCompile.Code} value
 * @return {!proto.failure_details.CppCompile} returns this
 */
proto.failure_details.CppCompile.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.StarlarkAction.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.StarlarkAction.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.StarlarkAction} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.StarlarkAction.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.StarlarkAction}
 */
proto.failure_details.StarlarkAction.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.StarlarkAction;
  return proto.failure_details.StarlarkAction.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.StarlarkAction} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.StarlarkAction}
 */
proto.failure_details.StarlarkAction.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.StarlarkAction.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.StarlarkAction.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.StarlarkAction.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.StarlarkAction} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.StarlarkAction.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.StarlarkAction.Code = {
  STARLARK_ACTION_UNKNOWN: 0,
  UNUSED_INPUT_LIST_READ_FAILURE: 1,
  UNUSED_INPUT_LIST_FILE_NOT_FOUND: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.StarlarkAction.Code}
 */
proto.failure_details.StarlarkAction.prototype.getCode = function() {
  return /** @type {!proto.failure_details.StarlarkAction.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.StarlarkAction.Code} value
 * @return {!proto.failure_details.StarlarkAction} returns this
 */
proto.failure_details.StarlarkAction.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.NinjaAction.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.NinjaAction.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.NinjaAction} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.NinjaAction.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.NinjaAction}
 */
proto.failure_details.NinjaAction.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.NinjaAction;
  return proto.failure_details.NinjaAction.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.NinjaAction} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.NinjaAction}
 */
proto.failure_details.NinjaAction.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.NinjaAction.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.NinjaAction.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.NinjaAction.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.NinjaAction} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.NinjaAction.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.NinjaAction.Code = {
  NINJA_ACTION_UNKNOWN: 0,
  INVALID_DEPFILE_DECLARED_DEPENDENCY: 1,
  D_FILE_PARSE_FAILURE: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.NinjaAction.Code}
 */
proto.failure_details.NinjaAction.prototype.getCode = function() {
  return /** @type {!proto.failure_details.NinjaAction.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.NinjaAction.Code} value
 * @return {!proto.failure_details.NinjaAction} returns this
 */
proto.failure_details.NinjaAction.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.DynamicExecution.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.DynamicExecution.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.DynamicExecution} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.DynamicExecution.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.DynamicExecution}
 */
proto.failure_details.DynamicExecution.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.DynamicExecution;
  return proto.failure_details.DynamicExecution.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.DynamicExecution} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.DynamicExecution}
 */
proto.failure_details.DynamicExecution.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.DynamicExecution.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.DynamicExecution.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.DynamicExecution.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.DynamicExecution} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.DynamicExecution.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.DynamicExecution.Code = {
  DYNAMIC_EXECUTION_UNKNOWN: 0,
  XCODE_RELATED_PREREQ_UNMET: 1,
  ACTION_LOG_MOVE_FAILURE: 2,
  RUN_FAILURE: 3,
  NO_USABLE_STRATEGY_FOUND: 4
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.DynamicExecution.Code}
 */
proto.failure_details.DynamicExecution.prototype.getCode = function() {
  return /** @type {!proto.failure_details.DynamicExecution.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.DynamicExecution.Code} value
 * @return {!proto.failure_details.DynamicExecution} returns this
 */
proto.failure_details.DynamicExecution.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.FailAction.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.FailAction.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.FailAction} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.FailAction.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.FailAction}
 */
proto.failure_details.FailAction.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.FailAction;
  return proto.failure_details.FailAction.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.FailAction} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.FailAction}
 */
proto.failure_details.FailAction.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.FailAction.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.FailAction.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.FailAction.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.FailAction} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.FailAction.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.FailAction.Code = {
  FAIL_ACTION_UNKNOWN: 0,
  INTENTIONAL_FAILURE: 1,
  INCORRECT_PYTHON_VERSION: 2,
  PROGUARD_SPECS_MISSING: 3,
  DYNAMIC_LINKING_NOT_SUPPORTED: 4,
  SOURCE_FILES_MISSING: 5,
  INCORRECT_TOOLCHAIN: 6,
  FRAGMENT_CLASS_MISSING: 7,
  CANT_BUILD_INCOMPATIBLE_TARGET: 10
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.FailAction.Code}
 */
proto.failure_details.FailAction.prototype.getCode = function() {
  return /** @type {!proto.failure_details.FailAction.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.FailAction.Code} value
 * @return {!proto.failure_details.FailAction} returns this
 */
proto.failure_details.FailAction.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.SymlinkAction.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.SymlinkAction.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.SymlinkAction} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.SymlinkAction.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.SymlinkAction}
 */
proto.failure_details.SymlinkAction.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.SymlinkAction;
  return proto.failure_details.SymlinkAction.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.SymlinkAction} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.SymlinkAction}
 */
proto.failure_details.SymlinkAction.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.SymlinkAction.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.SymlinkAction.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.SymlinkAction.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.SymlinkAction} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.SymlinkAction.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.SymlinkAction.Code = {
  SYMLINK_ACTION_UNKNOWN: 0,
  EXECUTABLE_INPUT_NOT_FILE: 1,
  EXECUTABLE_INPUT_IS_NOT: 2,
  EXECUTABLE_INPUT_CHECK_IO_EXCEPTION: 3,
  LINK_CREATION_IO_EXCEPTION: 4,
  LINK_TOUCH_IO_EXCEPTION: 5
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.SymlinkAction.Code}
 */
proto.failure_details.SymlinkAction.prototype.getCode = function() {
  return /** @type {!proto.failure_details.SymlinkAction.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.SymlinkAction.Code} value
 * @return {!proto.failure_details.SymlinkAction} returns this
 */
proto.failure_details.SymlinkAction.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.CppLink.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.CppLink.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.CppLink} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CppLink.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.CppLink}
 */
proto.failure_details.CppLink.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.CppLink;
  return proto.failure_details.CppLink.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.CppLink} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.CppLink}
 */
proto.failure_details.CppLink.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.CppLink.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.CppLink.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.CppLink.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.CppLink} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.CppLink.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.CppLink.Code = {
  CPP_LINK_UNKNOWN: 0,
  COMMAND_GENERATION_FAILURE: 1,
  FAKE_COMMAND_GENERATION_FAILURE: 2
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.CppLink.Code}
 */
proto.failure_details.CppLink.prototype.getCode = function() {
  return /** @type {!proto.failure_details.CppLink.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.CppLink.Code} value
 * @return {!proto.failure_details.CppLink} returns this
 */
proto.failure_details.CppLink.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.LtoAction.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.LtoAction.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.LtoAction} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.LtoAction.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.LtoAction}
 */
proto.failure_details.LtoAction.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.LtoAction;
  return proto.failure_details.LtoAction.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.LtoAction} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.LtoAction}
 */
proto.failure_details.LtoAction.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.LtoAction.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.LtoAction.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.LtoAction.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.LtoAction} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.LtoAction.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.LtoAction.Code = {
  LTO_ACTION_UNKNOWN: 0,
  INVALID_ABSOLUTE_PATH_IN_IMPORTS: 1,
  MISSING_BITCODE_FILES: 2,
  IMPORTS_READ_IO_EXCEPTION: 3
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.LtoAction.Code}
 */
proto.failure_details.LtoAction.prototype.getCode = function() {
  return /** @type {!proto.failure_details.LtoAction.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.LtoAction.Code} value
 * @return {!proto.failure_details.LtoAction} returns this
 */
proto.failure_details.LtoAction.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.TestAction.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.TestAction.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.TestAction} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.TestAction.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.TestAction}
 */
proto.failure_details.TestAction.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.TestAction;
  return proto.failure_details.TestAction.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.TestAction} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.TestAction}
 */
proto.failure_details.TestAction.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.TestAction.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.TestAction.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.TestAction.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.TestAction} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.TestAction.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.TestAction.Code = {
  TEST_ACTION_UNKNOWN: 0,
  NO_KEEP_GOING_TEST_FAILURE: 1,
  LOCAL_TEST_PREREQ_UNMET: 2,
  COMMAND_LINE_EXPANSION_FAILURE: 3,
  DUPLICATE_CPU_TAGS: 4,
  INVALID_CPU_TAG: 5
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.TestAction.Code}
 */
proto.failure_details.TestAction.prototype.getCode = function() {
  return /** @type {!proto.failure_details.TestAction.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.TestAction.Code} value
 * @return {!proto.failure_details.TestAction} returns this
 */
proto.failure_details.TestAction.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Worker.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Worker.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Worker} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Worker.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Worker}
 */
proto.failure_details.Worker.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Worker;
  return proto.failure_details.Worker.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Worker} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Worker}
 */
proto.failure_details.Worker.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Worker.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Worker.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Worker.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Worker} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Worker.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Worker.Code = {
  WORKER_UNKNOWN: 0,
  MULTIPLEXER_INSTANCE_REMOVAL_FAILURE: 1,
  MULTIPLEXER_DOES_NOT_EXIST: 2,
  NO_TOOLS: 3,
  NO_FLAGFILE: 4,
  VIRTUAL_INPUT_MATERIALIZATION_FAILURE: 5,
  BORROW_FAILURE: 6,
  PREFETCH_FAILURE: 7,
  PREPARE_FAILURE: 8,
  REQUEST_FAILURE: 9,
  PARSE_RESPONSE_FAILURE: 10,
  NO_RESPONSE: 11,
  FINISH_FAILURE: 12,
  FORBIDDEN_INPUT: 13
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Worker.Code}
 */
proto.failure_details.Worker.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Worker.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Worker.Code} value
 * @return {!proto.failure_details.Worker} returns this
 */
proto.failure_details.Worker.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Analysis.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Analysis.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Analysis} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Analysis.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Analysis}
 */
proto.failure_details.Analysis.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Analysis;
  return proto.failure_details.Analysis.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Analysis} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Analysis}
 */
proto.failure_details.Analysis.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Analysis.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Analysis.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Analysis.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Analysis} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Analysis.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Analysis.Code = {
  ANALYSIS_UNKNOWN: 0,
  LOAD_FAILURE: 1,
  GENERIC_LOADING_PHASE_FAILURE: 2,
  NOT_ALL_TARGETS_ANALYZED: 3,
  CYCLE: 4,
  PARAMETERIZED_TOP_LEVEL_ASPECT_INVALID: 5,
  ASPECT_LABEL_SYNTAX_ERROR: 6,
  ASPECT_PREREQ_UNMET: 7,
  ASPECT_NOT_FOUND: 8,
  ACTION_CONFLICT: 9,
  ARTIFACT_PREFIX_CONFLICT: 10,
  UNEXPECTED_ANALYSIS_EXCEPTION: 11,
  TARGETS_MISSING_ENVIRONMENTS: 12,
  INVALID_ENVIRONMENT: 13,
  ENVIRONMENT_MISSING_FROM_GROUPS: 14,
  EXEC_GROUP_MISSING: 15,
  INVALID_EXECUTION_PLATFORM: 16,
  ASPECT_CREATION_FAILED: 17,
  CONFIGURED_VALUE_CREATION_FAILED: 18,
  INCOMPATIBLE_TARGET_REQUESTED: 19
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Analysis.Code}
 */
proto.failure_details.Analysis.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Analysis.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Analysis.Code} value
 * @return {!proto.failure_details.Analysis} returns this
 */
proto.failure_details.Analysis.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.PackageLoading.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.PackageLoading.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.PackageLoading} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.PackageLoading.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.PackageLoading}
 */
proto.failure_details.PackageLoading.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.PackageLoading;
  return proto.failure_details.PackageLoading.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.PackageLoading} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.PackageLoading}
 */
proto.failure_details.PackageLoading.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.PackageLoading.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.PackageLoading.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.PackageLoading.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.PackageLoading} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.PackageLoading.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.PackageLoading.Code = {
  PACKAGE_LOADING_UNKNOWN: 0,
  WORKSPACE_FILE_ERROR: 1,
  MAX_COMPUTATION_STEPS_EXCEEDED: 2,
  BUILD_FILE_MISSING: 3,
  REPOSITORY_MISSING: 4,
  PERSISTENT_INCONSISTENT_FILESYSTEM_ERROR: 5,
  TRANSIENT_INCONSISTENT_FILESYSTEM_ERROR: 6,
  INVALID_NAME: 7,
  EVAL_GLOBS_SYMLINK_ERROR: 9,
  IMPORT_STARLARK_FILE_ERROR: 10,
  PACKAGE_MISSING: 11,
  TARGET_MISSING: 12,
  NO_SUCH_THING: 13,
  GLOB_IO_EXCEPTION: 14,
  DUPLICATE_LABEL: 15,
  INVALID_PACKAGE_SPECIFICATION: 16,
  SYNTAX_ERROR: 17,
  ENVIRONMENT_IN_DIFFERENT_PACKAGE: 18,
  DEFAULT_ENVIRONMENT_UNDECLARED: 19,
  ENVIRONMENT_IN_MULTIPLE_GROUPS: 20,
  ENVIRONMENT_DOES_NOT_EXIST: 21,
  ENVIRONMENT_INVALID: 22,
  ENVIRONMENT_NOT_IN_GROUP: 23,
  PACKAGE_NAME_INVALID: 24,
  STARLARK_EVAL_ERROR: 25,
  LICENSE_PARSE_FAILURE: 26,
  DISTRIBUTIONS_PARSE_FAILURE: 27,
  LABEL_CROSSES_PACKAGE_BOUNDARY: 28,
  BUILTINS_INJECTION_FAILURE: 29,
  SYMLINK_CYCLE_OR_INFINITE_EXPANSION: 30,
  OTHER_IO_EXCEPTION: 31
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.PackageLoading.Code}
 */
proto.failure_details.PackageLoading.prototype.getCode = function() {
  return /** @type {!proto.failure_details.PackageLoading.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.PackageLoading.Code} value
 * @return {!proto.failure_details.PackageLoading} returns this
 */
proto.failure_details.PackageLoading.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.Toolchain.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.Toolchain.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.Toolchain} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Toolchain.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.Toolchain}
 */
proto.failure_details.Toolchain.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.Toolchain;
  return proto.failure_details.Toolchain.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.Toolchain} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.Toolchain}
 */
proto.failure_details.Toolchain.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.Toolchain.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.Toolchain.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.Toolchain.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.Toolchain} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.Toolchain.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.Toolchain.Code = {
  TOOLCHAIN_UNKNOWN: 0,
  MISSING_PROVIDER: 1,
  INVALID_CONSTRAINT_VALUE: 2,
  INVALID_PLATFORM_VALUE: 3,
  INVALID_TOOLCHAIN: 4,
  NO_MATCHING_EXECUTION_PLATFORM: 5,
  NO_MATCHING_TOOLCHAIN: 6,
  INVALID_TOOLCHAIN_TYPE: 7
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.Toolchain.Code}
 */
proto.failure_details.Toolchain.prototype.getCode = function() {
  return /** @type {!proto.failure_details.Toolchain.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.Toolchain.Code} value
 * @return {!proto.failure_details.Toolchain} returns this
 */
proto.failure_details.Toolchain.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.StarlarkLoading.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.StarlarkLoading.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.StarlarkLoading} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.StarlarkLoading.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.StarlarkLoading}
 */
proto.failure_details.StarlarkLoading.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.StarlarkLoading;
  return proto.failure_details.StarlarkLoading.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.StarlarkLoading} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.StarlarkLoading}
 */
proto.failure_details.StarlarkLoading.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.StarlarkLoading.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.StarlarkLoading.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.StarlarkLoading.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.StarlarkLoading} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.StarlarkLoading.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.StarlarkLoading.Code = {
  STARLARK_LOADING_UNKNOWN: 0,
  CYCLE: 1,
  COMPILE_ERROR: 2,
  PARSE_ERROR: 3,
  EVAL_ERROR: 4,
  CONTAINING_PACKAGE_NOT_FOUND: 5,
  PACKAGE_NOT_FOUND: 6,
  IO_ERROR: 7,
  LABEL_CROSSES_PACKAGE_BOUNDARY: 8,
  BUILTINS_ERROR: 9
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.StarlarkLoading.Code}
 */
proto.failure_details.StarlarkLoading.prototype.getCode = function() {
  return /** @type {!proto.failure_details.StarlarkLoading.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.StarlarkLoading.Code} value
 * @return {!proto.failure_details.StarlarkLoading} returns this
 */
proto.failure_details.StarlarkLoading.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.ExternalDeps.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.ExternalDeps.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.ExternalDeps} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ExternalDeps.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.ExternalDeps}
 */
proto.failure_details.ExternalDeps.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.ExternalDeps;
  return proto.failure_details.ExternalDeps.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.ExternalDeps} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.ExternalDeps}
 */
proto.failure_details.ExternalDeps.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.ExternalDeps.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.ExternalDeps.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.ExternalDeps.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.ExternalDeps} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.ExternalDeps.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.ExternalDeps.Code = {
  EXTERNAL_DEPS_UNKNOWN: 0,
  MODULE_NOT_FOUND: 1,
  BAD_MODULE: 2,
  VERSION_RESOLUTION_ERROR: 3,
  INVALID_REGISTRY_URL: 4,
  ERROR_ACCESSING_REGISTRY: 5
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.ExternalDeps.Code}
 */
proto.failure_details.ExternalDeps.prototype.getCode = function() {
  return /** @type {!proto.failure_details.ExternalDeps.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.ExternalDeps.Code} value
 * @return {!proto.failure_details.ExternalDeps} returns this
 */
proto.failure_details.ExternalDeps.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.failure_details.DiffAwareness.prototype.toObject = function(opt_includeInstance) {
  return proto.failure_details.DiffAwareness.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.failure_details.DiffAwareness} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.DiffAwareness.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.failure_details.DiffAwareness}
 */
proto.failure_details.DiffAwareness.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.failure_details.DiffAwareness;
  return proto.failure_details.DiffAwareness.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.failure_details.DiffAwareness} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.failure_details.DiffAwareness}
 */
proto.failure_details.DiffAwareness.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.failure_details.DiffAwareness.Code} */ (reader.readEnum());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.failure_details.DiffAwareness.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.failure_details.DiffAwareness.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.failure_details.DiffAwareness} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.failure_details.DiffAwareness.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.failure_details.DiffAwareness.Code = {
  DIFF_AWARENESS_UNKNOWN: 0,
  DIFF_STAT_FAILED: 1
};

/**
 * optional Code code = 1;
 * @return {!proto.failure_details.DiffAwareness.Code}
 */
proto.failure_details.DiffAwareness.prototype.getCode = function() {
  return /** @type {!proto.failure_details.DiffAwareness.Code} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.failure_details.DiffAwareness.Code} value
 * @return {!proto.failure_details.DiffAwareness} returns this
 */
proto.failure_details.DiffAwareness.prototype.setCode = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};



/**
 * A tuple of {field number, class constructor} for the extension
 * field named `metadata`.
 * @type {!jspb.ExtensionFieldInfo<!proto.failure_details.FailureDetailMetadata>}
 */
proto.failure_details.metadata = new jspb.ExtensionFieldInfo(
    1078,
    {metadata: 0},
    proto.failure_details.FailureDetailMetadata,
     /** @type {?function((boolean|undefined),!jspb.Message=): !Object} */ (
         proto.failure_details.FailureDetailMetadata.toObject),
    0);

google_protobuf_descriptor_pb.EnumValueOptions.extensionsBinary[1078] = new jspb.ExtensionFieldBinaryInfo(
    proto.failure_details.metadata,
    jspb.BinaryReader.prototype.readMessage,
    jspb.BinaryWriter.prototype.writeMessage,
    proto.failure_details.FailureDetailMetadata.serializeBinaryToWriter,
    proto.failure_details.FailureDetailMetadata.deserializeBinaryFromReader,
    false);
// This registers the extension field with the extended class, so that
// toObject() will function correctly.
google_protobuf_descriptor_pb.EnumValueOptions.extensions[1078] = proto.failure_details.metadata;

goog.object.extend(exports, proto.failure_details);
