// source: build_event_stream/build_event_stream.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_duration_pb = require('google-protobuf/google/protobuf/duration_pb.js');
goog.object.extend(proto, google_protobuf_duration_pb);
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
goog.object.extend(proto, google_protobuf_timestamp_pb);
var build_event_stream_command_line_pb = require('../build_event_stream/command_line_pb.js');
goog.object.extend(proto, build_event_stream_command_line_pb);
var build_event_stream_failure_details_pb = require('../build_event_stream/failure_details_pb.js');
goog.object.extend(proto, build_event_stream_failure_details_pb);
var build_event_stream_invocation_policy_pb = require('../build_event_stream/invocation_policy_pb.js');
goog.object.extend(proto, build_event_stream_invocation_policy_pb);
goog.exportSymbol('proto.build_event_stream.Aborted', null, global);
goog.exportSymbol('proto.build_event_stream.Aborted.AbortReason', null, global);
goog.exportSymbol('proto.build_event_stream.ActionExecuted', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEvent', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEvent.PayloadCase', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.ActionCompletedId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.BuildFinishedId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.BuildMetadataId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.BuildMetricsId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.BuildStartedId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.BuildToolLogsId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.ConfigurationId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.ConfiguredLabelId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.FetchId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.IdCase', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.NamedSetOfFilesId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.OptionsParsedId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.PatternExpandedId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.ProgressId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.StructuredCommandLineId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.TargetCompletedId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.TargetConfiguredId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.TargetSummaryId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.TestResultId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.TestSummaryId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.UnconfiguredLabelId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.UnknownBuildEventId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.UnstructuredCommandLineId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.WorkspaceConfigId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildEventId.WorkspaceStatusId', null, global);
goog.exportSymbol('proto.build_event_stream.BuildFinished', null, global);
goog.exportSymbol('proto.build_event_stream.BuildFinished.AnomalyReport', null, global);
goog.exportSymbol('proto.build_event_stream.BuildFinished.ExitCode', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetadata', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.ActionSummary', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.ActionSummary.ActionData', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.ArtifactMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.BuildGraphMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.CumulativeMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.MemoryMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.PackageMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.TargetMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.TimingMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.WorkerMetrics', null, global);
goog.exportSymbol('proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats', null, global);
goog.exportSymbol('proto.build_event_stream.BuildStarted', null, global);
goog.exportSymbol('proto.build_event_stream.BuildToolLogs', null, global);
goog.exportSymbol('proto.build_event_stream.Configuration', null, global);
goog.exportSymbol('proto.build_event_stream.ConvenienceSymlink', null, global);
goog.exportSymbol('proto.build_event_stream.ConvenienceSymlink.Action', null, global);
goog.exportSymbol('proto.build_event_stream.ConvenienceSymlinksIdentified', null, global);
goog.exportSymbol('proto.build_event_stream.Fetch', null, global);
goog.exportSymbol('proto.build_event_stream.File', null, global);
goog.exportSymbol('proto.build_event_stream.File.FileCase', null, global);
goog.exportSymbol('proto.build_event_stream.NamedSetOfFiles', null, global);
goog.exportSymbol('proto.build_event_stream.OptionsParsed', null, global);
goog.exportSymbol('proto.build_event_stream.OutputGroup', null, global);
goog.exportSymbol('proto.build_event_stream.PatternExpanded', null, global);
goog.exportSymbol('proto.build_event_stream.PatternExpanded.TestSuiteExpansion', null, global);
goog.exportSymbol('proto.build_event_stream.Progress', null, global);
goog.exportSymbol('proto.build_event_stream.TargetComplete', null, global);
goog.exportSymbol('proto.build_event_stream.TargetConfigured', null, global);
goog.exportSymbol('proto.build_event_stream.TargetSummary', null, global);
goog.exportSymbol('proto.build_event_stream.TestResult', null, global);
goog.exportSymbol('proto.build_event_stream.TestResult.ExecutionInfo', null, global);
goog.exportSymbol('proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage', null, global);
goog.exportSymbol('proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown', null, global);
goog.exportSymbol('proto.build_event_stream.TestSize', null, global);
goog.exportSymbol('proto.build_event_stream.TestStatus', null, global);
goog.exportSymbol('proto.build_event_stream.TestSummary', null, global);
goog.exportSymbol('proto.build_event_stream.UnstructuredCommandLine', null, global);
goog.exportSymbol('proto.build_event_stream.WorkspaceConfig', null, global);
goog.exportSymbol('proto.build_event_stream.WorkspaceStatus', null, global);
goog.exportSymbol('proto.build_event_stream.WorkspaceStatus.Item', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.build_event_stream.BuildEventId.oneofGroups_);
};
goog.inherits(proto.build_event_stream.BuildEventId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.displayName = 'proto.build_event_stream.BuildEventId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.UnknownBuildEventId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.UnknownBuildEventId.displayName = 'proto.build_event_stream.BuildEventId.UnknownBuildEventId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.ProgressId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.ProgressId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.ProgressId.displayName = 'proto.build_event_stream.BuildEventId.ProgressId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.BuildStartedId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.BuildStartedId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.BuildStartedId.displayName = 'proto.build_event_stream.BuildEventId.BuildStartedId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.UnstructuredCommandLineId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.UnstructuredCommandLineId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.displayName = 'proto.build_event_stream.BuildEventId.UnstructuredCommandLineId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.StructuredCommandLineId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.StructuredCommandLineId.displayName = 'proto.build_event_stream.BuildEventId.StructuredCommandLineId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.WorkspaceStatusId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.WorkspaceStatusId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.WorkspaceStatusId.displayName = 'proto.build_event_stream.BuildEventId.WorkspaceStatusId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.OptionsParsedId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.OptionsParsedId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.OptionsParsedId.displayName = 'proto.build_event_stream.BuildEventId.OptionsParsedId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.FetchId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.FetchId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.FetchId.displayName = 'proto.build_event_stream.BuildEventId.FetchId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.PatternExpandedId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.BuildEventId.PatternExpandedId.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.PatternExpandedId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.PatternExpandedId.displayName = 'proto.build_event_stream.BuildEventId.PatternExpandedId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.WorkspaceConfigId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.WorkspaceConfigId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.WorkspaceConfigId.displayName = 'proto.build_event_stream.BuildEventId.WorkspaceConfigId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.BuildMetadataId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.BuildMetadataId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.BuildMetadataId.displayName = 'proto.build_event_stream.BuildEventId.BuildMetadataId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.TargetConfiguredId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.TargetConfiguredId.displayName = 'proto.build_event_stream.BuildEventId.TargetConfiguredId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.NamedSetOfFilesId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.NamedSetOfFilesId.displayName = 'proto.build_event_stream.BuildEventId.NamedSetOfFilesId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.ConfigurationId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.ConfigurationId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.ConfigurationId.displayName = 'proto.build_event_stream.BuildEventId.ConfigurationId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.TargetCompletedId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.TargetCompletedId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.TargetCompletedId.displayName = 'proto.build_event_stream.BuildEventId.TargetCompletedId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.ActionCompletedId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.ActionCompletedId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.ActionCompletedId.displayName = 'proto.build_event_stream.BuildEventId.ActionCompletedId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.UnconfiguredLabelId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.UnconfiguredLabelId.displayName = 'proto.build_event_stream.BuildEventId.UnconfiguredLabelId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.ConfiguredLabelId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.ConfiguredLabelId.displayName = 'proto.build_event_stream.BuildEventId.ConfiguredLabelId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.TestResultId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.TestResultId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.TestResultId.displayName = 'proto.build_event_stream.BuildEventId.TestResultId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.TestSummaryId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.TestSummaryId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.TestSummaryId.displayName = 'proto.build_event_stream.BuildEventId.TestSummaryId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.TargetSummaryId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.TargetSummaryId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.TargetSummaryId.displayName = 'proto.build_event_stream.BuildEventId.TargetSummaryId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.BuildFinishedId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.BuildFinishedId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.BuildFinishedId.displayName = 'proto.build_event_stream.BuildEventId.BuildFinishedId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.BuildToolLogsId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.BuildToolLogsId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.BuildToolLogsId.displayName = 'proto.build_event_stream.BuildEventId.BuildToolLogsId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.BuildMetricsId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.BuildMetricsId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.BuildMetricsId.displayName = 'proto.build_event_stream.BuildEventId.BuildMetricsId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.displayName = 'proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.Progress = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.Progress, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.Progress.displayName = 'proto.build_event_stream.Progress';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.Aborted = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.Aborted, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.Aborted.displayName = 'proto.build_event_stream.Aborted';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildStarted = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildStarted, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildStarted.displayName = 'proto.build_event_stream.BuildStarted';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.WorkspaceConfig = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.WorkspaceConfig, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.WorkspaceConfig.displayName = 'proto.build_event_stream.WorkspaceConfig';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.UnstructuredCommandLine = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.UnstructuredCommandLine.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.UnstructuredCommandLine, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.UnstructuredCommandLine.displayName = 'proto.build_event_stream.UnstructuredCommandLine';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.OptionsParsed = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.OptionsParsed.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.OptionsParsed, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.OptionsParsed.displayName = 'proto.build_event_stream.OptionsParsed';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.Fetch = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.Fetch, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.Fetch.displayName = 'proto.build_event_stream.Fetch';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.WorkspaceStatus = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.WorkspaceStatus.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.WorkspaceStatus, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.WorkspaceStatus.displayName = 'proto.build_event_stream.WorkspaceStatus';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.WorkspaceStatus.Item = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.WorkspaceStatus.Item, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.WorkspaceStatus.Item.displayName = 'proto.build_event_stream.WorkspaceStatus.Item';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetadata = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetadata, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetadata.displayName = 'proto.build_event_stream.BuildMetadata';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.Configuration = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.Configuration, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.Configuration.displayName = 'proto.build_event_stream.Configuration';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.PatternExpanded = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.PatternExpanded.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.PatternExpanded, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.PatternExpanded.displayName = 'proto.build_event_stream.PatternExpanded';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.PatternExpanded.TestSuiteExpansion.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.PatternExpanded.TestSuiteExpansion, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.PatternExpanded.TestSuiteExpansion.displayName = 'proto.build_event_stream.PatternExpanded.TestSuiteExpansion';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.TargetConfigured = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.TargetConfigured.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.TargetConfigured, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.TargetConfigured.displayName = 'proto.build_event_stream.TargetConfigured';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.File = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.File.repeatedFields_, proto.build_event_stream.File.oneofGroups_);
};
goog.inherits(proto.build_event_stream.File, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.File.displayName = 'proto.build_event_stream.File';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.NamedSetOfFiles = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.NamedSetOfFiles.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.NamedSetOfFiles, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.NamedSetOfFiles.displayName = 'proto.build_event_stream.NamedSetOfFiles';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.ActionExecuted = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.ActionExecuted.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.ActionExecuted, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.ActionExecuted.displayName = 'proto.build_event_stream.ActionExecuted';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.OutputGroup = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.OutputGroup.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.OutputGroup, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.OutputGroup.displayName = 'proto.build_event_stream.OutputGroup';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.TargetComplete = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.TargetComplete.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.TargetComplete, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.TargetComplete.displayName = 'proto.build_event_stream.TargetComplete';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.TestResult = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.TestResult.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.TestResult, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.TestResult.displayName = 'proto.build_event_stream.TestResult';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.TestResult.ExecutionInfo = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.TestResult.ExecutionInfo.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.TestResult.ExecutionInfo, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.TestResult.ExecutionInfo.displayName = 'proto.build_event_stream.TestResult.ExecutionInfo';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.displayName = 'proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.displayName = 'proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.TestSummary = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.TestSummary.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.TestSummary, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.TestSummary.displayName = 'proto.build_event_stream.TestSummary';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.TargetSummary = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.TargetSummary, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.TargetSummary.displayName = 'proto.build_event_stream.TargetSummary';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildFinished = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildFinished, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildFinished.displayName = 'proto.build_event_stream.BuildFinished';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildFinished.ExitCode = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildFinished.ExitCode, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildFinished.ExitCode.displayName = 'proto.build_event_stream.BuildFinished.ExitCode';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildFinished.AnomalyReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildFinished.AnomalyReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildFinished.AnomalyReport.displayName = 'proto.build_event_stream.BuildFinished.AnomalyReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.BuildMetrics.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.displayName = 'proto.build_event_stream.BuildMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.ActionSummary = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.BuildMetrics.ActionSummary.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.ActionSummary, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.ActionSummary.displayName = 'proto.build_event_stream.BuildMetrics.ActionSummary';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.ActionSummary.ActionData, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.displayName = 'proto.build_event_stream.BuildMetrics.ActionSummary.ActionData';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.displayName = 'proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.BuildMetrics.MemoryMetrics.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.MemoryMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.MemoryMetrics.displayName = 'proto.build_event_stream.BuildMetrics.MemoryMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.displayName = 'proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.TargetMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.TargetMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.TargetMetrics.displayName = 'proto.build_event_stream.BuildMetrics.TargetMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.PackageMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.PackageMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.PackageMetrics.displayName = 'proto.build_event_stream.BuildMetrics.PackageMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.TimingMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.TimingMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.TimingMetrics.displayName = 'proto.build_event_stream.BuildMetrics.TimingMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.CumulativeMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.CumulativeMetrics.displayName = 'proto.build_event_stream.BuildMetrics.CumulativeMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.ArtifactMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.ArtifactMetrics.displayName = 'proto.build_event_stream.BuildMetrics.ArtifactMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.displayName = 'proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.BuildGraphMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.BuildGraphMetrics.displayName = 'proto.build_event_stream.BuildMetrics.BuildGraphMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.BuildMetrics.WorkerMetrics.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.WorkerMetrics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.WorkerMetrics.displayName = 'proto.build_event_stream.BuildMetrics.WorkerMetrics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.displayName = 'proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildToolLogs = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.BuildToolLogs.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.BuildToolLogs, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildToolLogs.displayName = 'proto.build_event_stream.BuildToolLogs';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.ConvenienceSymlinksIdentified = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.ConvenienceSymlinksIdentified.repeatedFields_, null);
};
goog.inherits(proto.build_event_stream.ConvenienceSymlinksIdentified, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.ConvenienceSymlinksIdentified.displayName = 'proto.build_event_stream.ConvenienceSymlinksIdentified';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.ConvenienceSymlink = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.build_event_stream.ConvenienceSymlink, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.ConvenienceSymlink.displayName = 'proto.build_event_stream.ConvenienceSymlink';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.build_event_stream.BuildEvent = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.build_event_stream.BuildEvent.repeatedFields_, proto.build_event_stream.BuildEvent.oneofGroups_);
};
goog.inherits(proto.build_event_stream.BuildEvent, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.build_event_stream.BuildEvent.displayName = 'proto.build_event_stream.BuildEvent';
}

/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.build_event_stream.BuildEventId.oneofGroups_ = [[1,2,3,11,18,14,12,17,15,16,4,10,13,5,6,19,21,8,7,26,9,20,22,23,24,25]];

/**
 * @enum {number}
 */
proto.build_event_stream.BuildEventId.IdCase = {
  ID_NOT_SET: 0,
  UNKNOWN: 1,
  PROGRESS: 2,
  STARTED: 3,
  UNSTRUCTURED_COMMAND_LINE: 11,
  STRUCTURED_COMMAND_LINE: 18,
  WORKSPACE_STATUS: 14,
  OPTIONS_PARSED: 12,
  FETCH: 17,
  CONFIGURATION: 15,
  TARGET_CONFIGURED: 16,
  PATTERN: 4,
  PATTERN_SKIPPED: 10,
  NAMED_SET: 13,
  TARGET_COMPLETED: 5,
  ACTION_COMPLETED: 6,
  UNCONFIGURED_LABEL: 19,
  CONFIGURED_LABEL: 21,
  TEST_RESULT: 8,
  TEST_SUMMARY: 7,
  TARGET_SUMMARY: 26,
  BUILD_FINISHED: 9,
  BUILD_TOOL_LOGS: 20,
  BUILD_METRICS: 22,
  WORKSPACE: 23,
  BUILD_METADATA: 24,
  CONVENIENCE_SYMLINKS_IDENTIFIED: 25
};

/**
 * @return {proto.build_event_stream.BuildEventId.IdCase}
 */
proto.build_event_stream.BuildEventId.prototype.getIdCase = function() {
  return /** @type {proto.build_event_stream.BuildEventId.IdCase} */(jspb.Message.computeOneofCase(this, proto.build_event_stream.BuildEventId.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.toObject = function(includeInstance, msg) {
  var f, obj = {
    unknown: (f = msg.getUnknown()) && proto.build_event_stream.BuildEventId.UnknownBuildEventId.toObject(includeInstance, f),
    progress: (f = msg.getProgress()) && proto.build_event_stream.BuildEventId.ProgressId.toObject(includeInstance, f),
    started: (f = msg.getStarted()) && proto.build_event_stream.BuildEventId.BuildStartedId.toObject(includeInstance, f),
    unstructuredCommandLine: (f = msg.getUnstructuredCommandLine()) && proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.toObject(includeInstance, f),
    structuredCommandLine: (f = msg.getStructuredCommandLine()) && proto.build_event_stream.BuildEventId.StructuredCommandLineId.toObject(includeInstance, f),
    workspaceStatus: (f = msg.getWorkspaceStatus()) && proto.build_event_stream.BuildEventId.WorkspaceStatusId.toObject(includeInstance, f),
    optionsParsed: (f = msg.getOptionsParsed()) && proto.build_event_stream.BuildEventId.OptionsParsedId.toObject(includeInstance, f),
    fetch: (f = msg.getFetch()) && proto.build_event_stream.BuildEventId.FetchId.toObject(includeInstance, f),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.BuildEventId.ConfigurationId.toObject(includeInstance, f),
    targetConfigured: (f = msg.getTargetConfigured()) && proto.build_event_stream.BuildEventId.TargetConfiguredId.toObject(includeInstance, f),
    pattern: (f = msg.getPattern()) && proto.build_event_stream.BuildEventId.PatternExpandedId.toObject(includeInstance, f),
    patternSkipped: (f = msg.getPatternSkipped()) && proto.build_event_stream.BuildEventId.PatternExpandedId.toObject(includeInstance, f),
    namedSet: (f = msg.getNamedSet()) && proto.build_event_stream.BuildEventId.NamedSetOfFilesId.toObject(includeInstance, f),
    targetCompleted: (f = msg.getTargetCompleted()) && proto.build_event_stream.BuildEventId.TargetCompletedId.toObject(includeInstance, f),
    actionCompleted: (f = msg.getActionCompleted()) && proto.build_event_stream.BuildEventId.ActionCompletedId.toObject(includeInstance, f),
    unconfiguredLabel: (f = msg.getUnconfiguredLabel()) && proto.build_event_stream.BuildEventId.UnconfiguredLabelId.toObject(includeInstance, f),
    configuredLabel: (f = msg.getConfiguredLabel()) && proto.build_event_stream.BuildEventId.ConfiguredLabelId.toObject(includeInstance, f),
    testResult: (f = msg.getTestResult()) && proto.build_event_stream.BuildEventId.TestResultId.toObject(includeInstance, f),
    testSummary: (f = msg.getTestSummary()) && proto.build_event_stream.BuildEventId.TestSummaryId.toObject(includeInstance, f),
    targetSummary: (f = msg.getTargetSummary()) && proto.build_event_stream.BuildEventId.TargetSummaryId.toObject(includeInstance, f),
    buildFinished: (f = msg.getBuildFinished()) && proto.build_event_stream.BuildEventId.BuildFinishedId.toObject(includeInstance, f),
    buildToolLogs: (f = msg.getBuildToolLogs()) && proto.build_event_stream.BuildEventId.BuildToolLogsId.toObject(includeInstance, f),
    buildMetrics: (f = msg.getBuildMetrics()) && proto.build_event_stream.BuildEventId.BuildMetricsId.toObject(includeInstance, f),
    workspace: (f = msg.getWorkspace()) && proto.build_event_stream.BuildEventId.WorkspaceConfigId.toObject(includeInstance, f),
    buildMetadata: (f = msg.getBuildMetadata()) && proto.build_event_stream.BuildEventId.BuildMetadataId.toObject(includeInstance, f),
    convenienceSymlinksIdentified: (f = msg.getConvenienceSymlinksIdentified()) && proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId}
 */
proto.build_event_stream.BuildEventId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId;
  return proto.build_event_stream.BuildEventId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId}
 */
proto.build_event_stream.BuildEventId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.BuildEventId.UnknownBuildEventId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.UnknownBuildEventId.deserializeBinaryFromReader);
      msg.setUnknown(value);
      break;
    case 2:
      var value = new proto.build_event_stream.BuildEventId.ProgressId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ProgressId.deserializeBinaryFromReader);
      msg.setProgress(value);
      break;
    case 3:
      var value = new proto.build_event_stream.BuildEventId.BuildStartedId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.BuildStartedId.deserializeBinaryFromReader);
      msg.setStarted(value);
      break;
    case 11:
      var value = new proto.build_event_stream.BuildEventId.UnstructuredCommandLineId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.deserializeBinaryFromReader);
      msg.setUnstructuredCommandLine(value);
      break;
    case 18:
      var value = new proto.build_event_stream.BuildEventId.StructuredCommandLineId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.StructuredCommandLineId.deserializeBinaryFromReader);
      msg.setStructuredCommandLine(value);
      break;
    case 14:
      var value = new proto.build_event_stream.BuildEventId.WorkspaceStatusId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.WorkspaceStatusId.deserializeBinaryFromReader);
      msg.setWorkspaceStatus(value);
      break;
    case 12:
      var value = new proto.build_event_stream.BuildEventId.OptionsParsedId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.OptionsParsedId.deserializeBinaryFromReader);
      msg.setOptionsParsed(value);
      break;
    case 17:
      var value = new proto.build_event_stream.BuildEventId.FetchId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.FetchId.deserializeBinaryFromReader);
      msg.setFetch(value);
      break;
    case 15:
      var value = new proto.build_event_stream.BuildEventId.ConfigurationId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    case 16:
      var value = new proto.build_event_stream.BuildEventId.TargetConfiguredId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.TargetConfiguredId.deserializeBinaryFromReader);
      msg.setTargetConfigured(value);
      break;
    case 4:
      var value = new proto.build_event_stream.BuildEventId.PatternExpandedId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.PatternExpandedId.deserializeBinaryFromReader);
      msg.setPattern(value);
      break;
    case 10:
      var value = new proto.build_event_stream.BuildEventId.PatternExpandedId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.PatternExpandedId.deserializeBinaryFromReader);
      msg.setPatternSkipped(value);
      break;
    case 13:
      var value = new proto.build_event_stream.BuildEventId.NamedSetOfFilesId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.NamedSetOfFilesId.deserializeBinaryFromReader);
      msg.setNamedSet(value);
      break;
    case 5:
      var value = new proto.build_event_stream.BuildEventId.TargetCompletedId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.TargetCompletedId.deserializeBinaryFromReader);
      msg.setTargetCompleted(value);
      break;
    case 6:
      var value = new proto.build_event_stream.BuildEventId.ActionCompletedId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ActionCompletedId.deserializeBinaryFromReader);
      msg.setActionCompleted(value);
      break;
    case 19:
      var value = new proto.build_event_stream.BuildEventId.UnconfiguredLabelId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.UnconfiguredLabelId.deserializeBinaryFromReader);
      msg.setUnconfiguredLabel(value);
      break;
    case 21:
      var value = new proto.build_event_stream.BuildEventId.ConfiguredLabelId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfiguredLabelId.deserializeBinaryFromReader);
      msg.setConfiguredLabel(value);
      break;
    case 8:
      var value = new proto.build_event_stream.BuildEventId.TestResultId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.TestResultId.deserializeBinaryFromReader);
      msg.setTestResult(value);
      break;
    case 7:
      var value = new proto.build_event_stream.BuildEventId.TestSummaryId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.TestSummaryId.deserializeBinaryFromReader);
      msg.setTestSummary(value);
      break;
    case 26:
      var value = new proto.build_event_stream.BuildEventId.TargetSummaryId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.TargetSummaryId.deserializeBinaryFromReader);
      msg.setTargetSummary(value);
      break;
    case 9:
      var value = new proto.build_event_stream.BuildEventId.BuildFinishedId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.BuildFinishedId.deserializeBinaryFromReader);
      msg.setBuildFinished(value);
      break;
    case 20:
      var value = new proto.build_event_stream.BuildEventId.BuildToolLogsId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.BuildToolLogsId.deserializeBinaryFromReader);
      msg.setBuildToolLogs(value);
      break;
    case 22:
      var value = new proto.build_event_stream.BuildEventId.BuildMetricsId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.BuildMetricsId.deserializeBinaryFromReader);
      msg.setBuildMetrics(value);
      break;
    case 23:
      var value = new proto.build_event_stream.BuildEventId.WorkspaceConfigId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.WorkspaceConfigId.deserializeBinaryFromReader);
      msg.setWorkspace(value);
      break;
    case 24:
      var value = new proto.build_event_stream.BuildEventId.BuildMetadataId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.BuildMetadataId.deserializeBinaryFromReader);
      msg.setBuildMetadata(value);
      break;
    case 25:
      var value = new proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.deserializeBinaryFromReader);
      msg.setConvenienceSymlinksIdentified(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUnknown();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.build_event_stream.BuildEventId.UnknownBuildEventId.serializeBinaryToWriter
    );
  }
  f = message.getProgress();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.build_event_stream.BuildEventId.ProgressId.serializeBinaryToWriter
    );
  }
  f = message.getStarted();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.build_event_stream.BuildEventId.BuildStartedId.serializeBinaryToWriter
    );
  }
  f = message.getUnstructuredCommandLine();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.serializeBinaryToWriter
    );
  }
  f = message.getStructuredCommandLine();
  if (f != null) {
    writer.writeMessage(
      18,
      f,
      proto.build_event_stream.BuildEventId.StructuredCommandLineId.serializeBinaryToWriter
    );
  }
  f = message.getWorkspaceStatus();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      proto.build_event_stream.BuildEventId.WorkspaceStatusId.serializeBinaryToWriter
    );
  }
  f = message.getOptionsParsed();
  if (f != null) {
    writer.writeMessage(
      12,
      f,
      proto.build_event_stream.BuildEventId.OptionsParsedId.serializeBinaryToWriter
    );
  }
  f = message.getFetch();
  if (f != null) {
    writer.writeMessage(
      17,
      f,
      proto.build_event_stream.BuildEventId.FetchId.serializeBinaryToWriter
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter
    );
  }
  f = message.getTargetConfigured();
  if (f != null) {
    writer.writeMessage(
      16,
      f,
      proto.build_event_stream.BuildEventId.TargetConfiguredId.serializeBinaryToWriter
    );
  }
  f = message.getPattern();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.build_event_stream.BuildEventId.PatternExpandedId.serializeBinaryToWriter
    );
  }
  f = message.getPatternSkipped();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      proto.build_event_stream.BuildEventId.PatternExpandedId.serializeBinaryToWriter
    );
  }
  f = message.getNamedSet();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      proto.build_event_stream.BuildEventId.NamedSetOfFilesId.serializeBinaryToWriter
    );
  }
  f = message.getTargetCompleted();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.build_event_stream.BuildEventId.TargetCompletedId.serializeBinaryToWriter
    );
  }
  f = message.getActionCompleted();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.build_event_stream.BuildEventId.ActionCompletedId.serializeBinaryToWriter
    );
  }
  f = message.getUnconfiguredLabel();
  if (f != null) {
    writer.writeMessage(
      19,
      f,
      proto.build_event_stream.BuildEventId.UnconfiguredLabelId.serializeBinaryToWriter
    );
  }
  f = message.getConfiguredLabel();
  if (f != null) {
    writer.writeMessage(
      21,
      f,
      proto.build_event_stream.BuildEventId.ConfiguredLabelId.serializeBinaryToWriter
    );
  }
  f = message.getTestResult();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      proto.build_event_stream.BuildEventId.TestResultId.serializeBinaryToWriter
    );
  }
  f = message.getTestSummary();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.build_event_stream.BuildEventId.TestSummaryId.serializeBinaryToWriter
    );
  }
  f = message.getTargetSummary();
  if (f != null) {
    writer.writeMessage(
      26,
      f,
      proto.build_event_stream.BuildEventId.TargetSummaryId.serializeBinaryToWriter
    );
  }
  f = message.getBuildFinished();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      proto.build_event_stream.BuildEventId.BuildFinishedId.serializeBinaryToWriter
    );
  }
  f = message.getBuildToolLogs();
  if (f != null) {
    writer.writeMessage(
      20,
      f,
      proto.build_event_stream.BuildEventId.BuildToolLogsId.serializeBinaryToWriter
    );
  }
  f = message.getBuildMetrics();
  if (f != null) {
    writer.writeMessage(
      22,
      f,
      proto.build_event_stream.BuildEventId.BuildMetricsId.serializeBinaryToWriter
    );
  }
  f = message.getWorkspace();
  if (f != null) {
    writer.writeMessage(
      23,
      f,
      proto.build_event_stream.BuildEventId.WorkspaceConfigId.serializeBinaryToWriter
    );
  }
  f = message.getBuildMetadata();
  if (f != null) {
    writer.writeMessage(
      24,
      f,
      proto.build_event_stream.BuildEventId.BuildMetadataId.serializeBinaryToWriter
    );
  }
  f = message.getConvenienceSymlinksIdentified();
  if (f != null) {
    writer.writeMessage(
      25,
      f,
      proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.UnknownBuildEventId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.UnknownBuildEventId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId.toObject = function(includeInstance, msg) {
  var f, obj = {
    details: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.UnknownBuildEventId}
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.UnknownBuildEventId;
  return proto.build_event_stream.BuildEventId.UnknownBuildEventId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.UnknownBuildEventId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.UnknownBuildEventId}
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setDetails(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.UnknownBuildEventId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.UnknownBuildEventId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDetails();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string details = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId.prototype.getDetails = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.UnknownBuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.UnknownBuildEventId.prototype.setDetails = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.ProgressId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.ProgressId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.ProgressId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ProgressId.toObject = function(includeInstance, msg) {
  var f, obj = {
    opaqueCount: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.ProgressId}
 */
proto.build_event_stream.BuildEventId.ProgressId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.ProgressId;
  return proto.build_event_stream.BuildEventId.ProgressId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.ProgressId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.ProgressId}
 */
proto.build_event_stream.BuildEventId.ProgressId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setOpaqueCount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.ProgressId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.ProgressId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.ProgressId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ProgressId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOpaqueCount();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
};


/**
 * optional int32 opaque_count = 1;
 * @return {number}
 */
proto.build_event_stream.BuildEventId.ProgressId.prototype.getOpaqueCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildEventId.ProgressId} returns this
 */
proto.build_event_stream.BuildEventId.ProgressId.prototype.setOpaqueCount = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.BuildStartedId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.BuildStartedId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.BuildStartedId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildStartedId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.BuildStartedId}
 */
proto.build_event_stream.BuildEventId.BuildStartedId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.BuildStartedId;
  return proto.build_event_stream.BuildEventId.BuildStartedId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.BuildStartedId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.BuildStartedId}
 */
proto.build_event_stream.BuildEventId.BuildStartedId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.BuildStartedId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.BuildStartedId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.BuildStartedId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildStartedId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.UnstructuredCommandLineId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.UnstructuredCommandLineId}
 */
proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.UnstructuredCommandLineId;
  return proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.UnstructuredCommandLineId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.UnstructuredCommandLineId}
 */
proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.UnstructuredCommandLineId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.UnstructuredCommandLineId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.StructuredCommandLineId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.StructuredCommandLineId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId.toObject = function(includeInstance, msg) {
  var f, obj = {
    commandLineLabel: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.StructuredCommandLineId}
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.StructuredCommandLineId;
  return proto.build_event_stream.BuildEventId.StructuredCommandLineId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.StructuredCommandLineId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.StructuredCommandLineId}
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setCommandLineLabel(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.StructuredCommandLineId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.StructuredCommandLineId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCommandLineLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string command_line_label = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId.prototype.getCommandLineLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.StructuredCommandLineId} returns this
 */
proto.build_event_stream.BuildEventId.StructuredCommandLineId.prototype.setCommandLineLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.WorkspaceStatusId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.WorkspaceStatusId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.WorkspaceStatusId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.WorkspaceStatusId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.WorkspaceStatusId}
 */
proto.build_event_stream.BuildEventId.WorkspaceStatusId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.WorkspaceStatusId;
  return proto.build_event_stream.BuildEventId.WorkspaceStatusId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.WorkspaceStatusId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.WorkspaceStatusId}
 */
proto.build_event_stream.BuildEventId.WorkspaceStatusId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.WorkspaceStatusId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.WorkspaceStatusId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.WorkspaceStatusId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.WorkspaceStatusId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.OptionsParsedId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.OptionsParsedId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.OptionsParsedId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.OptionsParsedId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.OptionsParsedId}
 */
proto.build_event_stream.BuildEventId.OptionsParsedId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.OptionsParsedId;
  return proto.build_event_stream.BuildEventId.OptionsParsedId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.OptionsParsedId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.OptionsParsedId}
 */
proto.build_event_stream.BuildEventId.OptionsParsedId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.OptionsParsedId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.OptionsParsedId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.OptionsParsedId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.OptionsParsedId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.FetchId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.FetchId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.FetchId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.FetchId.toObject = function(includeInstance, msg) {
  var f, obj = {
    url: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.FetchId}
 */
proto.build_event_stream.BuildEventId.FetchId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.FetchId;
  return proto.build_event_stream.BuildEventId.FetchId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.FetchId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.FetchId}
 */
proto.build_event_stream.BuildEventId.FetchId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUrl(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.FetchId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.FetchId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.FetchId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.FetchId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUrl();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string url = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.FetchId.prototype.getUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.FetchId} returns this
 */
proto.build_event_stream.BuildEventId.FetchId.prototype.setUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.PatternExpandedId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.PatternExpandedId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.toObject = function(includeInstance, msg) {
  var f, obj = {
    patternList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.PatternExpandedId}
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.PatternExpandedId;
  return proto.build_event_stream.BuildEventId.PatternExpandedId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.PatternExpandedId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.PatternExpandedId}
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addPattern(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.PatternExpandedId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.PatternExpandedId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPatternList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
};


/**
 * repeated string pattern = 1;
 * @return {!Array<string>}
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.prototype.getPatternList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.BuildEventId.PatternExpandedId} returns this
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.prototype.setPatternList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildEventId.PatternExpandedId} returns this
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.prototype.addPattern = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.BuildEventId.PatternExpandedId} returns this
 */
proto.build_event_stream.BuildEventId.PatternExpandedId.prototype.clearPatternList = function() {
  return this.setPatternList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.WorkspaceConfigId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.WorkspaceConfigId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.WorkspaceConfigId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.WorkspaceConfigId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.WorkspaceConfigId}
 */
proto.build_event_stream.BuildEventId.WorkspaceConfigId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.WorkspaceConfigId;
  return proto.build_event_stream.BuildEventId.WorkspaceConfigId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.WorkspaceConfigId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.WorkspaceConfigId}
 */
proto.build_event_stream.BuildEventId.WorkspaceConfigId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.WorkspaceConfigId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.WorkspaceConfigId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.WorkspaceConfigId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.WorkspaceConfigId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.BuildMetadataId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.BuildMetadataId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.BuildMetadataId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildMetadataId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.BuildMetadataId}
 */
proto.build_event_stream.BuildEventId.BuildMetadataId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.BuildMetadataId;
  return proto.build_event_stream.BuildEventId.BuildMetadataId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.BuildMetadataId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.BuildMetadataId}
 */
proto.build_event_stream.BuildEventId.BuildMetadataId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.BuildMetadataId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.BuildMetadataId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.BuildMetadataId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildMetadataId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.TargetConfiguredId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.TargetConfiguredId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.toObject = function(includeInstance, msg) {
  var f, obj = {
    label: jspb.Message.getFieldWithDefault(msg, 1, ""),
    aspect: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.TargetConfiguredId}
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.TargetConfiguredId;
  return proto.build_event_stream.BuildEventId.TargetConfiguredId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.TargetConfiguredId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.TargetConfiguredId}
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setAspect(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.TargetConfiguredId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.TargetConfiguredId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getAspect();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string label = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.TargetConfiguredId} returns this
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string aspect = 2;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.prototype.getAspect = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.TargetConfiguredId} returns this
 */
proto.build_event_stream.BuildEventId.TargetConfiguredId.prototype.setAspect = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.NamedSetOfFilesId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId}
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.NamedSetOfFilesId;
  return proto.build_event_stream.BuildEventId.NamedSetOfFilesId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId}
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.NamedSetOfFilesId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId} returns this
 */
proto.build_event_stream.BuildEventId.NamedSetOfFilesId.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.ConfigurationId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.ConfigurationId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.ConfigurationId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ConfigurationId.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.ConfigurationId;
  return proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.ConfigurationId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.ConfigurationId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.ConfigurationId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.ConfigurationId.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.ConfigurationId} returns this
 */
proto.build_event_stream.BuildEventId.ConfigurationId.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.TargetCompletedId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.TargetCompletedId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.toObject = function(includeInstance, msg) {
  var f, obj = {
    label: jspb.Message.getFieldWithDefault(msg, 1, ""),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.BuildEventId.ConfigurationId.toObject(includeInstance, f),
    aspect: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.TargetCompletedId}
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.TargetCompletedId;
  return proto.build_event_stream.BuildEventId.TargetCompletedId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.TargetCompletedId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.TargetCompletedId}
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    case 3:
      var value = new proto.build_event_stream.BuildEventId.ConfigurationId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setAspect(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.TargetCompletedId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.TargetCompletedId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter
    );
  }
  f = message.getAspect();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string label = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.TargetCompletedId} returns this
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional ConfigurationId configuration = 3;
 * @return {?proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfigurationId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfigurationId, 3));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfigurationId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId.TargetCompletedId} returns this
*/
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.setConfiguration = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId.TargetCompletedId} returns this
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional string aspect = 2;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.getAspect = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.TargetCompletedId} returns this
 */
proto.build_event_stream.BuildEventId.TargetCompletedId.prototype.setAspect = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.ActionCompletedId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.ActionCompletedId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.toObject = function(includeInstance, msg) {
  var f, obj = {
    primaryOutput: jspb.Message.getFieldWithDefault(msg, 1, ""),
    label: jspb.Message.getFieldWithDefault(msg, 2, ""),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.BuildEventId.ConfigurationId.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.ActionCompletedId}
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.ActionCompletedId;
  return proto.build_event_stream.BuildEventId.ActionCompletedId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.ActionCompletedId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.ActionCompletedId}
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPrimaryOutput(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    case 3:
      var value = new proto.build_event_stream.BuildEventId.ConfigurationId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.ActionCompletedId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.ActionCompletedId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPrimaryOutput();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter
    );
  }
};


/**
 * optional string primary_output = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.getPrimaryOutput = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.ActionCompletedId} returns this
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.setPrimaryOutput = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string label = 2;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.ActionCompletedId} returns this
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional ConfigurationId configuration = 3;
 * @return {?proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfigurationId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfigurationId, 3));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfigurationId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId.ActionCompletedId} returns this
*/
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.setConfiguration = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId.ActionCompletedId} returns this
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.ActionCompletedId.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 3) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.UnconfiguredLabelId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.UnconfiguredLabelId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId.toObject = function(includeInstance, msg) {
  var f, obj = {
    label: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.UnconfiguredLabelId}
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.UnconfiguredLabelId;
  return proto.build_event_stream.BuildEventId.UnconfiguredLabelId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.UnconfiguredLabelId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.UnconfiguredLabelId}
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.UnconfiguredLabelId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.UnconfiguredLabelId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string label = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.UnconfiguredLabelId} returns this
 */
proto.build_event_stream.BuildEventId.UnconfiguredLabelId.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.ConfiguredLabelId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.ConfiguredLabelId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.toObject = function(includeInstance, msg) {
  var f, obj = {
    label: jspb.Message.getFieldWithDefault(msg, 1, ""),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.BuildEventId.ConfigurationId.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.ConfiguredLabelId}
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.ConfiguredLabelId;
  return proto.build_event_stream.BuildEventId.ConfiguredLabelId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.ConfiguredLabelId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.ConfiguredLabelId}
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    case 2:
      var value = new proto.build_event_stream.BuildEventId.ConfigurationId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.ConfiguredLabelId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.ConfiguredLabelId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter
    );
  }
};


/**
 * optional string label = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.ConfiguredLabelId} returns this
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional ConfigurationId configuration = 2;
 * @return {?proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfigurationId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfigurationId, 2));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfigurationId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId.ConfiguredLabelId} returns this
*/
proto.build_event_stream.BuildEventId.ConfiguredLabelId.prototype.setConfiguration = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId.ConfiguredLabelId} returns this
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.ConfiguredLabelId.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.TestResultId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.TestResultId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TestResultId.toObject = function(includeInstance, msg) {
  var f, obj = {
    label: jspb.Message.getFieldWithDefault(msg, 1, ""),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.BuildEventId.ConfigurationId.toObject(includeInstance, f),
    run: jspb.Message.getFieldWithDefault(msg, 2, 0),
    shard: jspb.Message.getFieldWithDefault(msg, 3, 0),
    attempt: jspb.Message.getFieldWithDefault(msg, 4, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.TestResultId}
 */
proto.build_event_stream.BuildEventId.TestResultId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.TestResultId;
  return proto.build_event_stream.BuildEventId.TestResultId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.TestResultId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.TestResultId}
 */
proto.build_event_stream.BuildEventId.TestResultId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    case 5:
      var value = new proto.build_event_stream.BuildEventId.ConfigurationId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setRun(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setShard(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setAttempt(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.TestResultId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.TestResultId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TestResultId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter
    );
  }
  f = message.getRun();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getShard();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getAttempt();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
};


/**
 * optional string label = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.TestResultId} returns this
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional ConfigurationId configuration = 5;
 * @return {?proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfigurationId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfigurationId, 5));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfigurationId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId.TestResultId} returns this
*/
proto.build_event_stream.BuildEventId.TestResultId.prototype.setConfiguration = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId.TestResultId} returns this
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional int32 run = 2;
 * @return {number}
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.getRun = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildEventId.TestResultId} returns this
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.setRun = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int32 shard = 3;
 * @return {number}
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.getShard = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildEventId.TestResultId} returns this
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.setShard = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 attempt = 4;
 * @return {number}
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.getAttempt = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildEventId.TestResultId} returns this
 */
proto.build_event_stream.BuildEventId.TestResultId.prototype.setAttempt = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.TestSummaryId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.TestSummaryId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.TestSummaryId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TestSummaryId.toObject = function(includeInstance, msg) {
  var f, obj = {
    label: jspb.Message.getFieldWithDefault(msg, 1, ""),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.BuildEventId.ConfigurationId.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.TestSummaryId}
 */
proto.build_event_stream.BuildEventId.TestSummaryId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.TestSummaryId;
  return proto.build_event_stream.BuildEventId.TestSummaryId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.TestSummaryId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.TestSummaryId}
 */
proto.build_event_stream.BuildEventId.TestSummaryId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    case 2:
      var value = new proto.build_event_stream.BuildEventId.ConfigurationId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.TestSummaryId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.TestSummaryId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.TestSummaryId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TestSummaryId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter
    );
  }
};


/**
 * optional string label = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.TestSummaryId.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.TestSummaryId} returns this
 */
proto.build_event_stream.BuildEventId.TestSummaryId.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional ConfigurationId configuration = 2;
 * @return {?proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.TestSummaryId.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfigurationId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfigurationId, 2));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfigurationId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId.TestSummaryId} returns this
*/
proto.build_event_stream.BuildEventId.TestSummaryId.prototype.setConfiguration = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId.TestSummaryId} returns this
 */
proto.build_event_stream.BuildEventId.TestSummaryId.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.TestSummaryId.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.TargetSummaryId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.TargetSummaryId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.toObject = function(includeInstance, msg) {
  var f, obj = {
    label: jspb.Message.getFieldWithDefault(msg, 1, ""),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.BuildEventId.ConfigurationId.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.TargetSummaryId}
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.TargetSummaryId;
  return proto.build_event_stream.BuildEventId.TargetSummaryId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.TargetSummaryId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.TargetSummaryId}
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    case 2:
      var value = new proto.build_event_stream.BuildEventId.ConfigurationId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.TargetSummaryId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.TargetSummaryId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter
    );
  }
};


/**
 * optional string label = 1;
 * @return {string}
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildEventId.TargetSummaryId} returns this
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional ConfigurationId configuration = 2;
 * @return {?proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfigurationId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfigurationId, 2));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfigurationId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId.TargetSummaryId} returns this
*/
proto.build_event_stream.BuildEventId.TargetSummaryId.prototype.setConfiguration = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId.TargetSummaryId} returns this
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.TargetSummaryId.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.BuildFinishedId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.BuildFinishedId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.BuildFinishedId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildFinishedId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.BuildFinishedId}
 */
proto.build_event_stream.BuildEventId.BuildFinishedId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.BuildFinishedId;
  return proto.build_event_stream.BuildEventId.BuildFinishedId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.BuildFinishedId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.BuildFinishedId}
 */
proto.build_event_stream.BuildEventId.BuildFinishedId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.BuildFinishedId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.BuildFinishedId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.BuildFinishedId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildFinishedId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.BuildToolLogsId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.BuildToolLogsId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.BuildToolLogsId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildToolLogsId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.BuildToolLogsId}
 */
proto.build_event_stream.BuildEventId.BuildToolLogsId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.BuildToolLogsId;
  return proto.build_event_stream.BuildEventId.BuildToolLogsId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.BuildToolLogsId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.BuildToolLogsId}
 */
proto.build_event_stream.BuildEventId.BuildToolLogsId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.BuildToolLogsId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.BuildToolLogsId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.BuildToolLogsId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildToolLogsId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.BuildMetricsId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.BuildMetricsId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.BuildMetricsId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildMetricsId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.BuildMetricsId}
 */
proto.build_event_stream.BuildEventId.BuildMetricsId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.BuildMetricsId;
  return proto.build_event_stream.BuildEventId.BuildMetricsId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.BuildMetricsId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.BuildMetricsId}
 */
proto.build_event_stream.BuildEventId.BuildMetricsId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.BuildMetricsId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.BuildMetricsId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.BuildMetricsId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.BuildMetricsId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId}
 */
proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId;
  return proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId}
 */
proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};


/**
 * optional UnknownBuildEventId unknown = 1;
 * @return {?proto.build_event_stream.BuildEventId.UnknownBuildEventId}
 */
proto.build_event_stream.BuildEventId.prototype.getUnknown = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.UnknownBuildEventId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.UnknownBuildEventId, 1));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.UnknownBuildEventId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setUnknown = function(value) {
  return jspb.Message.setOneofWrapperField(this, 1, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearUnknown = function() {
  return this.setUnknown(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasUnknown = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional ProgressId progress = 2;
 * @return {?proto.build_event_stream.BuildEventId.ProgressId}
 */
proto.build_event_stream.BuildEventId.prototype.getProgress = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ProgressId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ProgressId, 2));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ProgressId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setProgress = function(value) {
  return jspb.Message.setOneofWrapperField(this, 2, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearProgress = function() {
  return this.setProgress(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasProgress = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional BuildStartedId started = 3;
 * @return {?proto.build_event_stream.BuildEventId.BuildStartedId}
 */
proto.build_event_stream.BuildEventId.prototype.getStarted = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.BuildStartedId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.BuildStartedId, 3));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.BuildStartedId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setStarted = function(value) {
  return jspb.Message.setOneofWrapperField(this, 3, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearStarted = function() {
  return this.setStarted(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasStarted = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional UnstructuredCommandLineId unstructured_command_line = 11;
 * @return {?proto.build_event_stream.BuildEventId.UnstructuredCommandLineId}
 */
proto.build_event_stream.BuildEventId.prototype.getUnstructuredCommandLine = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.UnstructuredCommandLineId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.UnstructuredCommandLineId, 11));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.UnstructuredCommandLineId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setUnstructuredCommandLine = function(value) {
  return jspb.Message.setOneofWrapperField(this, 11, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearUnstructuredCommandLine = function() {
  return this.setUnstructuredCommandLine(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasUnstructuredCommandLine = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional StructuredCommandLineId structured_command_line = 18;
 * @return {?proto.build_event_stream.BuildEventId.StructuredCommandLineId}
 */
proto.build_event_stream.BuildEventId.prototype.getStructuredCommandLine = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.StructuredCommandLineId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.StructuredCommandLineId, 18));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.StructuredCommandLineId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setStructuredCommandLine = function(value) {
  return jspb.Message.setOneofWrapperField(this, 18, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearStructuredCommandLine = function() {
  return this.setStructuredCommandLine(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasStructuredCommandLine = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * optional WorkspaceStatusId workspace_status = 14;
 * @return {?proto.build_event_stream.BuildEventId.WorkspaceStatusId}
 */
proto.build_event_stream.BuildEventId.prototype.getWorkspaceStatus = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.WorkspaceStatusId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.WorkspaceStatusId, 14));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.WorkspaceStatusId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setWorkspaceStatus = function(value) {
  return jspb.Message.setOneofWrapperField(this, 14, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearWorkspaceStatus = function() {
  return this.setWorkspaceStatus(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasWorkspaceStatus = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional OptionsParsedId options_parsed = 12;
 * @return {?proto.build_event_stream.BuildEventId.OptionsParsedId}
 */
proto.build_event_stream.BuildEventId.prototype.getOptionsParsed = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.OptionsParsedId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.OptionsParsedId, 12));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.OptionsParsedId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setOptionsParsed = function(value) {
  return jspb.Message.setOneofWrapperField(this, 12, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearOptionsParsed = function() {
  return this.setOptionsParsed(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasOptionsParsed = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional FetchId fetch = 17;
 * @return {?proto.build_event_stream.BuildEventId.FetchId}
 */
proto.build_event_stream.BuildEventId.prototype.getFetch = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.FetchId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.FetchId, 17));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.FetchId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setFetch = function(value) {
  return jspb.Message.setOneofWrapperField(this, 17, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearFetch = function() {
  return this.setFetch(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasFetch = function() {
  return jspb.Message.getField(this, 17) != null;
};


/**
 * optional ConfigurationId configuration = 15;
 * @return {?proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.BuildEventId.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfigurationId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfigurationId, 15));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfigurationId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setConfiguration = function(value) {
  return jspb.Message.setOneofWrapperField(this, 15, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional TargetConfiguredId target_configured = 16;
 * @return {?proto.build_event_stream.BuildEventId.TargetConfiguredId}
 */
proto.build_event_stream.BuildEventId.prototype.getTargetConfigured = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.TargetConfiguredId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.TargetConfiguredId, 16));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.TargetConfiguredId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setTargetConfigured = function(value) {
  return jspb.Message.setOneofWrapperField(this, 16, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearTargetConfigured = function() {
  return this.setTargetConfigured(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasTargetConfigured = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * optional PatternExpandedId pattern = 4;
 * @return {?proto.build_event_stream.BuildEventId.PatternExpandedId}
 */
proto.build_event_stream.BuildEventId.prototype.getPattern = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.PatternExpandedId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.PatternExpandedId, 4));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.PatternExpandedId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setPattern = function(value) {
  return jspb.Message.setOneofWrapperField(this, 4, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearPattern = function() {
  return this.setPattern(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasPattern = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional PatternExpandedId pattern_skipped = 10;
 * @return {?proto.build_event_stream.BuildEventId.PatternExpandedId}
 */
proto.build_event_stream.BuildEventId.prototype.getPatternSkipped = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.PatternExpandedId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.PatternExpandedId, 10));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.PatternExpandedId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setPatternSkipped = function(value) {
  return jspb.Message.setOneofWrapperField(this, 10, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearPatternSkipped = function() {
  return this.setPatternSkipped(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasPatternSkipped = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional NamedSetOfFilesId named_set = 13;
 * @return {?proto.build_event_stream.BuildEventId.NamedSetOfFilesId}
 */
proto.build_event_stream.BuildEventId.prototype.getNamedSet = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.NamedSetOfFilesId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.NamedSetOfFilesId, 13));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.NamedSetOfFilesId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setNamedSet = function(value) {
  return jspb.Message.setOneofWrapperField(this, 13, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearNamedSet = function() {
  return this.setNamedSet(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasNamedSet = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional TargetCompletedId target_completed = 5;
 * @return {?proto.build_event_stream.BuildEventId.TargetCompletedId}
 */
proto.build_event_stream.BuildEventId.prototype.getTargetCompleted = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.TargetCompletedId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.TargetCompletedId, 5));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.TargetCompletedId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setTargetCompleted = function(value) {
  return jspb.Message.setOneofWrapperField(this, 5, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearTargetCompleted = function() {
  return this.setTargetCompleted(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasTargetCompleted = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional ActionCompletedId action_completed = 6;
 * @return {?proto.build_event_stream.BuildEventId.ActionCompletedId}
 */
proto.build_event_stream.BuildEventId.prototype.getActionCompleted = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ActionCompletedId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ActionCompletedId, 6));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ActionCompletedId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setActionCompleted = function(value) {
  return jspb.Message.setOneofWrapperField(this, 6, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearActionCompleted = function() {
  return this.setActionCompleted(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasActionCompleted = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional UnconfiguredLabelId unconfigured_label = 19;
 * @return {?proto.build_event_stream.BuildEventId.UnconfiguredLabelId}
 */
proto.build_event_stream.BuildEventId.prototype.getUnconfiguredLabel = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.UnconfiguredLabelId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.UnconfiguredLabelId, 19));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.UnconfiguredLabelId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setUnconfiguredLabel = function(value) {
  return jspb.Message.setOneofWrapperField(this, 19, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearUnconfiguredLabel = function() {
  return this.setUnconfiguredLabel(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasUnconfiguredLabel = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional ConfiguredLabelId configured_label = 21;
 * @return {?proto.build_event_stream.BuildEventId.ConfiguredLabelId}
 */
proto.build_event_stream.BuildEventId.prototype.getConfiguredLabel = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfiguredLabelId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfiguredLabelId, 21));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfiguredLabelId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setConfiguredLabel = function(value) {
  return jspb.Message.setOneofWrapperField(this, 21, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearConfiguredLabel = function() {
  return this.setConfiguredLabel(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasConfiguredLabel = function() {
  return jspb.Message.getField(this, 21) != null;
};


/**
 * optional TestResultId test_result = 8;
 * @return {?proto.build_event_stream.BuildEventId.TestResultId}
 */
proto.build_event_stream.BuildEventId.prototype.getTestResult = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.TestResultId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.TestResultId, 8));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.TestResultId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setTestResult = function(value) {
  return jspb.Message.setOneofWrapperField(this, 8, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearTestResult = function() {
  return this.setTestResult(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasTestResult = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional TestSummaryId test_summary = 7;
 * @return {?proto.build_event_stream.BuildEventId.TestSummaryId}
 */
proto.build_event_stream.BuildEventId.prototype.getTestSummary = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.TestSummaryId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.TestSummaryId, 7));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.TestSummaryId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setTestSummary = function(value) {
  return jspb.Message.setOneofWrapperField(this, 7, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearTestSummary = function() {
  return this.setTestSummary(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasTestSummary = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional TargetSummaryId target_summary = 26;
 * @return {?proto.build_event_stream.BuildEventId.TargetSummaryId}
 */
proto.build_event_stream.BuildEventId.prototype.getTargetSummary = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.TargetSummaryId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.TargetSummaryId, 26));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.TargetSummaryId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setTargetSummary = function(value) {
  return jspb.Message.setOneofWrapperField(this, 26, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearTargetSummary = function() {
  return this.setTargetSummary(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasTargetSummary = function() {
  return jspb.Message.getField(this, 26) != null;
};


/**
 * optional BuildFinishedId build_finished = 9;
 * @return {?proto.build_event_stream.BuildEventId.BuildFinishedId}
 */
proto.build_event_stream.BuildEventId.prototype.getBuildFinished = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.BuildFinishedId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.BuildFinishedId, 9));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.BuildFinishedId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setBuildFinished = function(value) {
  return jspb.Message.setOneofWrapperField(this, 9, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearBuildFinished = function() {
  return this.setBuildFinished(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasBuildFinished = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional BuildToolLogsId build_tool_logs = 20;
 * @return {?proto.build_event_stream.BuildEventId.BuildToolLogsId}
 */
proto.build_event_stream.BuildEventId.prototype.getBuildToolLogs = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.BuildToolLogsId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.BuildToolLogsId, 20));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.BuildToolLogsId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setBuildToolLogs = function(value) {
  return jspb.Message.setOneofWrapperField(this, 20, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearBuildToolLogs = function() {
  return this.setBuildToolLogs(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasBuildToolLogs = function() {
  return jspb.Message.getField(this, 20) != null;
};


/**
 * optional BuildMetricsId build_metrics = 22;
 * @return {?proto.build_event_stream.BuildEventId.BuildMetricsId}
 */
proto.build_event_stream.BuildEventId.prototype.getBuildMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.BuildMetricsId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.BuildMetricsId, 22));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.BuildMetricsId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setBuildMetrics = function(value) {
  return jspb.Message.setOneofWrapperField(this, 22, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearBuildMetrics = function() {
  return this.setBuildMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasBuildMetrics = function() {
  return jspb.Message.getField(this, 22) != null;
};


/**
 * optional WorkspaceConfigId workspace = 23;
 * @return {?proto.build_event_stream.BuildEventId.WorkspaceConfigId}
 */
proto.build_event_stream.BuildEventId.prototype.getWorkspace = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.WorkspaceConfigId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.WorkspaceConfigId, 23));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.WorkspaceConfigId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setWorkspace = function(value) {
  return jspb.Message.setOneofWrapperField(this, 23, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearWorkspace = function() {
  return this.setWorkspace(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasWorkspace = function() {
  return jspb.Message.getField(this, 23) != null;
};


/**
 * optional BuildMetadataId build_metadata = 24;
 * @return {?proto.build_event_stream.BuildEventId.BuildMetadataId}
 */
proto.build_event_stream.BuildEventId.prototype.getBuildMetadata = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.BuildMetadataId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.BuildMetadataId, 24));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.BuildMetadataId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setBuildMetadata = function(value) {
  return jspb.Message.setOneofWrapperField(this, 24, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearBuildMetadata = function() {
  return this.setBuildMetadata(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasBuildMetadata = function() {
  return jspb.Message.getField(this, 24) != null;
};


/**
 * optional ConvenienceSymlinksIdentifiedId convenience_symlinks_identified = 25;
 * @return {?proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId}
 */
proto.build_event_stream.BuildEventId.prototype.getConvenienceSymlinksIdentified = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId, 25));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConvenienceSymlinksIdentifiedId|undefined} value
 * @return {!proto.build_event_stream.BuildEventId} returns this
*/
proto.build_event_stream.BuildEventId.prototype.setConvenienceSymlinksIdentified = function(value) {
  return jspb.Message.setOneofWrapperField(this, 25, proto.build_event_stream.BuildEventId.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEventId} returns this
 */
proto.build_event_stream.BuildEventId.prototype.clearConvenienceSymlinksIdentified = function() {
  return this.setConvenienceSymlinksIdentified(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEventId.prototype.hasConvenienceSymlinksIdentified = function() {
  return jspb.Message.getField(this, 25) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.Progress.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.Progress.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.Progress} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.Progress.toObject = function(includeInstance, msg) {
  var f, obj = {
    stdout: jspb.Message.getFieldWithDefault(msg, 1, ""),
    stderr: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.Progress}
 */
proto.build_event_stream.Progress.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.Progress;
  return proto.build_event_stream.Progress.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.Progress} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.Progress}
 */
proto.build_event_stream.Progress.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStdout(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setStderr(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.Progress.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.Progress.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.Progress} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.Progress.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStdout();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getStderr();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string stdout = 1;
 * @return {string}
 */
proto.build_event_stream.Progress.prototype.getStdout = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.Progress} returns this
 */
proto.build_event_stream.Progress.prototype.setStdout = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string stderr = 2;
 * @return {string}
 */
proto.build_event_stream.Progress.prototype.getStderr = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.Progress} returns this
 */
proto.build_event_stream.Progress.prototype.setStderr = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.Aborted.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.Aborted.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.Aborted} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.Aborted.toObject = function(includeInstance, msg) {
  var f, obj = {
    reason: jspb.Message.getFieldWithDefault(msg, 1, 0),
    description: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.Aborted}
 */
proto.build_event_stream.Aborted.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.Aborted;
  return proto.build_event_stream.Aborted.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.Aborted} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.Aborted}
 */
proto.build_event_stream.Aborted.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.build_event_stream.Aborted.AbortReason} */ (reader.readEnum());
      msg.setReason(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.Aborted.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.Aborted.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.Aborted} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.Aborted.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getReason();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.build_event_stream.Aborted.AbortReason = {
  UNKNOWN: 0,
  USER_INTERRUPTED: 1,
  NO_ANALYZE: 8,
  NO_BUILD: 9,
  TIME_OUT: 2,
  REMOTE_ENVIRONMENT_FAILURE: 3,
  INTERNAL: 4,
  LOADING_FAILURE: 5,
  ANALYSIS_FAILURE: 6,
  SKIPPED: 7,
  INCOMPLETE: 10,
  OUT_OF_MEMORY: 11
};

/**
 * optional AbortReason reason = 1;
 * @return {!proto.build_event_stream.Aborted.AbortReason}
 */
proto.build_event_stream.Aborted.prototype.getReason = function() {
  return /** @type {!proto.build_event_stream.Aborted.AbortReason} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.build_event_stream.Aborted.AbortReason} value
 * @return {!proto.build_event_stream.Aborted} returns this
 */
proto.build_event_stream.Aborted.prototype.setReason = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string description = 2;
 * @return {string}
 */
proto.build_event_stream.Aborted.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.Aborted} returns this
 */
proto.build_event_stream.Aborted.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildStarted.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildStarted.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildStarted} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildStarted.toObject = function(includeInstance, msg) {
  var f, obj = {
    uuid: jspb.Message.getFieldWithDefault(msg, 1, ""),
    startTimeMillis: jspb.Message.getFieldWithDefault(msg, 2, 0),
    startTime: (f = msg.getStartTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    buildToolVersion: jspb.Message.getFieldWithDefault(msg, 3, ""),
    optionsDescription: jspb.Message.getFieldWithDefault(msg, 4, ""),
    command: jspb.Message.getFieldWithDefault(msg, 5, ""),
    workingDirectory: jspb.Message.getFieldWithDefault(msg, 6, ""),
    workspaceDirectory: jspb.Message.getFieldWithDefault(msg, 7, ""),
    serverPid: jspb.Message.getFieldWithDefault(msg, 8, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildStarted}
 */
proto.build_event_stream.BuildStarted.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildStarted;
  return proto.build_event_stream.BuildStarted.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildStarted} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildStarted}
 */
proto.build_event_stream.BuildStarted.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUuid(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setStartTimeMillis(value);
      break;
    case 9:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setStartTime(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setBuildToolVersion(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setOptionsDescription(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setCommand(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setWorkingDirectory(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setWorkspaceDirectory(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setServerPid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildStarted.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildStarted.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildStarted} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildStarted.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUuid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getStartTimeMillis();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getStartTime();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getBuildToolVersion();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getOptionsDescription();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getCommand();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getWorkingDirectory();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getWorkspaceDirectory();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getServerPid();
  if (f !== 0) {
    writer.writeInt64(
      8,
      f
    );
  }
};


/**
 * optional string uuid = 1;
 * @return {string}
 */
proto.build_event_stream.BuildStarted.prototype.getUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.setUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int64 start_time_millis = 2;
 * @return {number}
 */
proto.build_event_stream.BuildStarted.prototype.getStartTimeMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.setStartTimeMillis = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional google.protobuf.Timestamp start_time = 9;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.build_event_stream.BuildStarted.prototype.getStartTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 9));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
*/
proto.build_event_stream.BuildStarted.prototype.setStartTime = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.clearStartTime = function() {
  return this.setStartTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildStarted.prototype.hasStartTime = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional string build_tool_version = 3;
 * @return {string}
 */
proto.build_event_stream.BuildStarted.prototype.getBuildToolVersion = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.setBuildToolVersion = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string options_description = 4;
 * @return {string}
 */
proto.build_event_stream.BuildStarted.prototype.getOptionsDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.setOptionsDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string command = 5;
 * @return {string}
 */
proto.build_event_stream.BuildStarted.prototype.getCommand = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.setCommand = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string working_directory = 6;
 * @return {string}
 */
proto.build_event_stream.BuildStarted.prototype.getWorkingDirectory = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.setWorkingDirectory = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string workspace_directory = 7;
 * @return {string}
 */
proto.build_event_stream.BuildStarted.prototype.getWorkspaceDirectory = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.setWorkspaceDirectory = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional int64 server_pid = 8;
 * @return {number}
 */
proto.build_event_stream.BuildStarted.prototype.getServerPid = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildStarted} returns this
 */
proto.build_event_stream.BuildStarted.prototype.setServerPid = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.WorkspaceConfig.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.WorkspaceConfig.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.WorkspaceConfig} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.WorkspaceConfig.toObject = function(includeInstance, msg) {
  var f, obj = {
    localExecRoot: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.WorkspaceConfig}
 */
proto.build_event_stream.WorkspaceConfig.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.WorkspaceConfig;
  return proto.build_event_stream.WorkspaceConfig.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.WorkspaceConfig} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.WorkspaceConfig}
 */
proto.build_event_stream.WorkspaceConfig.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLocalExecRoot(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.WorkspaceConfig.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.WorkspaceConfig.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.WorkspaceConfig} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.WorkspaceConfig.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLocalExecRoot();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string local_exec_root = 1;
 * @return {string}
 */
proto.build_event_stream.WorkspaceConfig.prototype.getLocalExecRoot = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.WorkspaceConfig} returns this
 */
proto.build_event_stream.WorkspaceConfig.prototype.setLocalExecRoot = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.UnstructuredCommandLine.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.UnstructuredCommandLine.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.UnstructuredCommandLine.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.UnstructuredCommandLine} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.UnstructuredCommandLine.toObject = function(includeInstance, msg) {
  var f, obj = {
    argsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.UnstructuredCommandLine}
 */
proto.build_event_stream.UnstructuredCommandLine.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.UnstructuredCommandLine;
  return proto.build_event_stream.UnstructuredCommandLine.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.UnstructuredCommandLine} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.UnstructuredCommandLine}
 */
proto.build_event_stream.UnstructuredCommandLine.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addArgs(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.UnstructuredCommandLine.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.UnstructuredCommandLine.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.UnstructuredCommandLine} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.UnstructuredCommandLine.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getArgsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
};


/**
 * repeated string args = 1;
 * @return {!Array<string>}
 */
proto.build_event_stream.UnstructuredCommandLine.prototype.getArgsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.UnstructuredCommandLine} returns this
 */
proto.build_event_stream.UnstructuredCommandLine.prototype.setArgsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.UnstructuredCommandLine} returns this
 */
proto.build_event_stream.UnstructuredCommandLine.prototype.addArgs = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.UnstructuredCommandLine} returns this
 */
proto.build_event_stream.UnstructuredCommandLine.prototype.clearArgsList = function() {
  return this.setArgsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.OptionsParsed.repeatedFields_ = [1,2,3,4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.OptionsParsed.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.OptionsParsed.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.OptionsParsed} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.OptionsParsed.toObject = function(includeInstance, msg) {
  var f, obj = {
    startupOptionsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f,
    explicitStartupOptionsList: (f = jspb.Message.getRepeatedField(msg, 2)) == null ? undefined : f,
    cmdLineList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    explicitCmdLineList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f,
    invocationPolicy: (f = msg.getInvocationPolicy()) && build_event_stream_invocation_policy_pb.InvocationPolicy.toObject(includeInstance, f),
    toolTag: jspb.Message.getFieldWithDefault(msg, 6, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.OptionsParsed}
 */
proto.build_event_stream.OptionsParsed.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.OptionsParsed;
  return proto.build_event_stream.OptionsParsed.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.OptionsParsed} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.OptionsParsed}
 */
proto.build_event_stream.OptionsParsed.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addStartupOptions(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.addExplicitStartupOptions(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addCmdLine(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.addExplicitCmdLine(value);
      break;
    case 5:
      var value = new build_event_stream_invocation_policy_pb.InvocationPolicy;
      reader.readMessage(value,build_event_stream_invocation_policy_pb.InvocationPolicy.deserializeBinaryFromReader);
      msg.setInvocationPolicy(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setToolTag(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.OptionsParsed.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.OptionsParsed.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.OptionsParsed} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.OptionsParsed.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStartupOptionsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
  f = message.getExplicitStartupOptionsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      2,
      f
    );
  }
  f = message.getCmdLineList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
  f = message.getExplicitCmdLineList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      4,
      f
    );
  }
  f = message.getInvocationPolicy();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      build_event_stream_invocation_policy_pb.InvocationPolicy.serializeBinaryToWriter
    );
  }
  f = message.getToolTag();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
};


/**
 * repeated string startup_options = 1;
 * @return {!Array<string>}
 */
proto.build_event_stream.OptionsParsed.prototype.getStartupOptionsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.setStartupOptionsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.addStartupOptions = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.clearStartupOptionsList = function() {
  return this.setStartupOptionsList([]);
};


/**
 * repeated string explicit_startup_options = 2;
 * @return {!Array<string>}
 */
proto.build_event_stream.OptionsParsed.prototype.getExplicitStartupOptionsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 2));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.setExplicitStartupOptionsList = function(value) {
  return jspb.Message.setField(this, 2, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.addExplicitStartupOptions = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 2, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.clearExplicitStartupOptionsList = function() {
  return this.setExplicitStartupOptionsList([]);
};


/**
 * repeated string cmd_line = 3;
 * @return {!Array<string>}
 */
proto.build_event_stream.OptionsParsed.prototype.getCmdLineList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.setCmdLineList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.addCmdLine = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.clearCmdLineList = function() {
  return this.setCmdLineList([]);
};


/**
 * repeated string explicit_cmd_line = 4;
 * @return {!Array<string>}
 */
proto.build_event_stream.OptionsParsed.prototype.getExplicitCmdLineList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.setExplicitCmdLineList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.addExplicitCmdLine = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.clearExplicitCmdLineList = function() {
  return this.setExplicitCmdLineList([]);
};


/**
 * optional blaze.invocation_policy.InvocationPolicy invocation_policy = 5;
 * @return {?proto.blaze.invocation_policy.InvocationPolicy}
 */
proto.build_event_stream.OptionsParsed.prototype.getInvocationPolicy = function() {
  return /** @type{?proto.blaze.invocation_policy.InvocationPolicy} */ (
    jspb.Message.getWrapperField(this, build_event_stream_invocation_policy_pb.InvocationPolicy, 5));
};


/**
 * @param {?proto.blaze.invocation_policy.InvocationPolicy|undefined} value
 * @return {!proto.build_event_stream.OptionsParsed} returns this
*/
proto.build_event_stream.OptionsParsed.prototype.setInvocationPolicy = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.clearInvocationPolicy = function() {
  return this.setInvocationPolicy(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.OptionsParsed.prototype.hasInvocationPolicy = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional string tool_tag = 6;
 * @return {string}
 */
proto.build_event_stream.OptionsParsed.prototype.getToolTag = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.OptionsParsed} returns this
 */
proto.build_event_stream.OptionsParsed.prototype.setToolTag = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.Fetch.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.Fetch.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.Fetch} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.Fetch.toObject = function(includeInstance, msg) {
  var f, obj = {
    success: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.Fetch}
 */
proto.build_event_stream.Fetch.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.Fetch;
  return proto.build_event_stream.Fetch.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.Fetch} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.Fetch}
 */
proto.build_event_stream.Fetch.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSuccess(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.Fetch.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.Fetch.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.Fetch} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.Fetch.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSuccess();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool success = 1;
 * @return {boolean}
 */
proto.build_event_stream.Fetch.prototype.getSuccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.Fetch} returns this
 */
proto.build_event_stream.Fetch.prototype.setSuccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.WorkspaceStatus.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.WorkspaceStatus.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.WorkspaceStatus.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.WorkspaceStatus} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.WorkspaceStatus.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemList: jspb.Message.toObjectList(msg.getItemList(),
    proto.build_event_stream.WorkspaceStatus.Item.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.WorkspaceStatus}
 */
proto.build_event_stream.WorkspaceStatus.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.WorkspaceStatus;
  return proto.build_event_stream.WorkspaceStatus.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.WorkspaceStatus} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.WorkspaceStatus}
 */
proto.build_event_stream.WorkspaceStatus.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.WorkspaceStatus.Item;
      reader.readMessage(value,proto.build_event_stream.WorkspaceStatus.Item.deserializeBinaryFromReader);
      msg.addItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.WorkspaceStatus.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.WorkspaceStatus.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.WorkspaceStatus} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.WorkspaceStatus.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.build_event_stream.WorkspaceStatus.Item.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.WorkspaceStatus.Item.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.WorkspaceStatus.Item.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.WorkspaceStatus.Item} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.WorkspaceStatus.Item.toObject = function(includeInstance, msg) {
  var f, obj = {
    key: jspb.Message.getFieldWithDefault(msg, 1, ""),
    value: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.WorkspaceStatus.Item}
 */
proto.build_event_stream.WorkspaceStatus.Item.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.WorkspaceStatus.Item;
  return proto.build_event_stream.WorkspaceStatus.Item.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.WorkspaceStatus.Item} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.WorkspaceStatus.Item}
 */
proto.build_event_stream.WorkspaceStatus.Item.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setKey(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.WorkspaceStatus.Item.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.WorkspaceStatus.Item.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.WorkspaceStatus.Item} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.WorkspaceStatus.Item.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getKey();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getValue();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string key = 1;
 * @return {string}
 */
proto.build_event_stream.WorkspaceStatus.Item.prototype.getKey = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.WorkspaceStatus.Item} returns this
 */
proto.build_event_stream.WorkspaceStatus.Item.prototype.setKey = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string value = 2;
 * @return {string}
 */
proto.build_event_stream.WorkspaceStatus.Item.prototype.getValue = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.WorkspaceStatus.Item} returns this
 */
proto.build_event_stream.WorkspaceStatus.Item.prototype.setValue = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * repeated Item item = 1;
 * @return {!Array<!proto.build_event_stream.WorkspaceStatus.Item>}
 */
proto.build_event_stream.WorkspaceStatus.prototype.getItemList = function() {
  return /** @type{!Array<!proto.build_event_stream.WorkspaceStatus.Item>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.WorkspaceStatus.Item, 1));
};


/**
 * @param {!Array<!proto.build_event_stream.WorkspaceStatus.Item>} value
 * @return {!proto.build_event_stream.WorkspaceStatus} returns this
*/
proto.build_event_stream.WorkspaceStatus.prototype.setItemList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.build_event_stream.WorkspaceStatus.Item=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.WorkspaceStatus.Item}
 */
proto.build_event_stream.WorkspaceStatus.prototype.addItem = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.build_event_stream.WorkspaceStatus.Item, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.WorkspaceStatus} returns this
 */
proto.build_event_stream.WorkspaceStatus.prototype.clearItemList = function() {
  return this.setItemList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetadata.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetadata.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetadata} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetadata.toObject = function(includeInstance, msg) {
  var f, obj = {
    metadataMap: (f = msg.getMetadataMap()) ? f.toObject(includeInstance, undefined) : []
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetadata}
 */
proto.build_event_stream.BuildMetadata.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetadata;
  return proto.build_event_stream.BuildMetadata.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetadata} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetadata}
 */
proto.build_event_stream.BuildMetadata.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = msg.getMetadataMap();
      reader.readMessage(value, function(message, reader) {
        jspb.Map.deserializeBinary(message, reader, jspb.BinaryReader.prototype.readString, jspb.BinaryReader.prototype.readString, null, "", "");
         });
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetadata.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetadata.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetadata} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetadata.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMetadataMap(true);
  if (f && f.getLength() > 0) {
    f.serializeBinary(1, writer, jspb.BinaryWriter.prototype.writeString, jspb.BinaryWriter.prototype.writeString);
  }
};


/**
 * map<string, string> metadata = 1;
 * @param {boolean=} opt_noLazyCreate Do not create the map if
 * empty, instead returning `undefined`
 * @return {!jspb.Map<string,string>}
 */
proto.build_event_stream.BuildMetadata.prototype.getMetadataMap = function(opt_noLazyCreate) {
  return /** @type {!jspb.Map<string,string>} */ (
      jspb.Message.getMapField(this, 1, opt_noLazyCreate,
      null));
};


/**
 * Clears values from the map. The map will be non-null.
 * @return {!proto.build_event_stream.BuildMetadata} returns this
 */
proto.build_event_stream.BuildMetadata.prototype.clearMetadataMap = function() {
  this.getMetadataMap().clear();
  return this;};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.Configuration.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.Configuration.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.Configuration} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.Configuration.toObject = function(includeInstance, msg) {
  var f, obj = {
    mnemonic: jspb.Message.getFieldWithDefault(msg, 1, ""),
    platformName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    cpu: jspb.Message.getFieldWithDefault(msg, 3, ""),
    makeVariableMap: (f = msg.getMakeVariableMap()) ? f.toObject(includeInstance, undefined) : []
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.Configuration}
 */
proto.build_event_stream.Configuration.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.Configuration;
  return proto.build_event_stream.Configuration.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.Configuration} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.Configuration}
 */
proto.build_event_stream.Configuration.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setMnemonic(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPlatformName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setCpu(value);
      break;
    case 4:
      var value = msg.getMakeVariableMap();
      reader.readMessage(value, function(message, reader) {
        jspb.Map.deserializeBinary(message, reader, jspb.BinaryReader.prototype.readString, jspb.BinaryReader.prototype.readString, null, "", "");
         });
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.Configuration.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.Configuration.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.Configuration} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.Configuration.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMnemonic();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPlatformName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getCpu();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getMakeVariableMap(true);
  if (f && f.getLength() > 0) {
    f.serializeBinary(4, writer, jspb.BinaryWriter.prototype.writeString, jspb.BinaryWriter.prototype.writeString);
  }
};


/**
 * optional string mnemonic = 1;
 * @return {string}
 */
proto.build_event_stream.Configuration.prototype.getMnemonic = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.Configuration} returns this
 */
proto.build_event_stream.Configuration.prototype.setMnemonic = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string platform_name = 2;
 * @return {string}
 */
proto.build_event_stream.Configuration.prototype.getPlatformName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.Configuration} returns this
 */
proto.build_event_stream.Configuration.prototype.setPlatformName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string cpu = 3;
 * @return {string}
 */
proto.build_event_stream.Configuration.prototype.getCpu = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.Configuration} returns this
 */
proto.build_event_stream.Configuration.prototype.setCpu = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * map<string, string> make_variable = 4;
 * @param {boolean=} opt_noLazyCreate Do not create the map if
 * empty, instead returning `undefined`
 * @return {!jspb.Map<string,string>}
 */
proto.build_event_stream.Configuration.prototype.getMakeVariableMap = function(opt_noLazyCreate) {
  return /** @type {!jspb.Map<string,string>} */ (
      jspb.Message.getMapField(this, 4, opt_noLazyCreate,
      null));
};


/**
 * Clears values from the map. The map will be non-null.
 * @return {!proto.build_event_stream.Configuration} returns this
 */
proto.build_event_stream.Configuration.prototype.clearMakeVariableMap = function() {
  this.getMakeVariableMap().clear();
  return this;};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.PatternExpanded.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.PatternExpanded.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.PatternExpanded.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.PatternExpanded} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.PatternExpanded.toObject = function(includeInstance, msg) {
  var f, obj = {
    testSuiteExpansionsList: jspb.Message.toObjectList(msg.getTestSuiteExpansionsList(),
    proto.build_event_stream.PatternExpanded.TestSuiteExpansion.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.PatternExpanded}
 */
proto.build_event_stream.PatternExpanded.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.PatternExpanded;
  return proto.build_event_stream.PatternExpanded.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.PatternExpanded} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.PatternExpanded}
 */
proto.build_event_stream.PatternExpanded.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.PatternExpanded.TestSuiteExpansion;
      reader.readMessage(value,proto.build_event_stream.PatternExpanded.TestSuiteExpansion.deserializeBinaryFromReader);
      msg.addTestSuiteExpansions(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.PatternExpanded.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.PatternExpanded.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.PatternExpanded} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.PatternExpanded.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTestSuiteExpansionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.build_event_stream.PatternExpanded.TestSuiteExpansion.serializeBinaryToWriter
    );
  }
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.PatternExpanded.TestSuiteExpansion.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.toObject = function(includeInstance, msg) {
  var f, obj = {
    suiteLabel: jspb.Message.getFieldWithDefault(msg, 1, ""),
    testLabelsList: (f = jspb.Message.getRepeatedField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion}
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.PatternExpanded.TestSuiteExpansion;
  return proto.build_event_stream.PatternExpanded.TestSuiteExpansion.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion}
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSuiteLabel(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.addTestLabels(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.PatternExpanded.TestSuiteExpansion.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSuiteLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getTestLabelsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      2,
      f
    );
  }
};


/**
 * optional string suite_label = 1;
 * @return {string}
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.prototype.getSuiteLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion} returns this
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.prototype.setSuiteLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * repeated string test_labels = 2;
 * @return {!Array<string>}
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.prototype.getTestLabelsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 2));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion} returns this
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.prototype.setTestLabelsList = function(value) {
  return jspb.Message.setField(this, 2, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion} returns this
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.prototype.addTestLabels = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 2, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion} returns this
 */
proto.build_event_stream.PatternExpanded.TestSuiteExpansion.prototype.clearTestLabelsList = function() {
  return this.setTestLabelsList([]);
};


/**
 * repeated TestSuiteExpansion test_suite_expansions = 1;
 * @return {!Array<!proto.build_event_stream.PatternExpanded.TestSuiteExpansion>}
 */
proto.build_event_stream.PatternExpanded.prototype.getTestSuiteExpansionsList = function() {
  return /** @type{!Array<!proto.build_event_stream.PatternExpanded.TestSuiteExpansion>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.PatternExpanded.TestSuiteExpansion, 1));
};


/**
 * @param {!Array<!proto.build_event_stream.PatternExpanded.TestSuiteExpansion>} value
 * @return {!proto.build_event_stream.PatternExpanded} returns this
*/
proto.build_event_stream.PatternExpanded.prototype.setTestSuiteExpansionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.PatternExpanded.TestSuiteExpansion}
 */
proto.build_event_stream.PatternExpanded.prototype.addTestSuiteExpansions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.build_event_stream.PatternExpanded.TestSuiteExpansion, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.PatternExpanded} returns this
 */
proto.build_event_stream.PatternExpanded.prototype.clearTestSuiteExpansionsList = function() {
  return this.setTestSuiteExpansionsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.TargetConfigured.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.TargetConfigured.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.TargetConfigured.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.TargetConfigured} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TargetConfigured.toObject = function(includeInstance, msg) {
  var f, obj = {
    targetKind: jspb.Message.getFieldWithDefault(msg, 1, ""),
    testSize: jspb.Message.getFieldWithDefault(msg, 2, 0),
    tagList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.TargetConfigured}
 */
proto.build_event_stream.TargetConfigured.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.TargetConfigured;
  return proto.build_event_stream.TargetConfigured.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.TargetConfigured} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.TargetConfigured}
 */
proto.build_event_stream.TargetConfigured.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setTargetKind(value);
      break;
    case 2:
      var value = /** @type {!proto.build_event_stream.TestSize} */ (reader.readEnum());
      msg.setTestSize(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addTag(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.TargetConfigured.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.TargetConfigured.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.TargetConfigured} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TargetConfigured.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTargetKind();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getTestSize();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getTagList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
};


/**
 * optional string target_kind = 1;
 * @return {string}
 */
proto.build_event_stream.TargetConfigured.prototype.getTargetKind = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.TargetConfigured} returns this
 */
proto.build_event_stream.TargetConfigured.prototype.setTargetKind = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional TestSize test_size = 2;
 * @return {!proto.build_event_stream.TestSize}
 */
proto.build_event_stream.TargetConfigured.prototype.getTestSize = function() {
  return /** @type {!proto.build_event_stream.TestSize} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.build_event_stream.TestSize} value
 * @return {!proto.build_event_stream.TargetConfigured} returns this
 */
proto.build_event_stream.TargetConfigured.prototype.setTestSize = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * repeated string tag = 3;
 * @return {!Array<string>}
 */
proto.build_event_stream.TargetConfigured.prototype.getTagList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.TargetConfigured} returns this
 */
proto.build_event_stream.TargetConfigured.prototype.setTagList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.TargetConfigured} returns this
 */
proto.build_event_stream.TargetConfigured.prototype.addTag = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TargetConfigured} returns this
 */
proto.build_event_stream.TargetConfigured.prototype.clearTagList = function() {
  return this.setTagList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.File.repeatedFields_ = [4];

/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.build_event_stream.File.oneofGroups_ = [[2,3]];

/**
 * @enum {number}
 */
proto.build_event_stream.File.FileCase = {
  FILE_NOT_SET: 0,
  URI: 2,
  CONTENTS: 3
};

/**
 * @return {proto.build_event_stream.File.FileCase}
 */
proto.build_event_stream.File.prototype.getFileCase = function() {
  return /** @type {proto.build_event_stream.File.FileCase} */(jspb.Message.computeOneofCase(this, proto.build_event_stream.File.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.File.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.File.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.File} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.File.toObject = function(includeInstance, msg) {
  var f, obj = {
    pathPrefixList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f,
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    uri: jspb.Message.getFieldWithDefault(msg, 2, ""),
    contents: msg.getContents_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.File.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.File;
  return proto.build_event_stream.File.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.File} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.File.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.addPathPrefix(value);
      break;
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setUri(value);
      break;
    case 3:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setContents(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.File.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.File.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.File} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.File.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPathPrefixList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      4,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      2,
      f
    );
  }
  f = /** @type {!(string|Uint8Array)} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeBytes(
      3,
      f
    );
  }
};


/**
 * repeated string path_prefix = 4;
 * @return {!Array<string>}
 */
proto.build_event_stream.File.prototype.getPathPrefixList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.File} returns this
 */
proto.build_event_stream.File.prototype.setPathPrefixList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File} returns this
 */
proto.build_event_stream.File.prototype.addPathPrefix = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.File} returns this
 */
proto.build_event_stream.File.prototype.clearPathPrefixList = function() {
  return this.setPathPrefixList([]);
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.build_event_stream.File.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.File} returns this
 */
proto.build_event_stream.File.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string uri = 2;
 * @return {string}
 */
proto.build_event_stream.File.prototype.getUri = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.File} returns this
 */
proto.build_event_stream.File.prototype.setUri = function(value) {
  return jspb.Message.setOneofField(this, 2, proto.build_event_stream.File.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.build_event_stream.File} returns this
 */
proto.build_event_stream.File.prototype.clearUri = function() {
  return jspb.Message.setOneofField(this, 2, proto.build_event_stream.File.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.File.prototype.hasUri = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bytes contents = 3;
 * @return {string}
 */
proto.build_event_stream.File.prototype.getContents = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * optional bytes contents = 3;
 * This is a type-conversion wrapper around `getContents()`
 * @return {string}
 */
proto.build_event_stream.File.prototype.getContents_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getContents()));
};


/**
 * optional bytes contents = 3;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getContents()`
 * @return {!Uint8Array}
 */
proto.build_event_stream.File.prototype.getContents_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getContents()));
};


/**
 * @param {!(string|Uint8Array)} value
 * @return {!proto.build_event_stream.File} returns this
 */
proto.build_event_stream.File.prototype.setContents = function(value) {
  return jspb.Message.setOneofField(this, 3, proto.build_event_stream.File.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.build_event_stream.File} returns this
 */
proto.build_event_stream.File.prototype.clearContents = function() {
  return jspb.Message.setOneofField(this, 3, proto.build_event_stream.File.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.File.prototype.hasContents = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.NamedSetOfFiles.repeatedFields_ = [1,2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.NamedSetOfFiles.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.NamedSetOfFiles.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.NamedSetOfFiles} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.NamedSetOfFiles.toObject = function(includeInstance, msg) {
  var f, obj = {
    filesList: jspb.Message.toObjectList(msg.getFilesList(),
    proto.build_event_stream.File.toObject, includeInstance),
    fileSetsList: jspb.Message.toObjectList(msg.getFileSetsList(),
    proto.build_event_stream.BuildEventId.NamedSetOfFilesId.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.NamedSetOfFiles}
 */
proto.build_event_stream.NamedSetOfFiles.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.NamedSetOfFiles;
  return proto.build_event_stream.NamedSetOfFiles.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.NamedSetOfFiles} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.NamedSetOfFiles}
 */
proto.build_event_stream.NamedSetOfFiles.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.addFiles(value);
      break;
    case 2:
      var value = new proto.build_event_stream.BuildEventId.NamedSetOfFilesId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.NamedSetOfFilesId.deserializeBinaryFromReader);
      msg.addFileSets(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.NamedSetOfFiles.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.NamedSetOfFiles.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.NamedSetOfFiles} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.NamedSetOfFiles.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getFileSetsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.build_event_stream.BuildEventId.NamedSetOfFilesId.serializeBinaryToWriter
    );
  }
};


/**
 * repeated File files = 1;
 * @return {!Array<!proto.build_event_stream.File>}
 */
proto.build_event_stream.NamedSetOfFiles.prototype.getFilesList = function() {
  return /** @type{!Array<!proto.build_event_stream.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.File, 1));
};


/**
 * @param {!Array<!proto.build_event_stream.File>} value
 * @return {!proto.build_event_stream.NamedSetOfFiles} returns this
*/
proto.build_event_stream.NamedSetOfFiles.prototype.setFilesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.build_event_stream.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.NamedSetOfFiles.prototype.addFiles = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.build_event_stream.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.NamedSetOfFiles} returns this
 */
proto.build_event_stream.NamedSetOfFiles.prototype.clearFilesList = function() {
  return this.setFilesList([]);
};


/**
 * repeated BuildEventId.NamedSetOfFilesId file_sets = 2;
 * @return {!Array<!proto.build_event_stream.BuildEventId.NamedSetOfFilesId>}
 */
proto.build_event_stream.NamedSetOfFiles.prototype.getFileSetsList = function() {
  return /** @type{!Array<!proto.build_event_stream.BuildEventId.NamedSetOfFilesId>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.BuildEventId.NamedSetOfFilesId, 2));
};


/**
 * @param {!Array<!proto.build_event_stream.BuildEventId.NamedSetOfFilesId>} value
 * @return {!proto.build_event_stream.NamedSetOfFiles} returns this
*/
proto.build_event_stream.NamedSetOfFiles.prototype.setFileSetsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId}
 */
proto.build_event_stream.NamedSetOfFiles.prototype.addFileSets = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.build_event_stream.BuildEventId.NamedSetOfFilesId, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.NamedSetOfFiles} returns this
 */
proto.build_event_stream.NamedSetOfFiles.prototype.clearFileSetsList = function() {
  return this.setFileSetsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.ActionExecuted.repeatedFields_ = [9,10];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.ActionExecuted.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.ActionExecuted.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.ActionExecuted} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.ActionExecuted.toObject = function(includeInstance, msg) {
  var f, obj = {
    success: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    type: jspb.Message.getFieldWithDefault(msg, 8, ""),
    exitCode: jspb.Message.getFieldWithDefault(msg, 2, 0),
    stdout: (f = msg.getStdout()) && proto.build_event_stream.File.toObject(includeInstance, f),
    stderr: (f = msg.getStderr()) && proto.build_event_stream.File.toObject(includeInstance, f),
    label: jspb.Message.getFieldWithDefault(msg, 5, ""),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.BuildEventId.ConfigurationId.toObject(includeInstance, f),
    primaryOutput: (f = msg.getPrimaryOutput()) && proto.build_event_stream.File.toObject(includeInstance, f),
    commandLineList: (f = jspb.Message.getRepeatedField(msg, 9)) == null ? undefined : f,
    actionMetadataLogsList: jspb.Message.toObjectList(msg.getActionMetadataLogsList(),
    proto.build_event_stream.File.toObject, includeInstance),
    failureDetail: (f = msg.getFailureDetail()) && build_event_stream_failure_details_pb.FailureDetail.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.ActionExecuted}
 */
proto.build_event_stream.ActionExecuted.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.ActionExecuted;
  return proto.build_event_stream.ActionExecuted.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.ActionExecuted} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.ActionExecuted}
 */
proto.build_event_stream.ActionExecuted.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSuccess(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setExitCode(value);
      break;
    case 3:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.setStdout(value);
      break;
    case 4:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.setStderr(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabel(value);
      break;
    case 7:
      var value = new proto.build_event_stream.BuildEventId.ConfigurationId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.ConfigurationId.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    case 6:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.setPrimaryOutput(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.addCommandLine(value);
      break;
    case 10:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.addActionMetadataLogs(value);
      break;
    case 11:
      var value = new build_event_stream_failure_details_pb.FailureDetail;
      reader.readMessage(value,build_event_stream_failure_details_pb.FailureDetail.deserializeBinaryFromReader);
      msg.setFailureDetail(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.ActionExecuted.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.ActionExecuted.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.ActionExecuted} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.ActionExecuted.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSuccess();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getExitCode();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getStdout();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getStderr();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getLabel();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.build_event_stream.BuildEventId.ConfigurationId.serializeBinaryToWriter
    );
  }
  f = message.getPrimaryOutput();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getCommandLineList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      9,
      f
    );
  }
  f = message.getActionMetadataLogsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      10,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getFailureDetail();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      build_event_stream_failure_details_pb.FailureDetail.serializeBinaryToWriter
    );
  }
};


/**
 * optional bool success = 1;
 * @return {boolean}
 */
proto.build_event_stream.ActionExecuted.prototype.getSuccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.setSuccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional string type = 8;
 * @return {string}
 */
proto.build_event_stream.ActionExecuted.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional int32 exit_code = 2;
 * @return {number}
 */
proto.build_event_stream.ActionExecuted.prototype.getExitCode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.setExitCode = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional File stdout = 3;
 * @return {?proto.build_event_stream.File}
 */
proto.build_event_stream.ActionExecuted.prototype.getStdout = function() {
  return /** @type{?proto.build_event_stream.File} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.File, 3));
};


/**
 * @param {?proto.build_event_stream.File|undefined} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
*/
proto.build_event_stream.ActionExecuted.prototype.setStdout = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.clearStdout = function() {
  return this.setStdout(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.ActionExecuted.prototype.hasStdout = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional File stderr = 4;
 * @return {?proto.build_event_stream.File}
 */
proto.build_event_stream.ActionExecuted.prototype.getStderr = function() {
  return /** @type{?proto.build_event_stream.File} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.File, 4));
};


/**
 * @param {?proto.build_event_stream.File|undefined} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
*/
proto.build_event_stream.ActionExecuted.prototype.setStderr = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.clearStderr = function() {
  return this.setStderr(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.ActionExecuted.prototype.hasStderr = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional string label = 5;
 * @return {string}
 */
proto.build_event_stream.ActionExecuted.prototype.getLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.setLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional BuildEventId.ConfigurationId configuration = 7;
 * @return {?proto.build_event_stream.BuildEventId.ConfigurationId}
 */
proto.build_event_stream.ActionExecuted.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.BuildEventId.ConfigurationId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId.ConfigurationId, 7));
};


/**
 * @param {?proto.build_event_stream.BuildEventId.ConfigurationId|undefined} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
*/
proto.build_event_stream.ActionExecuted.prototype.setConfiguration = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.ActionExecuted.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional File primary_output = 6;
 * @return {?proto.build_event_stream.File}
 */
proto.build_event_stream.ActionExecuted.prototype.getPrimaryOutput = function() {
  return /** @type{?proto.build_event_stream.File} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.File, 6));
};


/**
 * @param {?proto.build_event_stream.File|undefined} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
*/
proto.build_event_stream.ActionExecuted.prototype.setPrimaryOutput = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.clearPrimaryOutput = function() {
  return this.setPrimaryOutput(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.ActionExecuted.prototype.hasPrimaryOutput = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * repeated string command_line = 9;
 * @return {!Array<string>}
 */
proto.build_event_stream.ActionExecuted.prototype.getCommandLineList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 9));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.setCommandLineList = function(value) {
  return jspb.Message.setField(this, 9, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.addCommandLine = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 9, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.clearCommandLineList = function() {
  return this.setCommandLineList([]);
};


/**
 * repeated File action_metadata_logs = 10;
 * @return {!Array<!proto.build_event_stream.File>}
 */
proto.build_event_stream.ActionExecuted.prototype.getActionMetadataLogsList = function() {
  return /** @type{!Array<!proto.build_event_stream.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.File, 10));
};


/**
 * @param {!Array<!proto.build_event_stream.File>} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
*/
proto.build_event_stream.ActionExecuted.prototype.setActionMetadataLogsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 10, value);
};


/**
 * @param {!proto.build_event_stream.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.ActionExecuted.prototype.addActionMetadataLogs = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 10, opt_value, proto.build_event_stream.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.clearActionMetadataLogsList = function() {
  return this.setActionMetadataLogsList([]);
};


/**
 * optional failure_details.FailureDetail failure_detail = 11;
 * @return {?proto.failure_details.FailureDetail}
 */
proto.build_event_stream.ActionExecuted.prototype.getFailureDetail = function() {
  return /** @type{?proto.failure_details.FailureDetail} */ (
    jspb.Message.getWrapperField(this, build_event_stream_failure_details_pb.FailureDetail, 11));
};


/**
 * @param {?proto.failure_details.FailureDetail|undefined} value
 * @return {!proto.build_event_stream.ActionExecuted} returns this
*/
proto.build_event_stream.ActionExecuted.prototype.setFailureDetail = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.ActionExecuted} returns this
 */
proto.build_event_stream.ActionExecuted.prototype.clearFailureDetail = function() {
  return this.setFailureDetail(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.ActionExecuted.prototype.hasFailureDetail = function() {
  return jspb.Message.getField(this, 11) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.OutputGroup.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.OutputGroup.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.OutputGroup.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.OutputGroup} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.OutputGroup.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    fileSetsList: jspb.Message.toObjectList(msg.getFileSetsList(),
    proto.build_event_stream.BuildEventId.NamedSetOfFilesId.toObject, includeInstance),
    incomplete: jspb.Message.getBooleanFieldWithDefault(msg, 4, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.OutputGroup}
 */
proto.build_event_stream.OutputGroup.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.OutputGroup;
  return proto.build_event_stream.OutputGroup.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.OutputGroup} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.OutputGroup}
 */
proto.build_event_stream.OutputGroup.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = new proto.build_event_stream.BuildEventId.NamedSetOfFilesId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.NamedSetOfFilesId.deserializeBinaryFromReader);
      msg.addFileSets(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIncomplete(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.OutputGroup.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.OutputGroup.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.OutputGroup} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.OutputGroup.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getFileSetsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.build_event_stream.BuildEventId.NamedSetOfFilesId.serializeBinaryToWriter
    );
  }
  f = message.getIncomplete();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.build_event_stream.OutputGroup.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.OutputGroup} returns this
 */
proto.build_event_stream.OutputGroup.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * repeated BuildEventId.NamedSetOfFilesId file_sets = 3;
 * @return {!Array<!proto.build_event_stream.BuildEventId.NamedSetOfFilesId>}
 */
proto.build_event_stream.OutputGroup.prototype.getFileSetsList = function() {
  return /** @type{!Array<!proto.build_event_stream.BuildEventId.NamedSetOfFilesId>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.BuildEventId.NamedSetOfFilesId, 3));
};


/**
 * @param {!Array<!proto.build_event_stream.BuildEventId.NamedSetOfFilesId>} value
 * @return {!proto.build_event_stream.OutputGroup} returns this
*/
proto.build_event_stream.OutputGroup.prototype.setFileSetsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildEventId.NamedSetOfFilesId}
 */
proto.build_event_stream.OutputGroup.prototype.addFileSets = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.build_event_stream.BuildEventId.NamedSetOfFilesId, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.OutputGroup} returns this
 */
proto.build_event_stream.OutputGroup.prototype.clearFileSetsList = function() {
  return this.setFileSetsList([]);
};


/**
 * optional bool incomplete = 4;
 * @return {boolean}
 */
proto.build_event_stream.OutputGroup.prototype.getIncomplete = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 4, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.OutputGroup} returns this
 */
proto.build_event_stream.OutputGroup.prototype.setIncomplete = function(value) {
  return jspb.Message.setProto3BooleanField(this, 4, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.TargetComplete.repeatedFields_ = [2,4,8,3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.TargetComplete.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.TargetComplete.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.TargetComplete} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TargetComplete.toObject = function(includeInstance, msg) {
  var f, obj = {
    success: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    targetKind: jspb.Message.getFieldWithDefault(msg, 5, ""),
    testSize: jspb.Message.getFieldWithDefault(msg, 6, 0),
    outputGroupList: jspb.Message.toObjectList(msg.getOutputGroupList(),
    proto.build_event_stream.OutputGroup.toObject, includeInstance),
    importantOutputList: jspb.Message.toObjectList(msg.getImportantOutputList(),
    proto.build_event_stream.File.toObject, includeInstance),
    directoryOutputList: jspb.Message.toObjectList(msg.getDirectoryOutputList(),
    proto.build_event_stream.File.toObject, includeInstance),
    tagList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    testTimeoutSeconds: jspb.Message.getFieldWithDefault(msg, 7, 0),
    testTimeout: (f = msg.getTestTimeout()) && google_protobuf_duration_pb.Duration.toObject(includeInstance, f),
    failureDetail: (f = msg.getFailureDetail()) && build_event_stream_failure_details_pb.FailureDetail.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.TargetComplete}
 */
proto.build_event_stream.TargetComplete.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.TargetComplete;
  return proto.build_event_stream.TargetComplete.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.TargetComplete} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.TargetComplete}
 */
proto.build_event_stream.TargetComplete.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSuccess(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setTargetKind(value);
      break;
    case 6:
      var value = /** @type {!proto.build_event_stream.TestSize} */ (reader.readEnum());
      msg.setTestSize(value);
      break;
    case 2:
      var value = new proto.build_event_stream.OutputGroup;
      reader.readMessage(value,proto.build_event_stream.OutputGroup.deserializeBinaryFromReader);
      msg.addOutputGroup(value);
      break;
    case 4:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.addImportantOutput(value);
      break;
    case 8:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.addDirectoryOutput(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addTag(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTestTimeoutSeconds(value);
      break;
    case 10:
      var value = new google_protobuf_duration_pb.Duration;
      reader.readMessage(value,google_protobuf_duration_pb.Duration.deserializeBinaryFromReader);
      msg.setTestTimeout(value);
      break;
    case 9:
      var value = new build_event_stream_failure_details_pb.FailureDetail;
      reader.readMessage(value,build_event_stream_failure_details_pb.FailureDetail.deserializeBinaryFromReader);
      msg.setFailureDetail(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.TargetComplete.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.TargetComplete.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.TargetComplete} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TargetComplete.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSuccess();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getTargetKind();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getTestSize();
  if (f !== 0.0) {
    writer.writeEnum(
      6,
      f
    );
  }
  f = message.getOutputGroupList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.build_event_stream.OutputGroup.serializeBinaryToWriter
    );
  }
  f = message.getImportantOutputList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getDirectoryOutputList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      8,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getTagList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
  f = message.getTestTimeoutSeconds();
  if (f !== 0) {
    writer.writeInt64(
      7,
      f
    );
  }
  f = message.getTestTimeout();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      google_protobuf_duration_pb.Duration.serializeBinaryToWriter
    );
  }
  f = message.getFailureDetail();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      build_event_stream_failure_details_pb.FailureDetail.serializeBinaryToWriter
    );
  }
};


/**
 * optional bool success = 1;
 * @return {boolean}
 */
proto.build_event_stream.TargetComplete.prototype.getSuccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.setSuccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional string target_kind = 5;
 * @return {string}
 */
proto.build_event_stream.TargetComplete.prototype.getTargetKind = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.setTargetKind = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional TestSize test_size = 6;
 * @return {!proto.build_event_stream.TestSize}
 */
proto.build_event_stream.TargetComplete.prototype.getTestSize = function() {
  return /** @type {!proto.build_event_stream.TestSize} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {!proto.build_event_stream.TestSize} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.setTestSize = function(value) {
  return jspb.Message.setProto3EnumField(this, 6, value);
};


/**
 * repeated OutputGroup output_group = 2;
 * @return {!Array<!proto.build_event_stream.OutputGroup>}
 */
proto.build_event_stream.TargetComplete.prototype.getOutputGroupList = function() {
  return /** @type{!Array<!proto.build_event_stream.OutputGroup>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.OutputGroup, 2));
};


/**
 * @param {!Array<!proto.build_event_stream.OutputGroup>} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
*/
proto.build_event_stream.TargetComplete.prototype.setOutputGroupList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.build_event_stream.OutputGroup=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.OutputGroup}
 */
proto.build_event_stream.TargetComplete.prototype.addOutputGroup = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.build_event_stream.OutputGroup, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.clearOutputGroupList = function() {
  return this.setOutputGroupList([]);
};


/**
 * repeated File important_output = 4;
 * @return {!Array<!proto.build_event_stream.File>}
 */
proto.build_event_stream.TargetComplete.prototype.getImportantOutputList = function() {
  return /** @type{!Array<!proto.build_event_stream.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.File, 4));
};


/**
 * @param {!Array<!proto.build_event_stream.File>} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
*/
proto.build_event_stream.TargetComplete.prototype.setImportantOutputList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.build_event_stream.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.TargetComplete.prototype.addImportantOutput = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.build_event_stream.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.clearImportantOutputList = function() {
  return this.setImportantOutputList([]);
};


/**
 * repeated File directory_output = 8;
 * @return {!Array<!proto.build_event_stream.File>}
 */
proto.build_event_stream.TargetComplete.prototype.getDirectoryOutputList = function() {
  return /** @type{!Array<!proto.build_event_stream.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.File, 8));
};


/**
 * @param {!Array<!proto.build_event_stream.File>} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
*/
proto.build_event_stream.TargetComplete.prototype.setDirectoryOutputList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 8, value);
};


/**
 * @param {!proto.build_event_stream.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.TargetComplete.prototype.addDirectoryOutput = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 8, opt_value, proto.build_event_stream.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.clearDirectoryOutputList = function() {
  return this.setDirectoryOutputList([]);
};


/**
 * repeated string tag = 3;
 * @return {!Array<string>}
 */
proto.build_event_stream.TargetComplete.prototype.getTagList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.setTagList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.addTag = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.clearTagList = function() {
  return this.setTagList([]);
};


/**
 * optional int64 test_timeout_seconds = 7;
 * @return {number}
 */
proto.build_event_stream.TargetComplete.prototype.getTestTimeoutSeconds = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.setTestTimeoutSeconds = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional google.protobuf.Duration test_timeout = 10;
 * @return {?proto.google.protobuf.Duration}
 */
proto.build_event_stream.TargetComplete.prototype.getTestTimeout = function() {
  return /** @type{?proto.google.protobuf.Duration} */ (
    jspb.Message.getWrapperField(this, google_protobuf_duration_pb.Duration, 10));
};


/**
 * @param {?proto.google.protobuf.Duration|undefined} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
*/
proto.build_event_stream.TargetComplete.prototype.setTestTimeout = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.clearTestTimeout = function() {
  return this.setTestTimeout(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TargetComplete.prototype.hasTestTimeout = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional failure_details.FailureDetail failure_detail = 9;
 * @return {?proto.failure_details.FailureDetail}
 */
proto.build_event_stream.TargetComplete.prototype.getFailureDetail = function() {
  return /** @type{?proto.failure_details.FailureDetail} */ (
    jspb.Message.getWrapperField(this, build_event_stream_failure_details_pb.FailureDetail, 9));
};


/**
 * @param {?proto.failure_details.FailureDetail|undefined} value
 * @return {!proto.build_event_stream.TargetComplete} returns this
*/
proto.build_event_stream.TargetComplete.prototype.setFailureDetail = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TargetComplete} returns this
 */
proto.build_event_stream.TargetComplete.prototype.clearFailureDetail = function() {
  return this.setFailureDetail(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TargetComplete.prototype.hasFailureDetail = function() {
  return jspb.Message.getField(this, 9) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.TestResult.repeatedFields_ = [2,7];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.TestResult.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.TestResult.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.TestResult} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestResult.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 5, 0),
    statusDetails: jspb.Message.getFieldWithDefault(msg, 9, ""),
    cachedLocally: jspb.Message.getBooleanFieldWithDefault(msg, 4, false),
    testAttemptStartMillisEpoch: jspb.Message.getFieldWithDefault(msg, 6, 0),
    testAttemptStart: (f = msg.getTestAttemptStart()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    testAttemptDurationMillis: jspb.Message.getFieldWithDefault(msg, 3, 0),
    testAttemptDuration: (f = msg.getTestAttemptDuration()) && google_protobuf_duration_pb.Duration.toObject(includeInstance, f),
    testActionOutputList: jspb.Message.toObjectList(msg.getTestActionOutputList(),
    proto.build_event_stream.File.toObject, includeInstance),
    warningList: (f = jspb.Message.getRepeatedField(msg, 7)) == null ? undefined : f,
    executionInfo: (f = msg.getExecutionInfo()) && proto.build_event_stream.TestResult.ExecutionInfo.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.TestResult}
 */
proto.build_event_stream.TestResult.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.TestResult;
  return proto.build_event_stream.TestResult.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.TestResult} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.TestResult}
 */
proto.build_event_stream.TestResult.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 5:
      var value = /** @type {!proto.build_event_stream.TestStatus} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatusDetails(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCachedLocally(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTestAttemptStartMillisEpoch(value);
      break;
    case 10:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setTestAttemptStart(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTestAttemptDurationMillis(value);
      break;
    case 11:
      var value = new google_protobuf_duration_pb.Duration;
      reader.readMessage(value,google_protobuf_duration_pb.Duration.deserializeBinaryFromReader);
      msg.setTestAttemptDuration(value);
      break;
    case 2:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.addTestActionOutput(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.addWarning(value);
      break;
    case 8:
      var value = new proto.build_event_stream.TestResult.ExecutionInfo;
      reader.readMessage(value,proto.build_event_stream.TestResult.ExecutionInfo.deserializeBinaryFromReader);
      msg.setExecutionInfo(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.TestResult.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.TestResult.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.TestResult} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestResult.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      5,
      f
    );
  }
  f = message.getStatusDetails();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getCachedLocally();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
  f = message.getTestAttemptStartMillisEpoch();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getTestAttemptStart();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getTestAttemptDurationMillis();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = message.getTestAttemptDuration();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      google_protobuf_duration_pb.Duration.serializeBinaryToWriter
    );
  }
  f = message.getTestActionOutputList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getWarningList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      7,
      f
    );
  }
  f = message.getExecutionInfo();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      proto.build_event_stream.TestResult.ExecutionInfo.serializeBinaryToWriter
    );
  }
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.TestResult.ExecutionInfo.repeatedFields_ = [5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.TestResult.ExecutionInfo.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestResult.ExecutionInfo.toObject = function(includeInstance, msg) {
  var f, obj = {
    timeoutSeconds: jspb.Message.getFieldWithDefault(msg, 1, 0),
    strategy: jspb.Message.getFieldWithDefault(msg, 2, ""),
    cachedRemotely: jspb.Message.getBooleanFieldWithDefault(msg, 6, false),
    exitCode: jspb.Message.getFieldWithDefault(msg, 7, 0),
    hostname: jspb.Message.getFieldWithDefault(msg, 3, ""),
    timingBreakdown: (f = msg.getTimingBreakdown()) && proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.toObject(includeInstance, f),
    resourceUsageList: jspb.Message.toObjectList(msg.getResourceUsageList(),
    proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo}
 */
proto.build_event_stream.TestResult.ExecutionInfo.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.TestResult.ExecutionInfo;
  return proto.build_event_stream.TestResult.ExecutionInfo.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo}
 */
proto.build_event_stream.TestResult.ExecutionInfo.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTimeoutSeconds(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setStrategy(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCachedRemotely(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setExitCode(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setHostname(value);
      break;
    case 4:
      var value = new proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown;
      reader.readMessage(value,proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.deserializeBinaryFromReader);
      msg.setTimingBreakdown(value);
      break;
    case 5:
      var value = new proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage;
      reader.readMessage(value,proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.deserializeBinaryFromReader);
      msg.addResourceUsage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.TestResult.ExecutionInfo.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestResult.ExecutionInfo.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTimeoutSeconds();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getStrategy();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getCachedRemotely();
  if (f) {
    writer.writeBool(
      6,
      f
    );
  }
  f = message.getExitCode();
  if (f !== 0) {
    writer.writeInt32(
      7,
      f
    );
  }
  f = message.getHostname();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getTimingBreakdown();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.serializeBinaryToWriter
    );
  }
  f = message.getResourceUsageList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.serializeBinaryToWriter
    );
  }
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.toObject = function(includeInstance, msg) {
  var f, obj = {
    childList: jspb.Message.toObjectList(msg.getChildList(),
    proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.toObject, includeInstance),
    name: jspb.Message.getFieldWithDefault(msg, 2, ""),
    timeMillis: jspb.Message.getFieldWithDefault(msg, 3, 0),
    time: (f = msg.getTime()) && google_protobuf_duration_pb.Duration.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown;
  return proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown;
      reader.readMessage(value,proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.deserializeBinaryFromReader);
      msg.addChild(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTimeMillis(value);
      break;
    case 4:
      var value = new google_protobuf_duration_pb.Duration;
      reader.readMessage(value,google_protobuf_duration_pb.Duration.deserializeBinaryFromReader);
      msg.setTime(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getChildList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.serializeBinaryToWriter
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTimeMillis();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = message.getTime();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_duration_pb.Duration.serializeBinaryToWriter
    );
  }
};


/**
 * repeated TimingBreakdown child = 1;
 * @return {!Array<!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown>}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.getChildList = function() {
  return /** @type{!Array<!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown, 1));
};


/**
 * @param {!Array<!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown>} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} returns this
*/
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.setChildList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.addChild = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.clearChildList = function() {
  return this.setChildList([]);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int64 time_millis = 3;
 * @return {number}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.getTimeMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.setTimeMillis = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional google.protobuf.Duration time = 4;
 * @return {?proto.google.protobuf.Duration}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.getTime = function() {
  return /** @type{?proto.google.protobuf.Duration} */ (
    jspb.Message.getWrapperField(this, google_protobuf_duration_pb.Duration, 4));
};


/**
 * @param {?proto.google.protobuf.Duration|undefined} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} returns this
*/
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.setTime = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.clearTime = function() {
  return this.setTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown.prototype.hasTime = function() {
  return jspb.Message.getField(this, 4) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    value: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage}
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage;
  return proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage}
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getValue();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int64 value = 2;
 * @return {number}
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.prototype.getValue = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage.prototype.setValue = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int32 timeout_seconds = 1;
 * @return {number}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.getTimeoutSeconds = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.setTimeoutSeconds = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string strategy = 2;
 * @return {string}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.getStrategy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.setStrategy = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool cached_remotely = 6;
 * @return {boolean}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.getCachedRemotely = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.setCachedRemotely = function(value) {
  return jspb.Message.setProto3BooleanField(this, 6, value);
};


/**
 * optional int32 exit_code = 7;
 * @return {number}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.getExitCode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.setExitCode = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional string hostname = 3;
 * @return {string}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.getHostname = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.setHostname = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional TimingBreakdown timing_breakdown = 4;
 * @return {?proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.getTimingBreakdown = function() {
  return /** @type{?proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown, 4));
};


/**
 * @param {?proto.build_event_stream.TestResult.ExecutionInfo.TimingBreakdown|undefined} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
*/
proto.build_event_stream.TestResult.ExecutionInfo.prototype.setTimingBreakdown = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.clearTimingBreakdown = function() {
  return this.setTimingBreakdown(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.hasTimingBreakdown = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * repeated ResourceUsage resource_usage = 5;
 * @return {!Array<!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage>}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.getResourceUsageList = function() {
  return /** @type{!Array<!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage, 5));
};


/**
 * @param {!Array<!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage>} value
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
*/
proto.build_event_stream.TestResult.ExecutionInfo.prototype.setResourceUsageList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage}
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.addResourceUsage = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.build_event_stream.TestResult.ExecutionInfo.ResourceUsage, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TestResult.ExecutionInfo} returns this
 */
proto.build_event_stream.TestResult.ExecutionInfo.prototype.clearResourceUsageList = function() {
  return this.setResourceUsageList([]);
};


/**
 * optional TestStatus status = 5;
 * @return {!proto.build_event_stream.TestStatus}
 */
proto.build_event_stream.TestResult.prototype.getStatus = function() {
  return /** @type {!proto.build_event_stream.TestStatus} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {!proto.build_event_stream.TestStatus} value
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 5, value);
};


/**
 * optional string status_details = 9;
 * @return {string}
 */
proto.build_event_stream.TestResult.prototype.getStatusDetails = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.setStatusDetails = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional bool cached_locally = 4;
 * @return {boolean}
 */
proto.build_event_stream.TestResult.prototype.getCachedLocally = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 4, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.setCachedLocally = function(value) {
  return jspb.Message.setProto3BooleanField(this, 4, value);
};


/**
 * optional int64 test_attempt_start_millis_epoch = 6;
 * @return {number}
 */
proto.build_event_stream.TestResult.prototype.getTestAttemptStartMillisEpoch = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.setTestAttemptStartMillisEpoch = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional google.protobuf.Timestamp test_attempt_start = 10;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.build_event_stream.TestResult.prototype.getTestAttemptStart = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 10));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.build_event_stream.TestResult} returns this
*/
proto.build_event_stream.TestResult.prototype.setTestAttemptStart = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.clearTestAttemptStart = function() {
  return this.setTestAttemptStart(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TestResult.prototype.hasTestAttemptStart = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional int64 test_attempt_duration_millis = 3;
 * @return {number}
 */
proto.build_event_stream.TestResult.prototype.getTestAttemptDurationMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.setTestAttemptDurationMillis = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional google.protobuf.Duration test_attempt_duration = 11;
 * @return {?proto.google.protobuf.Duration}
 */
proto.build_event_stream.TestResult.prototype.getTestAttemptDuration = function() {
  return /** @type{?proto.google.protobuf.Duration} */ (
    jspb.Message.getWrapperField(this, google_protobuf_duration_pb.Duration, 11));
};


/**
 * @param {?proto.google.protobuf.Duration|undefined} value
 * @return {!proto.build_event_stream.TestResult} returns this
*/
proto.build_event_stream.TestResult.prototype.setTestAttemptDuration = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.clearTestAttemptDuration = function() {
  return this.setTestAttemptDuration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TestResult.prototype.hasTestAttemptDuration = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * repeated File test_action_output = 2;
 * @return {!Array<!proto.build_event_stream.File>}
 */
proto.build_event_stream.TestResult.prototype.getTestActionOutputList = function() {
  return /** @type{!Array<!proto.build_event_stream.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.File, 2));
};


/**
 * @param {!Array<!proto.build_event_stream.File>} value
 * @return {!proto.build_event_stream.TestResult} returns this
*/
proto.build_event_stream.TestResult.prototype.setTestActionOutputList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.build_event_stream.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.TestResult.prototype.addTestActionOutput = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.build_event_stream.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.clearTestActionOutputList = function() {
  return this.setTestActionOutputList([]);
};


/**
 * repeated string warning = 7;
 * @return {!Array<string>}
 */
proto.build_event_stream.TestResult.prototype.getWarningList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 7));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.setWarningList = function(value) {
  return jspb.Message.setField(this, 7, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.addWarning = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 7, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.clearWarningList = function() {
  return this.setWarningList([]);
};


/**
 * optional ExecutionInfo execution_info = 8;
 * @return {?proto.build_event_stream.TestResult.ExecutionInfo}
 */
proto.build_event_stream.TestResult.prototype.getExecutionInfo = function() {
  return /** @type{?proto.build_event_stream.TestResult.ExecutionInfo} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.TestResult.ExecutionInfo, 8));
};


/**
 * @param {?proto.build_event_stream.TestResult.ExecutionInfo|undefined} value
 * @return {!proto.build_event_stream.TestResult} returns this
*/
proto.build_event_stream.TestResult.prototype.setExecutionInfo = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TestResult} returns this
 */
proto.build_event_stream.TestResult.prototype.clearExecutionInfo = function() {
  return this.setExecutionInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TestResult.prototype.hasExecutionInfo = function() {
  return jspb.Message.getField(this, 8) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.TestSummary.repeatedFields_ = [3,4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.TestSummary.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.TestSummary.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.TestSummary} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestSummary.toObject = function(includeInstance, msg) {
  var f, obj = {
    overallStatus: jspb.Message.getFieldWithDefault(msg, 5, 0),
    totalRunCount: jspb.Message.getFieldWithDefault(msg, 1, 0),
    runCount: jspb.Message.getFieldWithDefault(msg, 10, 0),
    attemptCount: jspb.Message.getFieldWithDefault(msg, 15, 0),
    shardCount: jspb.Message.getFieldWithDefault(msg, 11, 0),
    passedList: jspb.Message.toObjectList(msg.getPassedList(),
    proto.build_event_stream.File.toObject, includeInstance),
    failedList: jspb.Message.toObjectList(msg.getFailedList(),
    proto.build_event_stream.File.toObject, includeInstance),
    totalNumCached: jspb.Message.getFieldWithDefault(msg, 6, 0),
    firstStartTimeMillis: jspb.Message.getFieldWithDefault(msg, 7, 0),
    firstStartTime: (f = msg.getFirstStartTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    lastStopTimeMillis: jspb.Message.getFieldWithDefault(msg, 8, 0),
    lastStopTime: (f = msg.getLastStopTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    totalRunDurationMillis: jspb.Message.getFieldWithDefault(msg, 9, 0),
    totalRunDuration: (f = msg.getTotalRunDuration()) && google_protobuf_duration_pb.Duration.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.TestSummary}
 */
proto.build_event_stream.TestSummary.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.TestSummary;
  return proto.build_event_stream.TestSummary.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.TestSummary} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.TestSummary}
 */
proto.build_event_stream.TestSummary.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 5:
      var value = /** @type {!proto.build_event_stream.TestStatus} */ (reader.readEnum());
      msg.setOverallStatus(value);
      break;
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTotalRunCount(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setRunCount(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setAttemptCount(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setShardCount(value);
      break;
    case 3:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.addPassed(value);
      break;
    case 4:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.addFailed(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTotalNumCached(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setFirstStartTimeMillis(value);
      break;
    case 13:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setFirstStartTime(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setLastStopTimeMillis(value);
      break;
    case 14:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setLastStopTime(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTotalRunDurationMillis(value);
      break;
    case 12:
      var value = new google_protobuf_duration_pb.Duration;
      reader.readMessage(value,google_protobuf_duration_pb.Duration.deserializeBinaryFromReader);
      msg.setTotalRunDuration(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.TestSummary.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.TestSummary.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.TestSummary} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TestSummary.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOverallStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      5,
      f
    );
  }
  f = message.getTotalRunCount();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getRunCount();
  if (f !== 0) {
    writer.writeInt32(
      10,
      f
    );
  }
  f = message.getAttemptCount();
  if (f !== 0) {
    writer.writeInt32(
      15,
      f
    );
  }
  f = message.getShardCount();
  if (f !== 0) {
    writer.writeInt32(
      11,
      f
    );
  }
  f = message.getPassedList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getFailedList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
  f = message.getTotalNumCached();
  if (f !== 0) {
    writer.writeInt32(
      6,
      f
    );
  }
  f = message.getFirstStartTimeMillis();
  if (f !== 0) {
    writer.writeInt64(
      7,
      f
    );
  }
  f = message.getFirstStartTime();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getLastStopTimeMillis();
  if (f !== 0) {
    writer.writeInt64(
      8,
      f
    );
  }
  f = message.getLastStopTime();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getTotalRunDurationMillis();
  if (f !== 0) {
    writer.writeInt64(
      9,
      f
    );
  }
  f = message.getTotalRunDuration();
  if (f != null) {
    writer.writeMessage(
      12,
      f,
      google_protobuf_duration_pb.Duration.serializeBinaryToWriter
    );
  }
};


/**
 * optional TestStatus overall_status = 5;
 * @return {!proto.build_event_stream.TestStatus}
 */
proto.build_event_stream.TestSummary.prototype.getOverallStatus = function() {
  return /** @type {!proto.build_event_stream.TestStatus} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {!proto.build_event_stream.TestStatus} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setOverallStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 5, value);
};


/**
 * optional int32 total_run_count = 1;
 * @return {number}
 */
proto.build_event_stream.TestSummary.prototype.getTotalRunCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setTotalRunCount = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int32 run_count = 10;
 * @return {number}
 */
proto.build_event_stream.TestSummary.prototype.getRunCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 10, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setRunCount = function(value) {
  return jspb.Message.setProto3IntField(this, 10, value);
};


/**
 * optional int32 attempt_count = 15;
 * @return {number}
 */
proto.build_event_stream.TestSummary.prototype.getAttemptCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 15, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setAttemptCount = function(value) {
  return jspb.Message.setProto3IntField(this, 15, value);
};


/**
 * optional int32 shard_count = 11;
 * @return {number}
 */
proto.build_event_stream.TestSummary.prototype.getShardCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setShardCount = function(value) {
  return jspb.Message.setProto3IntField(this, 11, value);
};


/**
 * repeated File passed = 3;
 * @return {!Array<!proto.build_event_stream.File>}
 */
proto.build_event_stream.TestSummary.prototype.getPassedList = function() {
  return /** @type{!Array<!proto.build_event_stream.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.File, 3));
};


/**
 * @param {!Array<!proto.build_event_stream.File>} value
 * @return {!proto.build_event_stream.TestSummary} returns this
*/
proto.build_event_stream.TestSummary.prototype.setPassedList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.build_event_stream.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.TestSummary.prototype.addPassed = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.build_event_stream.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.clearPassedList = function() {
  return this.setPassedList([]);
};


/**
 * repeated File failed = 4;
 * @return {!Array<!proto.build_event_stream.File>}
 */
proto.build_event_stream.TestSummary.prototype.getFailedList = function() {
  return /** @type{!Array<!proto.build_event_stream.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.File, 4));
};


/**
 * @param {!Array<!proto.build_event_stream.File>} value
 * @return {!proto.build_event_stream.TestSummary} returns this
*/
proto.build_event_stream.TestSummary.prototype.setFailedList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.build_event_stream.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.TestSummary.prototype.addFailed = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.build_event_stream.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.clearFailedList = function() {
  return this.setFailedList([]);
};


/**
 * optional int32 total_num_cached = 6;
 * @return {number}
 */
proto.build_event_stream.TestSummary.prototype.getTotalNumCached = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setTotalNumCached = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int64 first_start_time_millis = 7;
 * @return {number}
 */
proto.build_event_stream.TestSummary.prototype.getFirstStartTimeMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setFirstStartTimeMillis = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional google.protobuf.Timestamp first_start_time = 13;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.build_event_stream.TestSummary.prototype.getFirstStartTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 13));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.build_event_stream.TestSummary} returns this
*/
proto.build_event_stream.TestSummary.prototype.setFirstStartTime = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.clearFirstStartTime = function() {
  return this.setFirstStartTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TestSummary.prototype.hasFirstStartTime = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional int64 last_stop_time_millis = 8;
 * @return {number}
 */
proto.build_event_stream.TestSummary.prototype.getLastStopTimeMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setLastStopTimeMillis = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional google.protobuf.Timestamp last_stop_time = 14;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.build_event_stream.TestSummary.prototype.getLastStopTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 14));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.build_event_stream.TestSummary} returns this
*/
proto.build_event_stream.TestSummary.prototype.setLastStopTime = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.clearLastStopTime = function() {
  return this.setLastStopTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TestSummary.prototype.hasLastStopTime = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional int64 total_run_duration_millis = 9;
 * @return {number}
 */
proto.build_event_stream.TestSummary.prototype.getTotalRunDurationMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.setTotalRunDurationMillis = function(value) {
  return jspb.Message.setProto3IntField(this, 9, value);
};


/**
 * optional google.protobuf.Duration total_run_duration = 12;
 * @return {?proto.google.protobuf.Duration}
 */
proto.build_event_stream.TestSummary.prototype.getTotalRunDuration = function() {
  return /** @type{?proto.google.protobuf.Duration} */ (
    jspb.Message.getWrapperField(this, google_protobuf_duration_pb.Duration, 12));
};


/**
 * @param {?proto.google.protobuf.Duration|undefined} value
 * @return {!proto.build_event_stream.TestSummary} returns this
*/
proto.build_event_stream.TestSummary.prototype.setTotalRunDuration = function(value) {
  return jspb.Message.setWrapperField(this, 12, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.TestSummary} returns this
 */
proto.build_event_stream.TestSummary.prototype.clearTotalRunDuration = function() {
  return this.setTotalRunDuration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.TestSummary.prototype.hasTotalRunDuration = function() {
  return jspb.Message.getField(this, 12) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.TargetSummary.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.TargetSummary.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.TargetSummary} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TargetSummary.toObject = function(includeInstance, msg) {
  var f, obj = {
    overallBuildSuccess: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    overallTestStatus: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.TargetSummary}
 */
proto.build_event_stream.TargetSummary.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.TargetSummary;
  return proto.build_event_stream.TargetSummary.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.TargetSummary} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.TargetSummary}
 */
proto.build_event_stream.TargetSummary.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setOverallBuildSuccess(value);
      break;
    case 2:
      var value = /** @type {!proto.build_event_stream.TestStatus} */ (reader.readEnum());
      msg.setOverallTestStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.TargetSummary.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.TargetSummary.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.TargetSummary} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.TargetSummary.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOverallBuildSuccess();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getOverallTestStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
};


/**
 * optional bool overall_build_success = 1;
 * @return {boolean}
 */
proto.build_event_stream.TargetSummary.prototype.getOverallBuildSuccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.TargetSummary} returns this
 */
proto.build_event_stream.TargetSummary.prototype.setOverallBuildSuccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional TestStatus overall_test_status = 2;
 * @return {!proto.build_event_stream.TestStatus}
 */
proto.build_event_stream.TargetSummary.prototype.getOverallTestStatus = function() {
  return /** @type {!proto.build_event_stream.TestStatus} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.build_event_stream.TestStatus} value
 * @return {!proto.build_event_stream.TargetSummary} returns this
 */
proto.build_event_stream.TargetSummary.prototype.setOverallTestStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildFinished.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildFinished.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildFinished} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildFinished.toObject = function(includeInstance, msg) {
  var f, obj = {
    overallSuccess: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    exitCode: (f = msg.getExitCode()) && proto.build_event_stream.BuildFinished.ExitCode.toObject(includeInstance, f),
    finishTimeMillis: jspb.Message.getFieldWithDefault(msg, 2, 0),
    finishTime: (f = msg.getFinishTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    anomalyReport: (f = msg.getAnomalyReport()) && proto.build_event_stream.BuildFinished.AnomalyReport.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildFinished}
 */
proto.build_event_stream.BuildFinished.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildFinished;
  return proto.build_event_stream.BuildFinished.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildFinished} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildFinished}
 */
proto.build_event_stream.BuildFinished.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setOverallSuccess(value);
      break;
    case 3:
      var value = new proto.build_event_stream.BuildFinished.ExitCode;
      reader.readMessage(value,proto.build_event_stream.BuildFinished.ExitCode.deserializeBinaryFromReader);
      msg.setExitCode(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setFinishTimeMillis(value);
      break;
    case 5:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setFinishTime(value);
      break;
    case 4:
      var value = new proto.build_event_stream.BuildFinished.AnomalyReport;
      reader.readMessage(value,proto.build_event_stream.BuildFinished.AnomalyReport.deserializeBinaryFromReader);
      msg.setAnomalyReport(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildFinished.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildFinished.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildFinished} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildFinished.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOverallSuccess();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getExitCode();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.build_event_stream.BuildFinished.ExitCode.serializeBinaryToWriter
    );
  }
  f = message.getFinishTimeMillis();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getFinishTime();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getAnomalyReport();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.build_event_stream.BuildFinished.AnomalyReport.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildFinished.ExitCode.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildFinished.ExitCode.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildFinished.ExitCode} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildFinished.ExitCode.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    code: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildFinished.ExitCode}
 */
proto.build_event_stream.BuildFinished.ExitCode.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildFinished.ExitCode;
  return proto.build_event_stream.BuildFinished.ExitCode.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildFinished.ExitCode} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildFinished.ExitCode}
 */
proto.build_event_stream.BuildFinished.ExitCode.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildFinished.ExitCode.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildFinished.ExitCode.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildFinished.ExitCode} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildFinished.ExitCode.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCode();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.build_event_stream.BuildFinished.ExitCode.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildFinished.ExitCode} returns this
 */
proto.build_event_stream.BuildFinished.ExitCode.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int32 code = 2;
 * @return {number}
 */
proto.build_event_stream.BuildFinished.ExitCode.prototype.getCode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildFinished.ExitCode} returns this
 */
proto.build_event_stream.BuildFinished.ExitCode.prototype.setCode = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildFinished.AnomalyReport.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildFinished.AnomalyReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildFinished.AnomalyReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildFinished.AnomalyReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    wasSuspended: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildFinished.AnomalyReport}
 */
proto.build_event_stream.BuildFinished.AnomalyReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildFinished.AnomalyReport;
  return proto.build_event_stream.BuildFinished.AnomalyReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildFinished.AnomalyReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildFinished.AnomalyReport}
 */
proto.build_event_stream.BuildFinished.AnomalyReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setWasSuspended(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildFinished.AnomalyReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildFinished.AnomalyReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildFinished.AnomalyReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildFinished.AnomalyReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getWasSuspended();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool was_suspended = 1;
 * @return {boolean}
 */
proto.build_event_stream.BuildFinished.AnomalyReport.prototype.getWasSuspended = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.BuildFinished.AnomalyReport} returns this
 */
proto.build_event_stream.BuildFinished.AnomalyReport.prototype.setWasSuspended = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional bool overall_success = 1;
 * @return {boolean}
 */
proto.build_event_stream.BuildFinished.prototype.getOverallSuccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.BuildFinished} returns this
 */
proto.build_event_stream.BuildFinished.prototype.setOverallSuccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional ExitCode exit_code = 3;
 * @return {?proto.build_event_stream.BuildFinished.ExitCode}
 */
proto.build_event_stream.BuildFinished.prototype.getExitCode = function() {
  return /** @type{?proto.build_event_stream.BuildFinished.ExitCode} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildFinished.ExitCode, 3));
};


/**
 * @param {?proto.build_event_stream.BuildFinished.ExitCode|undefined} value
 * @return {!proto.build_event_stream.BuildFinished} returns this
*/
proto.build_event_stream.BuildFinished.prototype.setExitCode = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildFinished} returns this
 */
proto.build_event_stream.BuildFinished.prototype.clearExitCode = function() {
  return this.setExitCode(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildFinished.prototype.hasExitCode = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 finish_time_millis = 2;
 * @return {number}
 */
proto.build_event_stream.BuildFinished.prototype.getFinishTimeMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildFinished} returns this
 */
proto.build_event_stream.BuildFinished.prototype.setFinishTimeMillis = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional google.protobuf.Timestamp finish_time = 5;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.build_event_stream.BuildFinished.prototype.getFinishTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 5));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.build_event_stream.BuildFinished} returns this
*/
proto.build_event_stream.BuildFinished.prototype.setFinishTime = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildFinished} returns this
 */
proto.build_event_stream.BuildFinished.prototype.clearFinishTime = function() {
  return this.setFinishTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildFinished.prototype.hasFinishTime = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional AnomalyReport anomaly_report = 4;
 * @return {?proto.build_event_stream.BuildFinished.AnomalyReport}
 */
proto.build_event_stream.BuildFinished.prototype.getAnomalyReport = function() {
  return /** @type{?proto.build_event_stream.BuildFinished.AnomalyReport} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildFinished.AnomalyReport, 4));
};


/**
 * @param {?proto.build_event_stream.BuildFinished.AnomalyReport|undefined} value
 * @return {!proto.build_event_stream.BuildFinished} returns this
*/
proto.build_event_stream.BuildFinished.prototype.setAnomalyReport = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildFinished} returns this
 */
proto.build_event_stream.BuildFinished.prototype.clearAnomalyReport = function() {
  return this.setAnomalyReport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildFinished.prototype.hasAnomalyReport = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.BuildMetrics.repeatedFields_ = [9];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    actionSummary: (f = msg.getActionSummary()) && proto.build_event_stream.BuildMetrics.ActionSummary.toObject(includeInstance, f),
    memoryMetrics: (f = msg.getMemoryMetrics()) && proto.build_event_stream.BuildMetrics.MemoryMetrics.toObject(includeInstance, f),
    targetMetrics: (f = msg.getTargetMetrics()) && proto.build_event_stream.BuildMetrics.TargetMetrics.toObject(includeInstance, f),
    packageMetrics: (f = msg.getPackageMetrics()) && proto.build_event_stream.BuildMetrics.PackageMetrics.toObject(includeInstance, f),
    timingMetrics: (f = msg.getTimingMetrics()) && proto.build_event_stream.BuildMetrics.TimingMetrics.toObject(includeInstance, f),
    cumulativeMetrics: (f = msg.getCumulativeMetrics()) && proto.build_event_stream.BuildMetrics.CumulativeMetrics.toObject(includeInstance, f),
    artifactMetrics: (f = msg.getArtifactMetrics()) && proto.build_event_stream.BuildMetrics.ArtifactMetrics.toObject(includeInstance, f),
    buildGraphMetrics: (f = msg.getBuildGraphMetrics()) && proto.build_event_stream.BuildMetrics.BuildGraphMetrics.toObject(includeInstance, f),
    workerMetricsList: jspb.Message.toObjectList(msg.getWorkerMetricsList(),
    proto.build_event_stream.BuildMetrics.WorkerMetrics.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics}
 */
proto.build_event_stream.BuildMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics;
  return proto.build_event_stream.BuildMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics}
 */
proto.build_event_stream.BuildMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.BuildMetrics.ActionSummary;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.ActionSummary.deserializeBinaryFromReader);
      msg.setActionSummary(value);
      break;
    case 2:
      var value = new proto.build_event_stream.BuildMetrics.MemoryMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.MemoryMetrics.deserializeBinaryFromReader);
      msg.setMemoryMetrics(value);
      break;
    case 3:
      var value = new proto.build_event_stream.BuildMetrics.TargetMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.TargetMetrics.deserializeBinaryFromReader);
      msg.setTargetMetrics(value);
      break;
    case 4:
      var value = new proto.build_event_stream.BuildMetrics.PackageMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.PackageMetrics.deserializeBinaryFromReader);
      msg.setPackageMetrics(value);
      break;
    case 5:
      var value = new proto.build_event_stream.BuildMetrics.TimingMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.TimingMetrics.deserializeBinaryFromReader);
      msg.setTimingMetrics(value);
      break;
    case 6:
      var value = new proto.build_event_stream.BuildMetrics.CumulativeMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.CumulativeMetrics.deserializeBinaryFromReader);
      msg.setCumulativeMetrics(value);
      break;
    case 7:
      var value = new proto.build_event_stream.BuildMetrics.ArtifactMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.ArtifactMetrics.deserializeBinaryFromReader);
      msg.setArtifactMetrics(value);
      break;
    case 8:
      var value = new proto.build_event_stream.BuildMetrics.BuildGraphMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.BuildGraphMetrics.deserializeBinaryFromReader);
      msg.setBuildGraphMetrics(value);
      break;
    case 9:
      var value = new proto.build_event_stream.BuildMetrics.WorkerMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.WorkerMetrics.deserializeBinaryFromReader);
      msg.addWorkerMetrics(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getActionSummary();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.build_event_stream.BuildMetrics.ActionSummary.serializeBinaryToWriter
    );
  }
  f = message.getMemoryMetrics();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.build_event_stream.BuildMetrics.MemoryMetrics.serializeBinaryToWriter
    );
  }
  f = message.getTargetMetrics();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.build_event_stream.BuildMetrics.TargetMetrics.serializeBinaryToWriter
    );
  }
  f = message.getPackageMetrics();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.build_event_stream.BuildMetrics.PackageMetrics.serializeBinaryToWriter
    );
  }
  f = message.getTimingMetrics();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.build_event_stream.BuildMetrics.TimingMetrics.serializeBinaryToWriter
    );
  }
  f = message.getCumulativeMetrics();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.build_event_stream.BuildMetrics.CumulativeMetrics.serializeBinaryToWriter
    );
  }
  f = message.getArtifactMetrics();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.build_event_stream.BuildMetrics.ArtifactMetrics.serializeBinaryToWriter
    );
  }
  f = message.getBuildGraphMetrics();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      proto.build_event_stream.BuildMetrics.BuildGraphMetrics.serializeBinaryToWriter
    );
  }
  f = message.getWorkerMetricsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      9,
      f,
      proto.build_event_stream.BuildMetrics.WorkerMetrics.serializeBinaryToWriter
    );
  }
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.BuildMetrics.ActionSummary.repeatedFields_ = [4,6];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.ActionSummary.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ActionSummary.toObject = function(includeInstance, msg) {
  var f, obj = {
    actionsCreated: jspb.Message.getFieldWithDefault(msg, 1, 0),
    actionsCreatedNotIncludingAspects: jspb.Message.getFieldWithDefault(msg, 3, 0),
    actionsExecuted: jspb.Message.getFieldWithDefault(msg, 2, 0),
    actionDataList: jspb.Message.toObjectList(msg.getActionDataList(),
    proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.toObject, includeInstance),
    remoteCacheHits: jspb.Message.getFieldWithDefault(msg, 5, 0),
    runnerCountList: jspb.Message.toObjectList(msg.getRunnerCountList(),
    proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.ActionSummary;
  return proto.build_event_stream.BuildMetrics.ActionSummary.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setActionsCreated(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setActionsCreatedNotIncludingAspects(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setActionsExecuted(value);
      break;
    case 4:
      var value = new proto.build_event_stream.BuildMetrics.ActionSummary.ActionData;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.deserializeBinaryFromReader);
      msg.addActionData(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setRemoteCacheHits(value);
      break;
    case 6:
      var value = new proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.deserializeBinaryFromReader);
      msg.addRunnerCount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.ActionSummary.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ActionSummary.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getActionsCreated();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getActionsCreatedNotIncludingAspects();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = message.getActionsExecuted();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getActionDataList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.serializeBinaryToWriter
    );
  }
  f = message.getRemoteCacheHits();
  if (f !== 0) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = message.getRunnerCountList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.toObject = function(includeInstance, msg) {
  var f, obj = {
    mnemonic: jspb.Message.getFieldWithDefault(msg, 1, ""),
    actionsExecuted: jspb.Message.getFieldWithDefault(msg, 2, 0),
    firstStartedMs: jspb.Message.getFieldWithDefault(msg, 3, 0),
    lastEndedMs: jspb.Message.getFieldWithDefault(msg, 4, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.ActionSummary.ActionData;
  return proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setMnemonic(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setActionsExecuted(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setFirstStartedMs(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setLastEndedMs(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMnemonic();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getActionsExecuted();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getFirstStartedMs();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = message.getLastEndedMs();
  if (f !== 0) {
    writer.writeInt64(
      4,
      f
    );
  }
};


/**
 * optional string mnemonic = 1;
 * @return {string}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.getMnemonic = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.setMnemonic = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int64 actions_executed = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.getActionsExecuted = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.setActionsExecuted = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int64 first_started_ms = 3;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.getFirstStartedMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.setFirstStartedMs = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int64 last_ended_ms = 4;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.getLastEndedMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.ActionData.prototype.setLastEndedMs = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    count: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount;
  return proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCount();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int32 count = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.prototype.getCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount.prototype.setCount = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int64 actions_created = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.getActionsCreated = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.setActionsCreated = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 actions_created_not_including_aspects = 3;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.getActionsCreatedNotIncludingAspects = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.setActionsCreatedNotIncludingAspects = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int64 actions_executed = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.getActionsExecuted = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.setActionsExecuted = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * repeated ActionData action_data = 4;
 * @return {!Array<!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData>}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.getActionDataList = function() {
  return /** @type{!Array<!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.BuildMetrics.ActionSummary.ActionData, 4));
};


/**
 * @param {!Array<!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData>} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary} returns this
*/
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.setActionDataList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.ActionData}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.addActionData = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.build_event_stream.BuildMetrics.ActionSummary.ActionData, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.clearActionDataList = function() {
  return this.setActionDataList([]);
};


/**
 * optional int64 remote_cache_hits = 5;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.getRemoteCacheHits = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.setRemoteCacheHits = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * repeated RunnerCount runner_count = 6;
 * @return {!Array<!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount>}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.getRunnerCountList = function() {
  return /** @type{!Array<!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount, 6));
};


/**
 * @param {!Array<!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount>} value
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary} returns this
*/
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.setRunnerCountList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 6, value);
};


/**
 * @param {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount}
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.addRunnerCount = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 6, opt_value, proto.build_event_stream.BuildMetrics.ActionSummary.RunnerCount, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.BuildMetrics.ActionSummary} returns this
 */
proto.build_event_stream.BuildMetrics.ActionSummary.prototype.clearRunnerCountList = function() {
  return this.setRunnerCountList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.MemoryMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.MemoryMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    usedHeapSizePostBuild: jspb.Message.getFieldWithDefault(msg, 1, 0),
    peakPostGcHeapSize: jspb.Message.getFieldWithDefault(msg, 2, 0),
    garbageMetricsList: jspb.Message.toObjectList(msg.getGarbageMetricsList(),
    proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.MemoryMetrics;
  return proto.build_event_stream.BuildMetrics.MemoryMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.MemoryMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setUsedHeapSizePostBuild(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPeakPostGcHeapSize(value);
      break;
    case 3:
      var value = new proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.deserializeBinaryFromReader);
      msg.addGarbageMetrics(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.MemoryMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.MemoryMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUsedHeapSizePostBuild();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getPeakPostGcHeapSize();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getGarbageMetricsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    type: jspb.Message.getFieldWithDefault(msg, 1, ""),
    garbageCollected: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics;
  return proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setGarbageCollected(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getGarbageCollected();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
};


/**
 * optional string type = 1;
 * @return {string}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int64 garbage_collected = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.prototype.getGarbageCollected = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics.prototype.setGarbageCollected = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int64 used_heap_size_post_build = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.getUsedHeapSizePostBuild = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.setUsedHeapSizePostBuild = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 peak_post_gc_heap_size = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.getPeakPostGcHeapSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.setPeakPostGcHeapSize = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * repeated GarbageMetrics garbage_metrics = 3;
 * @return {!Array<!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics>}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.getGarbageMetricsList = function() {
  return /** @type{!Array<!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics, 3));
};


/**
 * @param {!Array<!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics>} value
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.setGarbageMetricsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics}
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.addGarbageMetrics = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.build_event_stream.BuildMetrics.MemoryMetrics.GarbageMetrics, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.BuildMetrics.MemoryMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.MemoryMetrics.prototype.clearGarbageMetricsList = function() {
  return this.setGarbageMetricsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.TargetMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.TargetMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    targetsLoaded: jspb.Message.getFieldWithDefault(msg, 1, 0),
    targetsConfigured: jspb.Message.getFieldWithDefault(msg, 2, 0),
    targetsConfiguredNotIncludingAspects: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.TargetMetrics}
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.TargetMetrics;
  return proto.build_event_stream.BuildMetrics.TargetMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.TargetMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.TargetMetrics}
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTargetsLoaded(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTargetsConfigured(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTargetsConfiguredNotIncludingAspects(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.TargetMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.TargetMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTargetsLoaded();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getTargetsConfigured();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getTargetsConfiguredNotIncludingAspects();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
};


/**
 * optional int64 targets_loaded = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.prototype.getTargetsLoaded = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.TargetMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.prototype.setTargetsLoaded = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 targets_configured = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.prototype.getTargetsConfigured = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.TargetMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.prototype.setTargetsConfigured = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int64 targets_configured_not_including_aspects = 3;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.prototype.getTargetsConfiguredNotIncludingAspects = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.TargetMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.TargetMetrics.prototype.setTargetsConfiguredNotIncludingAspects = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.PackageMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.PackageMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.PackageMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.PackageMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    packagesLoaded: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.PackageMetrics}
 */
proto.build_event_stream.BuildMetrics.PackageMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.PackageMetrics;
  return proto.build_event_stream.BuildMetrics.PackageMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.PackageMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.PackageMetrics}
 */
proto.build_event_stream.BuildMetrics.PackageMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPackagesLoaded(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.PackageMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.PackageMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.PackageMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.PackageMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPackagesLoaded();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 packages_loaded = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.PackageMetrics.prototype.getPackagesLoaded = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.PackageMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.PackageMetrics.prototype.setPackagesLoaded = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.TimingMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.TimingMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    cpuTimeInMs: jspb.Message.getFieldWithDefault(msg, 1, 0),
    wallTimeInMs: jspb.Message.getFieldWithDefault(msg, 2, 0),
    analysisPhaseTimeInMs: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.TimingMetrics}
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.TimingMetrics;
  return proto.build_event_stream.BuildMetrics.TimingMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.TimingMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.TimingMetrics}
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCpuTimeInMs(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setWallTimeInMs(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setAnalysisPhaseTimeInMs(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.TimingMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.TimingMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCpuTimeInMs();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getWallTimeInMs();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getAnalysisPhaseTimeInMs();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
};


/**
 * optional int64 cpu_time_in_ms = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.prototype.getCpuTimeInMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.TimingMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.prototype.setCpuTimeInMs = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 wall_time_in_ms = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.prototype.getWallTimeInMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.TimingMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.prototype.setWallTimeInMs = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int64 analysis_phase_time_in_ms = 3;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.prototype.getAnalysisPhaseTimeInMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.TimingMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.TimingMetrics.prototype.setAnalysisPhaseTimeInMs = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.CumulativeMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.CumulativeMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    numAnalyses: jspb.Message.getFieldWithDefault(msg, 11, 0),
    numBuilds: jspb.Message.getFieldWithDefault(msg, 12, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.CumulativeMetrics}
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.CumulativeMetrics;
  return proto.build_event_stream.BuildMetrics.CumulativeMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.CumulativeMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.CumulativeMetrics}
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 11:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNumAnalyses(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNumBuilds(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.CumulativeMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.CumulativeMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getNumAnalyses();
  if (f !== 0) {
    writer.writeInt32(
      11,
      f
    );
  }
  f = message.getNumBuilds();
  if (f !== 0) {
    writer.writeInt32(
      12,
      f
    );
  }
};


/**
 * optional int32 num_analyses = 11;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.prototype.getNumAnalyses = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.CumulativeMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.prototype.setNumAnalyses = function(value) {
  return jspb.Message.setProto3IntField(this, 11, value);
};


/**
 * optional int32 num_builds = 12;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.prototype.getNumBuilds = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 12, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.CumulativeMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.CumulativeMetrics.prototype.setNumBuilds = function(value) {
  return jspb.Message.setProto3IntField(this, 12, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.ArtifactMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    sourceArtifactsRead: (f = msg.getSourceArtifactsRead()) && proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.toObject(includeInstance, f),
    outputArtifactsSeen: (f = msg.getOutputArtifactsSeen()) && proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.toObject(includeInstance, f),
    outputArtifactsFromActionCache: (f = msg.getOutputArtifactsFromActionCache()) && proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.toObject(includeInstance, f),
    topLevelArtifacts: (f = msg.getTopLevelArtifacts()) && proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.ArtifactMetrics;
  return proto.build_event_stream.BuildMetrics.ArtifactMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = new proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.deserializeBinaryFromReader);
      msg.setSourceArtifactsRead(value);
      break;
    case 3:
      var value = new proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.deserializeBinaryFromReader);
      msg.setOutputArtifactsSeen(value);
      break;
    case 4:
      var value = new proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.deserializeBinaryFromReader);
      msg.setOutputArtifactsFromActionCache(value);
      break;
    case 5:
      var value = new proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.deserializeBinaryFromReader);
      msg.setTopLevelArtifacts(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.ArtifactMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSourceArtifactsRead();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.serializeBinaryToWriter
    );
  }
  f = message.getOutputArtifactsSeen();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.serializeBinaryToWriter
    );
  }
  f = message.getOutputArtifactsFromActionCache();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.serializeBinaryToWriter
    );
  }
  f = message.getTopLevelArtifacts();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.toObject = function(includeInstance, msg) {
  var f, obj = {
    sizeInBytes: jspb.Message.getFieldWithDefault(msg, 1, 0),
    count: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric;
  return proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSizeInBytes(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSizeInBytes();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getCount();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
};


/**
 * optional int64 size_in_bytes = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.prototype.getSizeInBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} returns this
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.prototype.setSizeInBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int32 count = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.prototype.getCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} returns this
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric.prototype.setCount = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional FilesMetric source_artifacts_read = 2;
 * @return {?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.getSourceArtifactsRead = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric, 2));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.setSourceArtifactsRead = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.clearSourceArtifactsRead = function() {
  return this.setSourceArtifactsRead(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.hasSourceArtifactsRead = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional FilesMetric output_artifacts_seen = 3;
 * @return {?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.getOutputArtifactsSeen = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric, 3));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.setOutputArtifactsSeen = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.clearOutputArtifactsSeen = function() {
  return this.setOutputArtifactsSeen(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.hasOutputArtifactsSeen = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional FilesMetric output_artifacts_from_action_cache = 4;
 * @return {?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.getOutputArtifactsFromActionCache = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric, 4));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.setOutputArtifactsFromActionCache = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.clearOutputArtifactsFromActionCache = function() {
  return this.setOutputArtifactsFromActionCache(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.hasOutputArtifactsFromActionCache = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional FilesMetric top_level_artifacts = 5;
 * @return {?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.getTopLevelArtifacts = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric, 5));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.ArtifactMetrics.FilesMetric|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.setTopLevelArtifacts = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics.ArtifactMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.clearTopLevelArtifacts = function() {
  return this.setTopLevelArtifacts(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.ArtifactMetrics.prototype.hasTopLevelArtifacts = function() {
  return jspb.Message.getField(this, 5) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.BuildGraphMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    actionLookupValueCount: jspb.Message.getFieldWithDefault(msg, 1, 0),
    actionLookupValueCountNotIncludingAspects: jspb.Message.getFieldWithDefault(msg, 5, 0),
    actionCount: jspb.Message.getFieldWithDefault(msg, 2, 0),
    actionCountNotIncludingAspects: jspb.Message.getFieldWithDefault(msg, 6, 0),
    inputFileConfiguredTargetCount: jspb.Message.getFieldWithDefault(msg, 7, 0),
    outputFileConfiguredTargetCount: jspb.Message.getFieldWithDefault(msg, 8, 0),
    otherConfiguredTargetCount: jspb.Message.getFieldWithDefault(msg, 9, 0),
    outputArtifactCount: jspb.Message.getFieldWithDefault(msg, 3, 0),
    postInvocationSkyframeNodeCount: jspb.Message.getFieldWithDefault(msg, 4, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.BuildGraphMetrics;
  return proto.build_event_stream.BuildMetrics.BuildGraphMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setActionLookupValueCount(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setActionLookupValueCountNotIncludingAspects(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setActionCount(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setActionCountNotIncludingAspects(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setInputFileConfiguredTargetCount(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setOutputFileConfiguredTargetCount(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setOtherConfiguredTargetCount(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setOutputArtifactCount(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPostInvocationSkyframeNodeCount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.BuildGraphMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getActionLookupValueCount();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getActionLookupValueCountNotIncludingAspects();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getActionCount();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getActionCountNotIncludingAspects();
  if (f !== 0) {
    writer.writeInt32(
      6,
      f
    );
  }
  f = message.getInputFileConfiguredTargetCount();
  if (f !== 0) {
    writer.writeInt32(
      7,
      f
    );
  }
  f = message.getOutputFileConfiguredTargetCount();
  if (f !== 0) {
    writer.writeInt32(
      8,
      f
    );
  }
  f = message.getOtherConfiguredTargetCount();
  if (f !== 0) {
    writer.writeInt32(
      9,
      f
    );
  }
  f = message.getOutputArtifactCount();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getPostInvocationSkyframeNodeCount();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
};


/**
 * optional int32 action_lookup_value_count = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getActionLookupValueCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setActionLookupValueCount = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int32 action_lookup_value_count_not_including_aspects = 5;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getActionLookupValueCountNotIncludingAspects = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setActionLookupValueCountNotIncludingAspects = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional int32 action_count = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getActionCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setActionCount = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int32 action_count_not_including_aspects = 6;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getActionCountNotIncludingAspects = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setActionCountNotIncludingAspects = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int32 input_file_configured_target_count = 7;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getInputFileConfiguredTargetCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setInputFileConfiguredTargetCount = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional int32 output_file_configured_target_count = 8;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getOutputFileConfiguredTargetCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setOutputFileConfiguredTargetCount = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional int32 other_configured_target_count = 9;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getOtherConfiguredTargetCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setOtherConfiguredTargetCount = function(value) {
  return jspb.Message.setProto3IntField(this, 9, value);
};


/**
 * optional int32 output_artifact_count = 3;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getOutputArtifactCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setOutputArtifactCount = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 post_invocation_skyframe_node_count = 4;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.getPostInvocationSkyframeNodeCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.BuildGraphMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.BuildGraphMetrics.prototype.setPostInvocationSkyframeNodeCount = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.repeatedFields_ = [7];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.WorkerMetrics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.WorkerMetrics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.toObject = function(includeInstance, msg) {
  var f, obj = {
    workerId: jspb.Message.getFieldWithDefault(msg, 1, 0),
    processId: jspb.Message.getFieldWithDefault(msg, 2, 0),
    mnemonic: jspb.Message.getFieldWithDefault(msg, 3, ""),
    isMultiplex: jspb.Message.getBooleanFieldWithDefault(msg, 4, false),
    isSandbox: jspb.Message.getBooleanFieldWithDefault(msg, 5, false),
    isMeasurable: jspb.Message.getBooleanFieldWithDefault(msg, 6, false),
    workerStatsList: jspb.Message.toObjectList(msg.getWorkerStatsList(),
    proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.WorkerMetrics;
  return proto.build_event_stream.BuildMetrics.WorkerMetrics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.WorkerMetrics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setWorkerId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setProcessId(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setMnemonic(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsMultiplex(value);
      break;
    case 5:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsSandbox(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsMeasurable(value);
      break;
    case 7:
      var value = new proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.deserializeBinaryFromReader);
      msg.addWorkerStats(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.WorkerMetrics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.WorkerMetrics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getWorkerId();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getProcessId();
  if (f !== 0) {
    writer.writeUint32(
      2,
      f
    );
  }
  f = message.getMnemonic();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getIsMultiplex();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
  f = message.getIsSandbox();
  if (f) {
    writer.writeBool(
      5,
      f
    );
  }
  f = message.getIsMeasurable();
  if (f) {
    writer.writeBool(
      6,
      f
    );
  }
  f = message.getWorkerStatsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      7,
      f,
      proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.toObject = function(includeInstance, msg) {
  var f, obj = {
    collectTimeInMs: jspb.Message.getFieldWithDefault(msg, 1, 0),
    workerMemoryInKb: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats;
  return proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCollectTimeInMs(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setWorkerMemoryInKb(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCollectTimeInMs();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getWorkerMemoryInKb();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
};


/**
 * optional int64 collect_time_in_ms = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.prototype.getCollectTimeInMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.prototype.setCollectTimeInMs = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int32 worker_memory_in_kb = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.prototype.getWorkerMemoryInKb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats.prototype.setWorkerMemoryInKb = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int32 worker_id = 1;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.getWorkerId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.setWorkerId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional uint32 process_id = 2;
 * @return {number}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.getProcessId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.setProcessId = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string mnemonic = 3;
 * @return {string}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.getMnemonic = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.setMnemonic = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional bool is_multiplex = 4;
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.getIsMultiplex = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 4, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.setIsMultiplex = function(value) {
  return jspb.Message.setProto3BooleanField(this, 4, value);
};


/**
 * optional bool is_sandbox = 5;
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.getIsSandbox = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 5, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.setIsSandbox = function(value) {
  return jspb.Message.setProto3BooleanField(this, 5, value);
};


/**
 * optional bool is_measurable = 6;
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.getIsMeasurable = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.setIsMeasurable = function(value) {
  return jspb.Message.setProto3BooleanField(this, 6, value);
};


/**
 * repeated WorkerStats worker_stats = 7;
 * @return {!Array<!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats>}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.getWorkerStatsList = function() {
  return /** @type{!Array<!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats, 7));
};


/**
 * @param {!Array<!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats>} value
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.setWorkerStatsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 7, value);
};


/**
 * @param {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats}
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.addWorkerStats = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 7, opt_value, proto.build_event_stream.BuildMetrics.WorkerMetrics.WorkerStats, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.WorkerMetrics.prototype.clearWorkerStatsList = function() {
  return this.setWorkerStatsList([]);
};


/**
 * optional ActionSummary action_summary = 1;
 * @return {?proto.build_event_stream.BuildMetrics.ActionSummary}
 */
proto.build_event_stream.BuildMetrics.prototype.getActionSummary = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.ActionSummary} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.ActionSummary, 1));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.ActionSummary|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setActionSummary = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearActionSummary = function() {
  return this.setActionSummary(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.prototype.hasActionSummary = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional MemoryMetrics memory_metrics = 2;
 * @return {?proto.build_event_stream.BuildMetrics.MemoryMetrics}
 */
proto.build_event_stream.BuildMetrics.prototype.getMemoryMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.MemoryMetrics} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.MemoryMetrics, 2));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.MemoryMetrics|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setMemoryMetrics = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearMemoryMetrics = function() {
  return this.setMemoryMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.prototype.hasMemoryMetrics = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional TargetMetrics target_metrics = 3;
 * @return {?proto.build_event_stream.BuildMetrics.TargetMetrics}
 */
proto.build_event_stream.BuildMetrics.prototype.getTargetMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.TargetMetrics} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.TargetMetrics, 3));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.TargetMetrics|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setTargetMetrics = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearTargetMetrics = function() {
  return this.setTargetMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.prototype.hasTargetMetrics = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional PackageMetrics package_metrics = 4;
 * @return {?proto.build_event_stream.BuildMetrics.PackageMetrics}
 */
proto.build_event_stream.BuildMetrics.prototype.getPackageMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.PackageMetrics} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.PackageMetrics, 4));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.PackageMetrics|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setPackageMetrics = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearPackageMetrics = function() {
  return this.setPackageMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.prototype.hasPackageMetrics = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional TimingMetrics timing_metrics = 5;
 * @return {?proto.build_event_stream.BuildMetrics.TimingMetrics}
 */
proto.build_event_stream.BuildMetrics.prototype.getTimingMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.TimingMetrics} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.TimingMetrics, 5));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.TimingMetrics|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setTimingMetrics = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearTimingMetrics = function() {
  return this.setTimingMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.prototype.hasTimingMetrics = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional CumulativeMetrics cumulative_metrics = 6;
 * @return {?proto.build_event_stream.BuildMetrics.CumulativeMetrics}
 */
proto.build_event_stream.BuildMetrics.prototype.getCumulativeMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.CumulativeMetrics} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.CumulativeMetrics, 6));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.CumulativeMetrics|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setCumulativeMetrics = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearCumulativeMetrics = function() {
  return this.setCumulativeMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.prototype.hasCumulativeMetrics = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional ArtifactMetrics artifact_metrics = 7;
 * @return {?proto.build_event_stream.BuildMetrics.ArtifactMetrics}
 */
proto.build_event_stream.BuildMetrics.prototype.getArtifactMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.ArtifactMetrics} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.ArtifactMetrics, 7));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.ArtifactMetrics|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setArtifactMetrics = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearArtifactMetrics = function() {
  return this.setArtifactMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.prototype.hasArtifactMetrics = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional BuildGraphMetrics build_graph_metrics = 8;
 * @return {?proto.build_event_stream.BuildMetrics.BuildGraphMetrics}
 */
proto.build_event_stream.BuildMetrics.prototype.getBuildGraphMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics.BuildGraphMetrics} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics.BuildGraphMetrics, 8));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics.BuildGraphMetrics|undefined} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setBuildGraphMetrics = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearBuildGraphMetrics = function() {
  return this.setBuildGraphMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildMetrics.prototype.hasBuildGraphMetrics = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * repeated WorkerMetrics worker_metrics = 9;
 * @return {!Array<!proto.build_event_stream.BuildMetrics.WorkerMetrics>}
 */
proto.build_event_stream.BuildMetrics.prototype.getWorkerMetricsList = function() {
  return /** @type{!Array<!proto.build_event_stream.BuildMetrics.WorkerMetrics>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.BuildMetrics.WorkerMetrics, 9));
};


/**
 * @param {!Array<!proto.build_event_stream.BuildMetrics.WorkerMetrics>} value
 * @return {!proto.build_event_stream.BuildMetrics} returns this
*/
proto.build_event_stream.BuildMetrics.prototype.setWorkerMetricsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 9, value);
};


/**
 * @param {!proto.build_event_stream.BuildMetrics.WorkerMetrics=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildMetrics.WorkerMetrics}
 */
proto.build_event_stream.BuildMetrics.prototype.addWorkerMetrics = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 9, opt_value, proto.build_event_stream.BuildMetrics.WorkerMetrics, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.BuildMetrics} returns this
 */
proto.build_event_stream.BuildMetrics.prototype.clearWorkerMetricsList = function() {
  return this.setWorkerMetricsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.BuildToolLogs.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildToolLogs.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildToolLogs.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildToolLogs} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildToolLogs.toObject = function(includeInstance, msg) {
  var f, obj = {
    logList: jspb.Message.toObjectList(msg.getLogList(),
    proto.build_event_stream.File.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildToolLogs}
 */
proto.build_event_stream.BuildToolLogs.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildToolLogs;
  return proto.build_event_stream.BuildToolLogs.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildToolLogs} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildToolLogs}
 */
proto.build_event_stream.BuildToolLogs.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.File;
      reader.readMessage(value,proto.build_event_stream.File.deserializeBinaryFromReader);
      msg.addLog(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildToolLogs.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildToolLogs.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildToolLogs} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildToolLogs.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLogList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.build_event_stream.File.serializeBinaryToWriter
    );
  }
};


/**
 * repeated File log = 1;
 * @return {!Array<!proto.build_event_stream.File>}
 */
proto.build_event_stream.BuildToolLogs.prototype.getLogList = function() {
  return /** @type{!Array<!proto.build_event_stream.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.File, 1));
};


/**
 * @param {!Array<!proto.build_event_stream.File>} value
 * @return {!proto.build_event_stream.BuildToolLogs} returns this
*/
proto.build_event_stream.BuildToolLogs.prototype.setLogList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.build_event_stream.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.File}
 */
proto.build_event_stream.BuildToolLogs.prototype.addLog = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.build_event_stream.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.BuildToolLogs} returns this
 */
proto.build_event_stream.BuildToolLogs.prototype.clearLogList = function() {
  return this.setLogList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.ConvenienceSymlinksIdentified.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.ConvenienceSymlinksIdentified} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.toObject = function(includeInstance, msg) {
  var f, obj = {
    convenienceSymlinksList: jspb.Message.toObjectList(msg.getConvenienceSymlinksList(),
    proto.build_event_stream.ConvenienceSymlink.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.ConvenienceSymlinksIdentified}
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.ConvenienceSymlinksIdentified;
  return proto.build_event_stream.ConvenienceSymlinksIdentified.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.ConvenienceSymlinksIdentified} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.ConvenienceSymlinksIdentified}
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.ConvenienceSymlink;
      reader.readMessage(value,proto.build_event_stream.ConvenienceSymlink.deserializeBinaryFromReader);
      msg.addConvenienceSymlinks(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.ConvenienceSymlinksIdentified.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.ConvenienceSymlinksIdentified} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getConvenienceSymlinksList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.build_event_stream.ConvenienceSymlink.serializeBinaryToWriter
    );
  }
};


/**
 * repeated ConvenienceSymlink convenience_symlinks = 1;
 * @return {!Array<!proto.build_event_stream.ConvenienceSymlink>}
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.prototype.getConvenienceSymlinksList = function() {
  return /** @type{!Array<!proto.build_event_stream.ConvenienceSymlink>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.ConvenienceSymlink, 1));
};


/**
 * @param {!Array<!proto.build_event_stream.ConvenienceSymlink>} value
 * @return {!proto.build_event_stream.ConvenienceSymlinksIdentified} returns this
*/
proto.build_event_stream.ConvenienceSymlinksIdentified.prototype.setConvenienceSymlinksList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.build_event_stream.ConvenienceSymlink=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.ConvenienceSymlink}
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.prototype.addConvenienceSymlinks = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.build_event_stream.ConvenienceSymlink, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.ConvenienceSymlinksIdentified} returns this
 */
proto.build_event_stream.ConvenienceSymlinksIdentified.prototype.clearConvenienceSymlinksList = function() {
  return this.setConvenienceSymlinksList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.ConvenienceSymlink.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.ConvenienceSymlink.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.ConvenienceSymlink} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.ConvenienceSymlink.toObject = function(includeInstance, msg) {
  var f, obj = {
    path: jspb.Message.getFieldWithDefault(msg, 1, ""),
    action: jspb.Message.getFieldWithDefault(msg, 2, 0),
    target: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.ConvenienceSymlink}
 */
proto.build_event_stream.ConvenienceSymlink.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.ConvenienceSymlink;
  return proto.build_event_stream.ConvenienceSymlink.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.ConvenienceSymlink} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.ConvenienceSymlink}
 */
proto.build_event_stream.ConvenienceSymlink.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 2:
      var value = /** @type {!proto.build_event_stream.ConvenienceSymlink.Action} */ (reader.readEnum());
      msg.setAction(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setTarget(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.ConvenienceSymlink.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.ConvenienceSymlink.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.ConvenienceSymlink} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.ConvenienceSymlink.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getAction();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getTarget();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.build_event_stream.ConvenienceSymlink.Action = {
  UNKNOWN: 0,
  CREATE: 1,
  DELETE: 2
};

/**
 * optional string path = 1;
 * @return {string}
 */
proto.build_event_stream.ConvenienceSymlink.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.ConvenienceSymlink} returns this
 */
proto.build_event_stream.ConvenienceSymlink.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional Action action = 2;
 * @return {!proto.build_event_stream.ConvenienceSymlink.Action}
 */
proto.build_event_stream.ConvenienceSymlink.prototype.getAction = function() {
  return /** @type {!proto.build_event_stream.ConvenienceSymlink.Action} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.build_event_stream.ConvenienceSymlink.Action} value
 * @return {!proto.build_event_stream.ConvenienceSymlink} returns this
 */
proto.build_event_stream.ConvenienceSymlink.prototype.setAction = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional string target = 3;
 * @return {string}
 */
proto.build_event_stream.ConvenienceSymlink.prototype.getTarget = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.build_event_stream.ConvenienceSymlink} returns this
 */
proto.build_event_stream.ConvenienceSymlink.prototype.setTarget = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.build_event_stream.BuildEvent.repeatedFields_ = [2];

/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.build_event_stream.BuildEvent.oneofGroups_ = [[3,4,5,12,22,13,16,21,17,6,18,7,15,8,10,9,28,14,23,24,25,26,27]];

/**
 * @enum {number}
 */
proto.build_event_stream.BuildEvent.PayloadCase = {
  PAYLOAD_NOT_SET: 0,
  PROGRESS: 3,
  ABORTED: 4,
  STARTED: 5,
  UNSTRUCTURED_COMMAND_LINE: 12,
  STRUCTURED_COMMAND_LINE: 22,
  OPTIONS_PARSED: 13,
  WORKSPACE_STATUS: 16,
  FETCH: 21,
  CONFIGURATION: 17,
  EXPANDED: 6,
  CONFIGURED: 18,
  ACTION: 7,
  NAMED_SET_OF_FILES: 15,
  COMPLETED: 8,
  TEST_RESULT: 10,
  TEST_SUMMARY: 9,
  TARGET_SUMMARY: 28,
  FINISHED: 14,
  BUILD_TOOL_LOGS: 23,
  BUILD_METRICS: 24,
  WORKSPACE_INFO: 25,
  BUILD_METADATA: 26,
  CONVENIENCE_SYMLINKS_IDENTIFIED: 27
};

/**
 * @return {proto.build_event_stream.BuildEvent.PayloadCase}
 */
proto.build_event_stream.BuildEvent.prototype.getPayloadCase = function() {
  return /** @type {proto.build_event_stream.BuildEvent.PayloadCase} */(jspb.Message.computeOneofCase(this, proto.build_event_stream.BuildEvent.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.build_event_stream.BuildEvent.prototype.toObject = function(opt_includeInstance) {
  return proto.build_event_stream.BuildEvent.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.build_event_stream.BuildEvent} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEvent.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: (f = msg.getId()) && proto.build_event_stream.BuildEventId.toObject(includeInstance, f),
    childrenList: jspb.Message.toObjectList(msg.getChildrenList(),
    proto.build_event_stream.BuildEventId.toObject, includeInstance),
    lastMessage: jspb.Message.getBooleanFieldWithDefault(msg, 20, false),
    progress: (f = msg.getProgress()) && proto.build_event_stream.Progress.toObject(includeInstance, f),
    aborted: (f = msg.getAborted()) && proto.build_event_stream.Aborted.toObject(includeInstance, f),
    started: (f = msg.getStarted()) && proto.build_event_stream.BuildStarted.toObject(includeInstance, f),
    unstructuredCommandLine: (f = msg.getUnstructuredCommandLine()) && proto.build_event_stream.UnstructuredCommandLine.toObject(includeInstance, f),
    structuredCommandLine: (f = msg.getStructuredCommandLine()) && build_event_stream_command_line_pb.CommandLine.toObject(includeInstance, f),
    optionsParsed: (f = msg.getOptionsParsed()) && proto.build_event_stream.OptionsParsed.toObject(includeInstance, f),
    workspaceStatus: (f = msg.getWorkspaceStatus()) && proto.build_event_stream.WorkspaceStatus.toObject(includeInstance, f),
    fetch: (f = msg.getFetch()) && proto.build_event_stream.Fetch.toObject(includeInstance, f),
    configuration: (f = msg.getConfiguration()) && proto.build_event_stream.Configuration.toObject(includeInstance, f),
    expanded: (f = msg.getExpanded()) && proto.build_event_stream.PatternExpanded.toObject(includeInstance, f),
    configured: (f = msg.getConfigured()) && proto.build_event_stream.TargetConfigured.toObject(includeInstance, f),
    action: (f = msg.getAction()) && proto.build_event_stream.ActionExecuted.toObject(includeInstance, f),
    namedSetOfFiles: (f = msg.getNamedSetOfFiles()) && proto.build_event_stream.NamedSetOfFiles.toObject(includeInstance, f),
    completed: (f = msg.getCompleted()) && proto.build_event_stream.TargetComplete.toObject(includeInstance, f),
    testResult: (f = msg.getTestResult()) && proto.build_event_stream.TestResult.toObject(includeInstance, f),
    testSummary: (f = msg.getTestSummary()) && proto.build_event_stream.TestSummary.toObject(includeInstance, f),
    targetSummary: (f = msg.getTargetSummary()) && proto.build_event_stream.TargetSummary.toObject(includeInstance, f),
    finished: (f = msg.getFinished()) && proto.build_event_stream.BuildFinished.toObject(includeInstance, f),
    buildToolLogs: (f = msg.getBuildToolLogs()) && proto.build_event_stream.BuildToolLogs.toObject(includeInstance, f),
    buildMetrics: (f = msg.getBuildMetrics()) && proto.build_event_stream.BuildMetrics.toObject(includeInstance, f),
    workspaceInfo: (f = msg.getWorkspaceInfo()) && proto.build_event_stream.WorkspaceConfig.toObject(includeInstance, f),
    buildMetadata: (f = msg.getBuildMetadata()) && proto.build_event_stream.BuildMetadata.toObject(includeInstance, f),
    convenienceSymlinksIdentified: (f = msg.getConvenienceSymlinksIdentified()) && proto.build_event_stream.ConvenienceSymlinksIdentified.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.build_event_stream.BuildEvent}
 */
proto.build_event_stream.BuildEvent.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.build_event_stream.BuildEvent;
  return proto.build_event_stream.BuildEvent.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.build_event_stream.BuildEvent} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.build_event_stream.BuildEvent}
 */
proto.build_event_stream.BuildEvent.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.build_event_stream.BuildEventId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.deserializeBinaryFromReader);
      msg.setId(value);
      break;
    case 2:
      var value = new proto.build_event_stream.BuildEventId;
      reader.readMessage(value,proto.build_event_stream.BuildEventId.deserializeBinaryFromReader);
      msg.addChildren(value);
      break;
    case 20:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setLastMessage(value);
      break;
    case 3:
      var value = new proto.build_event_stream.Progress;
      reader.readMessage(value,proto.build_event_stream.Progress.deserializeBinaryFromReader);
      msg.setProgress(value);
      break;
    case 4:
      var value = new proto.build_event_stream.Aborted;
      reader.readMessage(value,proto.build_event_stream.Aborted.deserializeBinaryFromReader);
      msg.setAborted(value);
      break;
    case 5:
      var value = new proto.build_event_stream.BuildStarted;
      reader.readMessage(value,proto.build_event_stream.BuildStarted.deserializeBinaryFromReader);
      msg.setStarted(value);
      break;
    case 12:
      var value = new proto.build_event_stream.UnstructuredCommandLine;
      reader.readMessage(value,proto.build_event_stream.UnstructuredCommandLine.deserializeBinaryFromReader);
      msg.setUnstructuredCommandLine(value);
      break;
    case 22:
      var value = new build_event_stream_command_line_pb.CommandLine;
      reader.readMessage(value,build_event_stream_command_line_pb.CommandLine.deserializeBinaryFromReader);
      msg.setStructuredCommandLine(value);
      break;
    case 13:
      var value = new proto.build_event_stream.OptionsParsed;
      reader.readMessage(value,proto.build_event_stream.OptionsParsed.deserializeBinaryFromReader);
      msg.setOptionsParsed(value);
      break;
    case 16:
      var value = new proto.build_event_stream.WorkspaceStatus;
      reader.readMessage(value,proto.build_event_stream.WorkspaceStatus.deserializeBinaryFromReader);
      msg.setWorkspaceStatus(value);
      break;
    case 21:
      var value = new proto.build_event_stream.Fetch;
      reader.readMessage(value,proto.build_event_stream.Fetch.deserializeBinaryFromReader);
      msg.setFetch(value);
      break;
    case 17:
      var value = new proto.build_event_stream.Configuration;
      reader.readMessage(value,proto.build_event_stream.Configuration.deserializeBinaryFromReader);
      msg.setConfiguration(value);
      break;
    case 6:
      var value = new proto.build_event_stream.PatternExpanded;
      reader.readMessage(value,proto.build_event_stream.PatternExpanded.deserializeBinaryFromReader);
      msg.setExpanded(value);
      break;
    case 18:
      var value = new proto.build_event_stream.TargetConfigured;
      reader.readMessage(value,proto.build_event_stream.TargetConfigured.deserializeBinaryFromReader);
      msg.setConfigured(value);
      break;
    case 7:
      var value = new proto.build_event_stream.ActionExecuted;
      reader.readMessage(value,proto.build_event_stream.ActionExecuted.deserializeBinaryFromReader);
      msg.setAction(value);
      break;
    case 15:
      var value = new proto.build_event_stream.NamedSetOfFiles;
      reader.readMessage(value,proto.build_event_stream.NamedSetOfFiles.deserializeBinaryFromReader);
      msg.setNamedSetOfFiles(value);
      break;
    case 8:
      var value = new proto.build_event_stream.TargetComplete;
      reader.readMessage(value,proto.build_event_stream.TargetComplete.deserializeBinaryFromReader);
      msg.setCompleted(value);
      break;
    case 10:
      var value = new proto.build_event_stream.TestResult;
      reader.readMessage(value,proto.build_event_stream.TestResult.deserializeBinaryFromReader);
      msg.setTestResult(value);
      break;
    case 9:
      var value = new proto.build_event_stream.TestSummary;
      reader.readMessage(value,proto.build_event_stream.TestSummary.deserializeBinaryFromReader);
      msg.setTestSummary(value);
      break;
    case 28:
      var value = new proto.build_event_stream.TargetSummary;
      reader.readMessage(value,proto.build_event_stream.TargetSummary.deserializeBinaryFromReader);
      msg.setTargetSummary(value);
      break;
    case 14:
      var value = new proto.build_event_stream.BuildFinished;
      reader.readMessage(value,proto.build_event_stream.BuildFinished.deserializeBinaryFromReader);
      msg.setFinished(value);
      break;
    case 23:
      var value = new proto.build_event_stream.BuildToolLogs;
      reader.readMessage(value,proto.build_event_stream.BuildToolLogs.deserializeBinaryFromReader);
      msg.setBuildToolLogs(value);
      break;
    case 24:
      var value = new proto.build_event_stream.BuildMetrics;
      reader.readMessage(value,proto.build_event_stream.BuildMetrics.deserializeBinaryFromReader);
      msg.setBuildMetrics(value);
      break;
    case 25:
      var value = new proto.build_event_stream.WorkspaceConfig;
      reader.readMessage(value,proto.build_event_stream.WorkspaceConfig.deserializeBinaryFromReader);
      msg.setWorkspaceInfo(value);
      break;
    case 26:
      var value = new proto.build_event_stream.BuildMetadata;
      reader.readMessage(value,proto.build_event_stream.BuildMetadata.deserializeBinaryFromReader);
      msg.setBuildMetadata(value);
      break;
    case 27:
      var value = new proto.build_event_stream.ConvenienceSymlinksIdentified;
      reader.readMessage(value,proto.build_event_stream.ConvenienceSymlinksIdentified.deserializeBinaryFromReader);
      msg.setConvenienceSymlinksIdentified(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.build_event_stream.BuildEvent.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.build_event_stream.BuildEvent.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.build_event_stream.BuildEvent} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.build_event_stream.BuildEvent.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.build_event_stream.BuildEventId.serializeBinaryToWriter
    );
  }
  f = message.getChildrenList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.build_event_stream.BuildEventId.serializeBinaryToWriter
    );
  }
  f = message.getLastMessage();
  if (f) {
    writer.writeBool(
      20,
      f
    );
  }
  f = message.getProgress();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.build_event_stream.Progress.serializeBinaryToWriter
    );
  }
  f = message.getAborted();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.build_event_stream.Aborted.serializeBinaryToWriter
    );
  }
  f = message.getStarted();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.build_event_stream.BuildStarted.serializeBinaryToWriter
    );
  }
  f = message.getUnstructuredCommandLine();
  if (f != null) {
    writer.writeMessage(
      12,
      f,
      proto.build_event_stream.UnstructuredCommandLine.serializeBinaryToWriter
    );
  }
  f = message.getStructuredCommandLine();
  if (f != null) {
    writer.writeMessage(
      22,
      f,
      build_event_stream_command_line_pb.CommandLine.serializeBinaryToWriter
    );
  }
  f = message.getOptionsParsed();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      proto.build_event_stream.OptionsParsed.serializeBinaryToWriter
    );
  }
  f = message.getWorkspaceStatus();
  if (f != null) {
    writer.writeMessage(
      16,
      f,
      proto.build_event_stream.WorkspaceStatus.serializeBinaryToWriter
    );
  }
  f = message.getFetch();
  if (f != null) {
    writer.writeMessage(
      21,
      f,
      proto.build_event_stream.Fetch.serializeBinaryToWriter
    );
  }
  f = message.getConfiguration();
  if (f != null) {
    writer.writeMessage(
      17,
      f,
      proto.build_event_stream.Configuration.serializeBinaryToWriter
    );
  }
  f = message.getExpanded();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.build_event_stream.PatternExpanded.serializeBinaryToWriter
    );
  }
  f = message.getConfigured();
  if (f != null) {
    writer.writeMessage(
      18,
      f,
      proto.build_event_stream.TargetConfigured.serializeBinaryToWriter
    );
  }
  f = message.getAction();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.build_event_stream.ActionExecuted.serializeBinaryToWriter
    );
  }
  f = message.getNamedSetOfFiles();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      proto.build_event_stream.NamedSetOfFiles.serializeBinaryToWriter
    );
  }
  f = message.getCompleted();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      proto.build_event_stream.TargetComplete.serializeBinaryToWriter
    );
  }
  f = message.getTestResult();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      proto.build_event_stream.TestResult.serializeBinaryToWriter
    );
  }
  f = message.getTestSummary();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      proto.build_event_stream.TestSummary.serializeBinaryToWriter
    );
  }
  f = message.getTargetSummary();
  if (f != null) {
    writer.writeMessage(
      28,
      f,
      proto.build_event_stream.TargetSummary.serializeBinaryToWriter
    );
  }
  f = message.getFinished();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      proto.build_event_stream.BuildFinished.serializeBinaryToWriter
    );
  }
  f = message.getBuildToolLogs();
  if (f != null) {
    writer.writeMessage(
      23,
      f,
      proto.build_event_stream.BuildToolLogs.serializeBinaryToWriter
    );
  }
  f = message.getBuildMetrics();
  if (f != null) {
    writer.writeMessage(
      24,
      f,
      proto.build_event_stream.BuildMetrics.serializeBinaryToWriter
    );
  }
  f = message.getWorkspaceInfo();
  if (f != null) {
    writer.writeMessage(
      25,
      f,
      proto.build_event_stream.WorkspaceConfig.serializeBinaryToWriter
    );
  }
  f = message.getBuildMetadata();
  if (f != null) {
    writer.writeMessage(
      26,
      f,
      proto.build_event_stream.BuildMetadata.serializeBinaryToWriter
    );
  }
  f = message.getConvenienceSymlinksIdentified();
  if (f != null) {
    writer.writeMessage(
      27,
      f,
      proto.build_event_stream.ConvenienceSymlinksIdentified.serializeBinaryToWriter
    );
  }
};


/**
 * optional BuildEventId id = 1;
 * @return {?proto.build_event_stream.BuildEventId}
 */
proto.build_event_stream.BuildEvent.prototype.getId = function() {
  return /** @type{?proto.build_event_stream.BuildEventId} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildEventId, 1));
};


/**
 * @param {?proto.build_event_stream.BuildEventId|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearId = function() {
  return this.setId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated BuildEventId children = 2;
 * @return {!Array<!proto.build_event_stream.BuildEventId>}
 */
proto.build_event_stream.BuildEvent.prototype.getChildrenList = function() {
  return /** @type{!Array<!proto.build_event_stream.BuildEventId>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.build_event_stream.BuildEventId, 2));
};


/**
 * @param {!Array<!proto.build_event_stream.BuildEventId>} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setChildrenList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.build_event_stream.BuildEventId=} opt_value
 * @param {number=} opt_index
 * @return {!proto.build_event_stream.BuildEventId}
 */
proto.build_event_stream.BuildEvent.prototype.addChildren = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.build_event_stream.BuildEventId, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearChildrenList = function() {
  return this.setChildrenList([]);
};


/**
 * optional bool last_message = 20;
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.getLastMessage = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 20, false));
};


/**
 * @param {boolean} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.setLastMessage = function(value) {
  return jspb.Message.setProto3BooleanField(this, 20, value);
};


/**
 * optional Progress progress = 3;
 * @return {?proto.build_event_stream.Progress}
 */
proto.build_event_stream.BuildEvent.prototype.getProgress = function() {
  return /** @type{?proto.build_event_stream.Progress} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.Progress, 3));
};


/**
 * @param {?proto.build_event_stream.Progress|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setProgress = function(value) {
  return jspb.Message.setOneofWrapperField(this, 3, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearProgress = function() {
  return this.setProgress(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasProgress = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional Aborted aborted = 4;
 * @return {?proto.build_event_stream.Aborted}
 */
proto.build_event_stream.BuildEvent.prototype.getAborted = function() {
  return /** @type{?proto.build_event_stream.Aborted} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.Aborted, 4));
};


/**
 * @param {?proto.build_event_stream.Aborted|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setAborted = function(value) {
  return jspb.Message.setOneofWrapperField(this, 4, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearAborted = function() {
  return this.setAborted(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasAborted = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional BuildStarted started = 5;
 * @return {?proto.build_event_stream.BuildStarted}
 */
proto.build_event_stream.BuildEvent.prototype.getStarted = function() {
  return /** @type{?proto.build_event_stream.BuildStarted} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildStarted, 5));
};


/**
 * @param {?proto.build_event_stream.BuildStarted|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setStarted = function(value) {
  return jspb.Message.setOneofWrapperField(this, 5, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearStarted = function() {
  return this.setStarted(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasStarted = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional UnstructuredCommandLine unstructured_command_line = 12;
 * @return {?proto.build_event_stream.UnstructuredCommandLine}
 */
proto.build_event_stream.BuildEvent.prototype.getUnstructuredCommandLine = function() {
  return /** @type{?proto.build_event_stream.UnstructuredCommandLine} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.UnstructuredCommandLine, 12));
};


/**
 * @param {?proto.build_event_stream.UnstructuredCommandLine|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setUnstructuredCommandLine = function(value) {
  return jspb.Message.setOneofWrapperField(this, 12, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearUnstructuredCommandLine = function() {
  return this.setUnstructuredCommandLine(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasUnstructuredCommandLine = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional command_line.CommandLine structured_command_line = 22;
 * @return {?proto.command_line.CommandLine}
 */
proto.build_event_stream.BuildEvent.prototype.getStructuredCommandLine = function() {
  return /** @type{?proto.command_line.CommandLine} */ (
    jspb.Message.getWrapperField(this, build_event_stream_command_line_pb.CommandLine, 22));
};


/**
 * @param {?proto.command_line.CommandLine|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setStructuredCommandLine = function(value) {
  return jspb.Message.setOneofWrapperField(this, 22, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearStructuredCommandLine = function() {
  return this.setStructuredCommandLine(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasStructuredCommandLine = function() {
  return jspb.Message.getField(this, 22) != null;
};


/**
 * optional OptionsParsed options_parsed = 13;
 * @return {?proto.build_event_stream.OptionsParsed}
 */
proto.build_event_stream.BuildEvent.prototype.getOptionsParsed = function() {
  return /** @type{?proto.build_event_stream.OptionsParsed} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.OptionsParsed, 13));
};


/**
 * @param {?proto.build_event_stream.OptionsParsed|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setOptionsParsed = function(value) {
  return jspb.Message.setOneofWrapperField(this, 13, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearOptionsParsed = function() {
  return this.setOptionsParsed(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasOptionsParsed = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional WorkspaceStatus workspace_status = 16;
 * @return {?proto.build_event_stream.WorkspaceStatus}
 */
proto.build_event_stream.BuildEvent.prototype.getWorkspaceStatus = function() {
  return /** @type{?proto.build_event_stream.WorkspaceStatus} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.WorkspaceStatus, 16));
};


/**
 * @param {?proto.build_event_stream.WorkspaceStatus|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setWorkspaceStatus = function(value) {
  return jspb.Message.setOneofWrapperField(this, 16, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearWorkspaceStatus = function() {
  return this.setWorkspaceStatus(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasWorkspaceStatus = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * optional Fetch fetch = 21;
 * @return {?proto.build_event_stream.Fetch}
 */
proto.build_event_stream.BuildEvent.prototype.getFetch = function() {
  return /** @type{?proto.build_event_stream.Fetch} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.Fetch, 21));
};


/**
 * @param {?proto.build_event_stream.Fetch|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setFetch = function(value) {
  return jspb.Message.setOneofWrapperField(this, 21, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearFetch = function() {
  return this.setFetch(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasFetch = function() {
  return jspb.Message.getField(this, 21) != null;
};


/**
 * optional Configuration configuration = 17;
 * @return {?proto.build_event_stream.Configuration}
 */
proto.build_event_stream.BuildEvent.prototype.getConfiguration = function() {
  return /** @type{?proto.build_event_stream.Configuration} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.Configuration, 17));
};


/**
 * @param {?proto.build_event_stream.Configuration|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setConfiguration = function(value) {
  return jspb.Message.setOneofWrapperField(this, 17, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearConfiguration = function() {
  return this.setConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasConfiguration = function() {
  return jspb.Message.getField(this, 17) != null;
};


/**
 * optional PatternExpanded expanded = 6;
 * @return {?proto.build_event_stream.PatternExpanded}
 */
proto.build_event_stream.BuildEvent.prototype.getExpanded = function() {
  return /** @type{?proto.build_event_stream.PatternExpanded} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.PatternExpanded, 6));
};


/**
 * @param {?proto.build_event_stream.PatternExpanded|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setExpanded = function(value) {
  return jspb.Message.setOneofWrapperField(this, 6, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearExpanded = function() {
  return this.setExpanded(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasExpanded = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional TargetConfigured configured = 18;
 * @return {?proto.build_event_stream.TargetConfigured}
 */
proto.build_event_stream.BuildEvent.prototype.getConfigured = function() {
  return /** @type{?proto.build_event_stream.TargetConfigured} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.TargetConfigured, 18));
};


/**
 * @param {?proto.build_event_stream.TargetConfigured|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setConfigured = function(value) {
  return jspb.Message.setOneofWrapperField(this, 18, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearConfigured = function() {
  return this.setConfigured(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasConfigured = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * optional ActionExecuted action = 7;
 * @return {?proto.build_event_stream.ActionExecuted}
 */
proto.build_event_stream.BuildEvent.prototype.getAction = function() {
  return /** @type{?proto.build_event_stream.ActionExecuted} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.ActionExecuted, 7));
};


/**
 * @param {?proto.build_event_stream.ActionExecuted|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setAction = function(value) {
  return jspb.Message.setOneofWrapperField(this, 7, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearAction = function() {
  return this.setAction(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasAction = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional NamedSetOfFiles named_set_of_files = 15;
 * @return {?proto.build_event_stream.NamedSetOfFiles}
 */
proto.build_event_stream.BuildEvent.prototype.getNamedSetOfFiles = function() {
  return /** @type{?proto.build_event_stream.NamedSetOfFiles} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.NamedSetOfFiles, 15));
};


/**
 * @param {?proto.build_event_stream.NamedSetOfFiles|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setNamedSetOfFiles = function(value) {
  return jspb.Message.setOneofWrapperField(this, 15, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearNamedSetOfFiles = function() {
  return this.setNamedSetOfFiles(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasNamedSetOfFiles = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional TargetComplete completed = 8;
 * @return {?proto.build_event_stream.TargetComplete}
 */
proto.build_event_stream.BuildEvent.prototype.getCompleted = function() {
  return /** @type{?proto.build_event_stream.TargetComplete} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.TargetComplete, 8));
};


/**
 * @param {?proto.build_event_stream.TargetComplete|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setCompleted = function(value) {
  return jspb.Message.setOneofWrapperField(this, 8, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearCompleted = function() {
  return this.setCompleted(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasCompleted = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional TestResult test_result = 10;
 * @return {?proto.build_event_stream.TestResult}
 */
proto.build_event_stream.BuildEvent.prototype.getTestResult = function() {
  return /** @type{?proto.build_event_stream.TestResult} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.TestResult, 10));
};


/**
 * @param {?proto.build_event_stream.TestResult|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setTestResult = function(value) {
  return jspb.Message.setOneofWrapperField(this, 10, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearTestResult = function() {
  return this.setTestResult(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasTestResult = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional TestSummary test_summary = 9;
 * @return {?proto.build_event_stream.TestSummary}
 */
proto.build_event_stream.BuildEvent.prototype.getTestSummary = function() {
  return /** @type{?proto.build_event_stream.TestSummary} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.TestSummary, 9));
};


/**
 * @param {?proto.build_event_stream.TestSummary|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setTestSummary = function(value) {
  return jspb.Message.setOneofWrapperField(this, 9, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearTestSummary = function() {
  return this.setTestSummary(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasTestSummary = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional TargetSummary target_summary = 28;
 * @return {?proto.build_event_stream.TargetSummary}
 */
proto.build_event_stream.BuildEvent.prototype.getTargetSummary = function() {
  return /** @type{?proto.build_event_stream.TargetSummary} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.TargetSummary, 28));
};


/**
 * @param {?proto.build_event_stream.TargetSummary|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setTargetSummary = function(value) {
  return jspb.Message.setOneofWrapperField(this, 28, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearTargetSummary = function() {
  return this.setTargetSummary(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasTargetSummary = function() {
  return jspb.Message.getField(this, 28) != null;
};


/**
 * optional BuildFinished finished = 14;
 * @return {?proto.build_event_stream.BuildFinished}
 */
proto.build_event_stream.BuildEvent.prototype.getFinished = function() {
  return /** @type{?proto.build_event_stream.BuildFinished} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildFinished, 14));
};


/**
 * @param {?proto.build_event_stream.BuildFinished|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setFinished = function(value) {
  return jspb.Message.setOneofWrapperField(this, 14, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearFinished = function() {
  return this.setFinished(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasFinished = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional BuildToolLogs build_tool_logs = 23;
 * @return {?proto.build_event_stream.BuildToolLogs}
 */
proto.build_event_stream.BuildEvent.prototype.getBuildToolLogs = function() {
  return /** @type{?proto.build_event_stream.BuildToolLogs} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildToolLogs, 23));
};


/**
 * @param {?proto.build_event_stream.BuildToolLogs|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setBuildToolLogs = function(value) {
  return jspb.Message.setOneofWrapperField(this, 23, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearBuildToolLogs = function() {
  return this.setBuildToolLogs(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasBuildToolLogs = function() {
  return jspb.Message.getField(this, 23) != null;
};


/**
 * optional BuildMetrics build_metrics = 24;
 * @return {?proto.build_event_stream.BuildMetrics}
 */
proto.build_event_stream.BuildEvent.prototype.getBuildMetrics = function() {
  return /** @type{?proto.build_event_stream.BuildMetrics} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetrics, 24));
};


/**
 * @param {?proto.build_event_stream.BuildMetrics|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setBuildMetrics = function(value) {
  return jspb.Message.setOneofWrapperField(this, 24, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearBuildMetrics = function() {
  return this.setBuildMetrics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasBuildMetrics = function() {
  return jspb.Message.getField(this, 24) != null;
};


/**
 * optional WorkspaceConfig workspace_info = 25;
 * @return {?proto.build_event_stream.WorkspaceConfig}
 */
proto.build_event_stream.BuildEvent.prototype.getWorkspaceInfo = function() {
  return /** @type{?proto.build_event_stream.WorkspaceConfig} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.WorkspaceConfig, 25));
};


/**
 * @param {?proto.build_event_stream.WorkspaceConfig|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setWorkspaceInfo = function(value) {
  return jspb.Message.setOneofWrapperField(this, 25, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearWorkspaceInfo = function() {
  return this.setWorkspaceInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasWorkspaceInfo = function() {
  return jspb.Message.getField(this, 25) != null;
};


/**
 * optional BuildMetadata build_metadata = 26;
 * @return {?proto.build_event_stream.BuildMetadata}
 */
proto.build_event_stream.BuildEvent.prototype.getBuildMetadata = function() {
  return /** @type{?proto.build_event_stream.BuildMetadata} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.BuildMetadata, 26));
};


/**
 * @param {?proto.build_event_stream.BuildMetadata|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setBuildMetadata = function(value) {
  return jspb.Message.setOneofWrapperField(this, 26, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearBuildMetadata = function() {
  return this.setBuildMetadata(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasBuildMetadata = function() {
  return jspb.Message.getField(this, 26) != null;
};


/**
 * optional ConvenienceSymlinksIdentified convenience_symlinks_identified = 27;
 * @return {?proto.build_event_stream.ConvenienceSymlinksIdentified}
 */
proto.build_event_stream.BuildEvent.prototype.getConvenienceSymlinksIdentified = function() {
  return /** @type{?proto.build_event_stream.ConvenienceSymlinksIdentified} */ (
    jspb.Message.getWrapperField(this, proto.build_event_stream.ConvenienceSymlinksIdentified, 27));
};


/**
 * @param {?proto.build_event_stream.ConvenienceSymlinksIdentified|undefined} value
 * @return {!proto.build_event_stream.BuildEvent} returns this
*/
proto.build_event_stream.BuildEvent.prototype.setConvenienceSymlinksIdentified = function(value) {
  return jspb.Message.setOneofWrapperField(this, 27, proto.build_event_stream.BuildEvent.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.build_event_stream.BuildEvent} returns this
 */
proto.build_event_stream.BuildEvent.prototype.clearConvenienceSymlinksIdentified = function() {
  return this.setConvenienceSymlinksIdentified(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.build_event_stream.BuildEvent.prototype.hasConvenienceSymlinksIdentified = function() {
  return jspb.Message.getField(this, 27) != null;
};


/**
 * @enum {number}
 */
proto.build_event_stream.TestSize = {
  UNKNOWN: 0,
  SMALL: 1,
  MEDIUM: 2,
  LARGE: 3,
  ENORMOUS: 4
};

/**
 * @enum {number}
 */
proto.build_event_stream.TestStatus = {
  NO_STATUS: 0,
  PASSED: 1,
  FLAKY: 2,
  TIMEOUT: 3,
  FAILED: 4,
  INCOMPLETE: 5,
  REMOTE_FAILURE: 6,
  FAILED_TO_BUILD: 7,
  TOOL_HALTED_BEFORE_TESTING: 8
};

goog.object.extend(exports, proto.build_event_stream);
