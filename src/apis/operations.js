/**
  Copyright 2021 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import Axios from 'axios'
import { Buffer } from 'buffer'
import { CheckIcon, CogIcon, DocumentSearchIcon, PauseIcon, QuestionMarkCircleIcon, StopIcon, XIcon } from '@heroicons/vue/outline'

import {
  ListOperationsResponse,
  Operation
} from '@/protos/google/longrunning/operations_pb.js'
import {
  Code
} from '@/protos/google/rpc/code_pb.js'
import {
  ExecuteOperationMetadata,
  ExecuteResponse,
  ExecutionStage
} from '@/protos/build/bazel/remote/execution/v2/remote_execution_pb.js'

import base from '@/apis/base.js'
import bytestream from '@/apis/bytestream.js'
import reapi from '@/apis/reapi.js'

const config = base.getConfig()
const OPERATIONS_URL = `${config.backendUrl}/api/v1/operations`

function dateFromTimestamp (timestamp) {
  return new Date((timestamp.seconds * 1000) + (timestamp.nanos * 0.000001))
}

function totalExecutionTime (operation) {
  if (operation.response) {
    const executionMetadata = operation.response.result.executionMetadata
    if (executionMetadata) {
      const start = dateFromTimestamp(executionMetadata.queuedTimestamp)
      const end = dateFromTimestamp(executionMetadata.workerCompletedTimestamp)
      const deltaSecs = (end.getTime() - start.getTime()) / 1000
      return deltaSecs
    }
  }
  return undefined
}

function u8ToString (u8) {
  return Buffer.from(u8).toString()
}

export default {
  async list (filterString, pageSize, pageToken) {
    const params = new URLSearchParams({
      q: filterString,
      page_size: pageSize || 0,
      page_token: pageToken || ''
    })
    const { data } = await Axios.get(`${OPERATIONS_URL}`, { params, responseType: 'arraybuffer' })
    const response = ListOperationsResponse.deserializeBinary(data).toObject()

    const results = { nextPageToken: response.nextPageToken }
    results.operations = await Promise.all(response.operationsList.map(async (operation) => {
      const metadata = ExecuteOperationMetadata.deserializeBinary(operation.metadata.value)
      operation.metadata = metadata.toObject()
      if (operation.response) {
        const executeResponse = ExecuteResponse.deserializeBinary(operation.response.value)
        operation.response = executeResponse.toObject()

        // Protobuf's `toObject()` converts the optional `std{out, err}_raw`
        // fields, of type `bytes`, to Base64.
        // (https://developers.google.com/protocol-buffers/docs/reference/javascript-generated#bytes)
        // When outputs are inlined, decode them so that templates can render
        // logs directly.
        if (operation.response.result?.stdoutRaw.length > 0) {
          const stdoutRaw = executeResponse.getResult().getStdoutRaw_asU8()
          operation.response.result.stdoutRaw = u8ToString(stdoutRaw)
        }
        if (operation.response.result?.stderrRaw.length > 0) {
          const stderrRaw = executeResponse.getResult().getStderrRaw_asU8()
          operation.response.result.stderrRaw = u8ToString(stderrRaw)
        }
      }

      operation.totalExecutionTime = totalExecutionTime(operation)

      try {
        operation.action = await bytestream.readAction(
          operation.metadata.actionDigest.hash, operation.metadata.actionDigest.sizeBytes)
      } catch (_) {
        operation.action = null
        operation.command = null
      }

      if (operation.action) {
        try {
          operation.command = await bytestream.readCommand(
            operation.action.commandDigest.hash, operation.action.commandDigest.sizeBytes)
        } catch (_) {
          operation.command = null
        }
      }

      // Flag to keep track of whether we are displaying the whole information in the view:
      operation.expandedRowIsVisible = false

      return operation
    }))

    return results
  },

  async get (name) {
    const { data } = await Axios.get(`${OPERATIONS_URL}${name}`, { responseType: 'arraybuffer' })
    const operation = Operation.deserializeBinary(data).toObject()
    const metadata = ExecuteOperationMetadata.deserializeBinary(operation.metadata.value)
    operation.metadata = metadata.toObject()
    if (operation.response) {
      const executeResponse = ExecuteResponse.deserializeBinary(operation.response.value)
      operation.response = executeResponse.toObject()
    }
    return operation
  },

  async cancel (name) {
    try {
      await Axios.delete(`${OPERATIONS_URL}${name}`)
    } catch (error) {
      if (error.response) {
        return error.response.status
      } else if (error.request) {
        return error.request
      } else {
        return error.message
      }
    }
  },

  operationIsQueued (operation) {
    return operation.metadata?.stage === ExecutionStage.Value.QUEUED
  },
  operationIsExecuting (operation) {
    return operation.metadata?.stage === ExecutionStage.Value.EXECUTING
  },
  operationIsCompleted (operation) {
    const wasCancelled = !operation.response
    return (operation.metadata?.stage === ExecutionStage.Value.COMPLETED && !wasCancelled)
  },
  operationIsSuccessful (operation) {
    return (operation.metadata?.stage === ExecutionStage.Value.COMPLETED &&
            operation.response?.result?.exitCode === 0)
  },

  operationWasCancelled (operation) {
    return operation.done && (operation.error?.code === Code.CANCELLED)
  },
  operationResultIsMissing (operation) {
    // This is set by `ListOperations()` when the `ActionResult` message
    // for an operation could not be found.
    return operation.done && (operation.error?.code === Code.DATA_LOSS)
  },
  exitCode (operation) {
    if (operation.done) {
      return operation.response?.result?.exitCode
    }
    return undefined
  },
  commandFailed (operation) {
    const ec = this.exitCode(operation)
    return (ec && ec !== 0)
  },
  stageName (operation) {
    if (this.operationWasCancelled(operation)) {
      return 'Cancelled'
    }
    const stageValue = operation.metadata?.stage
    if (this.commandFailed(operation) && stageValue === ExecutionStage.Value.COMPLETED) {
      return 'Failed' // Special case: we don't want to show "COMPLETED" in red.
    }
    return reapi.stageName(stageValue)
  },
  stageColor (operation) {
    if (this.operationWasCancelled(operation) || this.operationResultIsMissing(operation)) {
      return '#666666'
    }

    switch (operation.metadata.stage) {
      case ExecutionStage.Value.QUEUED:
        return '#5c4708'
      case ExecutionStage.Value.EXECUTING:
        return 'blue'
      case ExecutionStage.Value.COMPLETED:
        return (this.commandFailed(operation) ? 'red' : '#06571a')
      default:
        return 'black'
    }
  },
  stageIcon (operation) {
    if (this.operationWasCancelled(operation)) {
      return StopIcon
    }

    if (this.operationResultIsMissing(operation)) {
      return QuestionMarkCircleIcon
    }

    switch (operation.metadata.stage) {
      case ExecutionStage.Value.EXECUTING:
        return CogIcon
      case ExecutionStage.Value.COMPLETED:
        if (this.commandFailed(operation)) {
          return XIcon
        } else {
          return CheckIcon
        }
      case ExecutionStage.Value.QUEUED:
        return PauseIcon
      case ExecutionStage.Value.CACHE_CHECK:
        return DocumentSearchIcon
      default:
        return QuestionMarkCircleIcon
    }
  }
}
