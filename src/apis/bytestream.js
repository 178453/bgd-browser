/**
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import Axios from 'axios'

import { Any } from 'google-protobuf/google/protobuf/any_pb.js'

import {
  Action,
  Command,
  Directory,
  Tree
} from '@/protos/build/bazel/remote/execution/v2/remote_execution_pb.js'

import base from '@/apis/base.js'

const config = base.getConfig()
const BLOBS_URL = `${config.backendUrl}/api/v1/blobs`

export default {
  async readBlob (hash, sizeBytes, responseType = 'text') {
    const resourceName = `${hash}/${sizeBytes}`
    const { data } = await Axios.get(`${BLOBS_URL}/${resourceName}`, { responseType })
    return data
  },
  async readMessage (hash, sizeBytes, deserializerCallback) {
    const data = await this.readBlob(hash, sizeBytes, 'arraybuffer')
    const message = deserializerCallback(data)
    return message.toObject()
  },
  async readAction (hash, sizeBytes) {
    return await this.readMessage(hash, sizeBytes, Action.deserializeBinary)
  },
  async readCommand (hash, sizeBytes) {
    return await this.readMessage(hash, sizeBytes, Command.deserializeBinary)
  },
  async readDirectory (hash, sizeBytes) {
    return await this.readMessage(hash, sizeBytes, Directory.deserializeBinary)
  },
  async readTree (hash, sizeBytes) {
    return await this.readMessage(hash, sizeBytes, Tree.deserializeBinary)
  },
  async readAny (hash, sizeBytes) {
    return await this.readMessage(hash, sizeBytes, Any.deserializeBinary)
  },
  blobUrl (digest) {
    return `${BLOBS_URL}/${digest.hash}/${digest.sizeBytes}`
  }
}
