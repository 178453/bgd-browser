/**
  Copyright 2021 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import Axios from 'axios'

import {
  ActionResult
} from '@/protos/build/bazel/remote/execution/v2/remote_execution_pb.js'

import base from '@/apis/base.js'

const config = base.getConfig()
const RESULTS_URL = `${config.backendUrl}/api/v1/action_results`

function u8ToString (u8) {
  return Buffer.from(u8).toString()
}

export default {
  async getResult (hash, sizeBytes) {
    const { data } = await Axios.get(`${RESULTS_URL}/${hash}/${sizeBytes}`, { responseType: 'arraybuffer' })
    const actionResult = ActionResult.deserializeBinary(data)
    const actionResultObject = actionResult.toObject()

    // Protobuf's `toObject()` converts the optional `std{out, err}_raw`
    // fields, of type `bytes`, to Base64.
    // (https://developers.google.com/protocol-buffers/docs/reference/javascript-generated#bytes)
    // When outputs are inlined, decode them so that templates can render
    // logs directly.
    if (actionResultObject?.stdoutRaw.length > 0) {
      const stdoutRaw = actionResult.getStdoutRaw_asU8()
      actionResultObject.stdoutRaw = u8ToString(stdoutRaw)
    }
    if (actionResultObject?.stderrRaw.length > 0) {
      const stderrRaw = actionResult.getStderrRaw_asU8()
      actionResultObject.stderrRaw = u8ToString(stderrRaw)
    }

    return actionResultObject
  }
}
