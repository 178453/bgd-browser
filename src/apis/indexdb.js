const DB_NAME = 'pagedb'
const DB_VERSION = 1
let DB

export default {

  async getDb () {
    return new Promise((resolve, reject) => {
      if (DB) { return resolve(DB) }
      console.log('OPENING DB', DB)
      const request = window.indexedDB.open(DB_NAME, DB_VERSION)

      request.onerror = e => {
        console.log('Error opening db', e)
        reject(e)
      }

      request.onsuccess = e => {
        DB = e.target.result
        resolve(DB)
      }

      request.onupgradeneeded = e => {
        console.log('onupgradeneeded')
        const db = e.target.result
        const store = db.createObjectStore('pages', { keyPath: 'url' })
        store.createIndex('timestamp', 'timestamp')
      }
    })
  },
  async deletePage (page) {
    const db = await this.getDb()

    return new Promise(resolve => {
      const trans = db.transaction(['pages'], 'readwrite')
      trans.oncomplete = () => {
        resolve()
      }

      const store = trans.objectStore('pages')
      store.delete(page.url)
    })
  },
  async getPages () {
    const db = await this.getDb()

    return new Promise(resolve => {
      const trans = db.transaction(['pages'], 'readonly')
      trans.oncomplete = () => {
        resolve(pages)
      }

      const store = trans.objectStore('pages')
      const index = store.index('timestamp')

      const pages = []

      // Read the pages from the database in reverse timestamp order.
      index.openCursor(null, 'prev').onsuccess = e => {
        const cursor = e.target.result
        if (cursor) {
          pages.push(cursor.value)
          cursor.continue()
        }
      }
    })
  },
  async savePage (page) {
    const db = await this.getDb()

    return new Promise(resolve => {
      const trans = db.transaction(['pages'], 'readwrite')
      trans.oncomplete = () => {
        resolve()
      }

      const store = trans.objectStore('pages')
      store.put(page)
    })
  }
}
