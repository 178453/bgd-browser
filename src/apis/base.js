/**
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import Axios from 'axios'

const config = {}

function _parseUrlConfig (urlConfig) {
  if (urlConfig.port) {
    return `${urlConfig.host}:${urlConfig.port}${urlConfig.path}`
  }
  return `${urlConfig.host}${urlConfig.path}`
}

async function loadConfig () {
  const { data } = await Axios.get('/config.json')
  config.logStreamUrl = process.env.VUE_APP_LOGSTREAM_URL || _parseUrlConfig(data.logStreamUrl)
  config.backendUrl = process.env.VUE_APP_BACKEND_URL || _parseUrlConfig(data.backendUrl)
  config.allowOperationCancelling = data.allowOperationCancelling || false
}

function getConfig () {
  return config
}

export default {
  loadConfig,
  getConfig
}
