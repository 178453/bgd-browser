/**
  Copyright 2022 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */
// eslint-disable-next-line
import hljs from 'highlight.js/lib/common'
import hljsVuePlugin from '@highlightjs/vue-plugin'
import { createApp } from 'vue'
import { createPinia } from 'pinia'

import base from '@/apis/base.js'
import router from './router'

import App from './App.vue'

async function startVue () {
  await base.loadConfig()
  const app = createApp(App)
  const pinia = createPinia()

  app.use(router)
  app.use(pinia)
  app.use(hljsVuePlugin)
  app.mount('#app')
}

startVue()
