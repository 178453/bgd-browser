/**
  Copyright 2022 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import { defineStore } from 'pinia'
import { LRUCache } from 'mnemonist'

import actions from '@/apis/actions.js'

function cacheKey (hash, sizeBytes) {
  return `${hash}/${sizeBytes}`
}

export const useActionResultCache = defineStore('action-results', {
  state: () => {
    return {
      ongoingFetches: {},
      results: new LRUCache(128)
    }
  },
  getters: {
    getActionResult () {
      return (hash, sizeBytes) => {
        const key = cacheKey(hash, sizeBytes)

        if (!(key in this.results)) {
          this.fetchActionResult(hash, sizeBytes)
        }
        return this.results[`${hash}/${sizeBytes}`] || null
      }
    }
  },
  actions: {
    async fetchActionResult (hash, sizeBytes) {
      const key = cacheKey(hash, sizeBytes)
      // TODO: add a TTL to this cached result
      if (this.results.has(key)) {
        return
      }

      // Deduplicate the actual fetch promise, just in case
      if (!(key in this.ongoingFetches)) {
        this.ongoingFetches[key] = actions.getResult(hash, sizeBytes)
      }
      try {
        const result = await this.ongoingFetches[key]
        this.results.set(key, result)
      } finally {
        delete this.ongoingFetches[key]
      }
    },
    storeActionResult (hash, sizeBytes, result) {
      this.results.set(cacheKey(hash, sizeBytes), result)
    }
  }
})
