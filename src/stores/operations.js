/**
  Copyright 2022 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import { defineStore } from 'pinia'
import { LRUCache } from 'mnemonist'

import operations from '@/apis/operations.js'
import { useActionResultCache } from './actionresults'

function maybeStoreResult (operation) {
  const actionResults = useActionResultCache()
  const result = operation.response?.result
  if (result) {
    const digest = operation.metadata?.actionDigest
    if (digest) {
      actionResults.storeActionResult(digest.hash, digest.sizeBytes, result)
    }
  }
}

export const useOperationsCache = defineStore('operations', {
  state: () => {
    return {
      ongoingFetches: {},
      operations: new LRUCache(128)
    }
  },
  actions: {
    async fetchOperation (name) {
      // TODO: add a TTL to this cached result
      if (this.operations.has(name) && this.operations.get(name).done) {
        return
      }

      // Deduplicate the actual fetch promise, just in case
      if (!(name in this.ongoingFetches)) {
        this.ongoingFetches[name] = operations.get(name)
      }
      try {
        const operation = await this.ongoingFetches[name]
        this.operations.set(name, operation)
        maybeStoreResult(operation)
      } finally {
        delete this.ongoingFetches[name]
      }
    },
    storeOperation (operation) {
      maybeStoreResult(operation)
      this.operations.set(operation.name, operation)
    }
  }
})
