<!--
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<template>
  <div class="action">
    <Breadcrumbs :crumbs="breadcrumbs" />

    <div v-if="notFound && !action" class="error">
      <header>
        <h1>
          Action couldn't be retrieved
        </h1>
      </header>
      <p>{{ error.message }}</p>
    </div>
    <div v-if="action">
      <header>
        <h1>Action</h1>
        <ul class="metadata">
          <li v-if="operation && operation.response" class="pill" :class="pillClass(operation.response.cachedResult)">
            Cache {{ operation.response.cachedResult ? 'hit' : 'miss' }}
          </li>
          <li class="pill" :class="pillClass(!action.doNotCache)">
            {{ action.doNotCache ? 'Uncacheable' : 'Cacheable' }}
          </li>
          <li v-if="result" class="pill" :class="pillClass(result.exitCode == 0)">
            Exited with code {{ result.exitCode }}
          </li>
          <li v-if="!result && !operation" class="pill pill-grey">
            ActionResult not found
          </li>
        </ul>
      </header>

      <LogViewer :command="command" :result="result" :operation="operation" />

      <CommandMetadata :command="command" />
      <ClientRequestMetadata v-if="requestMetadata" :requestMetadata="requestMetadata" />

      <div v-if="inputRoot">
        <h2>Input Root</h2>
        <DirectoryListing :directories="inputRoot.directoriesList"
                          :symlinks="inputRoot.symlinksList"
                          :files="inputRoot.filesList" />
      </div>

      <div v-if="hasOutputs">
        <h2>Outputs</h2>
        <DirectoryListing :directories="result.outputDirectoriesList"
                          :symlinks="result.outputSymlinksList"
                          :files="result.outputFilesList"
                          directory-route-base="tree" />
      </div>

      <div v-if="result">
        <h2>Timeline</h2>
        <ResultTimeline :result="result" />
      </div>
    </div>

    <div v-if="operation" class="operation-details">
      <h2>Operation</h2>
      <p>
        <span class="operation-name">{{ operation.name }}</span> &mdash;
        <span class="stage-name" v-bind:style="{ color: stageColor }">{{ stageName }}</span>
      </p>
      <p>
        <button v-if="operationIsCancellable" class="cancel-operation-button" v-on:click="cancelOperation">Cancel Operation</button>
      </p>
    </div>

    <div v-if="auxiliaryMetadataList.length > 0">
      <h2>Execution stats</h2>
      <ExecutionStats :auxiliaryMetadataList="auxiliaryMetadataList" />
    </div>

  </div>
</template>

<script>
import { mapActions, mapState } from 'pinia'

import actions from '@/apis/actions.js'
import base from '@/apis/base.js'
import bytestream from '@/apis/bytestream.js'
import operations from '@/apis/operations.js'

import Breadcrumbs from '@/components/Breadcrumbs.vue'
import CommandMetadata from '@/components/CommandMetadata.vue'
import ClientRequestMetadata from '@/components/ClientRequestMetadata.vue'
import DirectoryListing from '@/components/DirectoryListing.vue'
import ExecutionStats from '@/components/ExecutionStats.vue'
import LogViewer from '@/components/LogViewer.vue'
import ResultTimeline from '@/components/ResultTimeline.vue'

import { useActionResultCache } from '@/stores/actionresults.js'
import { useOperationsCache } from '@/stores/operations.js'

const config = base.getConfig()
const CANCEL_OPERATION_BUTTON_ENABLED = config.allowOperationCancelling

export default {
  name: 'Action',
  components: {
    Breadcrumbs,
    ClientRequestMetadata,
    CommandMetadata,
    DirectoryListing,
    ExecutionStats,
    LogViewer,
    ResultTimeline
  },
  data () {
    return {
      action: null,
      breadcrumbs: [
        {
          title: 'BuildGrid',
          target: '/'
        },
        {
          title: 'Operations',
          target: '/browse'
        }
      ],
      checkedResult: false,
      checkedOperation: false,
      command: null,
      error: null,
      inputRoot: null,
      notFound: false,
      requestMetadata: null,
      refreshTimer: null,
      notificationsAllowed: false
    }
  },
  watch: {
    operation: 'handleOperationChange'
  },
  computed: {
    ...mapState(useActionResultCache, ['results']),
    ...mapState(useOperationsCache, ['operations']),
    result () {
      return this.results.get(`${this.hash}/${this.sizeBytes}`) || null
    },
    operationName () {
      return this.$route.query.operation
    },
    operation () {
      if (!this.operationName) {
        return null
      }
      return this.operations.get(this.operationName)
    },
    hash () {
      return this.$route.params.hash
    },
    stageName () {
      return operations.stageName(this.operation)
    },
    stageColor () {
      return operations.stageColor(this.operation)
    },
    hasOutputs () {
      if (!this.result) {
        return false
      }
      const files = this.result.outputFilesList || []
      const directories = this.result.outputDirectoriesList || []
      const symlinks = this.result.outputSymlinksList || []
      return this.result && (files.length || directories.length || symlinks.length)
    },
    logsReady () {
      return this.command && this.checkedResult && this.checkedOperation
    },
    sizeBytes () {
      return this.$route.params.sizeBytes
    },
    stderrLines () {
      return this.result.stderrRaw.split('\n')
    },
    stdoutLines () {
      return this.result.stdoutRaw.split('\n')
    },
    operationIsCancellable () {
      return CANCEL_OPERATION_BUTTON_ENABLED &&
             this.checkedOperation && !this.operation.done && !this.operation.response?.result
    },
    auxiliaryMetadataList () {
      return this.operation?.response?.result?.executionMetadata?.auxiliaryMetadataList || []
    },
    operationCanChangeStatus () {
      return (this.operation &&
              !operations.operationIsCompleted(this.operation) &&
              !operations.operationWasCancelled(this.operation))
    }
  },
  created () {
    this.getAction(this.$route.params.hash, this.$route.params.sizeBytes)
    this.fetchActionResult(this.$route.params.hash, this.$route.params.sizeBytes)
    this.getOperation()
  },
  beforeUnmount () {
    this.stopAutoRefreshing()
  },
  methods: {
    ...mapActions(useActionResultCache, ['fetchActionResult']),
    ...mapActions(useOperationsCache, ['fetchOperation']),
    async startAutoRefreshing () {
      if (await Notification.requestPermission() === 'granted') {
        this.notificationsAllowed = true
      }

      this.refreshTimer = setInterval(this.refreshOperation, 5000)
      console.log('Auto refresh enabled')
    },
    stopAutoRefreshing () {
      clearInterval(this.refreshTimer)
      console.log('Auto refresh stopped')
    },
    async refreshOperation () {
      await this.fetchOperation(this.operationName)

      if (!this.operationCanChangeStatus) {
        this.stopAutoRefreshing()
      }
    },
    async getAction (hash, sizeBytes) {
      try {
        this.action = await bytestream.readAction(hash, sizeBytes)
      } catch (e) {
        this.notFound = true
        this.error = e
        return
      }

      try {
        this.command = await bytestream.readCommand(
          this.action.commandDigest.hash, this.action.commandDigest.sizeBytes)
      } catch (_) {
        this.command = null
      }

      try {
        this.inputRoot = await bytestream.readDirectory(
          this.action.inputRootDigest.hash, this.action.inputRootDigest.sizeBytes)
      } catch (_) {
        this.inputRoot = null
      }
    },
    async getResult (hash, sizeBytes) {
      try {
        this.result = await actions.getResult(hash, sizeBytes)
      } catch (_) {
        this.result = null
      }
      this.checkedResult = true
    },
    async getOperation () {
      if (this.operationName) {
        this.operation = await this.fetchOperation(this.operationName)

        if (!this.refreshTimer && this.operationCanChangeStatus) {
          this.startAutoRefreshing()
        }
      }

      this.checkedOperation = true
    },
    async cancelOperation () {
      if (confirm(`Cancel operation "${this.operation.name}"?`)) {
        const error = await operations.cancel(this.operation.name)
        if (error) {
          alert(`Failed to cancel operation "${this.operation.name}": Error ${error}`)
        } else {
          this.checkedOperation = false
        }

        // Refresh the operation:
        this.fetchOperation(this.operationName)
      }
    },
    handleOperationChange (newOperation, oldOperation) {
      if (!this.notificationsAllowed || !oldOperation) { // Ignore first transition when the page loads.
        return
      }

      const oldStage = operations.stageName(oldOperation)
      const newStage = operations.stageName(newOperation)
      if (oldStage === newStage) {
        return
      }

      const notification = new Notification(`Operation ${newStage}`, {
        body: `${newOperation.name}`,
        icon: '/bgd-logo-black.png'
      })
      setTimeout(() => notification.close(), 10000)
    },
    pillClass (condition) {
      return {
        'pill-green': condition,
        'pill-red': !condition
      }
    }
  }
}
</script>

<style lang="scss" scoped>
.action {
  h1 {
    font-size: 3rem;
    font-weight: 400;
    color: var(--color-title);
    margin-top: 0;
  }

  .error {
    h1 {
      margin: 0;
    }
    p {
      font-size: 1.25rem;
      color: var(--color-text-error);
    }
  }

  h2 {
    font-size: 2rem;
    font-weight: 300;
    margin: 3rem 0 0 0;
  }

  header {
    display: flex;
    justify-content: space-between;
    align-items: baseline;
  }

  .metadata {
    list-style: none;
    display: flex;
    padding: 0;

    .pill {
      padding: 15px;
      margin: 0 10px 10px 0;
      background-color: var(--color-bg-top);
      border-radius: 30px;
    }

    .pill-green {
      background-color: var(--color-green-light);
      color: var(--color-green-dark);
    }

    .pill-red {
      background-color: var(--color-red-light);
      color: var(--color-red-dark);
    }

    .pill-grey {
      background-color: var(--color-neutral-dark);
      color: var(--color-neutral-light);
    }

  }

  .operation-details {
    .operation-name {
      font-family: monospace;
    }

    .cancel-operation-button {
      display: block;
      padding: 8px;
      border-radius: 10px;

      color: var(--color-neutral-light);
      background-color: var(--color-red);
      border-color: var(--color-red);
    }
  }
}
</style>
