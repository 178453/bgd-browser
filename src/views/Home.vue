<!--
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<template>
  <div class="home">
    <h1>Welcome to BuildGrid</h1>
    <img class="logo" src="/bgd-logo.svg" alt="">

    <router-link to="/browse">
      <span>Browse Job Queue</span>
      <ArrowRightIcon />
    </router-link>

    <p>
      This tool allows you to view details about work (Operations) executed by BuildGrid, as
      well as view and download items stored in the CAS.
    </p>

    <p>
      In addition to browsing BuildGrid's list of known Operations, you can also view details
      of multiple related execution jobs using their Invocation ID and Correlated Invocations
      ID, as well as fetching individual Actions from the ActionCache.
    </p>
    <p>
      If you only have the digest and/or metadata from your client tool rather than a link,
      the formats to manually construct the links are as follows.
    </p>

    <dl>
      <dt>/action/{hash}/{sizeBytes}</dt>
      <dd>View an Action in the Action Cache, using the digest</dd>
      <dt>/correlated-invocations/{correlationId}</dt>
      <dd>View jobs which are correlated with eachother by the same correlationId</dd>
      <dt>/tool-invocations/{toolInvocationId}</dt>
      <dd>View jobs which are invoked by the same tool</dd>
      <dt>/worker/{workerName}</dt>
      <dd>View jobs which are executed on the same worker machine</dd>
    </dl>

    <p>
      The items in the CAS are not easily discoverable without knowing the digest beforehand,
      other than via Actions which are related to them. The digest (and browser link) will
      often be provided by the tools invoking remote execution requests. If you only know the
      digest and hash, the link formats are below.
    </p>

    <dl>
      <dt>/directory/{hash}/{sizeBytes}</dt>
      <dd>View a directory object in the CAS, by specifying the Directory message's digest</dd>
      <dt>/tree/{hash}/{sizeBytes}</dt>
      <dd>View a tree object in the CAS, by specifying the root's digest</dd>
      <dt>/file/{hash}/{sizeBytes}</dt>
      <dd>View a file in the CAS by providing its digest</dd>
    </dl>

    <h2>Recently Viewed</h2>
    <div v-if="pageDataLoaded">
      <div class="history-row" v-for="page in pages" :key="page.url">
        <div class="history-row-section link-section">
          <p class="target-name">{{ page.route?.name || "Link" }}</p>
          <router-link :to="page.route.fullPath" v-if="page.route">
            {{ renderPageLink(page) }}
          </router-link>
          <a class="fallback-link" :href="`${page.url}`" v-else>{{ page.text || page.url }}</a>
        </div>
        <div class="history-row-section timestamp-section">
          <p class="target-name">Viewed at</p>
          <span>{{ formatTimestamp(page.timestamp) }}</span>
        </div>
      </div>
    </div>
    <p class="error" v-else>Recently seen pages will be shown here once visited.</p>
    </div>
</template>

<script>
import { ArrowRightIcon } from '@heroicons/vue/solid'

import indexdb from '@/apis/indexdb.js'

export default {
  components: {
    ArrowRightIcon
  },
  data () {
    return {
      pageDataLoaded: false,
      pages: []
    }
  },
  methods: {
    renderPageLink (page) {
      if (!page.route) {
        return page.text || page.url
      }

      let text = page.text
      switch (page.route.name) {
        case 'Home':
          text = 'Home'
          break
        case 'Browse':
          text = page.route.query.filter || 'No filter used'
          break
        case 'Action':
          text = page.route.params.hash.slice(0, 16) + '.../' + page.route.params.sizeBytes
          break
        case 'Directory':
          text = page.route.params.hash.slice(0, 16) + '.../' + page.route.params.sizeBytes
          break
        case 'Tree':
          text = page.route.params.hash.slice(0, 16) + '.../' + page.route.params.sizeBytes
          break
        case 'File':
          text = page.route.params.hash.slice(0, 16) + '.../' + page.route.params.sizeBytes
          break
        case 'Correlated Invocations':
          text = page.route.params.correlationId
          break
        case 'Tool Invocation':
          text = page.route.params.toolInvocationId
          break
        case 'Worker Invocations':
          text = page.route.params.workerName
          break
      }
      return text
    },
    formatTimestamp (timestamp) {
      const datetime = new Date(timestamp)
      const date = datetime.toDateString()
      const hours = datetime.getUTCHours()
      const minutes = datetime.getUTCMinutes()
      const seconds = datetime.getUTCSeconds()
      return `${date} ${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds} UTC`
    }
  },
  async mounted () {
    const maxSize = 10
    const allPages = await indexdb.getPages()
    this.pages = allPages.filter(page => !page.route || page.route.name !== 'Home').slice(0, maxSize)
    this.pageDataLoaded = true
  }
}
</script>

<style lang="scss" scoped>
.home {
  position: relative;

  .logo {
    position: absolute;
    z-index: -1;
    right: -10rem;
    top: -2rem;
    height: 42rem;
  }

  h1 {
    font-size: 3rem;
    font-weight: 400;
    color: var(--color-title);
    margin-top: 4rem;
  }
  h2 {
    font-size: 2rem;
    font-weight: 400;
    color: var(--color-title);
    margin-top: 4rem;
  }

  a {
    display: flex;
    font-size: 1.25rem;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }

    span {
      margin-right: 0.5rem;
    }

    svg {
      width: 1.25rem;
    }
  }

  p {
    max-width: 40rem;
  }
  p.error {
    font-size: 1.25rem;
    font-weight: 400;
    color: var(--color-text-error);
  }

  dl {
    list-style: none;
    margin: 1rem 1rem 2rem 1rem;
  }

  dt {
    font-family: 'Roboto Mono';
    font-weight: 500;
    color: var(--color-title);
    display:list-item;
    margin: 1rem 0 0.25rem 0;
  }

  dd {
    margin: 0;
  }

  .history-row {
    display: flex;
    align-items: center;
    justify-content: flex-start;
    margin: 1.75rem 0;

    .history-row-section {
      display: flex;
      flex-direction: column;
      flex: 1 1 0;

      a {
        font-size: 1rem;
      }

      .target-name {
        color: var(--color-text-muted);
        margin: 0;
      }
    }

    .link-section {
      flex: 0 0 28rem;
      margin-right: 1rem;
    }
  }
}
</style>
