// Read Git SHA and tag to display in footer:
var getRepoInfo = require('git-repo-info');
var repoInfo = getRepoInfo();
process.env.VUE_APP_GIT_ABBREVIATED_HASH = repoInfo.abbreviatedSha
process.env.VUE_APP_GIT_TAG = repoInfo.tag? repoInfo.tag : ''
